/*
 * Copyright (C) 2015-2016 Theatre of Dreams <http://theatreofdreams.eu/>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include "Runable.hpp"

namespace Tod
{
    namespace Threading
    {
        void Runnable::Start()
        {
            m_stopRequested = false;

            m_thread = std::thread( [ this ]
            {
                Run();
            } );
        }

        void Runnable::Stop()
        {
            m_stopRequested = true;

            if (!m_ioService.stopped())
                m_ioService.stop();

            if (m_thread.joinable())
                m_thread.join();
        }
    }
}
