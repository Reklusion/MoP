/*
 * Copyright (C) 2008-2012 TrinityCore <http://www.trinitycore.org/>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __BATTLEGROUNDTK_H
#define __BATTLEGROUNDTK_H

class Battleground;

enum BG_TK_NPC
{
    BG_TK_MAX_TEAM_SCORE        = 1600,
    BG_TK_EVENT_START_BATTLE    = 8563,
    BG_TK_POINTS_UPDATE_TIME    = 8 * IN_MILLISECONDS,
    BG_TK_TIME_LIMIT            = 25 * MINUTE * IN_MILLISECONDS
};

enum BG_TK_Objects
{
    BG_TK_OBJECT_A_DOOR         = 0,
    BG_TK_OBJECT_H_DOOR         = 1,

    BG_TK_OBJECT_ORB_1          = 2,
    BG_TK_OBJECT_ORB_2          = 3,
    BG_TK_OBJECT_ORB_3          = 4,
    BG_TK_OBJECT_ORB_4          = 5,

    BG_TK_OBJECT_MAX            = 6
};

enum BG_TK_Creatures
{
    BG_TK_CREATURE_ORB_AURA_1   = 0,
    BG_TK_CREATURE_ORB_AURA_2   = 1,
    BG_TK_CREATURE_ORB_AURA_3   = 2,
    BG_TK_CREATURE_ORB_AURA_4   = 3,
    
    BG_TK_CREATURE_SPIRIT_1     = 4,
    BG_TK_CREATURE_SPIRIT_2     = 5,

    BG_TK_CREATURE_MAX          = 6
};

enum BG_TK_Objets_Entry
{
    BG_TK_OBJECT_DOOR_ENTRY     = 213172,

    BG_TK_OBJECT_ORB_1_ENTRY    = 212091,
    BG_TK_OBJECT_ORB_2_ENTRY    = 212092,
    BG_TK_OBJECT_ORB_3_ENTRY    = 212093,
    BG_TK_OBJECT_ORB_4_ENTRY    = 212094
};

enum BG_TK_Sound
{
    BG_TK_SOUND_ORB_PLACED      = 8232,
    BG_TK_SOUND_A_ORB_PICKED_UP = 8174,
    BG_TK_SOUND_H_ORB_PICKED_UP = 8174,
    BG_TK_SOUND_ORB_RESPAWNED   = 8232
};

enum BG_TK_SpellId
{
    BG_TK_SPELL_ORB_PICKED_UP_1 = 121176,   // GREEN
    BG_TK_SPELL_ORB_PICKED_UP_2 = 121175,   // PURPLE
    BG_TK_SPELL_ORB_PICKED_UP_3 = 121177,   // ORANGE
    BG_TK_SPELL_ORB_PICKED_UP_4 = 121164,   // BLUE

    BG_TK_SPELL_ORB_AURA_1      = 121220,   // GREEN
    BG_TK_SPELL_ORB_AURA_2      = 121219,   // PURPLE
    BG_TK_SPELL_ORB_AURA_3      = 121221,   // ORANGE
    BG_TK_SPELL_ORB_AURA_4      = 121217,   // BLUE

    BG_TK_ALLIANCE_INSIGNIA     = 131527,
    BG_TK_HORDE_INSIGNIA        = 131528
};

enum BG_TK_WorldStates
{
    BG_TK_ICON_A                = 6308,
    BG_TK_ICON_H                = 6307,
    BG_TK_ORB_POINTS_A          = 6303,
    BG_TK_ORB_POINTS_H          = 6304,
    BG_TK_ORB_STATE             = 6309,
    BG_TK_NEUTRAL_ORBS          = 6960,

    BG_TK_TIME_ENABLED          = 4247,
    BG_TK_TIME_REMAINING        = 4248
};

enum BG_TK_Graveyards
{
    TK_GRAVEYARD_RECTANGLEA1    = 3552,
    TK_GRAVEYARD_RECTANGLEA2    = 4058,
    TK_GRAVEYARD_RECTANGLEH1    = 3553,
    TK_GRAVEYARD_RECTANGLEH2    = 4057
};

enum BG_TK_ZONE
{
    TK_ZONE_OUT                 = 0,
    TK_ZONE_IN                  = 1,
    TK_ZONE_MIDDLE              = 2,
    TK_ZONE_MAX                 = 3
};

enum BG_TK_Events
{
    TK_EVENT_ORB                = 0,
    TK_EVENT_SPIRITGUIDES_SPAWN = 2 // Spirit guides will spawn at the same time as TP_EVENT_DOOR_OPEN.
};

#define MAX_ORBS                    4

const float BG_TK_DoorPositions[2][4] =
{
    { 1783.84f, 1100.66f, 20.60f, 1.625020f },
    { 1780.15f, 1570.22f, 24.59f, 4.711630f }
};

const float BG_TK_OrbPositions[MAX_ORBS][4] =
{
    { 1716.78f, 1416.64f, 13.5709f, 1.57239f }, // GREEN
    { 1850.26f, 1416.77f, 13.5709f, 1.56061f }, // PURPLE
    { 1850.29f, 1250.31f, 13.5708f, 4.70848f }, // RED
    { 1716.83f, 1249.93f, 13.5706f, 4.71397f }  // BLUE
};

const float BG_TK_SpiritPositions[MAX_ORBS][4] =
{
    { 1892.61f, 1151.69f, 14.7160f, 2.523528f },
    { 1672.40f, 1524.10f, 16.7387f, 6.032206f }
};

const uint32 BG_TK_ORBS_SPELLS[MAX_ORBS] =
{
    BG_TK_SPELL_ORB_PICKED_UP_1,
    BG_TK_SPELL_ORB_PICKED_UP_2,
    BG_TK_SPELL_ORB_PICKED_UP_3,
    BG_TK_SPELL_ORB_PICKED_UP_4
};

const uint32 BG_TK_ORBS_AURA[MAX_ORBS] =
{
    BG_TK_SPELL_ORB_AURA_1,
    BG_TK_SPELL_ORB_AURA_2,
    BG_TK_SPELL_ORB_AURA_3,
    BG_TK_SPELL_ORB_AURA_4
};

// Tick points according to carrier zone.
const uint32 BG_TK_TickPoints[3] = { 3, 4, 5 };

struct BattlegroundTKScore : public BattlegroundScore
{
    BattlegroundTKScore() : OrbHandles(0), Score(0) { }

    ~BattlegroundTKScore() { }

    uint32 OrbHandles;
    uint32 Score;
};

class BattlegroundTK : public Battleground
{
    public:
         BattlegroundTK();
        ~BattlegroundTK();

        /* Inherited from Battleground class. */

        void Reset() override;
        bool SetupBattleground() override;
        void EndBattleground(uint32 winner) override;

        /* Players. */
        void AddPlayer(Player* player) override;
        void RemovePlayer(Player* player, ObjectGuid guid, uint32 team) override;
        void HandleKillPlayer(Player* player, Player* killer) override;

        /* Doors. */
        void StartingEventCloseDoors() override;
        void StartingEventOpenDoors() override;

        /* WorldStates. */
        void FillInitialWorldStates(WorldStateBuilder& data) override;

        /* Areatriggers. */
        void HandleAreaTrigger(Player* Source, uint32 Trigger, bool Entered) override;

        /* Graveyards. */
        WorldSafeLocsEntry const* GetClosestGraveYard(Player* player) override;

        /* ScoreKeeping. */
        void AddPoint(Team team, uint32 Points = 1)     { m_TeamScores[GetTeamIndexByTeamId(team)] += Points; }
        void SetTeamPoint(Team team, uint32 Points = 0) { m_TeamScores[GetTeamIndexByTeamId(team)] = Points; }
        void RemovePoint(Team team, uint32 Points = 1)  { m_TeamScores[GetTeamIndexByTeamId(team)] -= Points; }

        void AccumulateScore(uint32 team, BG_TK_ZONE zone);
        void UpdateTeamScore(Team team);

        void UpdatePlayerScore(Player* Source, uint32 type, uint32 value, bool doAddHonor = true) override;

        /* Nodes occupying. */
        void UpdateOrbState(Team team, uint32 value);
        void EventPlayerClickedOnFlag(Player* player, WorldObject* obj) override { EventPlayerClickedOnOrb(player, obj); }
        void EventPlayerClickedOnOrb(Player* source, WorldObject* target_obj);
        void EventPlayerDroppedOrb(Player* source, ObjectGuid guid = ObjectGuid::Empty);

        /* Timers. */
        uint32 GetRemainingTimeInMinutes() { return m_EndTimer ? ((m_EndTimer - 1) / (MINUTE * IN_MILLISECONDS) + 1) : 0; }

    private:

        int32 _GetOrbNameId(uint8 orb);

        void PostUpdateImpl(uint32 diff);

        std::map<ObjectGuid, BG_TK_ZONE> m_playersZone;

        uint32 m_HonorWinKills;
        uint32 m_HonorEndKills;
        uint32 m_EndTimer;
        uint32 m_UpdatePointsTimer;
        Team   m_LastCapturedOrbTeam;
        int32 m_CheatersCheckTimer;
};

#endif
