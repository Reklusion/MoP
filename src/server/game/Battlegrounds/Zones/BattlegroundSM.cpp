/*
 * Copyright (C) 2008-2015 TrinityCore <http://www.trinitycore.org/>
 * Copyright (C) 2005-2014 MaNGOS <http://getmangos.com/>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 3 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "BattlegroundSM.h"
#include "Player.h"
#include "ScriptedCreature.h"
#include "BattlegroundMgr.h"
#include "GossipDef.h"

MineCartData mineCartsInfo[3] =
{
    { NPC_MINE_CART_1, { 744.542053f, 183.545883f, 319.658203f, 4.356342f } },
    { NPC_MINE_CART_2, { 739.400330f, 203.598511f, 319.603333f, 2.308198f } },
    { NPC_MINE_CART_3, { 760.184509f, 198.844742f, 319.446655f, 0.351249f } }
};

BattlegroundSM::BattlegroundSM()
{
    m_BuffChange = true;
    BgObjects.resize(BG_SM_OBJECT_MAX);
    BgCreatures.resize(BG_SM_CREATURES_MAX);

    StartMessageIds[BG_STARTING_EVENT_FIRST] = LANG_BG_SM_START_TWO_MINUTES;
    StartMessageIds[BG_STARTING_EVENT_SECOND] = LANG_BG_SM_START_ONE_MINUTE;
    StartMessageIds[BG_STARTING_EVENT_THIRD] = LANG_BG_SM_START_HALF_MINUTE;
    StartMessageIds[BG_STARTING_EVENT_FOURTH] = LANG_BG_SM_HAS_BEGUN;
}

BattlegroundSM::~BattlegroundSM() { }

void BattlegroundSM::Reset()
{
    //call parent's class reset
    Battleground::Reset();

    m_TeamScores[TEAM_ALLIANCE] = 0;
    m_TeamScores[TEAM_HORDE] = 0;
    m_HonorScoreTics[TEAM_ALLIANCE] = 0;
    m_HonorScoreTics[TEAM_HORDE] = 0;

    m_MineCartCheckTimer = 1000;

    m_FirstMineCartSummonTimer = 110 * IN_MILLISECONDS; // 10 sec before opening doors

    bool isBGWeekend = sBattlegroundMgr->IsBGWeekend(GetTypeID());

    m_HonorTics = (isBGWeekend) ? BG_SM_SMWeekendHonorTicks : BG_SM_NotSMWeekendHonorTicks;

    m_IsInformedNearVictory = false;

    m_MineCartSpawnTimer = 30 * IN_MILLISECONDS;
    m_MineCartAddPointsTimer = 2000;

    m_FirstMineCartSpawned = false;

    for (uint8 i = 0; i < SM_MINE_CART_MAX; ++i)
    {
        m_MineCartsProgressBar[i] = BG_SM_PROGRESS_BAR_NEUTRAL;
        m_MineCartSpawned[i] = false;
    }
}

void BattlegroundSM::PostUpdateImpl(uint32 diff)
{
    if (GetStatus() == STATUS_IN_PROGRESS)
    {
        SummonMineCart(diff);
        CheckPlayerNearMineCart(diff);
        MineCartAddPoints(diff);
    }
    else if (!m_FirstMineCartSpawned)
        FirstMineCartSummon(diff);
}

void BattlegroundSM::StartingEventCloseDoors()
{
    // Starting doors
    for (uint8 doorType = BG_SM_OBJECT_DOOR_A_1; doorType <= BG_SM_OBJECT_DOOR_H_2; ++doorType)
    {
        DoorClose(doorType);
        SpawnBGObject(doorType, RESPAWN_IMMEDIATELY);
    }

    for (uint8 i = BG_SM_OBJECT_WATERFALL_DEPOT; i < BG_SM_OBJECT_MAX; ++i)
        SpawnBGObject(i, RESPAWN_ONE_DAY);
}

void BattlegroundSM::FirstMineCartSummon(uint32 diff)
{
    if (m_FirstMineCartSummonTimer <= diff)
    {
        m_FirstMineCartSpawned = true;

        uint8 mineCart = Math::Rand(BG_SM_MINE_CART_1, BG_SM_MINE_CART_3);

        if (Creature* cart = AddCreature(mineCartsInfo[mineCart].entry, mineCart, mineCartsInfo[mineCart].pos.GetPositionX(), mineCartsInfo[mineCart].pos.GetPositionY(),
            mineCartsInfo[mineCart].pos.GetPositionZ(), mineCartsInfo[mineCart].pos.GetOrientation()))
        {
            cart->CastSpell(cart, BG_SM_CONTROL_VISUAL_NEUTRAL, true);
            cart->SetFlag(UNIT_FLAGS, UNIT_FLAG_NOT_SELECTABLE);
            cart->SetSpeed(MOVE_WALK, cart->GetSpeed(MOVE_WALK) / 2);

            cart->GetAI()->SetData(GetInstanceID(), GetTypeID());

            m_MineCartsProgressBar[mineCart] = BG_SM_PROGRESS_BAR_NEUTRAL;
            m_MineCartSpawned[mineCart] = true;
        }
    }
    else
        m_FirstMineCartSummonTimer -= diff;
}

void BattlegroundSM::StartingEventOpenDoors()
{
    //Open doors
    for (uint8 doorType = BG_SM_OBJECT_DOOR_A_1; doorType <= BG_SM_OBJECT_DOOR_H_2; ++doorType)
        DoorOpen(doorType);

    for (uint8 i = BG_SM_OBJECT_WATERFALL_DEPOT; i < BG_SM_OBJECT_MAX; ++i)
        SpawnBGObject(i, RESPAWN_IMMEDIATELY);
}

void BattlegroundSM::SummonMineCart(uint32 diff)
{
    if (m_MineCartSpawned[BG_SM_MINE_CART_1] && m_MineCartSpawned[BG_SM_MINE_CART_2] && m_MineCartSpawned[BG_SM_MINE_CART_3])
        return;

    if (m_MineCartSpawnTimer <= diff)
    {
        std::vector<uint8> mineCarts;
        for (uint8 i = 0; i < SM_MINE_CART_MAX; ++i)
            if (!m_MineCartSpawned[i])
                mineCarts.push_back(i);

        uint8 mineCart = Trinity::Containers::SelectRandomContainerElement(mineCarts);

        SendWarningToAll(LANG_BG_SM_MINE_CART_SPAWNED);
        PlaySoundToAll(BG_SM_SOUND_MINE_CART_SPAWNED);

        if (Creature* cart = AddCreature(mineCartsInfo[mineCart].entry, mineCart, mineCartsInfo[mineCart].pos.GetPositionX(), mineCartsInfo[mineCart].pos.GetPositionY(),
            mineCartsInfo[mineCart].pos.GetPositionZ(), mineCartsInfo[mineCart].pos.GetOrientation()))
        {
            cart->CastSpell(cart, BG_SM_CONTROL_VISUAL_NEUTRAL, true);
            cart->SetFlag(UNIT_FLAGS, UNIT_FLAG_NOT_SELECTABLE);
            cart->SetSpeed(MOVE_WALK, cart->GetSpeed(MOVE_WALK) / 2);

            cart->GetAI()->SetData(GetInstanceID(), GetTypeID());

            m_MineCartsProgressBar[mineCart] = BG_SM_PROGRESS_BAR_NEUTRAL;
            m_MineCartSpawned[mineCart] = true;
        }

        m_MineCartSpawnTimer = 30000;
    }
    else
        m_MineCartSpawnTimer -= diff;
}

void BattlegroundSM::CheckPlayerNearMineCart(uint32 diff)
{
    if (m_MineCartCheckTimer <= diff)
    {
        for (uint8 i = 0; i < SM_MINE_CART_MAX; ++i)
        {
            Creature* cart = GetBGCreature(i, false);
            // check if someone join the cart
            for (GuidSet::iterator guid = playersNearMine[SM_MINE_CART_MAX].begin(); guid != playersNearMine[SM_MINE_CART_MAX].end();)
            {
                Player* player = ObjectAccessor::FindPlayer(*guid);
                if (cart && cart->IsAlive() && player && player->GetDistance(cart) < 22.0f)
                {
                    playersNearMine[i].insert(*guid);
                    guid = playersNearMine[SM_MINE_CART_MAX].erase(guid);
                    UpdateWorldStateForPlayer(SM_DISPLAY_PROGRESS_BAR, BG_SM_PROGRESS_BAR_SHOW, player);
                }
                else
                    ++guid;
            }

            for (GuidSet::iterator guid = playersNearMine[i].begin(); guid != playersNearMine[i].end();)
            {
                Player* player = ObjectAccessor::FindPlayer(*guid);
                if (!cart || !cart->IsAlive() || !player || player->IsDead() || player->GetDistance(cart) >= 22.0f)
                {
                    if (player)
                        playersNearMine[SM_MINE_CART_MAX].insert(*guid);
                    guid = playersNearMine[i].erase(guid);
                    UpdateWorldStateForPlayer(SM_DISPLAY_PROGRESS_BAR, BG_SM_PROGRESS_BAR_DONT_SHOW, player);
                }
                else
                {
                    ++guid;

                    Team oldTeamOwnedCart = TEAM_OTHER;
                    if (m_MineCartsProgressBar[i] > BG_SM_PROGRESS_BAR_NEUTRAL)
                        oldTeamOwnedCart = ALLIANCE;
                    else if (m_MineCartsProgressBar[i] < BG_SM_PROGRESS_BAR_NEUTRAL)
                        oldTeamOwnedCart = HORDE;

                    if (player->GetBGTeam() == ALLIANCE)
                        m_MineCartsProgressBar[i]++;
                    else
                        m_MineCartsProgressBar[i]--;

                    if (m_MineCartsProgressBar[i] > 100)
                        m_MineCartsProgressBar[i] = 100;
                    else if (m_MineCartsProgressBar[i] < 0)
                        m_MineCartsProgressBar[i] = 0;

                    UpdateWorldStateForPlayer(SM_PROGRESS_BAR_STATUS, m_MineCartsProgressBar[i], player);

                    Team teamOwnedCart = TEAM_OTHER;
                    if (m_MineCartsProgressBar[i] > BG_SM_PROGRESS_BAR_NEUTRAL)
                        teamOwnedCart = ALLIANCE;
                    else if (m_MineCartsProgressBar[i] < BG_SM_PROGRESS_BAR_NEUTRAL)
                        teamOwnedCart = HORDE;

                    if (oldTeamOwnedCart != teamOwnedCart)
                    {
                        if (oldTeamOwnedCart == ALLIANCE)
                            cart->RemoveAurasDueToSpell(BG_SM_CONTROL_VISUAL_ALLIANCE, cart->GetGUID());
                        else if (oldTeamOwnedCart == HORDE)
                            cart->RemoveAurasDueToSpell(BG_SM_CONTROL_VISUAL_HORDE, cart->GetGUID());
                        else if (oldTeamOwnedCart == TEAM_OTHER)
                            cart->RemoveAurasDueToSpell(BG_SM_CONTROL_VISUAL_NEUTRAL, cart->GetGUID());

                        if (teamOwnedCart == ALLIANCE)
                        {
                            cart->CastSpell(cart, BG_SM_CONTROL_VISUAL_ALLIANCE, true);
                            SendMessage2ToAll(LANG_BG_SM_CART_TAKEN, CHAT_MSG_BG_SYSTEM_ALLIANCE, NULL, LANG_BG_SM_ALLY);
                        }
                        else if (teamOwnedCart == HORDE)
                        {
                            cart->CastSpell(cart, BG_SM_CONTROL_VISUAL_HORDE, true);
                            SendMessage2ToAll(LANG_BG_SM_CART_TAKEN, CHAT_MSG_BG_SYSTEM_HORDE, NULL, LANG_BG_SM_HORDE);
                        }
                        else if (teamOwnedCart == TEAM_OTHER)
                            cart->CastSpell(cart, BG_SM_CONTROL_VISUAL_NEUTRAL, true);                            
                    }
                }
            }
        }

        m_MineCartCheckTimer = 1000;
    }
    else
        m_MineCartCheckTimer -= diff;
}

void BattlegroundSM::EventBgCustomEvent(uint32 type, uint32 data)
{
    if (GetStatus() != STATUS_IN_PROGRESS)
        return;

    uint32 depotEntry = type;
    GameObject* depot = GetBGObject(depotEntry, false);
    if (!depot)
        return;

    depot->UseDoorOrButton();

    uint32 mineCart = data - NPC_MINE_CART_1;
    uint32 team = GetMineCartTeamKeeper(mineCart);

    for (GuidSet::const_iterator itr = playersNearMine[mineCart].begin(); itr != playersNearMine[mineCart].end(); ++itr)
        if (Player* player = ObjectAccessor::FindPlayer(*itr))
        {
            TeamId team_index = GetTeamIndexByTeamId(player->GetBGTeam());
            if (team_index == team)
            {
                UpdatePlayerScore(player, SCORE_CARTS_CONTROLLED, 1);
                UpdatePlayerScore(player, SCORE_BONUS_HONOR, Math::Rand(10, 12));
            }

            UpdateWorldStateForPlayer(SM_DISPLAY_PROGRESS_BAR, BG_SM_PROGRESS_BAR_DONT_SHOW, player);
            playersNearMine[SM_MINE_CART_MAX].insert(player->GetGUID());
        }

    playersNearMine[mineCart].clear();
    m_MineCartSpawned[mineCart] = false;

    if (team == TEAM_ALLIANCE)
        SendMessage2ToAll(LANG_BG_SM_CART_CAPTURED, CHAT_MSG_BG_SYSTEM_ALLIANCE, NULL, LANG_BG_SM_ALLY);
    else
        SendMessage2ToAll(LANG_BG_SM_CART_CAPTURED, CHAT_MSG_BG_SYSTEM_HORDE, NULL, LANG_BG_SM_HORDE);
}

uint32 BattlegroundSM::GetMineCartTeamKeeper(uint8 mineCart)
{
    if (m_MineCartsProgressBar[mineCart] > BG_SM_PROGRESS_BAR_NEUTRAL)
        return TEAM_ALLIANCE;

    if (m_MineCartsProgressBar[mineCart] < BG_SM_PROGRESS_BAR_NEUTRAL)
        return TEAM_HORDE;

    return TEAM_NEUTRAL;
}

void BattlegroundSM::MineCartAddPoints(uint32 diff)
{
    if (m_MineCartAddPointsTimer <= diff)
    {
        std::vector<uint8> minesControlled[3];
        for (uint8 i = 0; i < SM_MINE_CART_MAX; ++i)
            minesControlled[GetMineCartTeamKeeper(i)].push_back(i);

        for (uint8 i = 0; i < 2; ++i)
        {
            if (minesControlled[i].empty())
                continue;

            uint32 team = i ? HORDE : ALLIANCE;
            if (minesControlled[i].size() >= 3)
                AddPoints(team, 5);
            else if (minesControlled[i].size() > 1)
                AddPoints(team, 3);
            else
                AddPoints(team, 2);
        }

        m_MineCartAddPointsTimer = 2000;
    }
    else
        m_MineCartAddPointsTimer -= diff;
}

void BattlegroundSM::AddPoints(uint32 Team, uint32 Points)
{
    TeamId team_index = GetTeamIndexByTeamId(Team);
    m_TeamScores[team_index] += Points;
    m_HonorScoreTics[team_index] += Points;

    if (m_HonorScoreTics[team_index] >= m_HonorTics)
    {
        RewardHonorToTeam(GetBonusHonorFromKill(10), Team);
        m_HonorScoreTics[team_index] -= m_HonorTics;
    }

    UpdateTeamScore(team_index);
}

void BattlegroundSM::UpdateTeamScore(uint32 Team)
{
    uint32 score = GetTeamScore(Team);

    if (!m_IsInformedNearVictory && score >= BG_SM_WARNING_NEAR_VICTORY_SCORE)
    {
        if (Team == TEAM_ALLIANCE)
            SendMessageToAll(LANG_BG_SM_A_NEAR_VICTORY, CHAT_MSG_BG_SYSTEM_NEUTRAL);
        else
            SendMessageToAll(LANG_BG_SM_H_NEAR_VICTORY, CHAT_MSG_BG_SYSTEM_NEUTRAL);
        PlaySoundToAll(BG_SM_SOUND_NEAR_VICTORY);
        m_IsInformedNearVictory = true;
    }

    if (score >= BG_SM_MAX_TEAM_SCORE)
    {
        score = BG_SM_MAX_TEAM_SCORE;
        if (Team == TEAM_ALLIANCE)
            EndBattleground(ALLIANCE);
        else
            EndBattleground(HORDE);
    }
    else
    {
        if (Team == TEAM_ALLIANCE)
            UpdateWorldState(SM_ALLIANCE_RESOURCES, score);
        else
            UpdateWorldState(SM_HORDE_RESOURCES, score);
    }
}

void BattlegroundSM::EndBattleground(uint32 winner)
{
    Battleground::EndBattleground(winner);
}

void BattlegroundSM::AddPlayer(Player* player)
{
    Battleground::AddPlayer(player);
    //create score and add it to map
    BattlegroundSMScore* sc = new BattlegroundSMScore();

    PlayerScores[player->GetGUID()] = sc;

    playersNearMine[SM_MINE_CART_MAX].insert(player->GetGUID());
}

void BattlegroundSM::RemovePlayer(Player* /*player*/, ObjectGuid guid, uint32 /*team*/)
{
    for (uint8 i = 0; i <= SM_MINE_CART_MAX; ++i)
        if (playersNearMine[i].count(guid))
            playersNearMine[i].erase(guid);
}

bool BattlegroundSM::SetupBattleground()
{
    // doors
    if (!AddObject(BG_SM_OBJECT_WATERFALL_DEPOT, BG_SM_MINE_WATERFALL_DEPOT, BG_SM_DepotPos[SM_WATERFALL_DEPOT][0], BG_SM_DepotPos[SM_WATERFALL_DEPOT][1], BG_SM_DepotPos[SM_WATERFALL_DEPOT][2], BG_SM_DepotPos[SM_WATERFALL_DEPOT][3], 0, 0, 0.710569f, -0.703627f, RESPAWN_IMMEDIATELY)    // Waterfall
        || !AddObject(BG_SM_OBJECT_LAVA_DEPOT, BG_SM_MINE_LAVA_DEPOT, BG_SM_DepotPos[SM_LAVA_DEPOT][0], BG_SM_DepotPos[SM_LAVA_DEPOT][1], BG_SM_DepotPos[SM_LAVA_DEPOT][2], BG_SM_DepotPos[SM_LAVA_DEPOT][3], 0, 0, 0.710569f, -0.703627f, RESPAWN_IMMEDIATELY)                            // Lava
        || !AddObject(BG_SM_OBJECT_DIAMOND_DEPOT, BG_SM_MINE_DIAMOND_DEPOT, BG_SM_DepotPos[SM_DIAMOND_DEPOT][0], BG_SM_DepotPos[SM_DIAMOND_DEPOT][1], BG_SM_DepotPos[SM_DIAMOND_DEPOT][2], BG_SM_DepotPos[SM_DIAMOND_DEPOT][3], 0, 0, 0.710569f, -0.703627f, RESPAWN_IMMEDIATELY)            // Diamond
        || !AddObject(BG_SM_OBJECT_TROLL_DEPOT, BG_SM_MINE_TROLL_DEPOT, BG_SM_DepotPos[SM_TROLL_DEPOT][0], BG_SM_DepotPos[SM_TROLL_DEPOT][1], BG_SM_DepotPos[SM_TROLL_DEPOT][2], BG_SM_DepotPos[SM_TROLL_DEPOT][3], 0, 0, 0.710569f, -0.703627f, RESPAWN_IMMEDIATELY)                        // Troll
        || !AddObject(BG_SM_OBJECT_DOOR_A_1, BG_SM_DOOR_ALLIANCE_LEFT, BG_SM_DoorPos[0][0], BG_SM_DoorPos[0][1], BG_SM_DoorPos[0][2], BG_SM_DoorPos[0][3], 0, 0, 0.710569f, -0.703627f, RESPAWN_IMMEDIATELY)
        || !AddObject(BG_SM_OBJECT_DOOR_A_2, BG_SM_DOOR_ALLIANCE_RIGHT, BG_SM_DoorPos[1][0], BG_SM_DoorPos[1][1], BG_SM_DoorPos[1][2], BG_SM_DoorPos[1][3], 0, 0, 0.710569f, -0.703627f, RESPAWN_IMMEDIATELY)
        || !AddObject(BG_SM_OBJECT_DOOR_H_1, BG_SM_DOOR_HORDE_LEFT, BG_SM_DoorPos[2][0], BG_SM_DoorPos[2][1], BG_SM_DoorPos[2][2], BG_SM_DoorPos[2][3], 0, 0, 0.710569f, -0.703627f, RESPAWN_IMMEDIATELY)
        || !AddObject(BG_SM_OBJECT_DOOR_H_2, BG_SM_DOOR_HORDE_RIGHT, BG_SM_DoorPos[3][0], BG_SM_DoorPos[3][1], BG_SM_DoorPos[3][2], BG_SM_DoorPos[3][3], 0, 0, 0.710569f, -0.703627f, RESPAWN_IMMEDIATELY)
        || !AddObject(BG_SM_OBJECT_BERSERKING_BUFF_EAST, BG_OBJECTID_BERSERKERBUFF_ENTRY, BG_SM_BuffPos[0][0], BG_SM_BuffPos[0][1], BG_SM_BuffPos[0][2], BG_SM_BuffPos[0][3], 0, 0, 0.710569f, -0.703627f, RESPAWN_IMMEDIATELY)
        || !AddObject(BG_SM_OBJECT_BERSERKING_BUFF_WEST, BG_OBJECTID_BERSERKERBUFF_ENTRY, BG_SM_BuffPos[1][0], BG_SM_BuffPos[1][1], BG_SM_BuffPos[1][2], BG_SM_BuffPos[1][3], 0, 0, 0.710569f, -0.703627f, RESPAWN_IMMEDIATELY)
        || !AddObject(BG_SM_OBJECT_RESTORATION_BUFF_WATERFALL, BG_OBJECTID_REGENBUFF_ENTRY, BG_SM_BuffPos[2][0], BG_SM_BuffPos[2][1], BG_SM_BuffPos[2][2], BG_SM_BuffPos[2][3], 0, 0, 0.710569f, -0.703627f, RESPAWN_IMMEDIATELY)
        || !AddObject(BG_SM_OBJECT_RESTORATION_BUFF_LAVA, BG_OBJECTID_REGENBUFF_ENTRY, BG_SM_BuffPos[3][0], BG_SM_BuffPos[3][1], BG_SM_BuffPos[3][2], BG_SM_BuffPos[3][3], 0, 0, 0.710569f, -0.703627f, RESPAWN_IMMEDIATELY))
    {
        TC_LOG_ERROR("sql.sql", "BatteGroundSM: Failed to spawn some object Battleground not created!");
        return false;
    }

    // Npcs
    if (!AddCreature(NPC_TRACK_SWITCH, SM_TRACK_SWITCH_EAST, BG_SM_TrackPos[SM_EAST_PATH][0], BG_SM_TrackPos[SM_EAST_PATH][1], BG_SM_TrackPos[SM_EAST_PATH][2], BG_SM_TrackPos[SM_EAST_PATH][3]))
    {
        TC_LOG_ERROR("sql.sql", "BatteGroundSM: Failed to spawn some creatures Battleground not created!");
        return false;
    }

    if (!AddCreature(NPC_TRACK_SWITCH, SM_TRACK_SWITCH_NORTH, BG_SM_TrackPos[SM_NORTH_PATH][0], BG_SM_TrackPos[SM_NORTH_PATH][1], BG_SM_TrackPos[SM_NORTH_PATH][2], BG_SM_TrackPos[SM_NORTH_PATH][3]))
    {
        TC_LOG_ERROR("sql.sql", "BatteGroundSM: Failed to spawn some creatures Battleground not created!");
        return false;
    }

    WorldSafeLocsEntry const* sg = sWorldSafeLocsStore.LookupEntry(SM_GRAVEYARD_MAIN_ALLIANCE);
    if (!sg || !AddSpiritGuide(SM_SPIRIT_ALLIANCE, sg->Loc.X, sg->Loc.Y, sg->Loc.Z, sg->Facing, TEAM_ALLIANCE))
    {
        TC_LOG_ERROR("sql.sql", "BatteGroundSM: Failed to spawn spirit guide! Battleground not created!");
        return false;
    }

    sg = sWorldSafeLocsStore.LookupEntry(SM_GRAVEYARD_MAIN_HORDE);
    if (!sg || !AddSpiritGuide(SM_SPIRIT_HORDE, sg->Loc.X, sg->Loc.Y, sg->Loc.Z, sg->Facing, TEAM_HORDE))
    {
        TC_LOG_ERROR("sql.sql", "BatteGroundSM: Failed to spawn spirit guide! Battleground not created!");
        return false;
    }

    return true;
}

WorldSafeLocsEntry const * BattlegroundSM::GetClosestGraveYard(Player* player)
{
    SMBattlegroundGaveyards bggy = player->GetBGTeam() == ALLIANCE ? SM_GRAVEYARD_MAIN_ALLIANCE : SM_GRAVEYARD_MAIN_HORDE;
    WorldSafeLocsEntry const* sg = sWorldSafeLocsStore.LookupEntry(bggy);
    return sg;
}

void BattlegroundSM::HandleKillPlayer(Player* player, Player* killer)
{
    if (GetStatus() != STATUS_IN_PROGRESS)
        return;

    Battleground::HandleKillPlayer(player, killer);
    EventPlayerDroppedFlag(player);
}

void BattlegroundSM::UpdatePlayerScore(Player* player, uint32 type, uint32 value, bool doAddHonor)
{
    BattlegroundScoreMap::iterator itr = PlayerScores.find(player->GetGUID());
    if (itr == PlayerScores.end())                         // player not found
        return;

    switch (type)
    {
        case SCORE_CARTS_CONTROLLED:                           // Mine Carts captures
            ((BattlegroundSMScore*)itr->second)->MineCartCaptures += value;
            break;
        default:
            Battleground::UpdatePlayerScore(player, type, value, doAddHonor);
            break;
    }

    return;
}

void BattlegroundSM::FillInitialWorldStates(WorldStateBuilder& builder)
{
    builder.AppendState(SM_MINE_CARTS_DISPLAY, DISPLAY_WORLDSTATE);
    builder.AppendState(SM_ALLIANCE_RESOURCES, m_TeamScores[TEAM_HORDE]);
    builder.AppendState(SM_HORDE_RESOURCES, m_TeamScores[TEAM_ALLIANCE]);
    builder.AppendState(SM_MINE_CART_1, DISPLAY_WORLDSTATE);
    builder.AppendState(SM_MINE_CART_2, DISPLAY_WORLDSTATE);
    builder.AppendState(SM_DISPLAY_ALLIANCE_RESSOURCES, DISPLAY_WORLDSTATE);
    builder.AppendState(SM_MINE_CART_3, DISPLAY_WORLDSTATE);
    builder.AppendState(SM_DISPLAY_HORDE_RESSOURCES, DISPLAY_WORLDSTATE);
    builder.AppendState(SM_DISPLAY_PROGRESS_BAR, BG_SM_PROGRESS_BAR_DONT_SHOW); // This shows the mine cart control bar
    builder.AppendState(SM_PROGRESS_BAR_STATUS, BG_SM_PROGRESS_BAR_NEUTRAL); // Neutral
    builder.AppendState(SM_UNK, uint32(0));
}

uint32 BattlegroundSM::GetPrematureWinner()
{
    if (GetTeamScore(TEAM_ALLIANCE) > GetTeamScore(TEAM_HORDE))
        return ALLIANCE;

    else if (GetTeamScore(TEAM_HORDE) > GetTeamScore(TEAM_ALLIANCE))
        return HORDE;

    return Battleground::GetPrematureWinner();
}

