/*
 * Copyright (C) 2011-2015 Project SkyFire <http://www.projectskyfire.org/>
 * Copyright (C) 2008-2015 TrinityCore <http://www.trinitycore.org/>
 * Copyright (C) 2005-2015 MaNGOS <http://getmangos.com/>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 3 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef TRINITY_SMARTSCRIPT_H
#define TRINITY_SMARTSCRIPT_H

#include "Common.h"
#include "Creature.h"
#include "CreatureAI.h"
#include "Unit.h"
#include "Spell.h"
#include "GridNotifiers.h"

#include "SmartScriptMgr.h"
//#include "SmartAI.h"

class Player;

class SmartScript
{
    public:
        SmartScript();
        ~SmartScript();

        void OnInitialize(WorldObject* obj, AreaTriggerEntry const* at = nullptr, SceneTemplate const* scene = nullptr);
        void GetScript();
        void FillScript(SmartAIEventList e, WorldObject* obj, AreaTriggerEntry const* at, SceneTemplate const* scene);

        void ProcessEventsFor(SMART_EVENT e, Unit* unit = nullptr, uint32 var0 = 0, uint32 var1 = 0, bool bvar = false, const SpellInfo* spell = nullptr, GameObject* gob = nullptr, std::string const& varString = "");
        void ProcessEvent(SmartScriptHolder& e, Unit* unit = nullptr, uint32 var0 = 0, uint32 var1 = 0, bool bvar = false, const SpellInfo* spell = nullptr, GameObject* gob = nullptr, std::string const& varString = "");
        bool CheckTimer(SmartScriptHolder const& e) const;
        static void RecalcTimer(SmartScriptHolder& e, uint32 min, uint32 max);
        void UpdateTimer(SmartScriptHolder& e, uint32 const diff);
        static void InitTimer(SmartScriptHolder& e);
        void ProcessAction(SmartScriptHolder& e, Unit* unit = nullptr, uint32 var0 = 0, uint32 var1 = 0, bool bvar = false, const SpellInfo* spell = nullptr, GameObject* gob = nullptr, std::string const& varString = "");
        void ProcessTimedAction(SmartScriptHolder& e, uint32 const& min, uint32 const& max, Unit* unit = nullptr, uint32 var0 = 0, uint32 var1 = 0, bool bvar = false, const SpellInfo* spell = nullptr, GameObject* gob = nullptr, std::string const& varString = "");
        void GetTargets(ObjectVector& targets, SmartScriptHolder const& e, Unit* invoker = nullptr) const;
        void GetWorldObjectsInDist(ObjectVector& objects, float dist) const;
        void InstallTemplate(SmartScriptHolder const& e);
        static SmartScriptHolder CreateEvent(SMART_EVENT e, uint32 event_flags, uint32 event_param1, uint32 event_param2, uint32 event_param3, uint32 event_param4, SMART_ACTION action, uint32 action_param1, uint32 action_param2, uint32 action_param3, uint32 action_param4, uint32 action_param5, uint32 action_param6, SMARTAI_TARGETS t, uint32 target_param1, uint32 target_param2, uint32 target_param3, uint32 phaseMask = 0);
        void AddEvent(SMART_EVENT e, uint32 event_flags, uint32 event_param1, uint32 event_param2, uint32 event_param3, uint32 event_param4, SMART_ACTION action, uint32 action_param1, uint32 action_param2, uint32 action_param3, uint32 action_param4, uint32 action_param5, uint32 action_param6, SMARTAI_TARGETS t, uint32 target_param1, uint32 target_param2, uint32 target_param3, uint32 phaseMask = 0);
        void SetPathId(uint32 id) { mPathId = id; }
        uint32 GetPathId() const { return mPathId; }
        WorldObject* GetBaseObject() const
        {
            WorldObject* obj = nullptr;
            if (me)
                obj = me;
            else if (go)
                obj = go;
            return obj;
        }
        WorldObject* GetBaseObjectOrUnit(Unit* unit)
        {
            WorldObject* summoner = GetBaseObject();

            if (!summoner && unit)
                return unit;

            return summoner;
        }

        static bool IsUnit(WorldObject* obj)
        {
            return obj && (obj->GetTypeId() == TYPEID_UNIT || obj->GetTypeId() == TYPEID_PLAYER);
        }

        static bool IsPlayer(WorldObject* obj)
        {
            return obj && obj->GetTypeId() == TYPEID_PLAYER;
        }

        static bool IsCreature(WorldObject* obj)
        {
            return obj && obj->GetTypeId() == TYPEID_UNIT;
        }

        static bool IsCreatureInControlOfSelf(WorldObject* obj)
        {
            if (Creature* creatureObj = obj ? obj->ToCreature() : nullptr)
                return !creatureObj->IsCharmed() && !creatureObj->IsControlledByPlayer();
            else
                return false;
        }

        static bool IsGameObject(WorldObject* obj)
        {
            return obj && obj->GetTypeId() == TYPEID_GAMEOBJECT;
        }

        void OnUpdate(const uint32 diff);
        void OnMoveInLineOfSight(Unit* who);

        Unit* DoSelectLowestHpFriendly(float range, uint32 MinHPDiff) const;
        void DoFindFriendlyCC(std::vector<Creature*>& creatures, float range) const;
        void DoFindFriendlyMissingBuff(std::vector<Creature*>& creatures, float range, uint32 spellid) const;
        Unit* DoFindClosestFriendlyInRange(float range, bool playerOnly) const;

        void StoreTargetList(ObjectVector const& targets, uint32 id)
        {
            // insert or replace
            _storedTargets.erase(id);
            _storedTargets.emplace(id, ObjectGuidVector(targets));
        }

        bool IsSmart(Creature* c = nullptr)
        {
            bool smart = true;
            if (c && c->GetAIName() != "SmartAI")
                smart = false;

            if (!me || me->GetAIName() != "SmartAI")
                smart = false;

            if (!smart)
                TC_LOG_ERROR("sql.sql", "SmartScript: Action target Creature (GUID: %u Entry: %u) is not using SmartAI, action called by Creature (GUID: %u Entry: %u) skipped to prevent crash.", c ? c->GetSpawnId() : 0, c ? c->GetEntry() : 0, me ? me->GetSpawnId() : 0, me ? me->GetEntry() : 0);

            return smart;
        }

        bool IsSmartGO(GameObject* g = nullptr)
        {
            bool smart = true;
            if (g && g->GetAIName() != "SmartGameObjectAI")
                smart = false;

            if (!go || go->GetAIName() != "SmartGameObjectAI")
                smart = false;
            if (!smart)
                TC_LOG_ERROR("sql.sql", "SmartScript: Action target GameObject (GUID: %u Entry: %u) is not using SmartGameObjectAI, action called by GameObject (GUID: %u Entry: %u) skipped to prevent crash.", g ? g->GetSpawnId() : 0, g ? g->GetEntry() : 0, go ? go->GetSpawnId() : 0, go ? go->GetEntry() : 0);

            return smart;
        }

        ObjectVector const* GetStoredTargetVector(uint32 id, WorldObject const& ref) const
        {
            auto itr = _storedTargets.find(id);
            if (itr != _storedTargets.end())
                return itr->second.GetObjectVector(ref);
            return nullptr;
        }

        void StoreCounter(uint32 id, uint32 value, uint32 reset)
        {
            uint32 newVal = mCounterList[id] = reset ? value : mCounterList[id] + value;
            ProcessEventsFor(SMART_EVENT_COUNTER_SET, NULL, id, newVal);
        }

        GameObject* FindGameObjectNear(WorldObject* searchObject, uint32 guid) const
        {
            auto bounds = searchObject->GetMap()->GetGameObjectBySpawnIdStore().equal_range(guid);
            if (bounds.first == bounds.second)
                return nullptr;

            return bounds.first->second;
        }

        Creature* FindCreatureNear(WorldObject* searchObject, uint32 guid) const
        {
            auto bounds = searchObject->GetMap()->GetCreatureBySpawnIdStore().equal_range(guid);
            if (bounds.first == bounds.second)
                return nullptr;

            auto creatureItr = std::find_if(bounds.first, bounds.second, [](Map::CreatureBySpawnIdContainer::value_type const& pair)
            {
                return pair.second->IsAlive();
            });

            return creatureItr != bounds.second ? creatureItr->second : bounds.first->second;
        }

        void OnReset();
        void ResetBaseObject()
        {
            WorldObject* lookupRoot = me;
            if (!lookupRoot)
                lookupRoot = go;

            if (lookupRoot)
            {
                if (!meOrigGUID.IsEmpty())
                {
                    if (Creature* m = ObjectAccessor::GetCreature(*lookupRoot, meOrigGUID))
                    {
                        me = m;
                        go = nullptr;
                    }
                }
                if (!goOrigGUID.IsEmpty())
                {
                    if (GameObject* o = ObjectAccessor::GetGameObject(*lookupRoot, goOrigGUID))
                    {
                        me = nullptr;
                        go = o;
                    }
                }
            }
            goOrigGUID.Clear();
            meOrigGUID.Clear();
        }

        //TIMED_ACTIONLIST (script type 9 aka script9)
        void SetScript(SmartScriptHolder& e, uint32 entry);
        Unit* GetLastInvoker(Unit* invoker = nullptr) const;
        ObjectGuid mLastInvoker;
        typedef std::unordered_map<uint32, uint32> CounterMap;
        CounterMap mCounterList;

    private:

        void IncPhase(uint32 p);
        void DecPhase(uint32 p);

        void SetPhase(uint32 p);
        bool IsInPhase(uint32 p) const;

        SmartAIEventList mEvents;
        SmartAIEventList mInstallEvents;
        SmartAIEventList mTimedActionList;
        bool isProcessingTimedActionList;
        Creature* me;
        ObjectGuid meOrigGUID;
        GameObject* go;
        ObjectGuid goOrigGUID;
        AreaTriggerEntry const* trigger;
        SceneTemplate const* sceneTemplate;
        SmartScriptType mScriptType;
        uint32 mEventPhase;

        uint32 mPathId;
        SmartAIEventList mStoredEvents;
        std::list<uint32>mRemIDs;

        uint32 mTextTimer;
        uint32 mLastTextID;
        uint32 mTalkerEntry;
        bool mUseTextTimer;

        ObjectVectorMap _storedTargets;

        SMARTAI_TEMPLATE mTemplate;
        void InstallEvents();

        void RemoveStoredEvent(uint32 id)
        {
            if (!mStoredEvents.empty())
                for (SmartAIEventList::iterator i = mStoredEvents.begin(); i != mStoredEvents.end(); ++i)
                    if (i->event_id == id)
                    {
                        mStoredEvents.erase(i);
                        return;
                    }
        }
};

#endif
