/*
 * Copyright (C) 2015-2016 Theatre of Dreams <http://theatreofdreams.eu/>
 * Copyright (C) 2011-2015 Project SkyFire <http://www.projectskyfire.org/>
 * Copyright (C) 2008-2015 TrinityCore <http://www.trinitycore.org/>
 * Copyright (C) 2005-2015 MaNGOS <http://getmangos.com/>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 3 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SF_ADDONMGR_H
#define SF_ADDONMGR_H

#include "Singleton/Singleton.hpp"
#include "Common.h"

struct AddonInfo
{
    std::string Name;
    uint8       Enabled;
    uint32      CRC;
    uint8       State;
    bool        UsePublicKeyOrCRC;
};

class AddonMgr
{
public:
    struct SavedAddon
    {
        std::string Name;
        uint32      CRC;
    };

    typedef std::vector<SavedAddon>         SavedAddonsList;

    struct BannedAddon
    {
        uint32                  Id;
        std::array<uint8, 16>   NameMD5;
        std::array<uint8, 16>   VersionMD5;
        uint32                  Timestamp;
    };

    typedef std::vector<BannedAddon>        BannedAddonsList;

    void                                    LoadFromDB();
    void                                    SaveAddon( const AddonInfo& addonInfo );

    boost::optional< const SavedAddon& >    GetKnownAddon(const std::string& name) const;
    const BannedAddonsList&                 GetBannedAddons() const;

protected:
    SavedAddonsList                         m_knownAddons;
    BannedAddonsList                        m_bannedAddons;
};

#define sAddonMgr Tod::Singleton< AddonMgr >::GetSingleton()

#endif
