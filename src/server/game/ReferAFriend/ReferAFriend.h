/*
* Copyright (C) 2017 Project Aurora
*
* This program is free software; you can redistribute it and/or modify it
* under the terms of the GNU General Public License as published by the
* Free Software Foundation; either version 2 of the License, or (at your
* option) any later version.
*
* This program is distributed in the hope that it will be useful, but WITHOUT
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
* more details.
*
* You should have received a copy of the GNU General Public License along
* with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include "Common.h"
#include "DatabaseEnv.h"

#ifndef ReferAFriend_h__
#define ReferAFriend_h__

class RaFData
{
    public:
        RaFData(uint32 accountId);
        ~RaFData();

        void LoadFromDB(PreparedQueryResult rafDataResult, PreparedQueryResult rafRecruitsResult);
        void SaveToDB();

        uint32 GetRecruiter() const;

        uint32 GetRecruitsCount() const;
        bool IsARecruiter() const;

        void IncreaseInvitationsCount();
        void ResetInvitationsCount();
        uint32 GetInvitationsCount() const;

        void IncreaseRewardsCount();
        void DecreaseRewardsCount();
        uint32 GetRewardsCount() const;

        void SetRecruiterAwarded(bool award);
        bool IsRecruiterAwarded() const;

    private:
        uint32 _accountID;
        uint32 _recruiter;
        uint32 _recruitsCount;
        uint32 _rewardsCount;
        uint32 _invCount;

        bool _recruiterAwarded;
        bool _loadedFromDB;
        bool _alreadySavedToDB;
};

#endif // ReferAFriend_h__