/*
* Copyright (C) 2017 Project Aurora
*
* This program is free software; you can redistribute it and/or modify it
* under the terms of the GNU General Public License as published by the
* Free Software Foundation; either version 2 of the License, or (at your
* option) any later version.
*
* This program is distributed in the hope that it will be useful, but WITHOUT
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
* more details.
*
* You should have received a copy of the GNU General Public License along
* with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include "RecruitAFriendRewardsStore.h"
#include "DatabaseEnv.h"
#include "Log.h"
#include "Timer.h"
#include "BattlePayShopMgr.h"
#include "ObjectMgr.h"

void RecruitAFriendRewardsStore::LoadRecruitAFriendRewards()
{
    uint32 oldMSTime = getMSTime();

    PreparedStatement* stmt = WorldDatabase.GetPreparedStatement(WORLD_SEL_RAF_REWARDS_PRODUCTS);
    PreparedQueryResult result = WorldDatabase.Query(stmt);

    if (!result)
    {
        TC_LOG_INFO("sql.sql", ">> Loaded 0 RaF Rewards, table `raf_rewards` is empty!");
        return;
    }

    uint32 count = 0;
    do
    {
        Field* fields = result->Fetch();

        uint32 id               = fields[0].GetUInt32();
        std::string title       = fields[1].GetString();
        std::string title2      = fields[2].GetString();
        uint32 itemId           = fields[3].GetUInt32();
        uint32 displayId        = fields[4].GetUInt32();

        if (title.length() > MAX_BATTLE_PAY_SHOP_PRODUCT_TITLE_SIZE)
        {
            TC_LOG_ERROR("sql.sql", "Title for product id %d defined in `raf_rewards` is too large (max %d), skipped!",
                id, MAX_BATTLE_PAY_SHOP_PRODUCT_TITLE_SIZE);

            continue;
        }

        if (title2.length() > MAX_BATTLE_PAY_SHOP_PRODUCT_TITLE_SIZE)
        {
            TC_LOG_ERROR("sql.sql", "Title2 for product id %d defined in `raf_rewards` is too large (max %d), skipped!",
                id, MAX_BATTLE_PAY_SHOP_PRODUCT_TITLE_SIZE);

            continue;
        }

        if (!sObjectMgr->GetItemTemplate(itemId))
        {
            TC_LOG_ERROR("sql.sql", "Item id %u for product id %d defined in `raf_rewards` doesn't exist, skipped!", itemId, id);
            continue;
        }

        RaFReward reward;
        reward.Id = id;
        reward.Title = title;
        reward.Title2 = title2;
        reward.ItemId = itemId;
        reward.DisplayId = displayId;

        _rafRewards[id] = std::move(reward);

        count++;
    } while (result->NextRow());

    TC_LOG_INFO("server.loading", ">> Loaded %u RaF rewards in %u ms.", count, GetMSTimeDiffToNow(oldMSTime));
}

RaFReward const* RecruitAFriendRewardsStore::GetRaFReward(uint32 productID) const
{
    auto itr = _rafRewards.find(productID);
    if (itr != _rafRewards.end())
        return &itr->second;

    return nullptr;
}
