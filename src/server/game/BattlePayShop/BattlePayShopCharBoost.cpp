/*
 * Copyright (C) 2017-2017 Project Aurora
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 3 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "BattlePayShopCharBoost.h"
#include "DBCStores.h"
#include "Player.h"
#include "MapManager.h"
#include "CharacterCache.h"

BattlePayShopCharBoost::BattlePayShopCharBoost(BattlePayShop* battlePayShop)
{
    _battlePayShop = battlePayShop;
}

BattlePayShopCharBoost::~BattlePayShopCharBoost()
{
}

void BattlePayShopCharBoost::GetProfessions(uint32 guid, std::unordered_set<uint32>& professions) const
{
    PreparedStatement* stmt = CharacterDatabase.GetPreparedStatement(CHAR_SEL_CHAR_PROFESSIONS);
    stmt->setUInt32(0, guid);

    PreparedQueryResult result = CharacterDatabase.Query(stmt);
    if (!result)
        return;

    for (uint8 index = 0; index < PLAYER_MAX_PRIMARY_PROF; ++index)
    {
        uint32 profession = (*result)[index].GetUInt32();

        if (profession)
            professions.insert(profession);
    }
}

void BattlePayShopCharBoost::GetOldTalents(uint32 guid, std::vector<uint32>& spells) const
{
    PreparedStatement* stmt = CharacterDatabase.GetPreparedStatement(CHAR_SEL_CHAR_TALENT);
    stmt->setUInt32(0, guid);

    PreparedQueryResult result = CharacterDatabase.Query(stmt);
    if (!result)
        return;

    do
    {
        uint32 talentID = (*result)[0].GetUInt32();

        TalentEntry const* talent = sTalentStore.LookupEntry(talentID);
        if (!talentID)
            continue;

        spells.push_back(talent->SpellID);

    } while (result->NextRow());
}

void BattlePayShopCharBoost::GetOldSpellsFromSpecialization(uint32 guid, uint32 classId, std::vector<uint32>& spells) const
{
    for (uint32 i = 0; i < MAX_SPECIALIZATIONS; ++i)
        if (ChrSpecializationEntry const* specialization = sDBCManager->GetChrSpecializationByIndexArray(classId, i))
            if (std::vector<SpecializationSpellsEntry const*> const* specSpells = sDBCManager->GetSpecializationSpells(specialization->ID))
                for (size_t j = 0; j < specSpells->size(); ++j)
                    if (SpecializationSpellsEntry const* specSpell = specSpells->at(j))
                        spells.push_back(specSpell->SpellId);
}

void BattlePayShopCharBoost::GetOldEquippedItems(uint32 guid, std::vector<uint32>& items) const
{
    PreparedStatement* stmt = CharacterDatabase.GetPreparedStatement(CHAR_SEL_CHAR_INVENTORY_ITEM_GUID_UNTIL_BAG_SLOT);
    stmt->setUInt32(0, 0);
    stmt->setUInt8(1, EQUIPMENT_SLOT_END);
    stmt->setUInt32(2, guid);
    PreparedQueryResult result = CharacterDatabase.Query(stmt);

    if (!result)
        return;

    do
    {
        items.push_back((*result)[0].GetUInt32());
    }
    while (result->NextRow());
}

uint32 BattlePayShopCharBoost::PrepareMail(uint32 guid, SQLTransaction& trans) const
{
    uint32 mailId = sObjectMgr->GenerateMailID();

    std::string const subject = "Character Boost";
    std::string const body = "We've supplied you with a whole new set of high-level gear, but attached you'll find all the old items and equipment you once carried.\n\n - The WoW Dev Team";

    uint8 index = 0;
    PreparedStatement* stmt = CharacterDatabase.GetPreparedStatement(CHAR_INS_MAIL);

    stmt->setUInt32(index++, mailId);
    stmt->setUInt8(index++, MAIL_NORMAL);
    stmt->setInt8(index++, MAIL_STATIONERY_DEFAULT);
    stmt->setUInt16(index++, 0);
    stmt->setUInt32(index++, guid);
    stmt->setUInt32(index++, guid);
    stmt->setString(index++, subject);
    stmt->setString(index++, body);
    stmt->setBool(index++, true);
    stmt->setUInt64(index++, time(NULL) + 180 * DAY);
    stmt->setUInt64(index++, time(NULL));
    stmt->setUInt32(index++, 0);
    stmt->setUInt32(index++, 0);
    stmt->setUInt8(index++, 0);

    trans->Append(stmt);

    return mailId;
}

bool BattlePayShopCharBoost::ProcessCharBoost(uint32 guid, TeamId team, TalentSpecialization specialization)
{
    ObjectGuid playerGUID = ObjectGuid::Create<HighGuid::Player>(guid);

    CharacterCacheEntry const* characterInfo = sCharacterCache->GetCharacterCacheByGuid(playerGUID);
    if (!characterInfo)
    {
        TC_LOG_ERROR("battlepayshop.charboost",
            "CharBoosting: Cannot retrieve character data for player %s (account: %u) - abort.", playerGUID.ToString().c_str(), _battlePayShop->GetAccountId());
        return false;
    }

    // Step 1: Boost Level
    uint8 level = 90;

    // Step 2: Set new race (only for pandarens)
    uint8 oldRace = characterInfo->Race;
    if (!oldRace)
    {
        TC_LOG_ERROR("battlepayshop.charboost",
            "CharBoosting: Invalid race for player %s (account: %u) - abort.", playerGUID.ToString().c_str(), _battlePayShop->GetAccountId());
        return false;
    }

    uint8 newRace = oldRace;
    if (oldRace == RACE_PANDAREN_NEUTRAL)
        newRace = (team == TEAM_ALLIANCE) ? RACE_PANDAREN_ALLIANCE : RACE_PANDAREN_HORDE;

    // Step 3: Set new specialization
    uint32 newSpecialization = specialization;
    ChrSpecializationEntry const* specEntry = sChrSpecializationStore.LookupEntry(newSpecialization);
    if (!specEntry)
    {
        TC_LOG_ERROR("battlepayshop.charboost",
            "CharBoosting: Specialization %u specified for player %s (account: %u) does not exist- abort.", newSpecialization, playerGUID.ToString().c_str(), _battlePayShop->GetAccountId());
        return false;
    }

    uint32 classId = specEntry->ClassID;
    if (!classId)
    {
        TC_LOG_ERROR("battlepayshop.charboost",
            "CharBoosting: Invalid class for player %s (account: %u) - abort.", playerGUID.ToString().c_str(), _battlePayShop->GetAccountId());
        return false;
    }

    // Step 4: Set new professions (only for characters above 60 lvl!)
    std::unordered_set<uint32> professionsToLevelUp;

    uint8 oldLevel = characterInfo->Level;
    if (!oldLevel)
    {
        TC_LOG_ERROR("battlepayshop.charboost",
            "CharBoosting: Invalid old level for player %s (account: %u) - abort.", playerGUID.ToString().c_str(), _battlePayShop->GetAccountId());
        return false;
    }

    if (oldLevel >= 60)
    {
        // 4.1 Set up new or existing professions
        GetProfessions(guid, professionsToLevelUp);

        if (professionsToLevelUp.size() < PLAYER_MAX_PRIMARY_PROF)
            if (ProfessionsSet const* professions = sBattlePayShopCharBoostMgr->GetProfessions(classId))
                professionsToLevelUp.insert(professions->begin(), professions->end());

        // 4.2 Always add First Aid
        professionsToLevelUp.insert(SKILL_ID_FIRST_AID);

        TC_LOG_TRACE("battlepayshop.charboost",
            "CharBoosting: Prepared Veteran Bonus for player %s (account: %u).", playerGUID.ToString().c_str(), _battlePayShop->GetAccountId());
    }

    // Step 5: Learn spells
    std::vector<uint32> spellsToLearn;
    {
        // 5.1. Default spells
        SpellsStore const& defaultSpells = sBattlePayShopCharBoostMgr->GetSpells();
        spellsToLearn.insert(spellsToLearn.begin(), defaultSpells.begin(), defaultSpells.end());

        // 5.2: Pandaren Language Spells
        if (oldRace == RACE_PANDAREN_NEUTRAL)
        {
            uint32 const* languageSpells = (newRace == RACE_PANDAREN_ALLIANCE) ? pandarenLanguageSpellsAlliance : pandarenLanguageSpellsHorde;

            for (uint8 i = 0; i < PANDAREN_FACTION_LANGUAGE_COUNT; ++i)
                spellsToLearn.push_back(languageSpells[i]);
        }

        // 5.3: Misc spells
        spellsToLearn.push_back((team == TEAM_ALLIANCE) ? SWIFT_PURPLE_GRYPGON_SPELL : SWIFT_PURPLE_WIND_RIDER_SPELL);

        switch (classId)
        {
            case CLASS_WARRIOR:
            case CLASS_PALADIN:
                spellsToLearn.push_back(SPELL_PLATE_MAIL_ARMOR);
                break;
            default:
                break;
        }
    }

    // Step 6: Unlearn spells from old specialization/talents
    std::vector<uint32> spellsToRemove;
    {
        // 6.1: Old talents
        GetOldTalents(guid, spellsToRemove);

        // 6.2: Old spells from specialization
        GetOldSpellsFromSpecialization(guid, classId, spellsToRemove);
    }

    // Step 7: Remove old equipped items
    std::vector<uint32> itemsToUnEquip;
    GetOldEquippedItems(guid, itemsToUnEquip);

    // Step 8: Get new items
    EquipItemsMap const* itemsToEquip = sBattlePayShopCharBoostMgr->GetEquipItems(newSpecialization);
    if (!itemsToEquip)
    {
        TC_LOG_ERROR("battlepayshop.charboost",
            "CharBoosting: Player %s (account: %u) does not have a new equipment loaded for specialization %u - abort.",
            playerGUID.ToString().c_str(), _battlePayShop->GetAccountId(), newSpecialization);
        return false;
    }

    // Step 9: Get new misc items
    MiscItemsStore const& miscItems = sBattlePayShopCharBoostMgr->GetMiscItems();

    // Step 10: Set new locations
    uint32 mapID = startPosition[team].mapID;
    Position const pos = startPosition[team].pos;

    // Step 11: Set money
    uint64 money = GOLD_150;

    // Step 12: SAVE TO DB
    TC_LOG_TRACE("battlepayshop.charboost",
        "CharBoosting: Started finalizing character boosting for player %s (account: %u, level: %u, race: %u, class: %u, team: %u, spec: %u).",
        playerGUID.ToString().c_str(), _battlePayShop->GetAccountId(), oldLevel, oldRace, classId, team, specialization);

    uint32 zoneID = sMapMgr->GetZoneId(mapID, pos.GetPositionX(), pos.GetPositionY(), pos.GetPositionZ());

    uint8 index = 0;
    SQLTransaction trans = CharacterDatabase.BeginTransaction();
    {
        // 12.1: Updates: level, zone, race, money, positions, talent tree, equipment cache
        PreparedStatement* stmt = CharacterDatabase.GetPreparedStatement(CHAR_UPD_CHARACTER_FOR_BOOST);

        stmt->setUInt8(index++, newRace);
        stmt->setUInt16(index++, uint16(zoneID));
        stmt->setUInt64(index++, money);
        stmt->setUInt8(index++, level);
        stmt->setFloat(index++, pos.GetPositionX());
        stmt->setFloat(index++, pos.GetPositionY());
        stmt->setFloat(index++, pos.GetPositionZ());
        stmt->setFloat(index++, pos.GetOrientation());
        stmt->setUInt16(index++, mapID);

        std::ostringstream talentTree;
        talentTree << newSpecialization << " 0 ";
        stmt->setString(index++, talentTree.str());

        stmt->setUInt16(index++, AT_LOGIN_FIRST);

        std::ostringstream items;
        for (uint8 i = 0; i < INVENTORY_SLOT_BAG_END; i++)
        {
            auto itr = itemsToEquip->find(i);
            if (itr != itemsToEquip->end())
                items << itr->second << " 0 ";
            else
                items << "0 0 ";
        }

        stmt->setString(index++, items.str());
        stmt->setUInt32(index++, newSpecialization);
        stmt->setUInt32(index++, guid);

        trans->Append(stmt);

        // 12.2: Updates homebind
        index = 0;
        stmt = CharacterDatabase.GetPreparedStatement(CHAR_UPD_PLAYER_HOMEBIND);

        stmt->setUInt16(index++, mapID);
        stmt->setUInt16(index++, zoneID);
        stmt->setFloat(index++, pos.GetPositionX());
        stmt->setFloat(index++, pos.GetPositionY());
        stmt->setFloat(index++, pos.GetPositionZ());
        stmt->setUInt32(index++, guid);

        trans->Append(stmt);

        // 12.3: Updates professions
        {
            // 12.3.1: Delete skills (if presents)
            for (uint32 professionID : professionsToLevelUp)
            {
                index = 0;
                stmt = CharacterDatabase.GetPreparedStatement(CHAR_DEL_CHAR_SKILL_BY_SKILL);

                stmt->setUInt32(index++, guid);
                stmt->setUInt16(index++, uint16(professionID));

                trans->Append(stmt);
            }

            // 12.3.2: Insert skills
            for (uint32 professionID : professionsToLevelUp)
            {
                uint16 value = 600;
                uint16 max = 600;

                index = 0;
                stmt = CharacterDatabase.GetPreparedStatement(CHAR_INS_CHAR_SKILLS);

                stmt->setUInt32(index++, guid);
                stmt->setUInt16(index++, uint16(professionID));
                stmt->setUInt16(index++, value);
                stmt->setUInt16(index++, max);

                trans->Append(stmt);
            }
        }

        // 12.4: Delete old spells and talents
        for (uint32 spellID : spellsToRemove)
        {
            index = 0;
            stmt = CharacterDatabase.GetPreparedStatement(CHAR_DEL_CHAR_SPELL_BY_SPELL);

            stmt->setUInt32(index++, spellID);
            stmt->setUInt32(index++, guid);

            trans->Append(stmt);
        }

        index = 0;
        stmt = CharacterDatabase.GetPreparedStatement(CHAR_DEL_CHAR_TALENT);

        stmt->setUInt32(index++, guid);

        trans->Append(stmt);

        // 12.5: Learn new spells
        for (uint32 spellID : spellsToLearn)
        {
            index = 0;
            stmt = CharacterDatabase.GetPreparedStatement(CHAR_INS_CHAR_SPELL);

            stmt->setUInt32(index++, guid);
            stmt->setUInt32(index++, spellID);
            stmt->setBool(index++, true);
            stmt->setBool(index++, false);

            trans->Append(stmt);
        }

        // 12.6: Un-equip old items/save misc items and send them all in mail
        {
            // 12.6.1: Un-equip old items
            index = 0;
            stmt = CharacterDatabase.GetPreparedStatement(CHAR_DEL_CHAR_INVENTORY_UNTIL_BAG_SLOT);

            stmt->setUInt32(index++, 0);
            stmt->setUInt8(index++, EQUIPMENT_SLOT_END);
            stmt->setUInt32(index++, guid);

            trans->Append(stmt);

            // 12.6.2: Save to DB misc items
            std::vector<uint32> itemsToMerge;
            for (auto itr : miscItems)
            {
                uint32 itemID = itr.first;
                int32 itemCount = itr.second;

                // one item with multi stacks
                if (itemCount > 0)
                {
                    if (Item* item = Item::CreateItem(itemID, itemCount, playerGUID))
                    {
                        item->SetBinding(true);
                        item->SaveToDB(trans);

                        itemsToMerge.push_back(item->GetGUID().GetCounter());
                    }
                }
                else // multi items with one stack
                {
                    for (uint8 i = 0; i < uint8(-itemCount); ++i)
                        if (Item* item = Item::CreateItem(itemID, 1, playerGUID))
                        {
                            item->SetBinding(true);
                            item->SaveToDB(trans);

                            itemsToMerge.push_back(item->GetGUID().GetCounter());
                        }
                }
            }

            // 12.6.3: Merge un-equip items with misc items
            itemsToUnEquip.insert(itemsToUnEquip.end(), itemsToMerge.begin(), itemsToMerge.end());

            // 12.6.4: Send all items via mail
            uint32 mailId = PrepareMail(guid, trans);
            uint32 itemCount = 0;

            for (uint32 itemGUID : itemsToUnEquip)
            {
                if (itemCount++ > 10)
                {
                    itemCount = 0;
                    mailId = PrepareMail(guid, trans);
                }

                index = 0;
                stmt = CharacterDatabase.GetPreparedStatement(CHAR_INS_MAIL_ITEM);

                stmt->setUInt32(index++, mailId);
                stmt->setUInt32(index++, itemGUID);
                stmt->setUInt32(index++, guid);

                trans->Append(stmt);
            }
        }

        // 12.7: Save to DB and equip new items
        for (auto itr : *itemsToEquip)
        {
            uint8 slot = itr.first;
            uint32 itemID = itr.second;

            if (Item* item = Item::CreateItem(itemID, 1, playerGUID))
            {
                item->SetBinding(true);
                item->SaveToDB(trans);

                index = 0;
                stmt = CharacterDatabase.GetPreparedStatement(CHAR_INS_CHAR_INVENTORY);

                stmt->setUInt32(index++, guid);
                stmt->setUInt32(index++, 0);
                stmt->setUInt8(index++, slot);
                stmt->setUInt32(index++, item->GetGUID().GetCounter());

                trans->Append(stmt);
            }
        }
    }

    CharacterDatabase.CommitTransaction(trans);

    // Step 13: Finish boosting
    _battlePayShop->FinishBoostingChar(itemsToEquip);

    return true;
}
