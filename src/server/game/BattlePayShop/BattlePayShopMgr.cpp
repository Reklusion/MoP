/*
 * Copyright (C) 2015-2017 Theatre of Dreams <http://www.theatreofdreams.eu>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 3 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "BattlePayShopMgr.h"
#include "BattlePayShop.h"
#include "DatabaseEnv.h"
#include "Log.h"
#include "ObjectMgr.h"
#include "WorldPacket.h"
#include "Player.h"

static void strToLower(char* str)
{
    while (*str)
    {
        *str = tolower(*str);
        ++str;
    }
}

static void SearchFileDataIDForItem(uint32 itemId, uint32& fileDataId)
{
    ItemTemplate const* itemTemplate = sObjectMgr->GetItemTemplate(itemId);
    if (!itemTemplate)
        return;

    uint32 itemDisplayID = itemTemplate->GetDisplayId();

    ItemDisplayInfoEntry const* displayInfo = sItemDisplayInfoStore.LookupEntry(itemDisplayID);
    if (!displayInfo)
        return;

    char* IconName = displayInfo->Icon1;
    strToLower(IconName);

    for (FileDataEntry const* fileData : sFileDataStore)
    {
        char* fileName = fileData->FileName;
        strToLower(fileName);

        if (strstr(fileName, ".blp") == nullptr)
            continue;

        if (strstr(fileName, IconName) != nullptr)
        {
            fileDataId = fileData->ID;
            return;
        }
    }
}

BattlePayShopMgr::BattlePayShopMgr()
{
    m_enabled = false;
    m_currency = BATTLE_PAY_SHOP_CURRENCY_TEST;

    _purchaseID = 1;
    _distributeID = 1;
}

BattlePayShopMgr::~BattlePayShopMgr()
{
    m_productStore.clear();
    m_groupStore.clear();
    m_shopEntryStore.clear();
}

void BattlePayShopMgr::LoadFromDB()
{
    LoadProductsFromDB();
    LoadGroupsFromDB();
    LoadEntriesFromDB();
}

void BattlePayShopMgr::LoadProductsFromDB()
{
    uint32 oldMSTime = getMSTime();

    PreparedStatement* stmt = WorldDatabase.GetPreparedStatement(WORLD_SEL_BATTLE_PAY_SHOP_PRODUCTS);
    PreparedQueryResult result = WorldDatabase.Query(stmt);

    if (!result)
    {
        TC_LOG_INFO("sql.sql", ">> Loaded 0 Battle Pay store products, table `battle_pay_shop_product` is empty!");
        return;
    }

    uint32 count = 0;
    do
    {
        Field* fields = result->Fetch();

        int32 id                = fields[0].GetInt32();
        std::string title       = fields[1].GetString();
        std::string description = fields[2].GetString();
        uint64 normalPrice      = fields[3].GetUInt64();
        uint64 currentPrice     = fields[4].GetUInt64();
        uint32 itemId           = fields[5].GetUInt32();
        uint32 quantity         = fields[6].GetUInt32();
        uint32 displayId        = fields[7].GetUInt32();
        uint8 choiceType        = fields[8].GetUInt8();
        uint32 serviceFlags     = fields[9].GetUInt32();

        if (HasProductId(id))
        {
            TC_LOG_ERROR("sql.sql", "Product id %d defined in `battle_pay_shop_product` already exists, skipped!", id);
            continue;
        }

        if (title.size() > MAX_BATTLE_PAY_SHOP_PRODUCT_TITLE_SIZE)
        {
            TC_LOG_ERROR("sql.sql", "Title for product id %d defined in `battle_pay_shop_product` is too large (max %d), skipped!",
                id, MAX_BATTLE_PAY_SHOP_PRODUCT_TITLE_SIZE);

            continue;
        }

        if (description.size() > MAX_BATTLE_PAY_SHOP_PRODUCT_DESCRIPTION_SIZE)
        {
            TC_LOG_ERROR("sql.sql", "Description for product id %d defined in `battle_pay_shop_product` is too large (max %d), skipped!",
                id, MAX_BATTLE_PAY_SHOP_PRODUCT_DESCRIPTION_SIZE);

            continue;
        }

        if (currentPrice > normalPrice)
        {
            TC_LOG_ERROR("sql.sql", "Current price of product id %d defined in `battle_pay_shop_product` is larger then the normal price. "
                "Current price has been updated to the normal price", id);

            currentPrice = normalPrice;
        }

        if (itemId && !sObjectMgr->GetItemTemplate(itemId))
        {
            TC_LOG_ERROR("sql.sql", "Item id %u for product id %d defined in `battle_pay_shop_product` doesn't exist, skipped!", itemId, id);
            continue;
        }

        uint32 fileDataId = 0;
        uint32 creatureDisplayId = 0;

        if (itemId)
        {
            SearchFileDataIDForItem(itemId, fileDataId);

            // displayId > 0, if mount or pet
            if (displayId)
                creatureDisplayId = displayId;
        }
        else
            fileDataId = displayId;

        BattlePayShopProduct product;
        product.Id = id;
        product.Title = title;
        product.Description = description;
        product.NormalPrice = normalPrice;
        product.CurrentPrice = currentPrice;
        product.ItemId = itemId;
        product.Quantity = quantity;
        product.CreatureDisplayId = creatureDisplayId;
        product.FileDataId = fileDataId;
        product.ChoiceType = choiceType;
        product.ServiceFlags = serviceFlags;

        m_productStore[id] = std::move(product);

        count++;
    } while (result->NextRow());

    TC_LOG_INFO("server.loading", ">> Loaded %u Battle Pay store products in %u ms", count, GetMSTimeDiffToNow(oldMSTime));
}

void BattlePayShopMgr::LoadGroupsFromDB()
{
    uint32 oldMSTime = getMSTime();

    PreparedStatement* stmt = WorldDatabase.GetPreparedStatement(WORLD_SEL_BATTLE_PAY_SHOP_GROUPS);
    PreparedQueryResult result = WorldDatabase.Query(stmt);

    if (!result)
    {
        TC_LOG_INFO("sql.sql", ">> Loaded 0 Battle Pay store groups, table `battle_pay_shop_group` is empty!");
        return;
    }

    uint32 count = 0;
    do
    {
        Field* fields = result->Fetch();

        int32 id            = fields[0].GetInt32();
        uint32 order        = fields[1].GetUInt32();
        std::string name    = fields[2].GetString();
        uint32 fileDataId   = fields[3].GetUInt32();
        uint8 type          = fields[4].GetUInt8();

        if (HasGroupId(id))
        {
            TC_LOG_ERROR("sql.sql", "Group id %d defined in `battle_pay_shop_group` already exists, skipped!", id);
            continue;
        }

        if (name.size() > MAX_BATTLE_PAY_SHOP_GROUP_NAME_SIZE)
        {
            TC_LOG_ERROR("sql.sql", "Name for group id %d defined in `battle_pay_shop_group` is too large (max %d), skipped!",
                id, MAX_BATTLE_PAY_SHOP_GROUP_NAME_SIZE);

            continue;
        }

        if (type >= BATTLE_PAY_SHOP_GROUP_TYPE_END)
        {
            TC_LOG_ERROR("sql.sql", "Group id %d defined in `battle_pay_shop_group` has invalid group type %u, skipped!", id, type);
            continue;
        }

        BattlePayShopGroup group;
        group.Id = id;
        group.Order = order;
        group.Name = name;
        group.FileDataId = fileDataId;
        group.Type = type;

        m_groupStore[id] = std::move(group);

        count++;
    } while (result->NextRow());

    TC_LOG_INFO("server.loading", ">> Loaded %u Battle Pay store groups in %u ms", count, GetMSTimeDiffToNow(oldMSTime));
}

void BattlePayShopMgr::LoadEntriesFromDB()
{
    uint32 oldMSTime = getMSTime();

    PreparedStatement* stmt = WorldDatabase.GetPreparedStatement(WORLD_SEL_BATTLE_PAY_SHOP_ENTRIES);
    PreparedQueryResult result = WorldDatabase.Query(stmt);

    if (!result)
    {
        TC_LOG_INFO("sql.sql", ">> Loaded 0 Battle Pay store entries, table `battle_pay_shop_entry` is empty!");
        return;
    }

    uint32 count = 0;
    do
    {
        Field* fields = result->Fetch();

        int32 id                    = fields[0].GetInt32();
        uint32 order                = fields[1].GetUInt32();
        int32 groupId               = fields[2].GetInt32();
        int32 productId             = fields[3].GetInt32();
        uint32 flags                = fields[4].GetUInt32();
        uint8 banner                = fields[5].GetUInt8();

        if (!HasGroupId(groupId))
        {
            TC_LOG_ERROR("sql.sql", "Group id %d for entry id %d defined in `battle_pay_shop_entry` is invalid because the group doesn't exists, skipped!", groupId, id);
            continue;
        }

        if (!HasProductId(productId))
        {
            TC_LOG_ERROR("sql.sql", "Product id %d for entry id %d defined in `battle_pay_shop_entry` is invalid because the group doesn't exists, skipped!", productId, id);
            continue;
        }

        if (banner >= BATTLE_PAY_SHOP_BANNER_TYPE_END)
        {
            TC_LOG_ERROR("sql.sql", "Entry id %d defined in `battle_pay_shop_entry` has invalid banner type %u, skipped!", id, banner);
            continue;
        }

        BattlePayShopEntry entry;
        entry.Id = id;
        entry.Order = order;
        entry.GroupId = groupId;
        entry.ProductId = productId;
        entry.Flags = flags;
        entry.Banner = banner;

        m_shopEntryStore[id] = std::move(entry);

        count++;
    } while (result->NextRow());

    TC_LOG_INFO("server.loading", ">> Loaded %u Battle Pay store entries in %u ms", count, GetMSTimeDiffToNow(oldMSTime));
}

bool BattlePayShopMgr::HasProductId(int32 id)
{
    return m_productStore.count(id) > 0;
}

bool BattlePayShopMgr::HasGroupId(int32 id)
{
    return m_groupStore.count(id) > 0;
}

BattlePayShopProduct const* BattlePayShopMgr::GetProduct(uint32 productId) const
{
    auto itr = m_productStore.find(int32(productId));
    if (itr != m_productStore.end())
        return &itr->second;

    return nullptr;
}

void BattlePayShopMgr::GetProductsStore(BattlePayShopProductMap& products, bool IsOnCharSelectScreen)
{
    if (!IsOnCharSelectScreen)
    {
        BattlePayShopProductMap::iterator first = m_productStore.begin();
        std::advance(first, 1); // get second element, as first is BATTLE_PAY_SHOP_SHOW_ON_CHAR_SELECT_SCREEN_INDEX
        products.insert(first, m_productStore.end());
    }
    else
        products.insert(BattlePayShopProductMap::value_type(0, m_productStore[BATTLE_PAY_SHOP_SHOW_ON_CHAR_SELECT_SCREEN_INDEX]));
}

void BattlePayShopMgr::GetGroupsStore(BattlePayShopGroupMap& groups, bool IsOnCharSelectScreen)
{
    if (!IsOnCharSelectScreen)
    {
        BattlePayShopGroupMap::iterator first = m_groupStore.begin();
        std::advance(first, 1); // get second element, as first is BATTLE_PAY_SHOP_SHOW_ON_CHAR_SELECT_SCREEN_INDEX
        groups.insert(first, m_groupStore.end());
    }
    else
        groups.insert(BattlePayShopGroupMap::value_type(0, m_groupStore[BATTLE_PAY_SHOP_SHOW_ON_CHAR_SELECT_SCREEN_INDEX]));
}

void BattlePayShopMgr::GetEntriesStore(BattlePayShopEntryMap& entries, bool IsOnCharSelectScreen)
{
    if (!IsOnCharSelectScreen)
    {
        BattlePayShopEntryMap::iterator first = m_shopEntryStore.begin();
        std::advance(first, 1); // get second element, as first is BATTLE_PAY_SHOP_SHOW_ON_CHAR_SELECT_SCREEN_INDEX
        entries.insert(first, m_shopEntryStore.end());
    }
    else
        entries.insert(BattlePayShopEntryMap::value_type(0, m_shopEntryStore[BATTLE_PAY_SHOP_SHOW_ON_CHAR_SELECT_SCREEN_INDEX]));
}

bool BattlePayShopMgr::IsProductUnavailableToPlayer(WorldSession* session, BattlePayShopProduct const& product) const
{
    BattlePayShop const* battlePayShop = session->GetBattlePayShop();
    if (!battlePayShop)
        return false;

    if (battlePayShop->GetCurrencyQuantity() < product.CurrentPrice)
        return false;

    // Add here more conditions, if needed (e.g. checking guilds, battle pets, item requirements, etc.)

    return true;
}

uint64 BattlePayShopMgr::GeneratePurchaseID()
{
    if (_purchaseID >= uint64(0xFFFFFFFFFFFFFFFELL))
    {
        TC_LOG_ERROR("misc", "Battle Pay Shop Purchase id overflow!! Can't continue, shutting down server. Search on forum for TCE00007 for more info.");
        World::StopNow(ERROR_EXIT_CODE);
        return 0;
    }

    return _purchaseID++;
}

uint64 BattlePayShopMgr::GenerateDistributeID()
{
    if (_distributeID >= uint64(0xFFFFFFFFFFFFFFFELL))
    {
        TC_LOG_ERROR("misc", "Battle Pay Shop Distribute id overflow!! Can't continue, shutting down server. Search on forum for TCE00007 for more info.");
        World::StopNow(ERROR_EXIT_CODE);
        return 0;
    }

    return _distributeID++;
}
