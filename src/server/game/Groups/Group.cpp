/*
 * Copyright (C) 2011-2015 Project SkyFire <http://www.projectskyfire.org/>
 * Copyright (C) 2008-2015 TrinityCore <http://www.trinitycore.org/>
 * Copyright (C) 2005-2015 MaNGOS <http://getmangos.com/>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 3 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "CharacterCache.h"
#include "Common.h"
#include "Opcodes.h"
#include "WorldPacket.h"
#include "WorldSession.h"
#include "Player.h"
#include "World.h"
#include "ObjectMgr.h"
#include "GroupMgr.h"
#include "Group.h"
#include "Formulas.h"
#include "ObjectAccessor.h"
#include "Battleground.h"
#include "BattlegroundMgr.h"
#include "RatedMgr.h"
#include "RatedInfo.h"
#include "MapManager.h"
#include "InstanceSaveMgr.h"
#include "MapInstanced.h"
#include "Util.h"
#include "LFGMgr.h"
#include "UpdateFieldFlags.h"
#include "SpellAuras.h"

Roll::Roll(LootItem const& li, uint32 displayId) : ItemId(li.ItemId), ItemDisplayId(displayId),
ItemRandomPropId(li.RandomPropertyId), ItemRandomSuffix(li.RandomSuffix), ItemCount(li.Count), ItemUpgradeId(li.UpgradeId),
TotalPlayersRolling(0), TotalNeed(0), TotalGreed(0), TotalPass(0), ItemIndex(0),
RollVoteMask(ROLL_ALL_TYPE_NO_DISENCHANT) { }

Roll::~Roll() { }

void Roll::SetLoot(Loot* pLoot)
{
    link(pLoot, this);
}

Loot* Roll::GetLoot()
{
    return getTarget();
}

Group::Group() : m_leaderGuid(), m_leaderName(""), m_groupType(GROUPTYPE_NORMAL),
m_dungeonDifficulty(DIFFICULTY_NORMAL), m_raidDifficulty(DIFFICULTY_10_N), m_scenarioDifficulty(DIFFICULTY_N_SCENARIO),
m_bgGroup(NULL), m_bfGroup(NULL), m_lootMethod(FREE_FOR_ALL), m_lootThreshold(ITEM_QUALITY_UNCOMMON), m_looterGuid(),
m_masterLooterGuid(), m_subGroupsCounts(NULL), m_guid(), m_counter(0), m_maxEnchantingLevel(0), m_dbStoreId(0), _readyCheckInProgress(false)
{
    for (uint8 i = 0; i < TARGETICONCOUNT; ++i)
        m_targetIcons[i].Clear();
}

Group::~Group()
{
    if (m_bgGroup)
    {
        TC_LOG_DEBUG("bg.battleground", "Group::~Group: battleground group being deleted.");
        if (m_bgGroup->GetBgRaid(ALLIANCE) == this) m_bgGroup->SetBgRaid(ALLIANCE, NULL);
        else if (m_bgGroup->GetBgRaid(HORDE) == this) m_bgGroup->SetBgRaid(HORDE, NULL);
        else TC_LOG_ERROR("misc", "Group::~Group: battleground group is not linked to the correct battleground.");
    }
    Rolls::iterator itr;
    while (!RollId.empty())
    {
        itr = RollId.begin();
        Roll *r = *itr;
        RollId.erase(itr);
        delete (r);
    }

    // this may unload some instance saves
    for (uint8 i = 0; i < MAX_DIFFICULTY; ++i)
        for (BoundInstancesMap::iterator itr2 = m_boundInstances[i].begin(); itr2 != m_boundInstances[i].end(); ++itr2)
            itr2->second.save->RemoveGroup(this);

    RaidMarkers::iterator it, next;
    for (it = m_RaidMarkers.begin(); it != m_RaidMarkers.end(); it = next)
    {
        next = it;
        ++next;

        if (DynamicObject* dynObj = it->second)
            dynObj->Remove();

        m_RaidMarkers.erase(it);
    }

    // Sub group counters clean up
    delete [] m_subGroupsCounts;
}

bool Group::Create(Player* leader)
{
    ObjectGuid leaderGuid = leader->GetGUID();
    uint32 lowguid = sGroupMgr->GenerateGroupId();

    m_guid = ObjectGuid::Create<HighGuid::Party>(lowguid);
    m_leaderGuid = leaderGuid;
    m_leaderName = leader->GetName();
    leader->SetFlag(PLAYER_FLAGS, PLAYER_FLAGS_GROUP_LEADER);

    if (IsBGGroup() || IsBFGroup())
        m_groupType = GROUPTYPE_BGRAID;

    if (m_groupType & GROUPTYPE_RAID)
        _InitRaidSubGroupsCounter();

    if (!IsLFGGroup())
        m_lootMethod = GROUP_LOOT;

    m_lootThreshold = ITEM_QUALITY_UNCOMMON;
    m_looterGuid = leaderGuid;
    m_masterLooterGuid.Clear();

    m_dungeonDifficulty = DIFFICULTY_NORMAL;
    m_raidDifficulty = DIFFICULTY_10_N;
    m_scenarioDifficulty = DIFFICULTY_N_SCENARIO;

    if (!IsBGGroup() && !IsBFGroup())
    {
        m_dungeonDifficulty = leader->GetDungeonDifficultyID();
        m_raidDifficulty = leader->GetRaidDifficultyID();
        m_scenarioDifficulty = leader->GetScenarioDifficultyID();

        m_dbStoreId = sGroupMgr->GenerateNewGroupDbStoreId();

        sGroupMgr->RegisterGroupDbStoreId(m_dbStoreId, this);

        // Store group in database
        PreparedStatement* stmt = CharacterDatabase.GetPreparedStatement(CHAR_INS_GROUP);

        uint8 index = 0;

        stmt->setUInt32(index++, m_dbStoreId);
        stmt->setUInt32(index++, m_leaderGuid.GetCounter());
        stmt->setUInt8(index++, uint8(m_lootMethod));
        stmt->setUInt32(index++, m_looterGuid.GetCounter());
        stmt->setUInt8(index++, uint8(m_lootThreshold));
        stmt->setUInt64(index++, m_targetIcons[0].GetRawValue());
        stmt->setUInt64(index++, m_targetIcons[1].GetRawValue());
        stmt->setUInt64(index++, m_targetIcons[2].GetRawValue());
        stmt->setUInt64(index++, m_targetIcons[3].GetRawValue());
        stmt->setUInt64(index++, m_targetIcons[4].GetRawValue());
        stmt->setUInt64(index++, m_targetIcons[5].GetRawValue());
        stmt->setUInt64(index++, m_targetIcons[6].GetRawValue());
        stmt->setUInt64(index++, m_targetIcons[7].GetRawValue());
        stmt->setUInt8(index++, uint8(m_groupType));
        stmt->setUInt32(index++, uint8(m_dungeonDifficulty));
        stmt->setUInt32(index++, uint8(m_raidDifficulty));
        stmt->setUInt32(index++, uint8(m_scenarioDifficulty));
        stmt->setUInt32(index++, m_masterLooterGuid.GetCounter());

        CharacterDatabase.Execute(stmt);

        Group::ConvertLeaderInstancesToGroup(leader, this, false);

        ASSERT(AddMember(leader)); // If the leader can't be added to a new group because it appears full, something is clearly wrong.
    }
    else if (!AddMember(leader))
        return false;

    return true;
}

bool Group::LoadGroupFromDB(Field* fields)
{
    m_dbStoreId = fields[17].GetUInt32();
    m_guid = ObjectGuid::Create<HighGuid::Party>(sGroupMgr->GenerateGroupId());
    m_leaderGuid = ObjectGuid::Create<HighGuid::Player>(fields[0].GetUInt32());

    // group leader not exist
    if (!sCharacterCache->GetCharacterNameByGuid(m_leaderGuid, m_leaderName))
        return false;

    m_lootMethod = LootMethod(fields[1].GetUInt8());
    m_looterGuid = ObjectGuid::Create<HighGuid::Player>(fields[2].GetUInt32());
    m_lootThreshold = ItemQualities(fields[3].GetUInt8());

    for (uint8 i = 0; i < TARGETICONCOUNT; ++i)
        m_targetIcons[i].Set(fields[4 + i].GetUInt64());

    m_groupType  = GroupType(fields[12].GetUInt8());
    if (m_groupType & GROUPTYPE_RAID)
        _InitRaidSubGroupsCounter();

    m_dungeonDifficulty = Player::CheckLoadedDungeonDifficultyID(Difficulty(fields[13].GetUInt8()));
    m_raidDifficulty = Player::CheckLoadedRaidDifficultyID(Difficulty(fields[14].GetUInt8()));
    m_scenarioDifficulty = Difficulty(fields[15].GetUInt8());

    m_masterLooterGuid = ObjectGuid::Create<HighGuid::Player>(fields[16].GetUInt32());

    if (m_groupType & GROUPTYPE_LFG)
        sLFGMgr->_LoadFromDB(fields, GetGUID());

    return true;
}

bool Group::LoadMemberFromDB(uint32 guidLow, uint8 memberFlags, uint8 subgroup, uint8 roles)
{
    MemberSlot member;
    member.Guid = ObjectGuid::Create<HighGuid::Player>(guidLow);

    // skip non-existed member
    if (!sCharacterCache->GetCharacterNameByGuid(member.Guid, member.name))
    {
        PreparedStatement* stmt = CharacterDatabase.GetPreparedStatement(CHAR_DEL_GROUP_MEMBER);
        stmt->setUInt32(0, guidLow);
        CharacterDatabase.Execute(stmt);
        return false;
    }

    member.group = subgroup;
    member.flags = memberFlags;
    member.roles = roles;

    m_memberSlots.push_back(member);

    SubGroupCounterIncrease(subgroup);

    sLFGMgr->SetupGroupMember(member.Guid, GetGUID());

    return true;
}

void Group::ConvertToLFG()
{
    m_groupType = GroupType(m_groupType | GROUPTYPE_LFG | GROUPTYPE_LFG_RESTRICTED);
    m_lootMethod = NEED_BEFORE_GREED;
    if (!IsBGGroup() && !IsBFGroup())
    {
        PreparedStatement* stmt = CharacterDatabase.GetPreparedStatement(CHAR_UPD_GROUP_TYPE);

        stmt->setUInt8(0, uint8(m_groupType));
        stmt->setUInt32(1, m_dbStoreId);

        CharacterDatabase.Execute(stmt);
    }

    SendUpdate();
}

void Group::ConvertToRaid()
{
    m_groupType = GroupType(m_groupType | GROUPTYPE_RAID);

    _InitRaidSubGroupsCounter();

    if (!IsBGGroup() && !IsBFGroup())
    {
        PreparedStatement* stmt = CharacterDatabase.GetPreparedStatement(CHAR_UPD_GROUP_TYPE);

        stmt->setUInt8(0, uint8(m_groupType));
        stmt->setUInt32(1, m_dbStoreId);

        CharacterDatabase.Execute(stmt);
    }

    SendUpdate();

    // update quest related GO states (quest activity dependent from raid membership)
    for (member_citerator citr = m_memberSlots.begin(); citr != m_memberSlots.end(); ++citr)
        if (Player* player = ObjectAccessor::FindPlayer(citr->Guid))
            player->UpdateForQuestWorldObjects();
}

void Group::ConvertToGroup()
{
    if (m_memberSlots.size() > 5)
        return; // What message error should we send?

    m_groupType = GroupType(GROUPTYPE_NORMAL);

    if (m_subGroupsCounts)
    {
        delete [] m_subGroupsCounts;
        m_subGroupsCounts = NULL;
    }

    if (!IsBGGroup() && !IsBFGroup())
    {
        PreparedStatement* stmt = CharacterDatabase.GetPreparedStatement(CHAR_UPD_GROUP_TYPE);

        stmt->setUInt8(0, uint8(m_groupType));
        stmt->setUInt32(1, m_dbStoreId);

        CharacterDatabase.Execute(stmt);
    }

    SendUpdate();

    // update quest related GO states (quest activity dependent from raid membership)
    for (member_citerator citr = m_memberSlots.begin(); citr != m_memberSlots.end(); ++citr)
        if (Player* player = ObjectAccessor::FindPlayer(citr->Guid))
            player->UpdateForQuestWorldObjects();
}

void Group::ChangeFlagEveryoneAssistant(bool apply)
{
    if (apply)
        m_groupType = GroupType(m_groupType | GROUPTYPE_EVERYONE_ASSISTANT);
    else
        m_groupType = GroupType(m_groupType &~ GROUPTYPE_EVERYONE_ASSISTANT);

    SendUpdate();
}

bool Group::AddInvite(Player* player)
{
    if (!player || player->GetGroupInvite())
        return false;
    Group* group = player->GetGroup();
    if (group && (group->IsBGGroup() || group->IsBFGroup()))
        group = player->GetOriginalGroup();
    if (group)
        return false;

    RemoveInvite(player);

    m_invitees.insert(player);

    player->SetGroupInvite(this);

    sScriptMgr->OnGroupInviteMember(this, player->GetGUID());

    return true;
}

bool Group::AddLeaderInvite(Player* player)
{
    if (!AddInvite(player))
        return false;

    m_leaderGuid = player->GetGUID();
    m_leaderName = player->GetName();
    return true;
}

void Group::RemoveInvite(Player* player)
{
    if (player)
    {
        m_invitees.erase(player);
        player->SetGroupInvite(NULL);
    }
}

void Group::RemoveAllInvites()
{
    for (InvitesList::iterator itr=m_invitees.begin(); itr != m_invitees.end(); ++itr)
        if (*itr)
            (*itr)->SetGroupInvite(NULL);

    m_invitees.clear();
}

Player* Group::GetInvited(ObjectGuid guid) const
{
    for (InvitesList::const_iterator itr = m_invitees.begin(); itr != m_invitees.end(); ++itr)
    {
        if ((*itr) && (*itr)->GetGUID() == guid)
            return (*itr);
    }
    return NULL;
}

Player* Group::GetInvited(const std::string& name) const
{
    for (InvitesList::const_iterator itr = m_invitees.begin(); itr != m_invitees.end(); ++itr)
    {
        if ((*itr) && (*itr)->GetName() == name)
            return (*itr);
    }
    return NULL;
}

bool Group::AddMember(Player* player)
{
    // Get first not-full group
    uint8 subGroup = 0;
    if (m_subGroupsCounts)
    {
        bool groupFound = false;
        for (; subGroup < MAX_RAID_SUBGROUPS; ++subGroup)
        {
            if (m_subGroupsCounts[subGroup] < MAXGROUPSIZE)
            {
                groupFound = true;
                break;
            }
        }
        // We are raid group and no one slot is free
        if (!groupFound)
            return false;
    }

    MemberSlot member;
    member.Guid      = player->GetGUID();
    member.name      = player->GetName();
    member.group     = subGroup;
    member.flags     = 0;
    member.roles     = 0;
    m_memberSlots.push_back(member);

    SubGroupCounterIncrease(subGroup);

    player->SetGroupInvite(NULL);
    if (player->GetGroup())
    {
        if (IsBGGroup() || IsBFGroup()) // if player is in group and he is being added to BG raid group, then call SetBattlegroundRaid()
            player->SetBattlegroundOrBattlefieldRaid(this, subGroup);
        else //if player is in bg raid and we are adding him to normal group, then call SetOriginalGroup()
            player->SetOriginalGroup(this, subGroup);
    }
    else //if player is not in group, then call set group
        player->SetGroup(this, subGroup);

    // if the same group invites the player back, cancel the homebind timer
    player->m_InstanceValid = player->CheckInstanceValidity(false);

    // insert into the table if we're not a battleground group
    if (!IsBGGroup() && !IsBFGroup())
    {
        PreparedStatement* stmt = CharacterDatabase.GetPreparedStatement(CHAR_INS_GROUP_MEMBER);

        stmt->setUInt32(0, m_dbStoreId);
        stmt->setUInt32(1, member.Guid.GetCounter());
        stmt->setUInt8(2, member.flags);
        stmt->setUInt8(3, member.group);
        stmt->setUInt8(4, member.roles);

        CharacterDatabase.Execute(stmt);

    }

    SendUpdate();
    sScriptMgr->OnGroupAddMember(this, player->GetGUID());

    if (!IsLeader(player->GetGUID()) && !IsBGGroup() && !IsBFGroup())
    {
        // reset the new member's instances, unless he is currently in one of them
        // including raid/heroic instances that they are not permanently bound to!
        player->ResetInstances(INSTANCE_RESET_GROUP_JOIN, RESET_TYPE_ALL);

        if (player->GetLevel() >= LEVELREQUIREMENT_HEROIC)
        {
            if (player->GetDungeonDifficultyID() != GetDungeonDifficultyID())
            {
                player->SetDungeonDifficultyID(GetDungeonDifficultyID());
                player->SendDungeonDifficulty();
            }

            if (player->GetRaidDifficultyID() != GetRaidDifficultyID())
            {
                player->SetRaidDifficultyID(GetRaidDifficultyID());
                player->SendRaidDifficulty();
            }

            if (player->GetScenarioDifficultyID() != GetScenarioDifficultyID())
                player->SetScenarioDifficultyID(GetScenarioDifficultyID());
        }
    }

    // quest related GO state dependent from raid membership
    if (IsRaidGroup())
        player->UpdateForQuestWorldObjects();

    {
        // Broadcast new player group member fields to rest of the group
        player->SetFieldNotifyFlag(UF_FLAG_PARTY_MEMBER);

        UpdateData groupData(player->GetMapId());
        WorldPacket groupDataPacket;

        // Broadcast group members' fields to player
        for (GroupReference* itr = GetFirstMember(); itr != NULL; itr = itr->next())
        {
            if (itr->GetSource() == player)
                continue;

            if (Player* member = itr->GetSource())
            {
                if (player->HaveAtClient(member))
                {
                    member->SetFieldNotifyFlag(UF_FLAG_PARTY_MEMBER);
                    member->BuildValuesUpdateBlockForPlayer(&groupData, player);
                    member->RemoveFieldNotifyFlag(UF_FLAG_PARTY_MEMBER);
                }

                if (member->HaveAtClient(player))
                {
                    UpdateData newData(player->GetMapId());
                    WorldPacket newDataPacket;
                    player->BuildValuesUpdateBlockForPlayer(&newData, member);
                    if (newData.HasData())
                    {
                        newData.BuildPacket(&newDataPacket);
                        member->SendDirectMessage(&newDataPacket);
                    }
                }
            }
        }

        if (groupData.HasData())
        {
            groupData.BuildPacket(&groupDataPacket);
            player->SendDirectMessage(&groupDataPacket);
        }

        player->RemoveFieldNotifyFlag(UF_FLAG_PARTY_MEMBER);
    }

    if (m_maxEnchantingLevel < player->GetSkillValue(SKILL_ENCHANTING))
        m_maxEnchantingLevel = player->GetSkillValue(SKILL_ENCHANTING);

    return true;
}

bool Group::RemoveMember(ObjectGuid guid, const RemoveMethod& method /*= GROUP_REMOVEMETHOD_DEFAULT*/, ObjectGuid kicker /*= 0*/, const char* reason /*= NULL*/)
{
    BroadcastGroupUpdate();

    sScriptMgr->OnGroupRemoveMember(this, guid, method, kicker, reason);

    Player* player = ObjectAccessor::FindConnectedPlayer(guid);
    if (player)
    {
        for (GroupReference* itr = GetFirstMember(); itr != nullptr; itr = itr->next())
        {
            if (Player* groupMember = itr->GetSource())
            {
                if (groupMember->GetGUID() == guid)
                    continue;

                groupMember->RemoveAllGroupBuffsFromCaster(guid);
                player->RemoveAllGroupBuffsFromCaster(groupMember->GetGUID());
            }
        }
    }

    // LFG group vote kick handled in scripts
    if (IsLFGGroup() && method == GROUP_REMOVEMETHOD_KICK)
        return m_memberSlots.size();

    // remove member and change leader (if need) only if strong more 2 members _before_ member remove (BG/BF allow 1 member group)
    if (GetMembersCount() > ((IsBGGroup() || IsLFGGroup() || IsBFGroup()) ? 1u : 2u))
    {
        if (player)
        {
            // Battleground group handling
            if (IsBGGroup() || IsBFGroup())
                player->RemoveFromBattlegroundOrBattlefieldRaid();
            else
            // Regular group
            {
                if (player->GetOriginalGroup() == this)
                    player->SetOriginalGroup(NULL);
                else
                    player->SetGroup(NULL);

                // quest related GO state dependent from raid membership
                player->UpdateForQuestWorldObjects();
            }

            if (method == GROUP_REMOVEMETHOD_KICK || method == GROUP_REMOVEMETHOD_KICK_LFG)
            {
                WorldPacket data(SMSG_GROUP_UNINVITE, 0);
                player->SendDirectMessage(&data);
            }

            SendUpdateToPlayer(guid, NULL);

            _HomebindIfInstance(player);

            // reset scenario difficulty
            player->SetScenarioDifficultyID(DIFFICULTY_N_SCENARIO);
        }

        // Remove player from group in DB
        if (!IsBGGroup() && !IsBFGroup())
        {
            PreparedStatement* stmt = CharacterDatabase.GetPreparedStatement(CHAR_DEL_GROUP_MEMBER);
            stmt->setUInt32(0, guid.GetCounter());
            CharacterDatabase.Execute(stmt);
            DelinkMember(guid);
        }

        // Reevaluate group enchanter if the leaving player had enchanting skill or the player is offline
        if (!player || player->GetSkillValue(SKILL_ENCHANTING))
            ResetMaxEnchantingLevel();

        // Remove player from loot rolls
        for (Rolls::iterator it = RollId.begin(); it != RollId.end(); ++it)
        {
            Roll* roll = *it;
            Roll::PlayerVote::iterator itr2 = roll->playerVote.find(guid);
            if (itr2 == roll->playerVote.end())
                continue;

            if (!roll->GetLoot())
                continue;

            if (itr2->second == GREED || itr2->second == DISENCHANT)
                --roll->TotalGreed;
            else if (itr2->second == NEED)
                --roll->TotalNeed;
            else if (itr2->second == PASS)
                --roll->TotalPass;

            if (itr2->second != NOT_VALID)
                --roll->TotalPlayersRolling;

            roll->playerVote.erase(itr2);

            CountRollVote(guid, roll->GetLoot()->GetGUID(), roll->ItemIndex, MAX_ROLL_TYPE);
        }

        // Update subgroups
        member_witerator slot = _GetMemberWSlot(guid);
        if (slot != m_memberSlots.end())
        {
            SubGroupCounterDecrease(slot->group);
            m_memberSlots.erase(slot);
        }

        // Pick new leader if necessary
        if (m_leaderGuid == guid)
        {
            for (member_witerator itr = m_memberSlots.begin(); itr != m_memberSlots.end(); ++itr)
            {
                if (ObjectAccessor::FindConnectedPlayer(itr->Guid))
                {
                    ChangeLeader(itr->Guid);
                    break;
                }
            }
        }

        SendUpdate();

        if (IsLFGGroup() && GetMembersCount() == 1)
        {
            Player* leader = ObjectAccessor::FindConnectedPlayer(GetLeaderGUID());
            uint32 mapId = sLFGMgr->GetDungeonMapId(GetGUID());
            if (!mapId || !leader || (leader->IsAlive() && leader->GetMapId() != mapId))
            {
                Disband();
                return false;
            }
        }

        if (m_memberMgr.getSize() < ((IsLFGGroup() || IsBGGroup()) ? 1u : 2u))
            Disband();

        return true;
    }
    // If group size before player removal <= 2 then disband it
    else
    {
        Disband();
        return false;
    }
}

void Group::ChangeLeader(ObjectGuid newLeaderGuid, uint8 partyIndex)
{
    member_witerator slot = _GetMemberWSlot(newLeaderGuid);

    if (slot == m_memberSlots.end())
        return;

    Player* newLeader = ObjectAccessor::FindConnectedPlayer(slot->Guid);

    // Don't allow switching leader to offline players
    if (!newLeader)
        return;

    sScriptMgr->OnGroupChangeLeader(this, newLeaderGuid, m_leaderGuid);

    if (!IsBGGroup() && !IsBFGroup())
    {
        SQLTransaction trans = CharacterDatabase.BeginTransaction();

        // Remove the groups permanent instance bindings
        for (uint8 i = 0; i < MAX_DIFFICULTY; ++i)
        {
            for (BoundInstancesMap::iterator itr = m_boundInstances[i].begin(); itr != m_boundInstances[i].end();)
            {
                // Do not unbind saves of instances that already had map created (a newLeader entered)
                // forcing a new instance with another leader requires group disbanding (confirmed on retail)
                if (itr->second.perm && !sMapMgr->FindMap(itr->first, itr->second.save->GetInstanceId()))
                {
                    PreparedStatement* stmt = CharacterDatabase.GetPreparedStatement(CHAR_DEL_GROUP_INSTANCE_PERM_BINDING);
                    stmt->setUInt32(0, m_dbStoreId);
                    stmt->setUInt32(1, itr->second.save->GetInstanceId());
                    trans->Append(stmt);

                    itr->second.save->RemoveGroup(this);
                    m_boundInstances[i].erase(itr++);
                }
                else
                    ++itr;
            }
        }

        // Copy the permanent binds from the new leader to the group
        Group::ConvertLeaderInstancesToGroup(newLeader, this, true);

        // Update the group leader
        PreparedStatement* stmt = CharacterDatabase.GetPreparedStatement(CHAR_UPD_GROUP_LEADER);

        stmt->setUInt32(0, newLeader->GetGUID().GetCounter());
        stmt->setUInt32(1, m_dbStoreId);

        trans->Append(stmt);

        CharacterDatabase.CommitTransaction(trans);
    }

    if (Player* oldLeader = ObjectAccessor::FindConnectedPlayer(m_leaderGuid))
        oldLeader->RemoveFlag(PLAYER_FLAGS, PLAYER_FLAGS_GROUP_LEADER);

    newLeader->SetFlag(PLAYER_FLAGS, PLAYER_FLAGS_GROUP_LEADER);
    m_leaderGuid = newLeader->GetGUID();
    m_leaderName = newLeader->GetName();
    ToggleGroupMemberFlag(slot, MEMBER_FLAG_ASSISTANT, false);

    uint8 leaderNameLen = m_leaderName.size();

    WorldPacket data(SMSG_GROUP_SET_LEADER, 1 + 1 + leaderNameLen);

    data << uint8(partyIndex);

    data.WriteBits(leaderNameLen, 6);

    data.FlushBits();

    data.WriteString(m_leaderName);

    BroadcastPacket(&data, true);
}

/// convert the player's binds to the group
void Group::ConvertLeaderInstancesToGroup(Player* player, Group* group, bool switchLeader)
{
    // copy all binds to the group, when changing leader it's assumed the character
    // will not have any solo binds
    for (uint8 i = 0; i < MAX_DIFFICULTY; ++i)
    {
        for (Player::BoundInstancesMap::iterator itr = player->m_boundInstances[i].begin(); itr != player->m_boundInstances[i].end();)
        {
            if (!switchLeader || !group->GetBoundInstance(itr->second.save->GetDifficultyID(), itr->first))
                if (itr->second.extendState) // not expired
                    group->BindToInstance(itr->second.save, itr->second.perm, false);

            // permanent binds are not removed
            if (switchLeader && !itr->second.perm)
            {
                // increments itr in call
                player->UnbindInstance(itr, Difficulty(i), false);
            }
            else
                ++itr;
        }
    }

    /* if group leader is in a non-raid dungeon map and nobody is actually bound to this map then the group can "take over" the instance *
    * (example: two-player group disbanded by disconnect where the player reconnects within 60 seconds and the group is reformed)       */
    if (Map* playerMap = player->GetMap())
        if (!switchLeader && playerMap->IsNonRaidDungeon())
            if (InstanceSave* save = sInstanceSaveMgr->GetInstanceSave(playerMap->GetInstanceId()))
                if (save->GetGroupCount() == 0 && save->GetPlayerCount() == 0)
                {
                    TC_LOG_DEBUG("maps", "Group::ConvertLeaderInstancesToGroup: Group for player %s is taking over unbound instance map %d with Id %d", player->GetName().c_str(), playerMap->GetId(), playerMap->GetInstanceId());
                    // if nobody is saved to this, then the save wasn't permanent
                    group->BindToInstance(save, false, false);
                }
}

void Group::Disband(bool hideDestroy /* = false */)
{
    sScriptMgr->OnGroupDisband(this);

    Player* player;
    for (member_citerator citr = m_memberSlots.begin(); citr != m_memberSlots.end(); ++citr)
    {
        player = ObjectAccessor::FindConnectedPlayer(citr->Guid);
        if (!player)
            continue;

        //we cannot call _removeMember because it would invalidate member iterator
        //if we are removing player from battleground raid
        if (IsBGGroup() || IsBFGroup())
            player->RemoveFromBattlegroundOrBattlefieldRaid();
        else
        {
            //we can remove player who is in battleground from his original group
            if (player->GetOriginalGroup() == this)
                player->SetOriginalGroup(NULL);
            else
                player->SetGroup(NULL);
        }

        // quest related GO state dependent from raid membership
        if (IsRaidGroup())
            player->UpdateForQuestWorldObjects();

        if (!player->GetSession())
            continue;

        if (!hideDestroy)
        {
            WorldPacket data(SMSG_GROUP_DESTROYED, 0);
            player->SendDirectMessage(&data);
        }

        //we already removed player from group and in player->GetGroup() is his original group, send update
        if (Group* group = player->GetGroup())
            group->SendUpdate();
        else
            SendUpdateToPlayer(player->GetGUID(), NULL);

        _HomebindIfInstance(player);
    }
    RollId.clear();
    m_memberSlots.clear();

    RemoveAllInvites();

    if (!IsBGGroup() && !IsBFGroup())
    {
        SQLTransaction trans = CharacterDatabase.BeginTransaction();

        PreparedStatement* stmt = CharacterDatabase.GetPreparedStatement(CHAR_DEL_GROUP);
        stmt->setUInt32(0, m_dbStoreId);
        trans->Append(stmt);

        stmt = CharacterDatabase.GetPreparedStatement(CHAR_DEL_GROUP_MEMBER_ALL);
        stmt->setUInt32(0, m_dbStoreId);
        trans->Append(stmt);

        CharacterDatabase.CommitTransaction(trans);

        ResetInstances(INSTANCE_RESET_GROUP_DISBAND, RESET_TYPE_ALL, NULL);

        stmt = CharacterDatabase.GetPreparedStatement(CHAR_DEL_LFG_DATA);
        stmt->setUInt32(0, m_dbStoreId);
        CharacterDatabase.Execute(stmt);

        sGroupMgr->FreeGroupDbStoreId(this);
    }

    sGroupMgr->RemoveGroup(this);
    delete this;
}

/*********************************************************/
/***                   LOOT SYSTEM                     ***/
/*********************************************************/

void Group::SendLootStartRoll(uint32 countDown, uint32 mapid, Roll& r)
{
    if (!r.GetLoot())
        return;

    uint32 SlotType = LOOT_SLOT_TYPE_ROLL_ONGOING;
    uint8 Index = r.ItemIndex;

    uint8 LootItemType = 0;
    uint8 ItemType = 3;

    bool CanTradeToTapList = false;
    bool ShowLootItemType = LootItemType != 0;
    bool ShowIndex = Index >= 0;

    ObjectGuid guid = r.GetLoot()->GetGUID();

    WorldPacket data(SMSG_LOOT_START_ROLL, 1 + 8 + 1 + 4 + 4 + 4 + (ShowIndex ? 1 : 0) + 4 + (r.ItemUpgradeId ? (4 + 4) : 0) + 4 + 1 + 4 + 1 + 4 + (ShowLootItemType ? 1 : 0) + 4);

    data.WriteBits(SlotType, 3);

    data.WriteBit(!ShowIndex);

    data.WriteBit(CanTradeToTapList);

    data.WriteGuidMask(guid, 3, 1, 7, 6, 2, 4, 5, 0);

    data.WriteBit(!ShowLootItemType);

    data.WriteBits(ItemType, 2);

    data.WriteGuidBytes(guid, 7);

    data << int32(r.ItemRandomPropId);

    data.WriteGuidBytes(guid, 5);

    data << uint32(mapid);
    data << uint32(r.ItemRandomSuffix);

    if (ShowIndex)
        data << uint8(Index);

    data.WriteGuidBytes(guid, 4, 0, 3, 2);

    // Dynamic modifiers
    if (r.ItemUpgradeId)
    {
        uint32 Mask = 1 << ITEM_MODIFIER_UPGRADE_ID;
        uint32 UpgradeID = r.ItemUpgradeId;

        data << uint32(2 * sizeof(uint32));
        data << uint32(Mask);
        data << uint32(UpgradeID);
    }
    else
        data << uint32(0);

    data << uint32(r.ItemId);
    data << uint8(r.RollVoteMask);
    data << uint32(r.ItemCount);
    data << uint8(r.TotalPlayersRolling);
    data << uint32(countDown);

    if (ShowLootItemType)
        data << uint8(LootItemType);

    data.WriteGuidBytes(guid, 6);

    data << uint32(r.ItemDisplayId);

    data.WriteGuidBytes(guid, 1);

    for (Roll::PlayerVote::const_iterator itr = r.playerVote.begin(); itr != r.playerVote.end(); ++itr)
    {
        Player* p = ObjectAccessor::FindConnectedPlayer(itr->first);
        if (!p || !p->GetSession())
            continue;

        if (itr->second != NOT_VALID)
            p->SendDirectMessage(&data);
    }
}

void Group::SendLootStartRollToPlayer(uint32 countDown, uint32 mapid, Player* p, bool canNeed, Roll& r)
{
    if (!p || !p->GetSession())
        return;

    if (!r.GetLoot())
        return;

    uint32 SlotType = LOOT_SLOT_TYPE_ROLL_ONGOING;
    uint8 Index = r.ItemIndex;

    uint8 LootItemType = 0;
    uint8 ItemType = 3;

    uint8 VoteMask = r.RollVoteMask;
    if (!canNeed)
        VoteMask &= ~ROLL_FLAG_TYPE_NEED;

    bool CanTradeToTapList = false;
    bool ShowLootItemType = LootItemType != 0;
    bool ShowIndex = Index >= 0;

    ObjectGuid guid = r.GetLoot()->GetGUID();

    WorldPacket data(SMSG_LOOT_START_ROLL, 1 + 8 + 1 + 4 + 4 + 4 + (ShowIndex ? 1 : 0) + 4 + (r.ItemUpgradeId ? (4 + 4) : 0) + 4 + 1 + 4 + 1 + 4 + (ShowLootItemType ? 1 : 0) + 4);

    data.WriteBits(SlotType, 3);

    data.WriteBit(!ShowIndex);

    data.WriteBit(CanTradeToTapList);

    data.WriteGuidMask(guid, 3, 1, 7, 6, 2, 4, 5, 0);

    data.WriteBit(!ShowLootItemType);

    data.WriteBits(ItemType, 2);

    data.WriteGuidBytes(guid, 7);

    data << int32(r.ItemRandomPropId);

    data.WriteGuidBytes(guid, 5);

    data << uint32(mapid);
    data << uint32(r.ItemRandomSuffix);

    if (ShowIndex)
        data << uint8(Index);

    data.WriteGuidBytes(guid, 4, 0, 3, 2);

    // Dynamic modifiers
    if (r.ItemUpgradeId)
    {
        uint32 Mask = 1 << ITEM_MODIFIER_UPGRADE_ID;
        uint32 UpgradeID = r.ItemUpgradeId;

        data << uint32(2 * sizeof(uint32));
        data << uint32(Mask);
        data << uint32(UpgradeID);
    }
    else
        data << uint32(0);

    data << uint32(r.ItemId);
    data << uint8(VoteMask);
    data << uint32(r.ItemCount);
    data << uint8(r.TotalPlayersRolling);
    data << uint32(countDown);

    if (ShowLootItemType)
        data << uint8(LootItemType);

    data.WriteGuidBytes(guid, 6);

    data << uint32(r.ItemDisplayId);

    data.WriteGuidBytes(guid, 1);

    p->SendDirectMessage(&data);
}

void Group::SendLootRoll(ObjectGuid guid, ObjectGuid target, int32 rollNumber, uint8 rollType, Roll& roll, bool AutomaticallyPassed /*= false*/)
{
    if (!roll.GetLoot())
        return;

    uint32 SlotType = LOOT_SLOT_TYPE_ALLOW_LOOT;
    uint8 Index = roll.ItemIndex;

    uint8 LootItemType = 0;
    uint8 ItemType = 3;

    bool CanTradeToTapList = false;
    bool ShowLootItemType = LootItemType != 0;
    bool ShowIndex = Index >= 0;

    WorldPacket data(SMSG_LOOT_ROLL, 2 * (1 + 8) + 2 + (ShowIndex ? 1 : 0) + 4 + 4 + 4 + 4 + (roll.ItemUpgradeId ? (4 + 4) : 0) + 4 + 4 + (ShowLootItemType ? 1 : 0) + 1 + 4);

    data.WriteGuidMask(guid, 6);

    data.WriteGuidMask(target, 6);

    data.WriteGuidMask(guid, 5, 7);

    data.WriteBit(!ShowLootItemType);

    data.WriteGuidMask(target, 3, 7);

    data.WriteGuidMask(guid, 4);

    data.WriteBit(!ShowIndex);

    data.WriteGuidMask(guid, 0, 2);

    data.WriteGuidMask(target, 1, 0);

    data.WriteBit(CanTradeToTapList);

    data.WriteBits(ItemType, 2);

    data.WriteGuidMask(guid, 3);

    data.WriteBit(AutomaticallyPassed); // 1: "You automatically passed on: %s because you cannot loot that item."

    data.WriteGuidMask(target, 5);

    data.WriteBits(SlotType, 3);

    data.WriteGuidMask(target, 4, 2);

    data.WriteGuidMask(guid, 1);

    data.WriteGuidBytes(guid, 7);

    data.WriteGuidBytes(target, 6);

    data.WriteGuidBytes(guid, 0, 5, 3);

    if (ShowIndex)
        data << uint8(Index);

    data << uint32(roll.ItemDisplayId);
    data << int32(roll.ItemRandomPropId);
    data << uint32(roll.ItemRandomSuffix);

    // Dynamic modifiers
    if (roll.ItemUpgradeId)
    {
        uint32 Mask = 1 << ITEM_MODIFIER_UPGRADE_ID;
        uint32 UpgradeID = roll.ItemUpgradeId;

        data << uint32(2 * sizeof(uint32));
        data << uint32(Mask);
        data << uint32(UpgradeID);
    }
    else
        data << uint32(0);

    data.WriteGuidBytes(guid, 1, 4);

    data.WriteGuidBytes(target, 2);

    data.WriteGuidBytes(guid, 2);

    data << uint32(roll.ItemId);

    data.WriteGuidBytes(target, 4);

    data << int32(rollNumber);

    if (ShowLootItemType)
        data << uint8(LootItemType);

    data.WriteGuidBytes(guid, 6);

    data.WriteGuidBytes(target, 1, 0);

    data << uint8(rollType);

    data.WriteGuidBytes(target, 7);

    data << uint32(roll.ItemCount);

    data.WriteGuidBytes(target, 5, 3);

    for (Roll::PlayerVote::const_iterator itr = roll.playerVote.begin(); itr != roll.playerVote.end(); ++itr)
    {
        Player* p = ObjectAccessor::FindConnectedPlayer(itr->first);
        if (!p || !p->GetSession())
            continue;

        if (itr->second != NOT_VALID)
            p->SendDirectMessage(&data);
    }
}

void Group::SendLootRollWon(ObjectGuid guid, ObjectGuid target, int32 rollNumber, uint8 rollType, Roll& roll)
{
    if (!roll.GetLoot())
        return;

    uint32 SlotType = LOOT_SLOT_TYPE_ALLOW_LOOT;
    uint8 Index = roll.ItemIndex;

    uint8 LootItemType = 0;
    uint8 ItemType = 3;

    bool CanTradeToTapList = false;
    bool ShowLootItemType = LootItemType != 0;
    bool ShowIndex = Index >= 0;

    WorldPacket data(SMSG_LOOT_ROLL_WON, 2 * (1 + 8) + 4 + (roll.ItemUpgradeId ? (4 + 4) : 0) + 4 + 4 + 4 + 4 + 1 + (ShowIndex ? 1 : 0) + 4 + 4 + (ShowLootItemType ? 1 : 0));

    data.WriteGuidMask(target, 0);

    data.WriteGuidMask(guid, 3);

    data.WriteBits(ItemType, 2);

    data.WriteGuidMask(target, 3, 2, 6, 7);

    data.WriteBit(!ShowLootItemType);

    data.WriteGuidMask(guid, 7, 1);

    data.WriteGuidMask(target, 4, 1);

    data.WriteGuidMask(guid, 4);

    data.WriteGuidMask(target, 5);

    data.WriteBits(SlotType, 3);

    data.WriteGuidMask(guid, 0, 2);

    data.WriteBit(!ShowIndex);

    data.WriteBit(CanTradeToTapList);

    data.WriteGuidMask(guid, 6, 5);

    // Dynamic modifiers
    if (roll.ItemUpgradeId)
    {
        uint32 Mask = 1 << ITEM_MODIFIER_UPGRADE_ID;
        uint32 UpgradeID = roll.ItemUpgradeId;

        data << uint32(2 * sizeof(uint32));
        data << uint32(Mask);
        data << uint32(UpgradeID);
    }
    else
        data << uint32(0);

    data << uint32(roll.ItemDisplayId);

    data.WriteGuidBytes(target, 2);

    data.WriteGuidBytes(guid, 7);

    data << int32(roll.ItemRandomPropId);

    data.WriteGuidBytes(target, 5);

    data.WriteGuidBytes(guid, 3);

    data.WriteGuidBytes(target, 7);

    data.WriteGuidBytes(guid, 1, 2, 0);

    data.WriteGuidBytes(target, 3);

    data << int32(rollNumber);
    data << uint32(roll.ItemRandomSuffix);

    data.WriteGuidBytes(guid, 6);

    data.WriteGuidBytes(target, 1, 4);

    data << uint8(rollType);

    data.WriteGuidBytes(target, 6);

    if (ShowIndex)
        data << uint8(Index);

    data.WriteGuidBytes(guid, 4);

    data << uint32(roll.ItemId);

    data.WriteGuidBytes(guid, 5);

    data.WriteGuidBytes(target, 0);

    data << uint32(roll.ItemCount);

    if (ShowLootItemType)
        data << uint8(LootItemType);

    for (Roll::PlayerVote::const_iterator itr = roll.playerVote.begin(); itr != roll.playerVote.end(); ++itr)
    {
        Player* p = ObjectAccessor::FindConnectedPlayer(itr->first);
        if (!p || !p->GetSession())
            continue;

        if (itr->second != NOT_VALID)
            p->SendDirectMessage(&data);
    }
}

void Group::SendLootAllPassed(Roll& roll)
{
    if (!roll.GetLoot())
        return;

    uint32 SlotType = LOOT_SLOT_TYPE_ALLOW_LOOT;
    uint8 Index = roll.ItemIndex;

    uint8 LootItemType = 0;
    uint8 ItemType = 3;

    bool CanTradeToTapList = false;
    bool ShowLootItemType = LootItemType != 0;
    bool ShowIndex = Index >= 0;

    ObjectGuid guid = roll.GetLoot()->GetGUID();

    WorldPacket data(SMSG_LOOT_ALL_PASSED, 1 + 8 + 1 + 4 + (ShowLootItemType ? 1 : 0) + 4 + 4 + 4 + (roll.ItemUpgradeId ? (4 + 4) : 0) + 4 + 4 + (ShowIndex ? 1 : 0));

    data.WriteBit(!ShowIndex);

    data.WriteBit(!ShowLootItemType);

    data.WriteGuidMask(guid, 2, 4, 3, 1);

    data.WriteBit(CanTradeToTapList);

    data.WriteBits(ItemType, 2);
    data.WriteBits(SlotType, 3);

    data.WriteGuidMask(guid, 6, 5, 7, 0);

    data.WriteGuidBytes(guid, 6, 5, 0);

    data << uint32(roll.ItemCount);

    data.WriteGuidBytes(guid, 3, 2);

    if (ShowLootItemType)
        data << uint8(LootItemType);

    data << uint32(roll.ItemDisplayId);

    data.WriteGuidBytes(guid, 1);

    data << int32(roll.ItemRandomPropId);

    // Dynamic modifiers
    if (roll.ItemUpgradeId)
    {
        uint32 Mask = 1 << ITEM_MODIFIER_UPGRADE_ID;
        uint32 UpgradeID = roll.ItemUpgradeId;

        data << uint32(2 * sizeof(uint32));
        data << uint32(Mask);
        data << uint32(UpgradeID);
    }
    else
        data << uint32(0);

    data.WriteGuidBytes(guid, 4);

    data << uint32(roll.ItemId);
    data << uint32(roll.ItemRandomSuffix);

    if (ShowIndex)
        data << uint8(Index);

    data.WriteGuidBytes(guid, 7);

    for (Roll::PlayerVote::const_iterator itr = roll.playerVote.begin(); itr != roll.playerVote.end(); ++itr)
    {
        Player* player = ObjectAccessor::FindConnectedPlayer(itr->first);
        if (!player || !player->GetSession())
            continue;

        if (itr->second != NOT_VALID)
            player->SendDirectMessage(&data);
    }
}

void Group::SendLootRollComplete(Roll& roll)
{
    if (!roll.GetLoot())
        return;

    ObjectGuid guid = roll.GetLoot()->GetGUID();

    uint8 Index = roll.ItemIndex;

    WorldPacket data(SMSG_LOOT_ROLL_COMPLETE, 1 + 8 + 1);

    data.WriteGuidMask(guid, 6, 5, 2, 3, 7, 0, 1, 4);

    data.WriteGuidBytes(guid, 1, 0, 2);

    data << uint8(Index);

    data.WriteGuidBytes(guid, 7, 4, 6, 3, 5);

    for (Roll::PlayerVote::const_iterator itr = roll.playerVote.begin(); itr != roll.playerVote.end(); ++itr)
    {
        Player* p = ObjectAccessor::FindConnectedPlayer(itr->first);
        if (!p || !p->GetSession())
            continue;

        if (itr->second != NOT_VALID)
            p->SendDirectMessage(&data);
    }
}

// notify group members which player is the allowed looter for the given creature
void Group::SendLooter(Creature* creature, Player* groupLooter)
{
    ASSERT(creature);

    Player* masterLooter = NULL;
    if (GetLootMethod() == MASTER_LOOT && creature->loot && creature->loot->HasOverThresholdItem())
    {
        masterLooter = ObjectAccessor::FindPlayer(GetMasterLooterGuid());
        if (masterLooter)
            groupLooter = NULL;
    }

    WorldPacket data(SMSG_LOOT_LIST, 1 + 8 + 1 + (masterLooter ? (1 + 8) : 0) + (groupLooter ? (1 + 8) : 0));

    ObjectGuid creatureGuid = creature->GetGUID();

    ByteBuffer masterData;
    ByteBuffer lootData;

    data.WriteGuidMask(creatureGuid, 5);

    data.WriteBit(masterLooter != NULL);

    if (masterLooter)
    {
        ObjectGuid masterGUID = masterLooter->GetGUID();

        data.WriteGuidMask(masterGUID, 4, 6, 0, 7, 5, 2, 3, 1);
        masterData.WriteGuidBytes(masterGUID, 4, 5, 6, 3, 2, 7, 0, 1);
    }

    data.WriteGuidMask(creatureGuid, 1);

    data.WriteBit(groupLooter != NULL);

    data.WriteGuidMask(creatureGuid, 4, 3, 2);

    if (groupLooter)
    {
        ObjectGuid groupGUID = groupLooter->GetGUID();

        data.WriteGuidMask(groupGUID, 2, 3, 4, 5, 6, 0, 1, 7);
        lootData.WriteGuidBytes(groupGUID, 7, 1, 0, 6, 5, 3, 4, 2);
    }

    data.WriteGuidMask(creatureGuid, 7, 0, 6);

    data.append(lootData);
    data.append(masterData);

    data.WriteGuidBytes(creatureGuid, 5, 1, 6, 2, 3, 0, 7, 4);

    BroadcastPacket(&data, false);
}

void Group::GroupLoot(Loot* loot, WorldObject* lootedObject)
{
    if (!loot)
        return;

    std::vector<LootItem>::iterator i;
    ItemTemplate const* item = nullptr;
    uint8 itemSlot = 0;

    for (i = loot->Items.begin(); i != loot->Items.end(); ++i, ++itemSlot)
    {
        if (i->FreeForAll || i->IsBlocked || i->IsLooted || i->AlreadyAskedForRoll)
            continue;

        item = sObjectMgr->GetItemTemplate(i->ItemId);
        if (!item)
        {
            //TC_LOG_DEBUG("misc", "Group::GroupLoot: missing item prototype for item with id: %d", i->itemid);
            continue;
        }

        //roll for over-threshold item if it's one-player loot
        if (item->GetQuality() >= uint32(m_lootThreshold))
        {
            Roll* r = new Roll(*i, item->GetDisplayId());
            //a vector is filled with only near party members
            for (GroupReference* itr = GetFirstMember(); itr != NULL; itr = itr->next())
            {
                Player* member = itr->GetSource();
                if (!member || !member->GetSession())
                    continue;

                if (i->AllowedForPlayer(member))
                {
                    if (member->IsAtGroupRewardDistance(lootedObject))
                    {
                        r->TotalPlayersRolling++;

                        if (member->GetPassOnGroupLoot())
                        {
                            r->playerVote[member->GetGUID()] = PASS;
                            r->TotalPass++;
                            // can't broadcast the pass now. need to wait until all rolling players are known.
                        }
                        else if (member->CanEquipUniqueItem(item) != EQUIP_ERR_OK)
                        {
                            r->playerVote[member->GetGUID()] = AUTOMATIC_PASS;
                            r->TotalPass++;
                            // can't broadcast the pass now. need to wait until all rolling players are known.
                        }
                        else
                            r->playerVote[member->GetGUID()] = NOT_EMITED_YET;
                    }
                }
            }

            if (r->TotalPlayersRolling > 0)
            {
                r->SetLoot(loot);
                r->ItemIndex = itemSlot;
                if (item->DisenchantID && m_maxEnchantingLevel >= item->RequiredDisenchantSkill)
                    r->RollVoteMask |= ROLL_FLAG_TYPE_DISENCHANT;

                loot->Items[itemSlot].IsBlocked = true;
                loot->Items[itemSlot].AlreadyAskedForRoll = true;

                SendLootStartRoll(ROLL_TIME, lootedObject->GetMapId(), *r);

                // If there is any "auto pass", broadcast the pass now.
                if (r->TotalPass)
                {
                    for (Roll::PlayerVote::const_iterator itr = r->playerVote.begin(); itr != r->playerVote.end(); ++itr)
                    {
                        Player* player = ObjectAccessor::FindConnectedPlayer(itr->first);
                        if (!player || !player->GetSession())
                            continue;

                        if (itr->second == PASS)
                            SendLootRoll(loot->GetGUID(), player->GetGUID(), -1, ROLL_PASS, *r);
                        else if (itr->second == AUTOMATIC_PASS)
                            SendLootRoll(loot->GetGUID(), player->GetGUID(), -1, ROLL_PASS, *r, true);
                    }
                }

                if (r->TotalPass >= r->TotalPlayersRolling)
                {
                    CountTheRoll(r, lootedObject->GetMap());
                    delete r;
                }
                else
                {
                    RollId.push_back(r);

                    loot->StartRollTimer(ROLL_TIME);
                }
            }
            else
                delete r;
        }
        else
            i->IsUnderthreshold = true;
    }

    for (i = loot->QuestItems.begin(); i != loot->QuestItems.end(); ++i, ++itemSlot)
    {
        if (!i->FollowLootRules)
            continue;

        item = sObjectMgr->GetItemTemplate(i->ItemId);
        if (!item)
        {
            //TC_LOG_DEBUG("misc", "Group::GroupLoot: missing item prototype for item with id: %d", i->itemid);
            continue;
        }

        Roll* r = new Roll(*i, item->GetDisplayId());
        //a vector is filled with only near party members
        for (GroupReference* itr = GetFirstMember(); itr != NULL; itr = itr->next())
        {
            Player* member = itr->GetSource();
            if (!member || !member->GetSession())
                continue;

            if (i->AllowedForPlayer(member))
            {
                if (member->IsAtGroupRewardDistance(lootedObject))
                {
                    r->TotalPlayersRolling++;
                    r->playerVote[member->GetGUID()] = NOT_EMITED_YET;
                }
            }
        }

        if (r->TotalPlayersRolling > 0)
        {
            r->SetLoot(loot);
            r->ItemIndex = itemSlot;

            loot->QuestItems[itemSlot - loot->Items.size()].IsBlocked = true;

            SendLootStartRoll(ROLL_TIME, lootedObject->GetMapId(), *r);

            RollId.push_back(r);

            loot->StartRollTimer(ROLL_TIME);
        }
        else
            delete r;
    }
}

void Group::NeedBeforeGreed(Loot* loot, WorldObject* lootedObject)
{
    ItemTemplate const* item;
    uint8 itemSlot = 0;
    for (std::vector<LootItem>::iterator i = loot->Items.begin(); i != loot->Items.end(); ++i, ++itemSlot)
    {
        if (i->FreeForAll || i->IsBlocked || i->IsLooted)
            continue;

        item = sObjectMgr->GetItemTemplate(i->ItemId);
        if (!item)
            continue;

        //roll for over-threshold item if it's one-player loot
        if (item->GetQuality() >= uint32(m_lootThreshold))
        {
            Roll* r = new Roll(*i, item->GetDisplayId());
            for (GroupReference* itr = GetFirstMember(); itr != NULL; itr = itr->next())
            {
                Player* playerToRoll = itr->GetSource();
                if (!playerToRoll || !playerToRoll->GetSession())
                    continue;

                bool allowedForPlayer = i->AllowedForPlayer(playerToRoll);
                if (allowedForPlayer && playerToRoll->IsAtGroupRewardDistance(lootedObject))
                {
                    r->TotalPlayersRolling++;
                    if (playerToRoll->GetPassOnGroupLoot())
                    {
                        r->playerVote[playerToRoll->GetGUID()] = PASS;
                        r->TotalPass++;
                        // can't broadcast the pass now. need to wait until all rolling players are known.
                    }
                    else if (playerToRoll->CanEquipUniqueItem(item) != EQUIP_ERR_OK)
                    {
                        r->playerVote[playerToRoll->GetGUID()] = AUTOMATIC_PASS;
                        r->TotalPass++;
                        // can't broadcast the pass now. need to wait until all rolling players are known.
                    }
                    else
                        r->playerVote[playerToRoll->GetGUID()] = NOT_EMITED_YET;
                }
            }

            if (r->TotalPlayersRolling > 0)
            {
                r->SetLoot(loot);
                r->ItemIndex = itemSlot;
                if (item->DisenchantID && m_maxEnchantingLevel >= item->RequiredDisenchantSkill)
                    r->RollVoteMask |= ROLL_FLAG_TYPE_DISENCHANT;

                if (item->GetFlags2() & ITEM_PROTO_FLAG2_CAN_ONLY_ROLL_GREED)
                    r->RollVoteMask &= ~ROLL_FLAG_TYPE_NEED;

                loot->Items[itemSlot].IsBlocked = true;

                //Broadcast Pass and Send Rollstart
                for (Roll::PlayerVote::const_iterator itr = r->playerVote.begin(); itr != r->playerVote.end(); ++itr)
                {
                    Player* player = ObjectAccessor::FindConnectedPlayer(itr->first);
                    if (!player || !player->GetSession())
                        continue;

                    SendLootStartRollToPlayer(ROLL_TIME, lootedObject->GetMapId(), player, player->CanRollForItemInLFG(item, lootedObject) == EQUIP_ERR_OK, *r);

                    if (itr->second == PASS)
                        SendLootRoll(loot->GetGUID(), player->GetGUID(), -1, ROLL_PASS, *r);
                    else if (itr->second == AUTOMATIC_PASS)
                        SendLootRoll(loot->GetGUID(), player->GetGUID(), -1, ROLL_PASS, *r, true);
                }

                if (r->TotalPass >= r->TotalPlayersRolling)
                {
                    CountTheRoll(r, lootedObject->GetMap());
                    delete r;
                }
                else
                {
                    RollId.push_back(r);

                    loot->StartRollTimer(ROLL_TIME);
                }
            }
            else
                delete r;
        }
        else
            i->IsUnderthreshold = true;
    }

    for (std::vector<LootItem>::iterator i = loot->QuestItems.begin(); i != loot->QuestItems.end(); ++i, ++itemSlot)
    {
        if (!i->FollowLootRules)
            continue;

        item = sObjectMgr->GetItemTemplate(i->ItemId);
        if (!item)
            continue;

        Roll* r = new Roll(*i, item->GetDisplayId());
        for (GroupReference* itr = GetFirstMember(); itr != NULL; itr = itr->next())
        {
            Player* playerToRoll = itr->GetSource();
            if (!playerToRoll || !playerToRoll->GetSession())
                continue;

            if (playerToRoll->IsAtGroupRewardDistance(lootedObject) && i->AllowedForPlayer(playerToRoll))
            {
                r->TotalPlayersRolling++;
                r->playerVote[playerToRoll->GetGUID()] = NOT_EMITED_YET;
            }
        }

        if (r->TotalPlayersRolling > 0)
        {
            r->SetLoot(loot);
            r->ItemIndex = itemSlot;

            loot->QuestItems[itemSlot - loot->Items.size()].IsBlocked = true;

            //Broadcast Pass and Send Rollstart
            for (Roll::PlayerVote::const_iterator itr = r->playerVote.begin(); itr != r->playerVote.end(); ++itr)
            {
                Player* player = ObjectAccessor::FindConnectedPlayer(itr->first);
                if (!player || !player->GetSession())
                    continue;

                SendLootStartRollToPlayer(ROLL_TIME, lootedObject->GetMapId(), player, player->CanRollForItemInLFG(item, lootedObject) == EQUIP_ERR_OK, *r);
            }

            RollId.push_back(r);

            loot->StartRollTimer(ROLL_TIME);
        }
        else
            delete r;
    }
}

void Group::MasterLoot(Loot* loot, WorldObject* lootedObject)
{
    for (std::vector<LootItem>::iterator i = loot->Items.begin(); i != loot->Items.end(); ++i)
    {
        if (i->FreeForAll)
            continue;

        i->IsBlocked = !i->IsUnderthreshold;
    }

    for (std::vector<LootItem>::iterator i = loot->QuestItems.begin(); i != loot->QuestItems.end(); ++i)
    {
        if (!i->FollowLootRules)
            continue;

        i->IsBlocked = !i->IsUnderthreshold;
    }

    std::vector<Player*> groupMembers;
    for (GroupReference* itr = GetFirstMember(); itr != NULL; itr = itr->next())
    {
        Player* looter = itr->GetSource();
        if (!looter || !looter->IsInWorld())
            continue;

        if (looter->IsAtGroupRewardDistance(lootedObject))
            groupMembers.push_back(looter);
    }

    Player* masterLooter = ObjectAccessor::FindPlayer(GetMasterLooterGuid());
    if (!masterLooter)
        return;

    WorldPacket data(SMSG_LOOT_MASTER_LIST, 1 + 8 + 3 + groupMembers.size() * (1 + 8));

    ObjectGuid lootedGuid = loot->GetGUID();

    ByteBuffer groupData;

    data.WriteGuidMask(lootedGuid, 1);

    data.WriteBits(groupMembers.size(), 24);

    data.WriteGuidMask(lootedGuid, 5);

    for (Player* looter : groupMembers)
    {
        ObjectGuid looterGUID = looter->GetGUID();

        data.WriteGuidMask(looterGUID, 4, 1, 2, 5, 3, 7, 0, 6);
        groupData.WriteGuidBytes(looterGUID, 1, 3, 7, 2, 0, 6, 4, 5);
    }

    data.WriteGuidMask(lootedGuid, 2, 0, 6, 7, 3, 4);

    data.WriteGuidBytes(lootedGuid, 3, 0, 2);

    data.append(groupData);

    data.WriteGuidBytes(lootedGuid, 7, 1, 6, 4, 5);

    masterLooter->SendDirectMessage(&data);
}

void Group::DoRollForAllMembers(WorldObject* lootedObject, uint8 slot, uint32 mapid, Loot* loot, LootItem& item)
{
    if (item.FreeForAll)
        return;

    ItemTemplate const* proto = sObjectMgr->GetItemTemplate(item.ItemId);
    if (!proto)
        return;

    Roll* r = new Roll(item, proto->GetDisplayId());

    //a vector is filled with only near party members
    for (GroupReference* itr = GetFirstMember(); itr != NULL; itr = itr->next())
    {
        Player* member = itr->GetSource();
        if (!member || !member->GetSession())
            continue;

        if (item.AllowedForPlayer(member))
        {
            if (member->IsAtGroupRewardDistance(lootedObject))
            {
                r->TotalPlayersRolling++;

                if (member->GetPassOnGroupLoot())
                {
                    r->playerVote[member->GetGUID()] = PASS;
                    r->TotalPass++;
                    // can't broadcast the pass now. need to wait until all rolling players are known.
                }
                else if (member->CanEquipUniqueItem(proto) != EQUIP_ERR_OK)
                {
                    r->playerVote[member->GetGUID()] = AUTOMATIC_PASS;
                    r->TotalPass++;
                    // can't broadcast the pass now. need to wait until all rolling players are known.
                }
                else
                    r->playerVote[member->GetGUID()] = NOT_EMITED_YET;
            }
        }
    }

    if (r->TotalPlayersRolling > 0)
    {
        r->SetLoot(loot);
        r->ItemIndex = slot;

        uint32 itemID = item.ItemId;
        ItemTemplate const* Item = sObjectMgr->GetItemTemplate(itemID);
        if (!Item)
            return;

        if (Item->DisenchantID && m_maxEnchantingLevel >= Item->RequiredDisenchantSkill)
            r->RollVoteMask |= ROLL_FLAG_TYPE_DISENCHANT;

        loot->Items[slot].IsBlocked = true;
        loot->Items[slot].AlreadyAskedForRoll = true;

        // If there is any "auto pass", broadcast the pass now.
        if (r->TotalPass)
        {
            for (Roll::PlayerVote::const_iterator itr = r->playerVote.begin(); itr != r->playerVote.end(); ++itr)
            {
                Player* player = ObjectAccessor::FindPlayer(itr->first);
                if (!player || !player->GetSession())
                    continue;

                if (itr->second == PASS)
                    SendLootRoll(loot->GetGUID(), player->GetGUID(), -1, ROLL_PASS, *r);
                else if (itr->second == AUTOMATIC_PASS)
                    SendLootRoll(loot->GetGUID(), player->GetGUID(), -1, ROLL_PASS, *r, true);
            }
        }

        SendLootStartRoll(ROLL_TIME, mapid, *r);

        if (r->TotalPass >= r->TotalPlayersRolling)
        {
            CountTheRoll(r, lootedObject->GetMap());
            delete r;
        }
        else
        {
            RollId.push_back(r);

            loot->StartRollTimer(ROLL_TIME);
        }
    }
}

void Group::CountRollVote(ObjectGuid playerGUID, ObjectGuid guid, uint8 ItemIndex, uint8 Choice)
{
    Rolls::iterator rollI = GetRoll(guid, ItemIndex);
    if (rollI == RollId.end())
        return;

    Roll* roll = *rollI;

    Roll::PlayerVote::iterator itr = roll->playerVote.find(playerGUID);
    // this condition means that player joins to the party after roll begins
    if (itr == roll->playerVote.end())
        return;

    if (roll->GetLoot())
        if (roll->GetLoot()->Items.empty())
            return;

    switch (Choice)
    {
        case ROLL_PASS:                                     // Player choose pass
            SendLootRoll(guid, playerGUID, -1, ROLL_PASS, *roll);
            ++roll->TotalPass;
            itr->second = PASS;
            break;
        case ROLL_NEED:                                     // player choose Need
            SendLootRoll(guid, playerGUID, 128, ROLL_NEED, *roll);
            ++roll->TotalNeed;
            itr->second = NEED;
            break;
        case ROLL_GREED:                                    // player choose Greed
            SendLootRoll(guid, playerGUID, -7, ROLL_GREED, *roll);
            ++roll->TotalGreed;
            itr->second = GREED;
            break;
        case ROLL_DISENCHANT:                               // player choose Disenchant
            SendLootRoll(guid, playerGUID, -8, ROLL_DISENCHANT, *roll);
            ++roll->TotalGreed;
            itr->second = DISENCHANT;
            break;
    }

    if (roll->TotalPass + roll->TotalNeed + roll->TotalGreed >= roll->TotalPlayersRolling)
    {
        CountTheRoll(roll, nullptr);

        RollId.erase(rollI);
        delete roll;
    }
}

//called when roll timer expires
void Group::EndRoll(Loot* loot, Map* allowedMap)
{
    for (Rolls::iterator itr = RollId.begin(); itr != RollId.end();)
    {
        Roll* roll = *itr;
        if (roll->GetLoot() == loot)
        {
            if (roll->isValid())
                CountTheRoll(roll, allowedMap);

            itr = RollId.erase(itr);
            delete roll;
        }
        else
            ++itr;
    }
}

void Group::CountTheRoll(Roll* roll, Map* allowedMap)
{
    // first send pass to all not decided rolls
    if (roll->TotalNeed || roll->TotalGreed)
    {
        if (!roll->playerVote.empty())
            for (Roll::PlayerVote::iterator itr = roll->playerVote.begin(); itr != roll->playerVote.end(); ++itr)
            {
                if (itr->second != NOT_EMITED_YET)
                    continue;

                SendLootRoll(roll->GetLoot()->GetGUID(), itr->first, -1, ROLL_PASS, *roll);
                ++roll->TotalPass;
                itr->second = PASS;
            }
    }

    //end of the roll
    if (roll->TotalNeed > 0)
    {
        if (!roll->playerVote.empty())
        {
            uint8 maxresul = 0;
            ObjectGuid maxguid = ObjectGuid::Empty;
            Player* player = nullptr;

            for (Roll::PlayerVote::const_iterator itr = roll->playerVote.begin(); itr != roll->playerVote.end(); ++itr)
            {
                if (itr->second != NEED)
                    continue;

                player = ObjectAccessor::FindPlayer(itr->first);
                if (!player || (allowedMap != nullptr && player->FindMap() != allowedMap))
                {
                    --roll->TotalNeed;
                    continue;
                }

                int32 randomN = Math::Rand(1, 100);
                SendLootRoll(roll->GetLoot()->GetGUID(), itr->first, randomN, ROLL_NEED, *roll);
                if (maxresul < randomN)
                {
                    maxguid  = itr->first;
                    maxresul = randomN;
                }
            }

            if (!maxguid.IsEmpty())
            {
                SendLootRollWon(roll->GetLoot()->GetGUID(), maxguid, maxresul, ROLL_NEED, *roll);
                player = ObjectAccessor::FindConnectedPlayer(maxguid);

                if (player && player->GetSession())
                {
                    player->UpdateCriteria(CRITERIA_TYPE_ROLL_NEED_ON_LOOT, roll->ItemId, maxresul);

                    ItemPosCountVec dest;
                    LootItem* item = &(roll->ItemIndex >= roll->GetLoot()->Items.size() ? roll->GetLoot()->QuestItems[roll->ItemIndex - roll->GetLoot()->Items.size()] : roll->GetLoot()->Items[roll->ItemIndex]);
                    InventoryResult msg = player->CanStoreNewItem(NULL_BAG, NULL_SLOT, dest, roll->ItemId, item->Count);
                    if (msg == EQUIP_ERR_OK)
                    {
                        item->IsLooted = true;
                        roll->GetLoot()->NotifyItemRemoved(roll->ItemIndex);
                        roll->GetLoot()->UnlootedCount--;
                        player->StoreNewItem(dest, roll->ItemId, true, item->RandomPropertyId, item->UpgradeId, item->GetAllowedLooters());
                    }
                    else
                    {
                        item->IsBlocked = false;
                        player->SendEquipError(msg, NULL, NULL, roll->ItemId);
                    }
                }
            }
            else
                roll->TotalNeed = 0;
        }
    }

    if (roll->TotalNeed == 0 && roll->TotalGreed > 0) // if (roll->totalNeed == 0 && ...), not else if, because numbers can be modified above if player is on a different map
    {
        if (!roll->playerVote.empty())
        {
            uint8 maxresul = 0;
            ObjectGuid maxguid = ObjectGuid::Empty;
            Player* player = nullptr;
            RollVote rollvote = NOT_VALID;

            Roll::PlayerVote::iterator itr;
            for (itr = roll->playerVote.begin(); itr != roll->playerVote.end(); ++itr)
            {
                if (itr->second != GREED && itr->second != DISENCHANT)
                    continue;

                player = ObjectAccessor::FindPlayer(itr->first);
                if (!player || (allowedMap != NULL && player->FindMap() != allowedMap))
                {
                    --roll->TotalGreed;
                    continue;
                }

                int32 randomN = Math::Rand(1, 100);
                SendLootRoll(roll->GetLoot()->GetGUID(), itr->first, randomN, itr->second, *roll);
                if (maxresul < randomN)
                {
                    maxguid  = itr->first;
                    maxresul = randomN;
                    rollvote = itr->second;
                }
            }

            if (!maxguid.IsEmpty())
            {
                SendLootRollWon(roll->GetLoot()->GetGUID(), maxguid, maxresul, rollvote, *roll);
                player = ObjectAccessor::FindConnectedPlayer(maxguid);

                if (player && player->GetSession())
                {
                    player->UpdateCriteria(CRITERIA_TYPE_ROLL_GREED_ON_LOOT, roll->ItemId, maxresul);

                    LootItem* item = &(roll->ItemIndex >= roll->GetLoot()->Items.size() ? roll->GetLoot()->QuestItems[roll->ItemIndex - roll->GetLoot()->Items.size()] : roll->GetLoot()->Items[roll->ItemIndex]);

                    if (rollvote == GREED)
                    {
                        ItemPosCountVec dest;
                        InventoryResult msg = player->CanStoreNewItem(NULL_BAG, NULL_SLOT, dest, roll->ItemId, item->Count);
                        if (msg == EQUIP_ERR_OK)
                        {
                            item->IsLooted = true;
                            roll->GetLoot()->NotifyItemRemoved(roll->ItemIndex);
                            roll->GetLoot()->UnlootedCount--;
                            player->StoreNewItem(dest, roll->ItemId, true, item->RandomPropertyId, item->UpgradeId, item->GetAllowedLooters());
                        }
                        else
                        {
                            item->IsBlocked = false;
                            player->SendEquipError(msg, NULL, NULL, roll->ItemId);
                        }
                    }
                    else if (rollvote == DISENCHANT)
                    {
                        item->IsLooted = true;
                        roll->GetLoot()->NotifyItemRemoved(roll->ItemIndex);
                        roll->GetLoot()->UnlootedCount--;
                        ItemTemplate const* pProto = sObjectMgr->GetItemTemplate(roll->ItemId);
                        player->UpdateCriteria(CRITERIA_TYPE_CAST_SPELL, 13262); // Disenchant

                        ItemPosCountVec dest;
                        InventoryResult msg = player->CanStoreNewItem(NULL_BAG, NULL_SLOT, dest, roll->ItemId, item->Count);
                        if (msg == EQUIP_ERR_OK)
                            player->AutoStoreLoot(pProto->DisenchantID, LootTemplates_Disenchant, true);
                        else // If the player's inventory is full, send the disenchant result in a mail.
                        {
                            Loot loot(nullptr, player, LOOT_DISENCHANTING);
                            loot.FillLoot(pProto->DisenchantID, LootTemplates_Disenchant, player, true);

                            uint32 max_slot = loot.GetMaxSlotInLootFor(player);
                            for (uint32 i = 0; i < max_slot; ++i)
                            {
                                LootItem* lootItem = loot.LootItemInSlot(i, player);
                                player->SendEquipError(msg, NULL, NULL, lootItem->ItemId);
                                player->SendItemRetrievalMail(lootItem->ItemId, lootItem->Count);
                            }
                        }
                    }
                }
            }
            else
                roll->TotalGreed = 0;
        }
    }

    if (roll->TotalNeed == 0 && roll->TotalGreed == 0) // if, not else, because numbers can be modified above if player is on a different map
    {
        SendLootAllPassed(*roll);

        // remove IsBlocked so that the item is lootable by all players
        LootItem* item = &(roll->ItemIndex >= roll->GetLoot()->Items.size() ? roll->GetLoot()->QuestItems[roll->ItemIndex - roll->GetLoot()->Items.size()] : roll->GetLoot()->Items[roll->ItemIndex]);
        if (item)
            item->IsBlocked = false;
    }

    // Send completed roll
    SendLootRollComplete(*roll);
}

void Group::SetTargetIcon(uint8 id, ObjectGuid whoGuid, ObjectGuid targetGuid, uint8 index)
{
    if (id >= TARGETICONCOUNT)
        return;

    // clean other icons
    if (!targetGuid.IsEmpty())
        for (int i = 0; i < TARGETICONCOUNT; ++i)
            if (m_targetIcons[i] == targetGuid)
                SetTargetIcon(i, ObjectGuid::Empty, ObjectGuid::Empty, 0);

    m_targetIcons[id] = targetGuid;

    WorldPacket data(SMSG_RAID_TARGET_UPDATE_SINGLE, 2 * (1 + 8) + 1 + 1);

    data.WriteGuidMask(whoGuid, 6);

    data.WriteGuidMask(targetGuid, 4);

    data.WriteGuidMask(whoGuid, 0, 7);

    data.WriteGuidMask(targetGuid, 6);

    data.WriteGuidMask(whoGuid, 5, 3, 4);

    data.WriteGuidMask(targetGuid, 7, 2, 5, 1);

    data.WriteGuidMask(whoGuid, 2, 1);

    data.WriteGuidMask(targetGuid, 0, 3);

    data.WriteGuidBytes(targetGuid, 1);

    data << uint8(id);

    data.WriteGuidBytes(whoGuid, 0, 5, 3);

    data.WriteGuidBytes(targetGuid, 7, 6);

    data.WriteGuidBytes(whoGuid, 1);

    data.WriteGuidBytes(targetGuid, 2, 4, 0, 3, 5);

    data.WriteGuidBytes(whoGuid, 6);

    data << uint8(index);

    data.WriteGuidBytes(whoGuid, 4, 2, 7);

    BroadcastPacket(&data, true);
}

void Group::SendTargetIconList(WorldSession* session, uint8 Index)
{
    if (!session)
        return;

    std::unordered_map<uint8, ObjectGuid> targetIcons;
    for (uint8 i = 0; i < TARGETICONCOUNT; ++i)
    {
        if (m_targetIcons[i].IsEmpty())
            continue;

        targetIcons[i] = m_targetIcons[i];
    }

    ByteBuffer dataBuffer;

    WorldPacket data(SMSG_RAID_TARGET_UPDATE_ALL, 4 + targetIcons.size() * (1 + 8 + 1) + 1);

    data.WriteBits(targetIcons.size(), 25);

    for (auto & icon : targetIcons)
    {
        ObjectGuid guid = icon.second;

        data.WriteGuidMask(guid, 2, 1, 3, 7, 6, 4, 0, 5);

        dataBuffer.WriteGuidBytes(guid, 4, 7, 1, 0, 6, 5, 3);

        dataBuffer << uint8(icon.first);

        dataBuffer.WriteGuidBytes(guid, 2);
    }

    data.append(dataBuffer);                // append byte sequences

    data << uint8(Index);

    session->SendPacket(&data);
}

void Group::SetRaidMarkerInSlot(DynamicObject* dynObj, uint8 slotId)
{
    RaidMarkers::iterator raidMarker = m_RaidMarkers.find(slotId);
    if (raidMarker != m_RaidMarkers.end())
        return;

    m_RaidMarkers[slotId] = dynObj;

    SendRaidMarkersUpdate();
}

void Group::ResetRaidMarkerInSlot(uint8 slotId)
{
    RaidMarkers::iterator raidMarker = m_RaidMarkers.find(slotId);
    if (raidMarker == m_RaidMarkers.end())
        return;

    m_RaidMarkers.erase(raidMarker);

    SendRaidMarkersUpdate();
}

void Group::ClearRaidMarkers(uint8 slotId)
{
    // if slotId == MAX_RAID_MARKER_SLOT, clear all raid markers
    if (slotId == MAX_RAID_MARKER_SLOT)
    {
        RaidMarkers::iterator it, next;
        for (it = m_RaidMarkers.begin(); it != m_RaidMarkers.end(); it = next)
        {
            next = it;
            ++next;

            if (DynamicObject* dynObj = it->second)
                dynObj->Remove();

            m_RaidMarkers.erase(it);
        }
    }
    else
    {
        RaidMarkers::iterator raidMarker = m_RaidMarkers.find(slotId);
        if (raidMarker == m_RaidMarkers.end())
            return;

        if (DynamicObject* dynObj = raidMarker->second)
            dynObj->Remove();

        m_RaidMarkers.erase(raidMarker);
    }

    SendRaidMarkersUpdate();
}

DynamicObject* Group::GetRaidMarkerBySlot(uint8 slotId) const
{
    RaidMarkers::const_iterator itr = m_RaidMarkers.find(slotId);
    if (itr != m_RaidMarkers.end())
        return itr->second;

    return NULL;
}

void Group::SendRaidMarkersUpdate(WorldSession* session, int8 partyIndex)
{
    uint32 markerMask = 0;
    std::vector<DynamicObject*> markers;

    for (RaidMarkers::iterator itr = m_RaidMarkers.begin(); itr != m_RaidMarkers.end(); ++itr)
    {
        markerMask |= (1 << itr->first);
        markers.push_back(itr->second);
    }

    WorldPacket data(SMSG_RAID_MARKERS_CHANGED, 1 + 4 + 1 + markers.size() * (1 + 8 + 4 + 4 + 4 + 1));

    data << int8(partyIndex);

    data << uint32(markerMask);

    data.WriteBits(markers.size(), 3);

    ByteBuffer buffData;

    for (auto & itr : markers)
    {
        DynamicObject* dynObj = itr;
        if (!dynObj)
            continue;

        ObjectGuid guid = dynObj->GetGUID();

        data.WriteGuidMask(guid, 6, 2, 7, 1, 4, 3, 5, 0);

        buffData.WriteGuidBytes(guid, 6);

        buffData << float(dynObj->GetPositionX());

        buffData.WriteGuidBytes(guid, 2);

        buffData << float(dynObj->GetPositionY());

        buffData.WriteGuidBytes(guid, 7, 5, 0, 4);

        buffData << float(dynObj->GetPositionZ());

        buffData.WriteGuidBytes(guid, 3, 1);

        buffData << uint8(1); // unk
    }

    data.append(buffData);

    if (session)
        session->SendPacket(&data);
    else
        BroadcastPacket(&data, false);
}

void Group::SendUpdate()
{
    for (member_witerator witr = m_memberSlots.begin(); witr != m_memberSlots.end(); ++witr)
        SendUpdateToPlayer(witr->Guid, &(*witr));
}

void Group::SendUpdateToPlayer(ObjectGuid playerGUID, MemberSlot* slot)
{
    Player* player = ObjectAccessor::FindConnectedPlayer(playerGUID);

    if (!player || !player->GetSession())
        return;

    // if MemberSlot wasn't provided
    if (!slot)
    {
        ObjectGuid groupGuid = m_guid;
        ObjectGuid leaderGuid;

        WorldPacket data(SMSG_GROUP_LIST, 2 * (1 + 8) + 3 + 1 + 1 + 4 + 4 + 1);
        data.WriteGuidMask(groupGuid, 0);
        data.WriteGuidMask(leaderGuid, 7, 1);

        data.WriteBit(0);

        data.WriteGuidMask(groupGuid, 7);
        data.WriteGuidMask(leaderGuid, 6, 5);

        data.WriteBits(0, 21);

        data.WriteGuidMask(leaderGuid, 3, 0);

        data.WriteBit(0);

        data.WriteGuidMask(groupGuid, 5, 2, 4, 1);

        data.WriteBit(0);

        data.WriteGuidMask(leaderGuid, 2);
        data.WriteGuidMask(groupGuid, 6);
        data.WriteGuidMask(leaderGuid, 4);
        data.WriteGuidMask(groupGuid, 3);

        data.WriteGuidBytes(leaderGuid, 0);
        data.WriteGuidBytes(groupGuid, 1);
        data.WriteGuidBytes(leaderGuid, 4, 2);
        data.WriteGuidBytes(groupGuid, 6, 4);

        data << uint8(0x10);
        data << uint8(0);
        data << int32(-1);

        data.WriteGuidBytes(groupGuid, 7);
        data.WriteGuidBytes(leaderGuid, 3, 1);

        data << uint32(m_counter++);

        data.WriteGuidBytes(groupGuid, 0, 2, 5, 3);
        data.WriteGuidBytes(leaderGuid, 7);

        data << uint8(0);

        data.WriteGuidBytes(leaderGuid, 5, 6);

        player->SendDirectMessage(&data);

        return;
    }

    if (player->GetGroup() != this)
        return;

    uint8 groupPosition = 0;
    uint8 i = 0;

    for (member_citerator citr = m_memberSlots.begin(); citr != m_memberSlots.end(); ++citr)
    {
        if (citr->group != slot->group)
            continue;

        if (citr->Guid == slot->Guid)
        {
            groupPosition = i;
            break;
        }

        i++;
    }

    ByteBuffer memberData;
    ObjectGuid groupGuid = m_guid;
    ObjectGuid leaderGuid = m_leaderGuid;
    ObjectGuid looterGuid = m_lootMethod == MASTER_LOOT ? m_masterLooterGuid : ObjectGuid::Empty;

    WorldPacket data(SMSG_GROUP_LIST, 3 * (1 + 8) + 3 + GetMembersCount() * (1 + 8 + 1 + 1 + 1 + 1 + 1) + (IsLFGGroup() ? (1 + 4 + 1 + 1 + 4 + 1 + 1 + 1 + 4) : 0) + 4 + 4 + 1 + 1 + 1 + 1 + 4 + 4 + 1);

    data.WriteGuidMask(groupGuid, 0);
    data.WriteGuidMask(leaderGuid, 7, 1);

    data.WriteBit(1);                                   // has dungeon and raid difficulty

    data.WriteGuidMask(groupGuid, 7);
    data.WriteGuidMask(leaderGuid, 6, 5);

    data.WriteBits(GetMembersCount(), 21);

    for (member_citerator citr = m_memberSlots.begin(); citr != m_memberSlots.end(); ++citr)
    {
        ObjectGuid memberGuid = citr->Guid;
        std::string memberName = citr->name;

        Player* member = ObjectAccessor::FindPlayer(memberGuid);

        uint8 onlineState = member ? MEMBER_STATUS_ONLINE : MEMBER_STATUS_OFFLINE;
        onlineState = onlineState | ((IsBGGroup() || IsBFGroup()) ? MEMBER_STATUS_PVP : 0);

        data.WriteGuidMask(memberGuid, 1, 2, 5, 6);

        data.WriteBits(memberName.size(), 6);

        data.WriteGuidMask(memberGuid, 7, 3, 0, 4);

        memberData.WriteGuidBytes(memberGuid, 6, 3);

        memberData << uint8(citr->roles);
        memberData << uint8(onlineState);

        memberData.WriteGuidBytes(memberGuid, 7, 4, 1);

        memberData.WriteString(memberName);

        memberData.WriteGuidBytes(memberGuid, 5, 2);

        memberData << uint8(citr->group);

        memberData.WriteGuidBytes(memberGuid, 0);

        memberData << uint8(citr->flags);
    }

    data.WriteGuidMask(leaderGuid, 3, 0);

    data.WriteBit(1);                                   // has loot mode

    data.WriteGuidMask(groupGuid, 5);

    //if (hasLootMode)
    {
        data.WriteGuidMask(looterGuid, 6, 4, 5, 2, 1, 0, 7, 3);
    }

    data.WriteGuidMask(groupGuid, 2, 4);
    data.WriteGuidMask(groupGuid, 1);

    data.WriteBit(IsLFGGroup());    // is LFG

    data.WriteGuidMask(leaderGuid, 2);
    data.WriteGuidMask(groupGuid, 6);

    if (IsLFGGroup())
    {
        data.WriteBit(0); // LfgAborted (not sure)
        data.WriteBit(0); // MyLfgFirstReward (not sure)
    }

    data.WriteGuidMask(leaderGuid, 4);
    data.WriteGuidMask(groupGuid, 3);

    data.FlushBits();

    data.WriteGuidBytes(leaderGuid, 0);

    //if (hasInstanceDifficulty)
    {
        data << uint32(m_raidDifficulty);               // raid Difficulty
        data << uint32(m_dungeonDifficulty);            // dungeon Difficulty
    }

    data.append(memberData);

    data.WriteGuidBytes(groupGuid, 1);

    if (IsLFGGroup())
    {
        uint32 dungeonId = sLFGMgr->GetDungeon(m_guid);
        if (LFGDungeonEntry const* dungeon = sLFGDungeonStore.LookupEntry(dungeonId))
        {
            data << float(0.0f);                    // MyLfgGearDiff
            data << uint8(dungeon->Type);
            data << uint8(0);                       // MyLfgKickVoteCount (not sure)
            data << uint32(dungeon->Entry());
            data << uint8(0);                       // LfgBootCount (not sure)
            data << uint8(0);                       // MyLfgPartialClear (not sure)
            data << uint8(GetMembersCount() - 1);
            data << uint32(0);                      // MyLfgRandomSlot (not sure)
        }
    }

    data.WriteGuidBytes(leaderGuid, 4, 2);

    //if (hasLootMode)
    {
        data << uint8(m_lootMethod);

        data.WriteGuidBytes(looterGuid, 0, 5, 4, 3, 2);

        data << uint8(m_lootThreshold);

        data.WriteGuidBytes(looterGuid, 7, 1, 6);
    }

    data.WriteGuidBytes(groupGuid, 6, 4);

    data << uint8(m_groupType);
    data << uint8(0);
    data << uint32(groupPosition);

    data.WriteGuidBytes(groupGuid, 7);
    data.WriteGuidBytes(leaderGuid, 3, 1);

    data << uint32(m_counter++);

    data.WriteGuidBytes(groupGuid, 0, 2, 5, 3);
    data.WriteGuidBytes(leaderGuid, 7);

    data << uint8(0);

    data.WriteGuidBytes(leaderGuid, 5, 6);

    player->SendDirectMessage(&data);

    /*data << uint8(m_groupType);                         // group type (flags in 3.3)
    data << uint8(slot->group);
    data << uint8(slot->flags);
    data << uint8(slot->roles);
    if (IsLFGGroup())
    {
        data << uint8(sLFGMgr->GetState(m_guid) == LFG_STATE_FINISHED_DUNGEON ? 2 : 0); // FIXME - Dungeon save status? 2 = done
        data << uint32(sLFGMgr->GetDungeon(m_guid));
        data << uint8(0); // 4.x new
    }

    data << uint64(m_guid);
    data << uint32(m_counter++);                        // 3.3, value increases every time this packet gets sent
    data << uint32(GetMembersCount()-1);
    for (member_citerator citr = m_memberSlots.begin(); citr != m_memberSlots.end(); ++citr)
    {
        if (slot->guid == citr->guid)
            continue;

        Player* member = ObjectAccessor::FindConnectedPlayer(citr->guid);

        uint8 onlineState = (member && !member->GetSession()->PlayerLogout()) ? MEMBER_STATUS_ONLINE : MEMBER_STATUS_OFFLINE;
        onlineState = onlineState | ((IsBGGroup() || IsBFGroup()) ? MEMBER_STATUS_PVP : 0);

        data << citr->name;
        data << uint64(citr->guid);                     // guid
        data << uint8(onlineState);                     // online-state
        data << uint8(citr->group);                     // groupid
        data << uint8(citr->flags);                     // See enum GroupMemberFlags
        data << uint8(citr->roles);                     // Lfg Roles
    }

    data << uint64(m_leaderGuid);                       // leader guid

    if (GetMembersCount() - 1)
    {
        data << uint8(m_lootMethod);                    // loot method

        if (m_lootMethod == MASTER_LOOT)
            data << uint64(m_masterLooterGuid);         // master looter guid
        else
            data << uint64(0);

        data << uint8(m_lootThreshold);                 // loot threshold
        data << uint8(m_dungeonDifficulty);             // Dungeon Difficulty
        data << uint8(m_raidDifficulty);                // Raid Difficulty
    }*/
}

void Group::SendReadyCheckCompleted()
{
    ObjectGuid guid = m_guid;

    WorldPacket data(SMSG_RAID_READY_CHECK_COMPLETED, 1 + 8 + 1);
    data.WriteGuidMask(guid, 4, 2, 5, 7, 1, 0, 3, 6);

    data.WriteGuidBytes(guid, 6, 0, 3, 1, 5);

    data << uint8(0);

    data.WriteGuidBytes(guid, 7, 2, 4);

    BroadcastPacket(&data, false);
}

void Group::UpdatePlayerOutOfRange(Player* player)
{
    if (!player || !player->IsInWorld())
        return;

    std::vector<Player*> groupMembers;
    for (GroupReference* itr = GetFirstMember(); itr != nullptr; itr = itr->next())
    {
        Player* member = itr->GetSource();
        if (!member)
            continue;

        if (member->GetGUID() == player->GetGUID())
            continue;

        if (!member->IsInMap(player) || !member->IsWithinDist(player, member->GetSightRange(), false))
            groupMembers.push_back(member);
    }

    if (groupMembers.empty())
        return;

    WorldPacket data;
    player->GetSession()->BuildPartyMemberStatsChangedPacket(player, &data);

    for (Player* member : groupMembers)
        member->SendDirectMessage(&data);
}

void Group::BroadcastAddonMessagePacket(WorldPacket* packet, const std::string& prefix, bool ignorePlayersInBGRaid, int group, ObjectGuid ignore)
{
    for (GroupReference* itr = GetFirstMember(); itr != NULL; itr = itr->next())
    {
        Player* player = itr->GetSource();
        if (!player || (ignore != 0 && player->GetGUID() == ignore) || (ignorePlayersInBGRaid && player->GetGroup() != this))
            continue;

        if (WorldSession* session = player->GetSession())
            if (session && (group == -1 || itr->GetSubGroup() == group))
                if (session->IsAddonRegistered(prefix))
                    session->SendPacket(packet);
    }
}

void Group::BroadcastPacket(WorldPacket* packet, bool ignorePlayersInBGRaid, int group, ObjectGuid ignore)
{
    for (GroupReference* itr = GetFirstMember(); itr != NULL; itr = itr->next())
    {
        Player* player = itr->GetSource();
        if (!player || (!ignore.IsEmpty() && player->GetGUID() == ignore) || (ignorePlayersInBGRaid && player->GetGroup() != this))
            continue;

        if (player->GetSession() && (group == -1 || itr->GetSubGroup() == group))
            player->SendDirectMessage(packet);
    }
}

void Group::BroadcastLeadersPacket(WorldPacket* packet)
{
    for (GroupReference* itr = GetFirstMember(); itr != NULL; itr = itr->next())
    {
        Player* player = itr->GetSource();
        if (player && player->GetSession())
            if (!IsRaidGroup() || (IsLeader(player->GetGUID()) || IsAssistant(player->GetGUID()) || m_groupType & GROUPTYPE_EVERYONE_ASSISTANT))
                player->SendDirectMessage(packet);
    }
}

void Group::OfflineReadyCheck()
{
    ObjectGuid groupGuid = GetGUID();

    for (member_citerator citr = m_memberSlots.begin(); citr != m_memberSlots.end(); ++citr)
    {
        Player* player = ObjectAccessor::FindConnectedPlayer(citr->Guid);
        if (!player || !player->GetSession())
        {
            ObjectGuid playerGuid = citr->Guid;

            WorldPacket data(SMSG_RAID_READY_CHECK_CONFIRM, 2 * (1 + 8) + 1);

            data.WriteGuidMask(groupGuid, 4);

            data.WriteGuidMask(playerGuid, 5, 3);

            data.WriteBit(0);

            data.WriteGuidMask(groupGuid, 2);

            data.WriteGuidMask(playerGuid, 6);

            data.WriteGuidMask(groupGuid, 3);

            data.WriteGuidMask(playerGuid, 0, 1);

            data.WriteGuidMask(groupGuid, 1, 5);

            data.WriteGuidMask(playerGuid, 7,4);

            data.WriteGuidMask(groupGuid, 6);

            data.WriteGuidMask(playerGuid, 2);

            data.WriteGuidMask(groupGuid, 0, 7);

            data.FlushBits();

            data.WriteGuidBytes(playerGuid, 4, 2, 1);

            data.WriteGuidBytes(groupGuid, 4, 2);

            data.WriteGuidBytes(playerGuid, 0);

            data.WriteGuidBytes(groupGuid, 5, 3);

            data.WriteGuidBytes(playerGuid, 7);

            data.WriteGuidBytes(groupGuid, 6, 1);

            data.WriteGuidBytes(playerGuid, 6, 3, 5);

            data.WriteGuidBytes(groupGuid, 0, 7);

            BroadcastPacket(&data, false);

            ReadyCheckMemberHasResponded(playerGuid);
        }
    }
}

bool Group::_SetMembersGroup(ObjectGuid guid, uint8 group)
{
    member_witerator slot = _GetMemberWSlot(guid);
    if (slot == m_memberSlots.end())
        return false;

    slot->group = group;

    SubGroupCounterIncrease(group);

    if (!IsBGGroup() && !IsBFGroup())
    {
        PreparedStatement* stmt = CharacterDatabase.GetPreparedStatement(CHAR_UPD_GROUP_MEMBER_SUBGROUP);

        stmt->setUInt8(0, group);
        stmt->setUInt32(1, guid.GetCounter());

        CharacterDatabase.Execute(stmt);
    }

    return true;
}

bool Group::SameSubGroup(Player const* member1, Player const* member2) const
{
    if (!member1 || !member2)
        return false;

    if (member1->GetGroup() != this || member2->GetGroup() != this)
        return false;
    else
        return member1->GetSubGroup() == member2->GetSubGroup();
}

// Allows setting sub groups both for online or offline members
void Group::ChangeMembersGroup(ObjectGuid guid, uint8 group)
{
    // Only raid groups have sub groups
    if (!IsRaidGroup())
        return;

    // Check if player is really in the raid
    member_witerator slot = _GetMemberWSlot(guid);
    if (slot == m_memberSlots.end())
        return;

    // Abort if the player is already in the target sub group
    uint8 prevSubGroup = GetMemberGroup(guid);
    if (prevSubGroup == group)
        return;

    // Update the player slot with the new sub group setting
    slot->group = group;

    // Increase the counter of the new sub group..
    SubGroupCounterIncrease(group);

    // ..and decrease the counter of the previous one
    SubGroupCounterDecrease(prevSubGroup);

    // Preserve new sub group in database for non-raid groups
    if (!IsBGGroup() && !IsBFGroup())
    {
        PreparedStatement* stmt = CharacterDatabase.GetPreparedStatement(CHAR_UPD_GROUP_MEMBER_SUBGROUP);

        stmt->setUInt8(0, group);
        stmt->setUInt32(1, guid.GetCounter());

        CharacterDatabase.Execute(stmt);
    }

    // In case the moved player is online, update the player object with the new sub group references
    if (Player* player = ObjectAccessor::FindConnectedPlayer(guid))
    {
        if (player->GetGroup() == this)
            player->GetGroupRef().SetSubGroup(group);
        else
        {
            // If player is in BG raid, it is possible that he is also in normal raid - and that normal raid is stored in m_originalGroup reference
            prevSubGroup = player->GetOriginalSubGroup();
            player->GetOriginalGroupRef().SetSubGroup(group);
        }
    }

    // Broadcast the changes to the group
    SendUpdate();
}

// Retrieve the next Round-Roubin player for the group
//
// No update done if loot method is FFA.
//
// If the RR player is not yet set for the group, the first group member becomes the round-robin player.
// If the RR player is set, the next player in group becomes the round-robin player.
//
// If ifneed is true,
//      the current RR player is checked to be near the looted object.
//      if yes, no update done.
//      if not, he loses his turn.
void Group::UpdateLooterGuid(WorldObject* pLootedObject, bool ifneed)
{
    switch (GetLootMethod())
    {
        case MASTER_LOOT:
        case FREE_FOR_ALL:
            return;
        default:
            // round robin style looting applies for all low
            // quality items in each loot method except free for all and master loot
            break;
    }

    ObjectGuid oldLooterGUID = GetLooterGuid();
    member_citerator guid_itr = _GetMemberCSlot(oldLooterGUID);
    if (guid_itr != m_memberSlots.end())
    {
        if (ifneed)
        {
            // not update if only update if need and ok
            Player* looter = ObjectAccessor::FindPlayer(guid_itr->Guid);
                if (looter && looter->IsAtGroupRewardDistance(pLootedObject))
                    return;
        }
        ++guid_itr;
    }

    // search next after current
    Player* pNewLooter = NULL;
    for (member_citerator itr = guid_itr; itr != m_memberSlots.end(); ++itr)
    {
        if (Player* player = ObjectAccessor::FindPlayer(itr->Guid))
            if (player->IsAtGroupRewardDistance(pLootedObject))
            {
                pNewLooter = player;
                break;
            }
    }

    if (!pNewLooter)
    {
        // search from start
        for (member_citerator itr = m_memberSlots.begin(); itr != guid_itr; ++itr)
        {
            if (Player* player = ObjectAccessor::FindPlayer(itr->Guid))
                if (player->IsAtGroupRewardDistance(pLootedObject))
                {
                    pNewLooter = player;
                    break;
                }
        }
    }

    if (pNewLooter)
    {
        if (oldLooterGUID != pNewLooter->GetGUID())
        {
            SetLooterGuid(pNewLooter->GetGUID());
            SendUpdate();
        }
    }
    else
    {
        SetLooterGuid(ObjectGuid::Empty);
        SendUpdate();
    }
}

GroupJoinBattlegroundResult Group::CanJoinBattlegroundQueue(Battleground const* bgOrTemplate, BattlegroundQueueTypeId bgQueueTypeId, uint32 MinPlayerCount, uint32 /*MaxPlayerCount*/, bool IsRated, uint32 arenaSlot, ObjectGuid& errorGuid, bool IsWarGame)
{
    // check if this group is LFG group
    if (IsLFGGroup())
        return ERR_LFG_CANT_USE_BATTLEGROUND;

    BattlemasterListEntry const* bgEntry = sBattlemasterListStore.LookupEntry(bgOrTemplate->GetTypeID());
    if (!bgEntry)
        return ERR_BATTLEGROUND_JOIN_FAILED;                // shouldn't happen

    // check for min / max count
    uint32 memberscount = GetMembersCount();

    if (memberscount > bgEntry->MaxGroupSize)               // no MinPlayerCount for battlegrounds
        return ERR_BATTLEGROUND_JOIN_FAILED;                // ERR_GROUP_JOIN_BATTLEGROUND_TOO_MANY handled on client side

    // get a player as reference, to compare other players' stats to (queue id based on level, etc.)
    Player* reference = GetFirstMember()->GetSource();
    // no reference found, can't join this way
    if (!reference)
        return ERR_BATTLEGROUND_JOIN_FAILED;

    PvPDifficultyEntry const* bracketEntry = sDBCManager->GetBattlegroundBracketByLevel(bgOrTemplate->GetMapId(), reference->GetLevel());
    if (!bracketEntry)
        return ERR_BATTLEGROUND_JOIN_FAILED;

    uint32 team = reference->GetTeam();

    BattlegroundQueueTypeId bgQueueTypeIdRandom = BattlegroundMgr::BGQueueTypeId(BATTLEGROUND_RB, RATED_TYPE_NOT_RATED);
    BattlegroundQueueTypeId bgQueueTypeIdRated = BattlegroundMgr::BGQueueTypeId(BATTLEGROUND_RATED_10_VS_10, RATED_TYPE_10v10);

    // check every member of the group to be able to join
    memberscount = 0;
    for (GroupReference* itr = GetFirstMember(); itr != NULL; itr = itr->next(), ++memberscount)
    {
        Player* member = itr->GetSource();

        // offline member? don't let join
        if (!member)
            return ERR_BATTLEGROUND_JOIN_FAILED;

        // don't allow cross-faction join as group
        if (member->GetTeam() != team)
        {
            errorGuid = member->GetGUID();
            return ERR_BATTLEGROUND_JOIN_TIMED_OUT;
        }

        // not in the same battleground level braket, don't let join
        PvPDifficultyEntry const* memberBracketEntry = sDBCManager->GetBattlegroundBracketByLevel(bracketEntry->MapID, member->GetLevel());
        if (memberBracketEntry != bracketEntry)
            return ERR_BATTLEGROUND_JOIN_RANGE_INDEX;

        // don't let join if someone from the group is already in that bg queue
        if (member->InBattlegroundQueueForBattlegroundQueueType(bgQueueTypeId))
            return ERR_BATTLEGROUND_JOIN_FAILED;            // not blizz-like

        // don't let join if someone from the group is in bg queue random
        if (member->InBattlegroundQueueForBattlegroundQueueType(bgQueueTypeIdRandom))
            return ERR_IN_RANDOM_BG;

        // don't let join if someone from the group is in rated queue
        if (member->InBattlegroundQueueForBattlegroundQueueType(bgQueueTypeIdRated))
            return ERR_BATTLEDGROUND_QUEUED_FOR_RATED;

        // don't let join to bg/arena/rated queue if someone from the group is already in bg queue
        switch (bgOrTemplate->GetTypeID())
        {
            case BATTLEGROUND_RB:
                if (member->InBattlegroundQueue())
                    return ERR_IN_NON_RANDOM_BG;
                break;
            case BATTLEGROUND_RATED_10_VS_10:
            case BATTLEGROUND_AA:
                if (member->InBattlegroundQueue())
                    return ERR_BATTLEGROUND_CANNOT_QUEUE_FOR_RATED;
                break;
            default:
                break;
        }

        // check for deserter debuff in case not arena queue
        if (bgOrTemplate->GetTypeID() != BATTLEGROUND_AA && !member->CanJoinToBattleground(bgOrTemplate))
            return ERR_GROUP_JOIN_BATTLEGROUND_DESERTERS;

        // check if member can join any more battleground queues
        if (!member->HasFreeBattlegroundQueueId())
            return ERR_BATTLEGROUND_TOO_MANY_QUEUES;        // not blizz-like

        // check if someone in party is using dungeon system
        if (member->IsUsingLfg())
            return ERR_LFG_CANT_USE_BATTLEGROUND;

        // check Freeze debuff
        if (member->HasAura(9454))
            return ERR_BATTLEGROUND_JOIN_FAILED;

        // check is someone in party is loading or teleporting
        if (member->GetSession()->PlayerLoading() || member->IsBeingTeleported())
            return ERR_BATTLEGROUND_JOIN_FAILED;

        if (member->IsLockedToRatedBG())
            return ERR_BATTLEDGROUND_QUEUED_FOR_RATED;

        if (IsWarGame)
            if (member->InBattlegroundQueue())
                return ERR_BATTLEGROUND_JOIN_FAILED;
    }

    // only check for MinPlayerCount since MinPlayerCount == MaxPlayerCount for rated...
    if (IsRated && memberscount != MinPlayerCount)
        return ERR_ARENA_TEAM_PARTY_SIZE;

    return ERR_BATTLEGROUND_NONE;
}

GroupRatedStats Group::GetRatedStats(RatedType ratedType)
{
    uint32 matchMakerRating = 0;
    uint32 rating = 0;
    uint32 playerDivider = 0;

    for (GroupReference* itr = GetFirstMember(); itr != NULL; itr = itr->next())
    {
        Player* member = itr->GetSource();
        if (!member)
            continue;   // this should never happen

        // Skip if player is not online
        if (!ObjectAccessor::FindPlayer(member->GetGUID()))
            continue;

        RatedInfo* rinfo = sRatedMgr->GetRatedInfo(member->GetGUID());
        if (!rinfo)
            continue;

        StatsBySlot* stats = rinfo->GetStatsBySlot(ratedType);
        if (!stats)
            continue;

        matchMakerRating += rinfo->GetMatchMakerRating();
        rating += stats->PersonalRating;
        ++playerDivider;
    }

    // x/0 = crash
    if (playerDivider == 0)
        playerDivider = 1;

    matchMakerRating /= playerDivider;
    rating /= playerDivider;

    GroupRatedStats groupStats;
    groupStats.averageMMR = matchMakerRating;
    groupStats.averageRating = rating;

    return groupStats;
}

//===================================================
//============== Roll ===============================
//===================================================

void Roll::TargetObjectBuildLink()
{
    // called from link()
    getTarget()->AddLootValidatorRef(this);
}

void Group::SetDungeonDifficultyID(Difficulty difficulty)
{
    m_dungeonDifficulty = difficulty;
    if (!IsBGGroup() && !IsBFGroup())
    {
        PreparedStatement* stmt = CharacterDatabase.GetPreparedStatement(CHAR_UPD_GROUP_DIFFICULTY);

        stmt->setUInt8(0, uint8(m_dungeonDifficulty));
        stmt->setUInt32(1, m_dbStoreId);

        CharacterDatabase.Execute(stmt);
    }

    for (GroupReference* itr = GetFirstMember(); itr != NULL; itr = itr->next())
    {
        Player* player = itr->GetSource();
        if (!player->GetSession())
            continue;

        player->SetDungeonDifficultyID(difficulty);
        player->SendDungeonDifficulty();
    }
}

void Group::SetRaidDifficultyID(Difficulty difficulty)
{
    m_raidDifficulty = difficulty;
    if (!IsBGGroup() && !IsBFGroup())
    {
        PreparedStatement* stmt = CharacterDatabase.GetPreparedStatement(CHAR_UPD_GROUP_RAID_DIFFICULTY);

        stmt->setUInt8(0, uint8(m_raidDifficulty));
        stmt->setUInt32(1, m_dbStoreId);

        CharacterDatabase.Execute(stmt);
    }

    for (GroupReference* itr = GetFirstMember(); itr != NULL; itr = itr->next())
    {
        Player* player = itr->GetSource();
        if (!player->GetSession())
            continue;

        player->SetRaidDifficultyID(difficulty);
        player->SendRaidDifficulty();
    }
}

void Group::SetScenarioDifficultyID(Difficulty difficulty)
{
    m_scenarioDifficulty = difficulty;
    if (!IsBGGroup() && !IsBFGroup())
    {
        PreparedStatement* stmt = CharacterDatabase.GetPreparedStatement(CHAR_UPD_GROUP_SCENARIO_DIFFICULTY);

        stmt->setUInt8(0, uint8(m_scenarioDifficulty));
        stmt->setUInt32(1, m_dbStoreId);

        CharacterDatabase.Execute(stmt);
    }

    for (GroupReference* itr = GetFirstMember(); itr != NULL; itr = itr->next())
    {
        Player* player = itr->GetSource();
        if (!player->GetSession())
            continue;

        player->SetScenarioDifficultyID(difficulty);
    }
}

Difficulty Group::GetDifficultyID(MapEntry const* mapEntry) const
{
    if (!mapEntry->IsRaid())
    {
        if (mapEntry->IsScenario())
            return m_scenarioDifficulty;
        else
            return m_dungeonDifficulty;
    }

    return m_raidDifficulty;
}

void Group::ResetInstances(uint8 method, uint8 type, Player* SendMsgTo)
{
    if (IsBGGroup() || IsBFGroup())
        return;

    // method can be INSTANCE_RESET_ALL, INSTANCE_RESET_CHANGE_DIFFICULTY, INSTANCE_RESET_GROUP_DISBAND

    for (uint8 i = 0; type != 0; type >>= 1, ++i)
        if ((type & 1) != 0)
        {
            // we assume that when the difficulty changes, all instances that can be reset will be
            Difficulty diff = DIFFICULTY_NONE;

            switch (type)
            {
                case RESET_TYPE_INSTANCE:
                    diff = GetDungeonDifficultyID();
                    break;
                case RESET_TYPE_RAID:
                    diff = GetRaidDifficultyID();
                    break;
                case RESET_TYPE_SCENARIO:
                    diff = GetScenarioDifficultyID();
                    break;
            }

            if (diff == DIFFICULTY_NONE)
                continue;

            DifficultyEntry const* difficultyEntry = sDifficultyStore.LookupEntry(diff);
            if (!difficultyEntry)
                continue;

            for (BoundInstancesMap::iterator itr = m_boundInstances[diff].begin(); itr != m_boundInstances[diff].end();)
            {
                InstanceSave* instanceSave = itr->second.save;
                const MapEntry* entry = sMapStore.LookupEntry(itr->first);
                if (!entry || (!instanceSave->CanReset() && method != INSTANCE_RESET_GROUP_DISBAND))
                {
                    ++itr;
                    continue;
                }

                if (method == INSTANCE_RESET_ALL)
                {
                    // the "reset all instances" method can only reset normal maps
                    if (entry->InstanceType == MAP_RAID || (difficultyEntry->Flags & DIFFICULTY_FLAG_HEROIC) != 0)
                    {
                        ++itr;
                        continue;
                    }
                }

                bool isEmpty = true;
                // if the map is loaded, reset it
                Map* map = sMapMgr->FindMap(instanceSave->GetMapId(), instanceSave->GetInstanceId());
                if (map && map->IsDungeon() && !(method == INSTANCE_RESET_GROUP_DISBAND && !instanceSave->CanReset()))
                {
                    if (instanceSave->CanReset())
                        isEmpty = ((InstanceMap*)map)->Reset(method);
                    else
                        isEmpty = !map->HavePlayers();
                }

                if (SendMsgTo)
                {
                    if (!isEmpty)
                        SendMsgTo->SendResetInstanceFailed(INSTANCE_RESET_FAILED, instanceSave->GetMapId());
                    else if (sWorld->getBoolConfig(CONFIG_INSTANCES_RESET_ANNOUNCE))
                    {
                        if (Group* group = SendMsgTo->GetGroup())
                        {
                            for (GroupReference* itr = group->GetFirstMember(); itr != NULL; itr = itr->next())
                                if (Player* player = itr->GetSource())
                                    player->SendResetInstanceSuccess(instanceSave->GetMapId());
                        }

                        else
                            SendMsgTo->SendResetInstanceSuccess(instanceSave->GetMapId());
                    }
                    else
                        SendMsgTo->SendResetInstanceSuccess(instanceSave->GetMapId());
                }

                if (isEmpty || method == INSTANCE_RESET_GROUP_DISBAND || method == INSTANCE_RESET_CHANGE_DIFFICULTY)
                {
                    // do not reset the instance, just unbind if others are permanently bound to it
                    if (isEmpty && instanceSave->CanReset())
                        instanceSave->DeleteFromDB();
                    else
                    {
                        PreparedStatement* stmt = CharacterDatabase.GetPreparedStatement(CHAR_DEL_GROUP_INSTANCE_BY_INSTANCE);

                        stmt->setUInt32(0, instanceSave->GetInstanceId());

                        CharacterDatabase.Execute(stmt);
                    }


                    // i don't know for sure if hash_map iterators
                    m_boundInstances[diff].erase(itr);
                    itr = m_boundInstances[diff].begin();
                    // this unloads the instance save unless online players are bound to it
                    // (eg. permanent binds or GM solo binds)
                    instanceSave->RemoveGroup(this);
                }
                else
                    ++itr;
            }
        }
}

InstanceGroupBind* Group::GetBoundInstance(Player* player)
{
    uint32 mapid = player->GetMapId();
    MapEntry const* mapEntry = sMapStore.LookupEntry(mapid);
    return GetBoundInstance(mapEntry);
}

InstanceGroupBind* Group::GetBoundInstance(Map* aMap)
{
    return GetBoundInstance(aMap->GetEntry());
}

InstanceGroupBind* Group::GetBoundInstance(MapEntry const* mapEntry)
{
    if (!mapEntry || !mapEntry->IsDungeon())
        return NULL;

    Difficulty difficulty = GetDifficultyID(mapEntry);
    return GetBoundInstance(difficulty, mapEntry->ID);
}

InstanceGroupBind* Group::GetBoundInstance(Difficulty difficulty, uint32 mapId)
{
    // some instances only have one difficulty
    sDBCManager->GetDownscaledMapDifficultyData(mapId, difficulty);

    BoundInstancesMap::iterator itr = m_boundInstances[difficulty].find(mapId);
    if (itr != m_boundInstances[difficulty].end())
        return &itr->second;
    else
        return NULL;
}

InstanceGroupBind* Group::BindToInstance(InstanceSave* save, bool permanent, bool load)
{
    if (!save || IsBGGroup() || IsBFGroup())
        return NULL;

    InstanceGroupBind& bind = m_boundInstances[save->GetDifficultyID()][save->GetMapId()];
    if (!load && (!bind.save || permanent != bind.perm || save != bind.save))
    {
        PreparedStatement* stmt = CharacterDatabase.GetPreparedStatement(CHAR_REP_GROUP_INSTANCE);

        stmt->setUInt32(0, m_dbStoreId);
        stmt->setUInt32(1, save->GetInstanceId());
        stmt->setBool(2, permanent);

        CharacterDatabase.Execute(stmt);
    }

    if (bind.save != save)
    {
        if (bind.save)
            bind.save->RemoveGroup(this);
        save->AddGroup(this);
    }

    bind.save = save;
    bind.perm = permanent;
    if (!load)
        TC_LOG_DEBUG("maps", "Group::BindToInstance: %s, storage id: %u is now bound to map %d, instance %d, difficulty %d",
            GetGUID().ToString().c_str(), m_dbStoreId, save->GetMapId(), save->GetInstanceId(), save->GetDifficultyID());

    return &bind;
}

void Group::UnbindInstance(uint32 mapid, uint8 difficulty, bool unload)
{
    BoundInstancesMap::iterator itr = m_boundInstances[difficulty].find(mapid);
    if (itr != m_boundInstances[difficulty].end())
    {
        if (!unload)
        {
            PreparedStatement* stmt = CharacterDatabase.GetPreparedStatement(CHAR_DEL_GROUP_INSTANCE_BY_GUID);

            stmt->setUInt32(0, m_dbStoreId);
            stmt->setUInt32(1, itr->second.save->GetInstanceId());

            CharacterDatabase.Execute(stmt);
        }

        itr->second.save->RemoveGroup(this);                // save can become invalid
        m_boundInstances[difficulty].erase(itr);
    }
}

void Group::_HomebindIfInstance(Player* player)
{
    if (player && !player->IsGameMaster() && sMapStore.LookupEntry(player->GetMapId())->IsDungeon())
        player->m_InstanceValid = false;
}

void Group::BroadcastGroupUpdate(void)
{
    // FG: HACK: force flags update on group leave - for values update hack
    // -- not very efficient but safe
    for (member_citerator citr = m_memberSlots.begin(); citr != m_memberSlots.end(); ++citr)
    {
        Player* pp = ObjectAccessor::FindPlayer(citr->Guid);
        if (pp)
        {
            pp->ForceValuesUpdateAtIndex(UNIT_BYTES_2);
            pp->ForceValuesUpdateAtIndex(UNIT_FACTION_TEMPLATE);
            TC_LOG_DEBUG("misc", "-- Forced group value update for '%s'", pp->GetName().c_str());
        }
    }
}

void Group::ResetMaxEnchantingLevel()
{
    m_maxEnchantingLevel = 0;
    Player* member = NULL;
    for (member_citerator citr = m_memberSlots.begin(); citr != m_memberSlots.end(); ++citr)
    {
        member = ObjectAccessor::FindPlayer(citr->Guid);
        if (member && m_maxEnchantingLevel < member->GetSkillValue(SKILL_ENCHANTING))
            m_maxEnchantingLevel = member->GetSkillValue(SKILL_ENCHANTING);
    }
}

void Group::SetLootMethod(LootMethod method)
{
    m_lootMethod = method;
}

void Group::SetLooterGuid(ObjectGuid guid)
{
    m_looterGuid = guid;
}

void Group::SetMasterLooterGuid(ObjectGuid guid)
{
    m_masterLooterGuid = guid;
}

void Group::SetLootThreshold(ItemQualities threshold)
{
    m_lootThreshold = threshold;
}

bool Group::IsFull() const
{
    return IsRaidGroup() ? (m_memberSlots.size() >= MAXRAIDSIZE) : (m_memberSlots.size() >= MAXGROUPSIZE);
}

bool Group::IsLFGGroup() const
{
    return m_groupType & GROUPTYPE_LFG;
}

bool Group::IsRaidGroup() const
{
    return m_groupType & GROUPTYPE_RAID;
}

bool Group::IsBGGroup() const
{
    return m_bgGroup != NULL;
}

bool Group::IsBFGroup() const
{
    return m_bfGroup != NULL;
}

bool Group::IsCreated() const
{
    return GetMembersCount() > 0;
}

ObjectGuid Group::GetLeaderGUID() const
{
    return m_leaderGuid;
}

ObjectGuid Group::GetGUID() const
{
    return m_guid;
}

uint32 Group::GetLowGUID() const
{
    return m_guid.GetCounter();
}

char const* Group::GetLeaderName() const
{
    return m_leaderName.c_str();
}

LootMethod Group::GetLootMethod() const
{
    return m_lootMethod;
}

ObjectGuid Group::GetLooterGuid() const
{
    if (GetLootMethod() == FREE_FOR_ALL)
        return ObjectGuid::Empty;
    return m_looterGuid;
}

ObjectGuid Group::GetMasterLooterGuid() const
{
    return m_masterLooterGuid;
}

ItemQualities Group::GetLootThreshold() const
{
    return m_lootThreshold;
}

bool Group::IsMember(ObjectGuid guid) const
{
    return _GetMemberCSlot(guid) != m_memberSlots.end();
}

bool Group::IsLeader(ObjectGuid guid) const
{
    return (GetLeaderGUID() == guid);
}

ObjectGuid Group::GetMemberGUID(const std::string& name)
{
    for (member_citerator itr = m_memberSlots.begin(); itr != m_memberSlots.end(); ++itr)
        if (itr->name == name)
            return itr->Guid;
    return ObjectGuid::Empty;
}

bool Group::IsAssistant(ObjectGuid guid) const
{
    member_citerator mslot = _GetMemberCSlot(guid);
    if (mslot == m_memberSlots.end())
        return false;
    return mslot->flags & MEMBER_FLAG_ASSISTANT;
}

bool Group::SameSubGroup(ObjectGuid guid1, ObjectGuid guid2) const
{
    member_citerator mslot2 = _GetMemberCSlot(guid2);
    if (mslot2 == m_memberSlots.end())
       return false;
    return SameSubGroup(guid1, &*mslot2);
}

bool Group::SameSubGroup(ObjectGuid guid1, MemberSlot const* slot2) const
{
    member_citerator mslot1 = _GetMemberCSlot(guid1);
    if (mslot1 == m_memberSlots.end() || !slot2)
        return false;
    return (mslot1->group == slot2->group);
}

bool Group::HasFreeSlotSubGroup(uint8 subgroup) const
{
    return (m_subGroupsCounts && m_subGroupsCounts[subgroup] < MAXGROUPSIZE);
}


uint8 Group::GetMemberGroup(ObjectGuid guid) const
{
    member_citerator mslot = _GetMemberCSlot(guid);
    if (mslot == m_memberSlots.end())
       return (MAX_RAID_SUBGROUPS+1);
    return mslot->group;
}

void Group::SetBattlegroundGroup(Battleground* bg)
{
    m_bgGroup = bg;
}

void Group::SetBattlefieldGroup(Battlefield *bg)
{
    m_bfGroup = bg;
}

void Group::SetGroupMemberFlag(ObjectGuid guid, bool apply, GroupMemberFlags flag)
{
    // Assistants, main assistants and main tanks are only available in raid groups
    if (!IsRaidGroup())
        return;

    // Check if player is really in the raid
    member_witerator slot = _GetMemberWSlot(guid);
    if (slot == m_memberSlots.end())
        return;

    // Do flag specific actions, e.g ensure uniqueness
    if (apply)
    {
        switch (flag)
        {
            case MEMBER_FLAG_MAINASSIST:
                RemoveUniqueGroupMemberFlag(MEMBER_FLAG_MAINASSIST);        // Remove main assist flag from current if any.
                break;
            case MEMBER_FLAG_MAINTANK:
                RemoveUniqueGroupMemberFlag(MEMBER_FLAG_MAINTANK);          // Remove main tank flag from current if any.
                break;
            default:
                return;
            }
    }

    // Switch the actual flag
    ToggleGroupMemberFlag(slot, flag, apply);

    // Preserve the new setting in the db
    PreparedStatement* stmt = CharacterDatabase.GetPreparedStatement(CHAR_UPD_GROUP_MEMBER_FLAG);

    stmt->setUInt8(0, slot->flags);
    stmt->setUInt32(1, guid.GetCounter());

    CharacterDatabase.Execute(stmt);

    // Broadcast the changes to the group
    SendUpdate();
}

Group::Rolls::iterator Group::GetRoll(ObjectGuid guid, uint8 ItemIndex)
{
    Rolls::iterator iter;
    for (iter = RollId.begin(); iter != RollId.end();)
    {
        if (!(*iter)->isValid())
            iter = RollId.erase(iter);
        else if ((*iter)->GetLoot()->GetGUID() == guid && (*iter)->ItemIndex == ItemIndex)
            return iter;
        else
            ++iter;
    }
    return RollId.end();
}

void Group::LinkMember(GroupReference* pRef)
{
    m_memberMgr.insertFirst(pRef);
}

void Group::DelinkMember(ObjectGuid guid)
{
    GroupReference* ref = m_memberMgr.getFirst();
    while (ref)
    {
        GroupReference* nextRef = ref->next();
        if (ref->GetSource()->GetGUID() == guid)
        {
            ref->unlink();
            break;
        }
        ref = nextRef;
    }
}

Group::BoundInstancesMap& Group::GetBoundInstances(Difficulty difficulty)
{
    return m_boundInstances[difficulty];
}

void Group::_InitRaidSubGroupsCounter()
{
    // Sub group counters initialization
    if (!m_subGroupsCounts)
        m_subGroupsCounts = new uint8[MAX_RAID_SUBGROUPS];

    memset((void*)m_subGroupsCounts, 0, (MAX_RAID_SUBGROUPS)*sizeof(uint8));

    for (member_citerator itr = m_memberSlots.begin(); itr != m_memberSlots.end(); ++itr)
        ++m_subGroupsCounts[itr->group];
}

Group::member_citerator Group::_GetMemberCSlot(ObjectGuid Guid) const
{
    for (member_citerator itr = m_memberSlots.begin(); itr != m_memberSlots.end(); ++itr)
        if (itr->Guid == Guid)
            return itr;
    return m_memberSlots.end();
}

Group::member_witerator Group::_GetMemberWSlot(ObjectGuid Guid)
{
    for (member_witerator itr = m_memberSlots.begin(); itr != m_memberSlots.end(); ++itr)
        if (itr->Guid == Guid)
            return itr;
    return m_memberSlots.end();
}

void Group::SubGroupCounterIncrease(uint8 subgroup)
{
    if (m_subGroupsCounts)
        ++m_subGroupsCounts[subgroup];
}

void Group::SubGroupCounterDecrease(uint8 subgroup)
{
    if (m_subGroupsCounts)
        --m_subGroupsCounts[subgroup];
}

void Group::RemoveUniqueGroupMemberFlag(GroupMemberFlags flag)
{
    for (member_witerator itr = m_memberSlots.begin(); itr != m_memberSlots.end(); ++itr)
        if (itr->flags & flag)
            itr->flags &= ~flag;
}

void Group::ToggleGroupMemberFlag(member_witerator slot, uint8 flag, bool apply)
{
    if (apply)
        slot->flags |= flag;
    else
        slot->flags &= ~flag;
}

ObjectGuid Group::GetMemberByUniqueGroupMemberFlag(GroupMemberFlags flag)
{
    for (member_witerator itr = m_memberSlots.begin(); itr != m_memberSlots.end(); ++itr)
        if (itr->flags & flag)
            return itr->Guid;

    return ObjectGuid::Empty;
}

void Group::SetMemberRole(ObjectGuid guid, uint32 role)
{
    member_witerator itr = _GetMemberWSlot(guid);
    if (itr == m_memberSlots.end())
        return;

    itr->roles = role;

    PreparedStatement* prepStatement = CharacterDatabase.GetPreparedStatement(CHAR_UPD_GROUP_ROLE);
    prepStatement->setUInt8(0, role);
    prepStatement->setUInt32(1, guid.GetCounter());

    CharacterDatabase.Execute(prepStatement);

    SendUpdate();
}

uint32 Group::GetMemberRole(ObjectGuid guid) const
{
    member_citerator itr = _GetMemberCSlot(guid);
    if (itr == m_memberSlots.end())
        return 0;

    return itr->roles;
}

void Group::ReadyCheckMemberHasResponded(ObjectGuid guid)
{
    member_witerator itr = _GetMemberWSlot(guid);
    if (itr == m_memberSlots.end())
        return;

    itr->readyCheckHasResponded = true;
}

void Group::ReadyCheckResetResponded()
{
    for (member_witerator itr = m_memberSlots.begin(); itr != m_memberSlots.end(); itr++)
        itr->readyCheckHasResponded = false;
}

bool Group::ReadyCheckAllResponded() const
{
    for (member_citerator itr = m_memberSlots.begin(); itr != m_memberSlots.end(); itr++)
        if (!itr->readyCheckHasResponded)
            return false;

    return true;
}

uint32 Group::IsGuildGroupForPlayer(Player* player) const
{
    uint32 guildId = player->GetGuildId();
    if (!guildId)
        return 0;

    Map const* playerMap = player->GetMap();

    uint32 requiredGuildSizeForGroup = 0;
    if (IsBGGroup())
    {
        if (Battleground* bg = m_bgGroup)
        {
            if (bg->IsRatedBattleground())
                requiredGuildSizeForGroup = 0.8 * GetMembersCount();
            else if (bg->IsArena() && bg->IsRated())
                requiredGuildSizeForGroup = GetMembersCount();
        }
    }
    else if (playerMap->IsNonRaidDungeon())
        requiredGuildSizeForGroup = 3;
    else if (playerMap->IsRaid())
    {
        if (playerMap->GetEntry()->Expansion() <= EXPANSION_THE_BURNING_CRUSADE)
            requiredGuildSizeForGroup = 10;
        else
            requiredGuildSizeForGroup = 0.8 * GetMembersCount();
    }

    if (!requiredGuildSizeForGroup)
        return 0;

    uint32 sameGuildSize = 0;
    for (GroupReference const* itr = GetFirstMember(); itr != nullptr; itr = itr->next())
        if (Player* member = itr->GetSource())
        {
            if (member->GetGUID() == player->GetGUID())
                continue;

            if (member->GetGuildId() == guildId)
                if (member->GetMap() == playerMap)
                    ++sameGuildSize;
        }

    if (!sameGuildSize)
        return 0;

    if (sameGuildSize < requiredGuildSizeForGroup)
        return 0;

    return sameGuildSize;
}
