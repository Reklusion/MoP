/*
 * Copyright (C) 2011-2015 Project SkyFire <http://www.projectskyfire.org/>
 * Copyright (C) 2008-2015 TrinityCore <http://www.trinitycore.org/>
 * Copyright (C) 2005-2015 MaNGOS <http://getmangos.com/>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 3 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef BATTLEFIELD_MGR_H_
#define BATTLEFIELD_MGR_H_

#include "Battlefield.h"
#include "ObjectGuid.h"

#include "Singleton/Singleton.hpp"

class Player;
class ZoneScript;

// class to handle player enter / leave / areatrigger / GO use events
class BattlefieldMgr
{
public:
    // ctor
    BattlefieldMgr();
    // dtor
    ~BattlefieldMgr();

    typedef std::vector<Battlefield *> BattlefieldSet;
    typedef std::map<uint32 /* zoneid */, Battlefield *> BattlefieldMap;

    // create battlefield events
    void InitBattlefield();

    // called when a player enters an battlefield area
    void HandlePlayerEnterZone(Player* player, uint32 areaflag);

    // called when player leaves an battlefield area
    void HandlePlayerLeaveZone(Player* player, uint32 areaflag);

    // called when player resurrects
    void HandlePlayerResurrects(Player* player, uint32 areaflag);

    // return assigned battlefield
    Battlefield* GetBattlefieldByZoneId(uint32 zoneid);
    Battlefield* GetBattlefieldByBattleId(uint32 battleid);
    Battlefield* GetBattlefieldByQueueGUID(ObjectGuid guid);

    ZoneScript* GetZoneScript(uint32 zoneId);

    void AddZone(uint32 zoneId, Battlefield* bf);

    void Update(uint32 diff);

    void HandleDropFlag(Player* player, uint32 spellId);

private:
    // contains all initiated battlefield events
    // used when initing / cleaning up
    BattlefieldSet _battlefieldSet;

    // maps the zone ids to an battlefield event
    // used in player event handling
    BattlefieldMap _battlefieldMap;

    // update interval
    uint32 _updateTimer;
};

#define sBattlefieldMgr Tod::Singleton<BattlefieldMgr>::GetSingleton()

#endif // BATTLEFIELD_MGR_H_