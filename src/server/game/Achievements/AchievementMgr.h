/*
* Copyright (C) 2008-2016 TrinityCore <http://www.trinitycore.org/>
* Copyright (C) 2005-2009 MaNGOS <http://getmangos.com/>
*
* This program is free software; you can redistribute it and/or modify it
* under the terms of the GNU General Public License as published by the
* Free Software Foundation; either version 2 of the License, or (at your
* option) any later version.
*
* This program is distributed in the hope that it will be useful, but WITHOUT
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
* more details.
*
* You should have received a copy of the GNU General Public License along
* with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef __TRINITY_ACHIEVEMENTMGR_H
#define __TRINITY_ACHIEVEMENTMGR_H

#include "CriteriaHandler.h"

#include "Singleton/Singleton.hpp"

class Guild;

enum AchievementRewardTypes
{
    ACHIEVEMENT_REWARD_TYPE_NONE                    = -1,

    ACHIEVEMENT_REWARD_TYPE_FACTION_TITLE           = 0,
    ACHIEVEMENT_REWARD_TYPE_GENDER_TITLE            = 1,
    ACHIEVEMENT_REWARD_TYPE_MAIL_TEMPLATE           = 2,
    ACHIEVEMENT_REWARD_TYPE_MAIL                    = 3,
    ACHIEVEMENT_REWARD_TYPE_BATTLE_PET_LOADOUT_FLAG = 4,
    ACHIEVEMENT_REWARD_TYPE_BATTLE_PET_LOADOUT_SLOT = 5,

    MAX_ACHIEVEMENT_REWARD_TYPE
};

struct AchievementReward
{

    AchievementRewardTypes RewardType;
    union
    {
        // ACHIEVEMENT_REWARD_TYPE_FACTION_TITLE
        struct
        {
            uint32 AllianceTitleID;
            uint32 HordeTitleID;
        } FactionTitle;

        // ACHIEVEMENT_REWARD_TYPE_GENDER_TITLE
        struct
        {
            uint32 MaleTitleID;
            uint32 FemaleTitleID;
        } GenderTitle;

        // ACHIEVEMENT_REWARD_TYPE_MAIL_TEMPLATE
        struct
        {
            uint32 MailID;
            uint32 SenderID;
        } MailTemplate;

        // ACHIEVEMENT_REWARD_TYPE_MAIL
        struct
        {
            uint32 ItemID;
            uint32 SenderID;
        } Mail;

        // ACHIEVEMENT_REWARD_TYPE_BATTLE_PET_LOADOUT_FLAG
        struct
        {
            uint32 BattlePetFlag;
        } BattlePetLoadoutFlag;

        // ACHIEVEMENT_REWARD_TYPE_BATTLE_PET_LOADOUT_SLOT
        struct
        {
            uint32 BattlePetSlot;
        } BattlePetLoadoutSlot;

        struct
        {
            uint32 MiscValue1;
            uint32 MiscValue2;
        } Raw;
    };

    std::string MailSubject;
    std::string MailText;

    AchievementReward() : RewardType(ACHIEVEMENT_REWARD_TYPE_NONE)
    {
        Raw.MiscValue1 = 0;
        Raw.MiscValue2 = 0;
    }

    AchievementReward(uint8 type, uint32 value1, uint32 value2, std::string& text1, std::string& text2) : RewardType(AchievementRewardTypes(type))
    {
        Raw.MiscValue1 = value1;
        Raw.MiscValue2 = value2;

        MailSubject = text1;
        MailText = text2;
    }
};

struct AchievementRewardLocale
{
    std::vector<std::string> Subject;
    std::vector<std::string> Body;
};

struct CompletedAchievementData
{
    time_t Date = time_t(0);
    GuidSet CompletingPlayers;
    ObjectGuid FirstGuid;
    bool LoadedFromDB = false;
    bool CompletedByThisCharacter = false;
    AchievementsAndCriteriaDBStatus DBStatus = ACHIEVEMENT_OR_CRITERIA_DB_STATUS_NONE;
};

class AchievementMgr : public CriteriaHandler
{
public:
    AchievementMgr();
    ~AchievementMgr();

    void CheckAllAchievementCriteria(Player* referencePlayer);

    virtual void CompletedAchievement(AchievementEntry const* entry, Player* referencePlayer) = 0;
    bool HasAchieved(uint32 achievementId) const;
    uint32 GetAchievementPoints() const;

    bool HasAccountAchieved(uint32 achievementId) const;
    ObjectGuid GetFirstAchievedCharacterOnAccount(uint32 achievementId) const;

protected:
    bool CanUpdateCriteriaTree(Criteria const* criteria, CriteriaTree const* tree, Player* referencePlayer) const override;
    bool CanCompleteCriteriaTree(CriteriaTree const* tree) override;
    void CompletedCriteriaTree(CriteriaTree const* tree, Player* referencePlayer) override;
    void AfterCriteriaTreeUpdate(CriteriaTree const* tree, Player* referencePlayer) override;

    bool IsCompletedAchievement(AchievementEntry const* entry);

    bool RequiredAchievementSatisfied(uint32 achievementId) const override;

protected:
    std::unordered_map<uint32, CompletedAchievementData> _completedAchievements;
    uint32 _achievementPoints;
};

class PlayerAchievementMgr : public AchievementMgr
{
public:
    explicit PlayerAchievementMgr(Player* owner);

    void Reset(AchievementEntry const* achievement);

    void LoadFromDB(PreparedQueryResult achievementResult, PreparedQueryResult criteriaResult, PreparedQueryResult accountAchievementResult, PreparedQueryResult accountCriteriaResult);
    void SaveToDB(SQLTransaction& charTrans, SQLTransaction& authTrans);

    void ResetCriteria(CriteriaCondition condition, uint64 miscValue1 = 0, bool evenIfCriteriaComplete = false);

    void SendAllData(Player* receiver) const override;
    void SendAchievementInfo(Player* receiver, uint32 achievementId = 0) const;

    void CompletedAchievement(AchievementEntry const* entry, Player* referencePlayer) override;

    void SendAccountCriteriaUpdate(Criteria const* criteria, CriteriaProgress const* progress, uint32 timeElapsed, bool timedCompleted) const override;
    void SendAllAccountCriteria() const;

protected:
    void SendCriteriaUpdate(Criteria const* entry, CriteriaProgress const* progress, uint32 timeElapsed, bool timedCompleted) const override;
    void SendCriteriaProgressRemoved(uint32 criteriaId) override;

    void SendAchievementEarned(AchievementEntry const* achievement) const;

    void SendPacket(WorldPacket* data) const override;

    std::string GetOwnerInfo() const override;
    CriteriaList const& GetCriteriaByType(CriteriaTypes type) const override;

private:
    Player* _owner;
};

class GuildAchievementMgr : public AchievementMgr
{
public:
    explicit GuildAchievementMgr(Guild* owner);

    void Reset(AchievementEntry const* achievement);

    void LoadFromDB(PreparedQueryResult achievementResult, PreparedQueryResult criteriaResult);
    void SaveToDB(SQLTransaction& trans);

    void SendAllData(Player* receiver) const override;
    void SendAchievementInfo(Player* receiver, uint32 achievementId = 0) const;
    void SendAllTrackedCriterias(Player* receiver, std::set<uint32> const& trackedCriterias) const;

    void CompletedAchievement(AchievementEntry const* entry, Player* referencePlayer) override;

    void SendAccountCriteriaUpdate(Criteria const* criteria, CriteriaProgress const* progress, uint32 timeElapsed, bool timedCompleted) const override { }

    CompletedAchievementData const* GetCompletedDataForAchievement(uint32 achievementId)
    {
        return &_completedAchievements[achievementId];
    }

protected:
    void SendCriteriaUpdate(Criteria const* entry, CriteriaProgress const* progress, uint32 timeElapsed, bool timedCompleted) const override;
    void SendCriteriaProgressRemoved(uint32 criteriaId) override;

    void SendAchievementEarned(AchievementEntry const* achievement) const;

    void SendPacket(WorldPacket* data) const override;

    std::string GetOwnerInfo() const override;
    CriteriaList const& GetCriteriaByType(CriteriaTypes type) const override;

private:
    Guild* _owner;
};

typedef std::vector<AchievementReward> AchievementRewardsVector;

class AchievementGlobalMgr
{
    AchievementGlobalMgr() { }
    ~AchievementGlobalMgr() { }

public:
    friend class Tod::Singleton<AchievementGlobalMgr>;

    std::vector<AchievementEntry const*> const* GetAchievementByReferencedId(uint32 id) const;
    AchievementRewardsVector const* GetAchievementRewards(AchievementEntry const* achievement) const;
    AchievementRewardLocale const* GetAchievementRewardLocale(AchievementEntry const* achievement) const;

    bool IsRealmCompleted(AchievementEntry const* achievement) const;
    void SetRealmCompleted(AchievementEntry const* achievement);

    void LoadAchievementReferenceList();
    void LoadCompletedAchievements();
    void LoadRewards();
    void LoadRewardLocales();

private:
    // store achievements by referenced achievement id to speed up lookup
    std::unordered_map<uint32, std::vector<AchievementEntry const*>> _achievementListByReferencedId;

    // store realm first achievements
    // std::chrono::system_clock::time_point::min() is a placeholder value for realm firsts not yet completed
    // std::chrono::system_clock::time_point::max() is a value assigned to realm firsts complete before worldserver started
    std::unordered_map<uint32 /*achievementId*/, std::chrono::system_clock::time_point /*completionTime*/> _allCompletedAchievements;

    std::unordered_map<uint32, AchievementRewardsVector> _achievementRewards;
    std::unordered_map<uint32, AchievementRewardLocale> _achievementRewardLocales;
};

#define sAchievementMgr Tod::Singleton<AchievementGlobalMgr>::GetSingleton()

#endif
