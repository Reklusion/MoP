/*
 * Copyright (C) 2011-2015 Project SkyFire <http://www.projectskyfire.org/>
 * Copyright (C) 2008-2015 TrinityCore <http://www.trinitycore.org/>
 * Copyright (C) 2005-2015 MaNGOS <http://getmangos.com/>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 3 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef TRINITY_DBCSTORES_H
#define TRINITY_DBCSTORES_H

#include "Common.h"
#include "DBCStore.h"
#include "DBCStructure.h"
#include "SharedDefines.h"

#include "Singleton/Singleton.hpp"

enum ContentLevels
{
    CONTENT_1_60    = 0,
    CONTENT_61_70   = 1,
    CONTENT_71_80   = 2,
    CONTENT_81_85   = 3,
    CONTENT_86_90   = 4,
    MAX_CONTENT
};

struct VectorArray
{
    std::vector<std::string> stringVectorArray[2];
};

struct TaxiPathBySourceAndDestination
{
    TaxiPathBySourceAndDestination() : ID(0), price(0) { }
    TaxiPathBySourceAndDestination(uint32 _id, uint32 _price) : ID(_id), price(_price) { }

    uint32    ID;
    uint32    price;
};

struct WMOAreaTableTripple
{
    WMOAreaTableTripple(int32 r, int32 a, int32 g) :  groupId(g), rootId(r), adtId(a)
    {
    }

    bool operator <(const WMOAreaTableTripple& b) const
    {
        return memcmp(this, &b, sizeof(WMOAreaTableTripple))<0;
    }

    // ordered by entropy; that way memcmp will have a minimal medium runtime
    int32 groupId;
    int32 rootId;
    int32 adtId;
};

template<class T>
class GameTable
{
public:
    GameTable(char const* format) : _storage(format), _gtEntry(nullptr) { }

    void SetGameTableEntry(GameTablesEntry const* gtEntry) { _gtEntry = gtEntry; }

    T const* EvaluateTable(uint32 row, uint32 column) const
    {
        ASSERT(row < _gtEntry->NumRows);
        ASSERT(column < _gtEntry->NumColumns);

        return _storage.LookupEntry(_gtEntry->NumRows * column + row);
    }

    char const* GetFormat() const { return _storage.GetFormat(); }
    uint32 GetFieldCount() const { return _storage.GetFieldCount(); }
    bool Load(char const* fileName) { return _storage.Load(fileName, nullptr); }

    uint32 GetTableRowCount() const { return _gtEntry->NumRows; }
    uint32 GetTableColumnCount() const { return _gtEntry->NumColumns; }

private:
    DBCStorage<T> _storage;
    GameTablesEntry const* _gtEntry;
};

typedef std::set<uint32> PetFamilySpellsSet;
typedef std::map<uint32, PetFamilySpellsSet > PetFamilySpellsStore;
typedef std::vector<uint32> SimpleFactionsList;
typedef std::array<uint8, MAX_TAXI_MASK_SIZE> TaxiMask;
typedef std::vector<TaxiPathNodeEntry const*> TaxiPathNodeList;
typedef std::vector<TaxiPathNodeList> TaxiPathNodesByPath;
typedef std::map<uint32, TaxiPathBySourceAndDestination> TaxiPathSetForSource;
typedef std::multimap<uint32, CharSectionsEntry const*> CharSectionsMap;
typedef ChrSpecializationEntry const* ChrSpecializationByIndexArray[MAX_CLASSES][MAX_SPECIALIZATIONS];
typedef std::tuple<uint32, uint32, uint32> EmotesTextSoundKey;
typedef std::map<EmotesTextSoundKey, EmotesTextSoundEntry const*> EmotesTextSoundMap;
typedef std::map<uint32, SimpleFactionsList> FactionTeamMap;
typedef std::map<uint32, VectorArray> NameGenVectorArraysMap;
typedef std::unordered_map<uint32, std::vector<MapDifficultyEntry const*>> MapDifficultyMap;
typedef std::unordered_map<uint32, std::set<uint32>> PhaseGroupContainer;
typedef std::unordered_map<uint32, SkillLineAbilityEntry const*> SkillLineAbilityContainer;
typedef std::unordered_multimap<uint32, SkillRaceClassInfoEntry const*> SkillRaceClassInfoMap;
typedef std::pair<SkillRaceClassInfoMap::iterator, SkillRaceClassInfoMap::iterator> SkillRaceClassInfoBounds;
typedef std::unordered_map<uint32, std::list<SkillLineAbilityEntry const*> > SpellsPerClassStore;
typedef std::unordered_map<uint32, SpellEffectScalingEntry const*> SpellEffectScallingByEffectId;
typedef std::unordered_map<uint32, std::vector<SpellPowerEntry const*>> SpellPowerContainer;
typedef std::unordered_map<uint32, std::unordered_map<uint32, std::vector<SpellPowerEntry const*>>> SpellPowerDifficultyContainer;
typedef std::unordered_map<uint32, std::vector<SpellProcsPerMinuteModEntry const* >> SpellProcsPerMinuteModContainer;
typedef std::vector<TalentEntry const*> TalentsByPosition[MAX_CLASSES][MAX_TALENT_TIERS][MAX_TALENT_COLUMNS];
typedef std::map<uint32, uint32> TalentSpellPosMap;
typedef std::map<uint32, TaxiPathSetForSource> TaxiPathSetBySource;
typedef std::map<WMOAreaTableTripple, WMOAreaTableEntry const*> WMOAreaInfoByTripple;
typedef std::vector<std::pair<int32, int32> > DigsitePOIPolygon;
typedef std::map<uint32, DigsitePOIPolygon> DigsitePOIPolygonContainer;
typedef std::vector<uint32> ParentMapsVector;
typedef std::unordered_map<uint32, ParentMapsVector> ParentMapsContainer;

uint32 GetLiquidFlags(uint32 liquidType);

class DBCManager
{
    friend class Tod::Singleton<DBCManager>;
    DBCManager();
    ~DBCManager();

private:
    uint32 DBCFileCount;
    typedef std::list<std::string> DBCStoreProblemList;

    uint32 PowersByClass[MAX_CLASSES][MAX_POWERS];

    std::map<uint32, CharStartOutfitEntry const*> sCharStartOutfitMap;
    std::unordered_map<uint32, std::vector<ItemSpecOverrideEntry const*>> sItemSpecOverridesStore;
    std::unordered_map<uint32, std::vector<SpecializationSpellsEntry const*>> sSpecializationSpellsBySpecStore;

    FactionTeamMap sFactionTeamMap;
    WMOAreaInfoByTripple sWMOAreaInfoByTripple;

    CharSectionsMap sCharSectionMap;
    ChrSpecializationByIndexArray sChrSpecializationByIndexStore;
    MapDifficultyMap sMapDifficultyMap;
    NameGenVectorArraysMap sGenNameVectoArraysMap;
    PetFamilySpellsStore sPetFamilySpellsStore;
    PhaseGroupContainer sPhasesByGroup;
    SkillLineAbilityContainer SkillLineAbilities;
    SkillRaceClassInfoMap SkillRaceClassInfoBySkill;
    SpellEffectScallingByEffectId sSpellEffectScallingByEffectId;
    SpellPowerContainer _spellPowers;
    SpellPowerDifficultyContainer _spellPowerDifficulties;
    SpellProcsPerMinuteModContainer _spellProcsPerMinuteMods;
    TalentsByPosition sTalentByPos;
    TaxiMask sTaxiNodesMask;
    TaxiMask sOldContinentsNodesMask;
    TaxiMask sHordeTaxiNodesMask;
    TaxiMask sAllianceTaxiNodesMask;
    TaxiPathSetBySource sTaxiPathSetBySource;
    TaxiPathNodesByPath sTaxiPathNodesByPath;
    DigsitePOIPolygonContainer sDigsitePOIPolygons;
    EmotesTextSoundMap sEmotesTextSoundMap;
    ParentMapsContainer sParentMaps;

public:
    void LoadDBCStores(const std::string& dataPath);

    template<class T>
    void LoadDBC(uint32& availableDbcLocales, DBCStoreProblemList& errors, DBCStorage<T>& storage, std::string const& dbcPath, std::string const& filename, std::string const* customFormat = NULL, std::string const* customIndexName = NULL);

    template<class T>
    void LoadGameTable(DBCStoreProblemList& errors, std::string const& tableName, GameTable<T>& storage, std::string const& dbcPath, std::string const& filename);

    CharStartOutfitEntry const* GetCharStartOutfitEntry(uint8 race, uint8 class_, uint8 gender);
    CharSectionsEntry const* GetCharSectionEntry(uint8 race, CharSectionType genType, uint8 gender, uint8 type, uint8 color);

    ChrSpecializationEntry const* GetChrSpecializationByIndexArray(uint8 class_, uint8 spec_) const { return sChrSpecializationByIndexStore[class_][spec_]; }

    SimpleFactionsList const* GetFactionTeamList(uint32 faction);

    char const* GetPetName(uint32 petfamily, uint32 dbclang);
    char const* GetRaceName(uint8 race, uint8 locale);
    char const* GetClassName(uint8 class_, uint8 locale);

    const std::string* GetRandomCharacterName(uint8 race, uint8 gender);

    uint32 GetMaxLevelForExpansion(uint32 expansion);
    uint32 GetExpansionForLevel(uint8 level);

    ContentLevels GetContentLevelsForMapAndZone(uint32 mapid, uint32 zoneId);

    LFGDungeonEntry const* GetLFGDungeon(uint32 mapId, Difficulty difficulty);

    std::vector<ItemSpecOverrideEntry const*> const* GetItemSpecOverrides(uint32 itemId);

    uint32 GetQuestUniqueBitFlag(uint32 questId);

    PetFamilySpellsStore const& GetPetFamilySpellsStore() const { return sPetFamilySpellsStore; }
    void PopulatePetFamilySpellsStore(uint8 index, uint32 spellId) { sPetFamilySpellsStore[index].insert(spellId); }

    std::set<uint32> const& GetPhasesForGroup(uint32 group);

    MapDifficultyEntry const* GetDefaultMapDifficulty(uint32 mapId, Difficulty* difficulty = nullptr);
    MapDifficultyEntry const* GetMapDifficultyData(uint32 mapId, Difficulty difficulty);
    MapDifficultyEntry const* GetDownscaledMapDifficultyData(uint32 mapId, Difficulty &difficulty);

    MapDifficultyMap const& GetMapDifficulties() const { return sMapDifficultyMap; }

    uint32 GetVirtualMapForMapAndZone(uint32 mapid, uint32 zoneId);
    uint32 GetDefaultMapLight(uint32 mapId);

    void Zone2MapCoordinates(float &x, float &y, uint32 zone);
    void Map2ZoneCoordinates(float &x, float &y, uint32 zone);

    void DeterminaAlternateMapPosition(uint32 mapId, float x, float y, float z, uint32* newMapId = nullptr, DBCPosition2D* newPos = nullptr);

    PvPDifficultyEntry const* GetBattlegroundBracketByLevel(uint32 mapid, uint32 level);
    PvPDifficultyEntry const* GetBattlegroundBracketById(uint32 mapid, BattlegroundBracketId id);

    SpellEffectScalingEntry const* GetSpellEffectScallingEntry(uint32 effectId) const;

    std::vector<SpellPowerEntry const*> GetSpellPowers(uint32 spellId, Difficulty difficulty = DIFFICULTY_NONE, bool* hasDifficultyPowers = nullptr);

    uint32 GetPowerIndexByClass(uint32 powerType, uint32 classId);

    bool IsTotemCategoryCompatibleWith(uint32 itemTotemCategoryId, uint32 requiredTotemCategoryId);

    SkillLineAbilityEntry const* GetSkillLineAbilityBySpell(uint32 spellId);

    SkillRaceClassInfoEntry const* GetSkillRaceClassInfo(uint32 skill, uint8 race, uint8 class_);

    std::vector<SpecializationSpellsEntry const*> const* GetSpecializationSpells(uint32 specId);

    WMOAreaTableEntry const* GetWMOAreaTableEntryByTripple(int32 rootid, int32 adtid, int32 groupid);

    std::vector<TalentEntry const*> const& GetTalentsByPosition(uint8 class_, uint8 tier_, uint8 index_) const { return sTalentByPos[class_][tier_][index_]; }

    TaxiPathSetForSource const* GetTaxiPathSetForSource(uint32 source) const;

    TaxiPathNodesByPath const& GetTaxiPathNodesByPath() const { return sTaxiPathNodesByPath; }
    TaxiPathNodeList const& GetTaxiPathNodeList(uint32 path) const { return sTaxiPathNodesByPath[path]; }
    TaxiPathNodeEntry const* GetTaxiPathNodeEntry(uint32 path, uint32 node) const { return sTaxiPathNodesByPath[path][node]; }

    uint32 GetTaxiMaskForOldContinentByID(uint8 index) const { return sOldContinentsNodesMask[index]; }

    TaxiMask const& GetTaxiNodesAllianceMask() const { return sAllianceTaxiNodesMask; }
    TaxiMask const& GetTaxiNodesHordeMask() const { return sHordeTaxiNodesMask; }

    TaxiMask const& GetTaxiNodesMask() const { return sTaxiNodesMask; }

    uint32 GetTaxiNodesMaskByID(uint8 index) const { return sTaxiNodesMask[index]; }

    DigsitePOIPolygon const* GetDigsitePOIPolygon(uint32 digsiteId) const;

    EmotesTextSoundEntry const* FindTextSoundEmoteFor(uint32 emote, uint32 race, uint32 gender);

    std::vector<SpellProcsPerMinuteModEntry const*> GetSpellProcsPerMinuteMods(uint32 spellprocsPerMinuteId) const;

    ParentMapsVector GetParentMapsForMapID(uint32 mapID) const;
};

#define sDBCManager Tod::Singleton<DBCManager>::GetSingleton()

extern DBCStorage <AchievementEntry>                    sAchievementStore;
extern DBCStorage <AchievementCategory>                 sAchievementCategoryStore;
extern DBCStorage <AnimKitEntry>                        sAnimKitStore;
extern DBCStorage <AreaTableEntry>                      sAreaStore;
extern DBCStorage <AreaGroupEntry>                      sAreaGroupStore;
extern DBCStorage <AreaTriggerEntry>                    sAreaTriggerStore;
extern DBCStorage <ArmorLocationEntry>                  sArmorLocationStore;
extern DBCStorage <AuctionHouseEntry>                   sAuctionHouseStore;
extern DBCStorage <BankBagSlotPricesEntry>              sBankBagSlotPricesStore;
extern DBCStorage <BannedAddOnsEntry>                   sBannedAddOnsStore;
extern DBCStorage <BarberShopStyleEntry>                sBarberShopStyleStore;
extern DBCStorage <BattlemasterListEntry>               sBattlemasterListStore;
extern DBCStorage <ChatChannelsEntry>                   sChatChannelsStore;
extern DBCStorage <CharSectionsEntry>                   sCharSectionsStore;
extern DBCStorage <CharStartOutfitEntry>                sCharStartOutfitStore;
extern DBCStorage <CharTitlesEntry>                     sCharTitlesStore;
extern DBCStorage <ChrClassesEntry>                     sChrClassesStore;
extern DBCStorage <ChrRacesEntry>                       sChrRacesStore;
extern DBCStorage <ChrPowerTypesEntry>                  sChrPowerTypesStore;
extern DBCStorage <ChrSpecializationEntry>              sChrSpecializationStore;
extern DBCStorage <CinematicCameraEntry>                sCinematicCameraStore;
extern DBCStorage <CinematicSequencesEntry>             sCinematicSequencesStore;
extern DBCStorage <CreatureDisplayInfoEntry>            sCreatureDisplayInfoStore;
extern DBCStorage <CreatureDisplayInfoExtraEntry>       sCreatureDisplayInfoExtraStore;
extern DBCStorage <CreatureFamilyEntry>                 sCreatureFamilyStore;
extern DBCStorage <CreatureModelDataEntry>              sCreatureModelDataStore;
extern DBCStorage <CreatureSpellDataEntry>              sCreatureSpellDataStore;
extern DBCStorage <CreatureTypeEntry>                   sCreatureTypeStore;
extern DBCStorage <CurrencyTypesEntry>                  sCurrencyTypesStore;
extern DBCStorage <CriteriaEntry>                       sCriteriaStore;
extern DBCStorage <CriteriaTreeEntry>                   sCriteriaTreeStore;
extern DBCStorage <DestructibleModelDataEntry>          sDestructibleModelDataStore;
extern DBCStorage <DifficultyEntry>                     sDifficultyStore;
extern DBCStorage <DungeonEncounterEntry>               sDungeonEncounterStore;
extern DBCStorage <DurabilityCostsEntry>                sDurabilityCostsStore;
extern DBCStorage <DurabilityQualityEntry>              sDurabilityQualityStore;
extern DBCStorage <EmotesEntry>                         sEmotesStore;
extern DBCStorage <EmotesTextEntry>                     sEmotesTextStore;
extern DBCStorage <FactionEntry>                        sFactionStore;
extern DBCStorage <FactionTemplateEntry>                sFactionTemplateStore;
extern DBCStorage <FileDataEntry>                       sFileDataStore;
extern DBCStorage <GameObjectDisplayInfoEntry>          sGameObjectDisplayInfoStore;
extern DBCStorage <GemPropertiesEntry>                  sGemPropertiesStore;
extern DBCStorage <GlyphPropertiesEntry>                sGlyphPropertiesStore;
extern DBCStorage <GlyphSlotEntry>                      sGlyphSlotStore;
extern DBCStorage <GuildPerkSpellsEntry>                sGuildPerkSpellsStore;
extern DBCStorage <GuildColorBackgroundEntry>           sGuildColorBackgroundStore;
extern DBCStorage <GuildColorBorderEntry>               sGuildColorBorderStore;
extern DBCStorage <GuildColorEmblemEntry>               sGuildColorEmblemStore;
extern DBCStorage <HolidaysEntry>                       sHolidaysStore;
extern DBCStorage <ImportPriceArmorEntry>               sImportPriceArmorStore;
extern DBCStorage <ImportPriceQualityEntry>             sImportPriceQualityStore;
extern DBCStorage <ImportPriceShieldEntry>              sImportPriceShieldStore;
extern DBCStorage <ImportPriceWeaponEntry>              sImportPriceWeaponStore;
extern DBCStorage <ItemPriceBaseEntry>                  sItemPriceBaseStore;
extern DBCStorage <ItemReforgeEntry>                    sItemReforgeStore;
extern DBCStorage <ItemArmorQualityEntry>               sItemArmorQualityStore;
extern DBCStorage <ItemArmorShieldEntry>                sItemArmorShieldStore;
extern DBCStorage <ItemArmorTotalEntry>                 sItemArmorTotalStore;
extern DBCStorage <ItemClassEntry>                      sItemClassStore;
extern DBCStorage <ItemBagFamilyEntry>                  sItemBagFamilyStore;
extern DBCStorage <ItemDamageEntry>                     sItemDamageAmmoStore;
extern DBCStorage <ItemDamageEntry>                     sItemDamageOneHandStore;
extern DBCStorage <ItemDamageEntry>                     sItemDamageOneHandCasterStore;
extern DBCStorage <ItemDamageEntry>                     sItemDamageRangedStore;
extern DBCStorage <ItemDamageEntry>                     sItemDamageThrownStore;
extern DBCStorage <ItemDamageEntry>                     sItemDamageTwoHandStore;
extern DBCStorage <ItemDamageEntry>                     sItemDamageTwoHandCasterStore;
extern DBCStorage <ItemDamageEntry>                     sItemDamageWandStore;
extern DBCStorage <ItemDisenchantLootEntry>             sItemDisenchantLootStore;
extern DBCStorage <ItemDisplayInfoEntry>                sItemDisplayInfoStore;
extern DBCStorage <ItemLimitCategoryEntry>              sItemLimitCategoryStore;
extern DBCStorage <ItemRandomPropertiesEntry>           sItemRandomPropertiesStore;
extern DBCStorage <ItemRandomSuffixEntry>               sItemRandomSuffixStore;
extern DBCStorage <ItemSetEntry>                        sItemSetStore;
extern DBCStorage <ItemSpecOverrideEntry>               sItemSpecOverrideStore;
extern DBCStorage <ItemSpecEntry>                       sItemSpecStore;
extern DBCStorage <LFGDungeonEntry>                     sLFGDungeonStore;
extern DBCStorage <LiquidTypeEntry>                     sLiquidTypeStore;
extern DBCStorage <LockEntry>                           sLockStore;
extern DBCStorage <MailTemplateEntry>                   sMailTemplateStore;
extern DBCStorage <MapEntry>                            sMapStore;
extern DBCStorage <ModifierTreeEntry>                   sModifierTreeStore;
extern DBCStorage <MountCapabilityEntry>                sMountCapabilityStore;
extern DBCStorage <MountTypeEntry>                      sMountTypeStore;
extern DBCStorage <NameGenEntry>                        sNameGenStore;
extern DBCStorage <PhaseEntry>                          sPhaseStore;
extern DBCStorage <PhaseGroupEntry>                     sPhaseGroupStore;
extern DBCStorage <MapDifficultyEntry>                  sMapDifficultyStore;
extern DBCStorage <MovieEntry>                          sMovieStore;
extern DBCStorage <OverrideSpellDataEntry>              sOverrideSpellDataStore;
extern DBCStorage <PlayerConditionEntry>                sPlayerConditionStore;
extern DBCStorage <PowerDisplayEntry>                   sPowerDisplayStore;
extern DBCStorage <QuestSortEntry>                      sQuestSortStore;
extern DBCStorage <QuestMoneyRewardEntry>               sQuestMoneyRewardStore;
extern DBCStorage <QuestXPEntry>                        sQuestXPStore;
extern DBCStorage <QuestFactionRewEntry>                sQuestFactionRewardStore;
extern DBCStorage <QuestPOIPointEntry>                  sQuestPOIPointStore;
extern DBCStorage <RandomPropertiesPointsEntry>         sRandomPropertiesPointsStore;
extern DBCStorage <ResearchBranchEntry>                 sResearchBranchStore;
extern DBCStorage <ResearchProjectEntry>                sResearchProjectStore;
extern DBCStorage <ResearchSiteEntry>                   sResearchSiteStore;
extern DBCStorage <ScalingStatDistributionEntry>        sScalingStatDistributionStore;
extern DBCStorage <ScalingStatValuesEntry>              sScalingStatValuesStore;
extern DBCStorage <ScenarioEntry>                       sScenarioStore;
extern DBCStorage <ScenarioStepEntry>                   sScenarioStepStore;
extern DBCStorage <SkillLineEntry>                      sSkillLineStore;
extern DBCStorage <SkillLineAbilityEntry>               sSkillLineAbilityStore;
extern DBCStorage <SkillRaceClassInfoEntry>             sSkillRaceClassInfoStore;
extern DBCStorage <SkillTiersEntry>                     sSkillTiersStore;
extern DBCStorage <SoundEntriesEntry>                   sSoundEntriesStore;
extern DBCStorage <SpellCastTimesEntry>                 sSpellCastTimesStore;
extern DBCStorage <SpellCategoryEntry>                  sSpellCategoryStore;
extern DBCStorage <SpellDurationEntry>                  sSpellDurationStore;
extern DBCStorage <SpellFocusObjectEntry>               sSpellFocusObjectStore;
extern DBCStorage <SpellItemEnchantmentEntry>           sSpellItemEnchantmentStore;
extern DBCStorage <SpellItemEnchantmentConditionEntry>  sSpellItemEnchantmentConditionStore;
extern DBCStorage <SpecializationSpellsEntry>           sSpecializationSpellsStore;
extern DBCStorage <SpellProcsPerMinuteEntry>            sSpellProcsPerMinuteStore;
extern DBCStorage <SpellProcsPerMinuteModEntry>         sSpellProcsPerMinuteModStore;
extern DBCStorage <SpellRadiusEntry>                    sSpellRadiusStore;
extern DBCStorage <SpellRangeEntry>                     sSpellRangeStore;
extern DBCStorage <SpellRuneCostEntry>                  sSpellRuneCostStore;
extern DBCStorage <SpellShapeshiftEntry>                sSpellShapeshiftStore;
extern DBCStorage <SpellShapeshiftFormEntry>            sSpellShapeshiftFormStore;
extern DBCStorage <SpellEntry>                          sSpellStore;
extern DBCStorage <SpellLearnSpellEntry>                sSpellLearnSpellStore;
extern DBCStorage <SpellMiscEntry>                      sSpellMiscStore;
extern DBCStorage <SpellEffectScalingEntry>             sSpellEffectScalingStore;
extern DBCStorage <SpellAuraOptionsEntry>               sSpellAuraOptionsStore;
extern DBCStorage <SpellAuraRestrictionsEntry>          sSpellAuraRestrictionsStore;
extern DBCStorage <SpellCastingRequirementsEntry>       sSpellCastingRequirementsStore;
extern DBCStorage <SpellCategoriesEntry>                sSpellCategoriesStore;
extern DBCStorage <SpellClassOptionsEntry>              sSpellClassOptionsStore;
extern DBCStorage <SpellCooldownsEntry>                 sSpellCooldownsStore;
extern DBCStorage <SpellEffectEntry>                    sSpellEffectStore;
extern DBCStorage <SpellEquippedItemsEntry>             sSpellEquippedItemsStore;
extern DBCStorage <SpellInterruptsEntry>                sSpellInterruptsStore;
extern DBCStorage <SpellKeyboundOverrideEntry>          sSpellKeyboundOverrideStore;
extern DBCStorage <SpellLevelsEntry>                    sSpellLevelsStore;
extern DBCStorage <SpellPowerEntry>                     sSpellPowerStore;
extern DBCStorage <SpellTargetRestrictionsEntry>        sSpellTargetRestrictionsStore;
extern DBCStorage <SpellTotemsEntry>                    sSpellTotemsStore;
extern DBCStorage <SpellScalingEntry>                   sSpellScalingStore;
extern DBCStorage <SummonPropertiesEntry>               sSummonPropertiesStore;
extern DBCStorage <TalentEntry>                         sTalentStore;
extern DBCStorage <TaxiNodesEntry>                      sTaxiNodesStore;
extern DBCStorage <TaxiPathEntry>                       sTaxiPathStore;
extern DBCStorage <TaxiPathNodeEntry>                   sTaxiPathNodeStore;
extern DBCStorage <TotemCategoryEntry>                  sTotemCategoryStore;
extern DBCStorage <UnitPowerBarEntry>                   sUnitPowerBarStore;
extern DBCStorage <VehicleEntry>                        sVehicleStore;
extern DBCStorage <VehicleSeatEntry>                    sVehicleSeatStore;
extern DBCStorage <WMOAreaTableEntry>                   sWMOAreaTableStore;
extern DBCStorage <WorldMapAreaEntry>                   sWorldMapAreaStore;
extern DBCStorage <WorldMapOverlayEntry>                sWorldMapOverlayStore;
extern DBCStorage <WorldSafeLocsEntry>                  sWorldSafeLocsStore;

extern GameTable <GtBarberShopCostBaseEntry>            sGtBarberShopCostBaseStore;
extern GameTable <GtBattlePetTypeDamageModEntry>        sGtBattlePetTypeDamageModStore;
extern GameTable <GtBattlePetXpEntry>                   sGtBattlePetXpStore;
extern GameTable <GtCombatRatingsEntry>                 sGtCombatRatingsStore;
extern GameTable <GtChanceToMeleeCritBaseEntry>         sGtChanceToMeleeCritBaseStore;
extern GameTable <GtChanceToMeleeCritEntry>             sGtChanceToMeleeCritStore;
extern GameTable <GtChanceToSpellCritBaseEntry>         sGtChanceToSpellCritBaseStore;
extern GameTable <GtChanceToSpellCritEntry>             sGtChanceToSpellCritStore;
extern GameTable <GtItemSocketCostPerLevelEntry>        sGtItemSocketCostPerLevelStore;
extern GameTable <GtNPCManaCostScalerEntry>             sGtNPCManaCostScalerStore;
extern GameTable <GtOCTClassCombatRatingScalarEntry>    sGtOCTClassCombatRatingScalarStore;
extern GameTable <GtOCTHpPerStaminaEntry>               sGtOCTHpPerStaminaStore;
extern GameTable <GtRegenMPPerSptEntry>                 sGtRegenMPPerSptStore;
extern GameTable <GtSpellScalingEntry>                  sGtSpellScalingStore;
extern GameTable <GtOCTBaseHPByClassEntry>              sGtOCTBaseHPByClassStore;
extern GameTable <GtOCTBaseMPByClassEntry>              sGtOCTBaseMPByClassStore;

#endif
