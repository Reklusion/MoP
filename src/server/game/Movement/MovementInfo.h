/*
* Copyright (C) 2015-2016 Theatre of Dreams <http://theatreofdreams.eu/>
*
* This program is free software; you can redistribute it and/or modify it
* under the terms of the GNU General Public License as published by the
* Free Software Foundation; either version 3 of the License, or (at your
* option) any later version.
*
* This program is distributed in the hope that it will be useful, but WITHOUT
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
* more details.
*
* You should have received a copy of the GNU General Public License along
* with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef MOVEMENT_INFO_H_
#define MOVEMENT_INFO_H_

#include "Opcodes.h"
#include "Position.h"
#include "ObjectGuid.h"

class Unit;
class WorldSession;

enum HackTypes
{
    HACK_SPEED              = 0,
    HACK_FLYING             = 1,
    HACK_NO_FALLING         = 2,
    HACK_FALLING_SLOW       = 3,
    HACK_WATER_WALKING      = 4,
    HACK_WALL_CLIMBING      = 5,
    HACK_LOW_GRAVITY        = 6,
    HACK_CLIPPING           = 7
};

enum HackDetectionTypes
{
    HACK_DETECTION_NONE             = 0x00,
    HACK_DETECTION_SPEED            = 0x01,
    HACK_DETECTION_FLYING           = 0x02,     // flag: CAN_FLY
    HACK_DETECTION_NO_FALLING       = 0x04,     // flag: DISABLE_GRAVITY
    HACK_DETECTION_FALLING_SLOW     = 0x08,     // flag: FALLING_SLOW
    HACK_DETECTION_WATER_WALKING    = 0x10,     // flag: WATERWALKING
    HACK_DETECTION_WALL_CLIMBING    = 0x20,     // flag: SPLINE_ELEVATION
    HACK_DETECTION_LOW_GRAVITY      = 0x40,     // flag: FALLING (former: jumping)
    HACK_DETECTION_CLIPPING         = 0x80      // NYI
};

class ExtraMovementStatusElement;

struct MovementForce
{
    MovementForce() : ID(0), TransportID(0), Magnitude(0.0f), Type(0)
    {
        Direction.Relocate(0.0f, 0.0f, 0.0f, 0.0f);
    }

    uint32 ID;
    Position Direction;
    uint32 TransportID;
    float Magnitude;
    uint8 Type;
};

struct MovementInfo
{
    MovementInfo() : flags(0), flags2(0), time(0), ackIndex(0), pitch(0.0f), splineElevation(0.0f)
    {
        pos.Relocate(0.0f, 0.0f, 0.0f, 0.0f);
        transport.Reset();
        fall.Reset();
    }

    // common
    uint32 flags;
    uint32 flags2;

    // transport
    struct TransportInfo
    {
        TransportInfo() : seat(-1), time(0), prevTime(0), vehicleId(0)
        {
            pos.Relocate(0.0f, 0.0f, 0.0f, 0.0f);
        }

        void Reset()
        {
            guid.Clear();
            pos.Relocate(0.0f, 0.0f, 0.0f, 0.0f);
            seat = -1;
            time = 0;
            prevTime = 0;
            vehicleId = 0;
        }

        ObjectGuid guid;
        Position pos;
        int8 seat;
        uint32 time;
        uint32 prevTime;
        uint32 vehicleId;
    } transport;

    // falling
    struct FallInfo
    {
        FallInfo() : fallTime(0), zspeed(0.0f), sinAngle(0.0f), cosAngle(0.0f), xyspeed(0.0f) { }

        void Reset()
        {
            fallTime = 0;
            zspeed = 0.0f;
            sinAngle = 0.0f;
            cosAngle = 0.0f;
            xyspeed = 0.0f;
        }

        uint32 fallTime;

        float zspeed;
        float sinAngle;
        float cosAngle;
        float xyspeed;
    } fall;

    // MovementForce
    struct ForcedMovement
    {
        std::vector<MovementForce> ForcedMovements;
        std::vector<uint32> RemovedForcedMovements;
    } forcedMovement;

    // Helpers
    bool HasMovementFlag(uint32 flag) const
    {
        return (flags & flag) != 0;
    }

    void RemoveMovementFlag(uint32 flag)
    {
        flags &= ~flag;
    }

    void AddMovementFlag(uint32 flag)
    {
        flags |= flag;
    }

    bool HasExtraMovementFlag(uint32 flag) const
    {
        return (flags2 & flag) != 0;
    }

    void RemoveExtraMovementFlag(uint32 flag)
    {
        flags2 &= ~flag;
    }

    void AddExtraMovementFlag(uint32 flag)
    {
        flags2 |= flag;
    }

    void ReadFromPacket(WorldPacket& packet, ExtraMovementStatusElement* extras = NULL);
    HackDetectionTypes SanitizeFlags(Unit* target, WorldSession* session = nullptr);

    // These below are only clientside (affects players/movers from packets)

    ObjectGuid guid;
    Position pos;
    uint32 time;
    uint32 ackIndex;

    // swimming/flying
    float pitch;

    // spline
    float splineElevation;

    void ResetBaseInfo()
    {
        guid.Clear();
        pos.Relocate(0.0f, 0.0f, 0.0f, 0.0f);
        time = 0;
    }

    static void GetHacksDetected(std::ostringstream& message, uint32 hacks);
};

#endif