/*
 * Copyright (C) 2015-2016 Theatre of Dreams <http://www.theatreofdreams.eu>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef BATTLE_PET_QUEUE_H
#define BATTLE_PET_QUEUE_H

#include "BattlePetMgr.h"

#include "Singleton/Singleton.hpp"

enum PetBattleQueueError
{
    PET_BATTLE_QUEUE_ERROR_NONE                 = 0,
    PET_BATTLE_QUEUE_ERROR_QUEUED               = 1,    // You are now queued for a pet battle.
    PET_BATTLE_QUEUE_ERROR_ALREADY_QUEUED       = 3,    // You are already queued for a pet battle.
    PET_BATTLE_QUEUE_ERROR_JOIN_FAILED          = 4,    // Unable to join pet battle queue.
    PET_BATTLE_QUEUE_ERROR_SLOT                 = 5,
    PET_BATTLE_QUEUE_ERROR_JOURNAL_LOCK         = 6,    // Pet Journal unavailable for battle.
    PET_BATTLE_QUEUE_ERROR_NEUTRAL              = 7,    // Neutral characters cannot pet battle.
    PET_BATTLE_QUEUE_PROPOSAL_ACCEPTED          = 8,
    PET_BATTLE_QUEUE_ERROR_PROPOSAL_DECLINED    = 9,    // You declined the pet battle invitation.
    PET_BATTLE_QUEUE_ERROR_OPPONENT_DECLINED    = 10,   // Opponent declined pet battle invitation.You have been returned to the queue.
    PET_BATTLE_QUEUE_ERROR_PROPOSAL_TIMEOUT     = 11,   // You did not respond to the pet battle invitation.
    PET_BATTLE_QUEUE_ERROR_REMOVED              = 12,   // You are no longer in the pet battle queue.
    PET_BATTLE_QUEUE_ERROR_REQUEUED_INTERNAL    = 13,   // Internal pet battle error.You have been requeued for battle.
    PET_BATTLE_QUEUE_ERROR_REQUEUED_REMOVED     = 14,   // Pet battle opponent unavailable.You have been requeued for battle.
    PET_BATTLE_QUEUE_ERROR_SUSPENDED            = 18, 
    PET_BATTLE_QUEUE_ERROR_REQUEUED             = 19
};

enum PetBattleQueueSlotError
{
    PET_BATTLE_QUEUE_ERROR_SLOT_NONE            = 0,
    PET_BATTLE_QUEUE_ERROR_SLOT_LOCKED          = 1,    // Pet battle slot %d is locked.
    PET_BATTLE_QUEUE_ERROR_SLOT_EMPTY           = 2,    // Pet battle slot %d is empty.
    PET_BATTLE_QUEUE_ERROR_SLOT_NO_TRACKER      = 3,    // Pet battle slot %d is invalid.
    PET_BATTLE_QUEUE_ERROR_SLOT_NO_SPECIES      = 4,
    PET_BATTLE_QUEUE_ERROR_SLOT_CANT_BATTLE     = 5,    // Pet in slot %d can't battle.
    PET_BATTLE_QUEUE_ERROR_SLOT_REVOKED         = 6,    // Pet in slot %d is unavailable for battle.
    PET_BATTLE_QUEUE_ERROR_SLOT_DEAD            = 7,    // Pet in slot %d is dead.
    PET_BATTLE_QUEUE_ERROR_SLOT_NO_PET          = 8     // Pet Battle slot %d has no pet.
};

struct PetBattleQueueInfo
{
    PetBattleQueueInfo(ObjectGuid ownerGUID, uint32 petLevel, uint32 joinTime) : 
        OwnerGUID(ownerGUID), PetLevel(petLevel), JoinTime(joinTime), IsSuspended(false)
    {
        BattlePosition.Relocate(0.0f, 0.0f, 0.0f, 0.0f);
        PlayerPosition.Relocate(0.0f, 0.0f, 0.0f, 0.0f);
    }

    uint32 PetLevel;
    uint32 JoinTime;
    bool IsSuspended;
    ObjectGuid OwnerGUID;
    ObjectGuid OpponentGUID;
    Position BattlePosition;
    WorldLocation PlayerPosition;
};

#define PET_BATTLE_TIMER    30 * IN_MILLISECONDS

typedef std::unordered_map<ObjectGuid, PetBattleQueueInfo*> PetBattleQueueList;
typedef std::vector<ObjectGuid> PetBattleQueueProposedList;

class PetBattleQueueRemoveEvent : public BasicEvent
{
    public:
        PetBattleQueueRemoveEvent(ObjectGuid queue1GUID, ObjectGuid queue2GUID)
            : challengerQueueGUID(queue1GUID), opponentQueueGUID(queue2GUID) { }

        virtual bool Execute(uint64 e_time, uint32 p_time) override;
        virtual void Abort(uint64 e_time) override;

    private:
        ObjectGuid challengerQueueGUID;
        ObjectGuid opponentQueueGUID;
};

class PetBattleQueue
{
public:
    PetBattleQueue();
    ~PetBattleQueue();

    uint32 AddPlayerToPetBattleQueue(ObjectGuid guid, uint32 petLevel);
    void RemovePlayerFromPetBattleQueue(ObjectGuid guid);

    void Update(uint32 diff);

    bool IsInPetBattleQueue(ObjectGuid guid) const
    {
        auto itr = _petBattleQueueList.find(guid);
        if (itr != _petBattleQueueList.end())
            return true;

        return false;
    }

    PetBattleQueueInfo* GetPetBattleQueueInfo(ObjectGuid guid) const
    {
        auto itr = _petBattleQueueList.find(guid);
        if (itr != _petBattleQueueList.end())
            return itr->second;

        return nullptr;
    }

    uint32 GetAverageWaitTimer() const;
    void UpdateAverageWaitTimer(uint32 waitedTimer);

    PetBattleQueueSlotError GetPetBattleQueueSlotError(BattlePetMgr const* battlePetMgr, uint8 Index) const;

private:

    // Event handler
    EventProcessor m_events;

    PetBattleQueueList _petBattleQueueList;
    PetBattleQueueProposedList _petBattleQueueProposedList;

    uint32 _queueTimer;
    uint32 _averageSumTimer;
    uint32 _averageCount;
};

#define sPetBattleQueueMgr Tod::Singleton<PetBattleQueue>::GetSingleton()

#endif
