/*
 * Copyright (C) 2015-2016 Theatre of Dreams <http://www.theatreofdreams.eu>
 * Copyright (C) 2015 Warmane <http://www.warmane.com/>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "BattlePetMgr.h"
#include "ByteBuffer.h"
#include "DatabaseEnv.h"
#include "DB2Enums.h"
#include "DB2Stores.h"
#include "ObjectMgr.h"
#include "Player.h"
#include "SpellHistory.h"
#include "WorldPacket.h"
#include "WorldSession.h"

BattlePetMgr::~BattlePetMgr()
{
    for (BattlePet* battlePet : BattlePets)
        delete battlePet;

    BattlePets.clear();
}

void BattlePetMgr::LoadFromDB(PreparedQueryResult result)
{
    if (!result)
        return;

    do
    {
        Field* fields           = result->Fetch();
        uint64 id               = fields[0].GetUInt64();
        uint32 speciesId        = fields[1].GetUInt32();
        std::string nickname    = fields[2].GetString();
        uint32 timestamp        = fields[3].GetUInt32();
        uint8 level             = fields[4].GetUInt8();
        uint32 xp               = fields[5].GetUInt32();
        uint32 health           = fields[6].GetUInt32();
        uint8 quality           = fields[7].GetUInt8();
        uint8 breedId           = fields[8].GetUInt8();
        uint32 flags            = fields[9].GetUInt32();

        // check if battle pet species exists
        BattlePetSpeciesEntry const* battlePetSpeciesEntry = sBattlePetSpeciesStore.LookupEntry(speciesId);
        if (!battlePetSpeciesEntry)
        {
            TC_LOG_ERROR("sql.sql", "Species %u defined in `account_battle_pet` for Battle Pet %d  does not exist, skipped!", speciesId, id);
            continue;
        }

        // check if battle pet breed is valid
        if (breedId > BATTLE_PET_MAX_BREED)
        {
            TC_LOG_ERROR("sql.sql", "Breed %u defined in `account_battle_pet` for Battle Pet %d is not valid, skipped!", breedId, id);
            continue;
        }

        // client supports up to legendary quality but currently players can not obtain legendary pets
        if (quality > ITEM_QUALITY_LEGENDARY)
        {
            TC_LOG_ERROR("sql.sql", "Quality %u defined in `account_battle_pet` for Battle Pet %d is invalid, skipped!", quality, id);
            continue;
        }

        // softcap, client supports up to level 255
        if (level > BATTLE_PET_MAX_LEVEL)
        {
            TC_LOG_ERROR("sql.sql", "Level %u defined in `account_battle_pet` for Battle Pet %d is invalid, skipped!", quality, id);
            continue;
        }

        ObjectGuid battlePetGuid = ObjectGuid(id);

        BattlePet* battlePet = new BattlePet(battlePetGuid, speciesId, battlePetSpeciesEntry->FamilyId, nickname, timestamp, level, xp, health, quality, breedId, flags, m_owner);
        battlePet->InitializeAbilities(false);

        BattlePets.push_back(battlePet);

    } while (result->NextRow());
}

void BattlePetMgr::SaveToDB(SQLTransaction& trans)
{
    // save slots to database before battle pets
    SaveSlotsToDB(trans);

    // player has no battle pets, no need to save
    if (BattlePets.empty())
        return;

    auto battlePetItr = BattlePets.begin();
    while (battlePetItr != BattlePets.end())
    {
        BattlePet* battlePet = *battlePetItr++;
        if (!battlePet)
            return;

        switch (battlePet->GetDBState())
        {
            // remove any pending battle pets from memory and database
            case BATTLE_PET_DB_STATE_DELETE:
            {
                PreparedStatement* stmt = LoginDatabase.GetPreparedStatement(LOGIN_DEL_ACCOUNT_BATTLE_PET);
                stmt->setUInt32(0, m_owner->GetSession()->GetAccountId());
                stmt->setUInt64(1, battlePet->GetGUID().GetRawValue());
                trans->Append(stmt);

                BattlePets.erase(std::remove(BattlePets.begin(), BattlePets.end(), battlePet));
                delete battlePet;

                break;
            }
            // save any pending battle pets to the database
            case BATTLE_PET_DB_STATE_SAVE:
            {
                PreparedStatement* stmt = LoginDatabase.GetPreparedStatement(LOGIN_DEL_ACCOUNT_BATTLE_PET);
                stmt->setUInt32(0, m_owner->GetSession()->GetAccountId());
                stmt->setUInt64(1, battlePet->GetGUID().GetRawValue());
                trans->Append(stmt);

                stmt = LoginDatabase.GetPreparedStatement(LOGIN_INS_ACCOUNT_BATTLE_PET);
                stmt->setUInt32(0, m_owner->GetSession()->GetAccountId());
                stmt->setUInt64(1, battlePet->GetGUID().GetRawValue());
                stmt->setUInt32(2, battlePet->GetSpecies());
                stmt->setString(3, battlePet->GetNickname());
                stmt->setUInt32(4, battlePet->GetTimestamp());
                stmt->setUInt8 (5, battlePet->GetLevel());
                stmt->setUInt32(6, battlePet->GetXp());
                stmt->setUInt32(7, battlePet->GetCurrentHealth());
                stmt->setUInt8 (8, battlePet->GetQuality());
                stmt->setUInt8 (9, battlePet->GetBreed());
                stmt->setUInt16(10, battlePet->GetFlags());
                trans->Append(stmt);

                battlePet->SetDBState(BATTLE_PET_DB_STATE_NONE);
                break;

            }
            // no changes have been made to the battle pet, no need to save
            case BATTLE_PET_DB_STATE_NONE:
            default:
                break;
        }
    }
}

void BattlePetMgr::LoadSlotsFromDB(PreparedQueryResult result)
{
    if (!result)
        return;

    Field* fields = result->Fetch();
    m_loadoutFlags = fields[3].GetUInt8();

    // 3 minute cooldown on login for Revive Battle Pets (125439)
    uint32 cooldownRemaining = m_owner->GetSpellHistory()->GetRemainingCooldown(SPELL_REVIVE_BATTLE_PETS);
    if (!cooldownRemaining || cooldownRemaining < 3 * 60 * IN_MILLISECONDS)
        m_owner->GetSpellHistory()->AddCooldown(SPELL_REVIVE_BATTLE_PETS, 0, std::chrono::minutes(3));

    for (uint8 i = 0; i < BATTLE_PET_MAX_LOADOUT_SLOTS; i++)
    {
        uint64 id = fields[i].GetUInt64();

        ObjectGuid battlePetGuid = ObjectGuid(id);

        if ((!HasLoadoutSlot(i) || !GetBattlePet(battlePetGuid)) && id != 0)
        {
            TC_LOG_ERROR("sql.sql", "Battle Pet slot %u in `account_battle_pet_slots` for account %u is invalid!", i, m_owner->GetSession()->GetAccountId());

            // error occured, reset loadout slot and flag for database save
            SetLoadoutSlot(i, ObjectGuid::Empty, true);
        }
        else
            SetLoadoutSlot(i, battlePetGuid, false);
    }
}

void BattlePetMgr::SaveSlotsToDB(SQLTransaction& trans)
{
    // no changes have been made to the loadout slots, no need to save
    if (!m_loadoutSave)
        return;

    PreparedStatement* stmt = LoginDatabase.GetPreparedStatement(LOGIN_DEL_ACCOUNT_BATTLE_PET_SLOTS);
    stmt->setUInt32(0, m_owner->GetSession()->GetAccountId());
    trans->Append(stmt);

    // save loadout slots to database
    stmt = LoginDatabase.GetPreparedStatement(LOGIN_INS_ACCOUNT_BATTLE_PET_SLOTS);
    stmt->setUInt32(0, m_owner->GetSession()->GetAccountId());
    stmt->setUInt64(1, GetLoadoutSlot(BATTLE_PET_LOADOUT_SLOT_1).GetRawValue());
    stmt->setUInt64(2, GetLoadoutSlot(BATTLE_PET_LOADOUT_SLOT_2).GetRawValue());
    stmt->setUInt64(3, GetLoadoutSlot(BATTLE_PET_LOADOUT_SLOT_3).GetRawValue());
    stmt->setUInt8 (4, m_loadoutFlags);
    trans->Append(stmt);

    m_loadoutSave = false;
}

uint8 BattlePetMgr::GetLoadoutSlotForBattlePet(ObjectGuid guid) const
{
    for (uint8 i = 0; i < BATTLE_PET_MAX_LOADOUT_SLOTS; i++)
        if (GetLoadoutSlot(i) == guid)
            return i;

    return BATTLE_PET_LOADOUT_SLOT_NONE;
}

void BattlePetMgr::UnlockLoadoutSlot(uint8 slot)
{
    if (HasLoadoutSlot(slot))
        return;

    if (slot >= BATTLE_PET_MAX_LOADOUT_SLOTS)
        return;

    switch (slot)
    {
        case BATTLE_PET_LOADOUT_SLOT_1:
            SetLoadoutFlag(BATTLE_PET_LOADOUT_SLOT_FLAG_SLOT_1);
            break;
        // unlock both second slot and trap
        case BATTLE_PET_LOADOUT_SLOT_2:
            SetLoadoutFlag(BATTLE_PET_LOADOUT_SLOT_FLAG_SLOT_2);
            SetLoadoutFlag(BATTLE_PET_LOADOUT_TRAP);
            break;
        case BATTLE_PET_LOADOUT_SLOT_3:
            SetLoadoutFlag(BATTLE_PET_LOADOUT_SLOT_FLAG_SLOT_3);
            break;
    }

    SetLoadoutSlot(slot, ObjectGuid::Empty, true);

    // alert client of new loadout slot
    SendPetBattleSlotUpdate(true, slot);
}

void BattlePetMgr::SetLoadoutSlot(uint8 slot, ObjectGuid guid, bool save)
{
    if (!HasLoadoutSlot(slot))
        return;

    m_loadout[slot] = guid;

    if (save)
        m_loadoutSave = true;
}

ObjectGuid BattlePetMgr::GetLoadoutSlot(uint8 slot) const
{
    if (!HasLoadoutSlot(slot))
        return ObjectGuid::Empty;

    return m_loadout[slot];
}

bool BattlePetMgr::HasLoadoutSlot(uint8 slot) const
{
    if (!m_loadoutFlags)
        return false;

    switch (slot)
    {
        case BATTLE_PET_LOADOUT_SLOT_1:
            return HasLoadoutFlag(BATTLE_PET_LOADOUT_SLOT_FLAG_SLOT_1);
        case BATTLE_PET_LOADOUT_SLOT_2:
            return HasLoadoutFlag(BATTLE_PET_LOADOUT_SLOT_FLAG_SLOT_2);
        case BATTLE_PET_LOADOUT_SLOT_3:
            return HasLoadoutFlag(BATTLE_PET_LOADOUT_SLOT_FLAG_SLOT_3);
    }

    return false;
}

void BattlePetMgr::SetLoadoutFlag(uint8 flag)
{
    if (HasLoadoutFlag(flag))
        return;

    m_loadoutFlags |= flag;
    m_loadoutSave = true;
}

bool BattlePetMgr::CanStoreBattlePet(uint32 species) const
{
    // check if battle pet species exists
    BattlePetSpeciesEntry const* battlePetEntry = sBattlePetSpeciesStore.LookupEntry(species);
    if (!battlePetEntry)
        return false;

    // player is only allowed a maximum of 500 battle pets
    if (BattlePets.size() > BATTLE_PET_MAX_JOURNAL_PETS)
        return false;

    // player is only allowed 3 of each battle pet unless it has BATTLE_PET_FLAG_UNIQUE flag
    uint8 speciesCount = GetBattlePetCount(species);
    if (sDB2Manager->HasBattlePetSpeciesFlag(species, BATTLE_PET_FLAG_UNIQUE) && speciesCount >= 1)
        return false;

    if (speciesCount >= BATTLE_PET_MAX_JOURNAL_SPECIES)
        return false;

    return true;
}

uint32 BattlePetMgr::GetTrapAbility() const
{
    if (HasLoadoutFlag(BATTLE_PET_LOADOUT_TRAP))
        return BATTLE_PET_ABILITY_TRAP;
    if (HasLoadoutFlag(BATTLE_PET_LOADOUT_STRONG_TRAP))
        return BATTLE_PET_ABILITY_STRONG_TRAP;
    if (HasLoadoutFlag(BATTLE_PET_LOADOUT_PRISTINE_TRAP))
        return BATTLE_PET_ABILITY_PRISTINE_TRAP;
    if (HasLoadoutFlag(BATTLE_PET_LOADOUT_GM_TRAP))
        return BATTLE_PET_ABILITY_GM_TRAP;

    return 0;
}

BattlePet* BattlePetMgr::Create(uint32 speciesId, uint8 level, uint8 breed, uint8 quality, uint8 family, bool ignoreChecks)
{
    // make sure player can store more battle pets
    if (!CanStoreBattlePet(speciesId) && !ignoreChecks)
        return nullptr;

    BattlePetSpeciesEntry const* battlePetEntry = sBattlePetSpeciesStore.LookupEntry(speciesId);
    ASSERT(battlePetEntry);

    // create and add battle pet to player
    uint64 id = sObjectMgr->GenerateBattlePetId();

    if (!breed)
        breed = sObjectMgr->GetBattlePetRandomBreedID(speciesId);

    if (!quality)
        quality = sObjectMgr->GetBattlePetRandomQuality(speciesId);

    if (!family)
        family = battlePetEntry->FamilyId;

    ObjectGuid battlePetGuid = ObjectGuid(id);

    BattlePet* battlePet = new BattlePet(battlePetGuid, speciesId, family, level, quality, breed, m_owner);
    battlePet->InitializeAbilities(false);

    BattlePets.push_back(battlePet);

    // notify client of the new battle pet
    SendBattlePetUpdate(battlePet, true);

    GetOwner()->UpdateCriteria(CRITERIA_TYPE_OBTAIN_BATTLE_PET, battlePetEntry->NpcId);
    GetOwner()->UpdateCriteria(CRITERIA_TYPE_OWN_BATTLE_PET, speciesId);
    GetOwner()->UpdateCriteria(CRITERIA_TYPE_OWN_BATTLE_PET_COUNT, 1);

    return battlePet;
}

void BattlePetMgr::Delete(BattlePet* battlePet)
{
    if (!battlePet)
        return;

    if (battlePet->GetDBState() == BATTLE_PET_DB_STATE_DELETE)
        return;

    // this shouldn't happen since the client doesn't allow releasing of slotted Battle Pets
    uint8 srcSlot = GetLoadoutSlotForBattlePet(battlePet->GetGUID());
    if (srcSlot != BATTLE_PET_LOADOUT_SLOT_NONE)
    {
        SetLoadoutSlot(srcSlot, ObjectGuid::Empty, true);
        SendPetBattleSlotUpdate(false, srcSlot);
    }

    battlePet->SetDBState(BATTLE_PET_DB_STATE_DELETE);

    SendBattlePetJournal();
}

BattlePet* BattlePetMgr::GetBattlePet(ObjectGuid guid) const
{
    for (BattlePet* battlePet : BattlePets)
        if (battlePet->GetGUID() == guid)
            return battlePet;

    return nullptr;
}

// returns the amount of battle pets the player has of a single species
uint8 BattlePetMgr::GetBattlePetCount(uint32 speciesId) const
{
    uint8 counter = 0;
    for (BattlePet const* battlePet : BattlePets)
        if (battlePet->GetSpecies() == speciesId)
            counter++;

    return counter;
}

void BattlePetMgr::UnSummonCurrentBattlePet(bool temporary)
{
    if (!m_summon || !m_summonGUID)
        return;

    // store summon id for temporary removal
    m_summonLastId = temporary ? m_summonGUID : ObjectGuid::Empty;
    m_summonGUID = ObjectGuid::Empty;

    // remove summoned battle pet from the world
    m_summon->UnSummon();

    m_summon = NULL;
}

void BattlePetMgr::ResummonLastBattlePet()
{
    if (!m_summonLastId)
        return;

    if (!m_owner->IsAlive())
        return;

    // resummon battle pet into the world
    m_summonGUID = m_summonLastId;
    m_owner->CastSpell(m_owner, sDB2Manager->GetBattlePetSummonSpell(GetBattlePet(m_summonGUID)->GetSpecies()), true);
    m_summonLastId = ObjectGuid::Empty;
}

void BattlePetMgr::SendBattlePetJournal()
{
    ByteBuffer journalData, loadoutData;

    uint32 nicknameSize = 0;

    std::vector<BattlePet const*> battlePets;
    for (BattlePet const* battlePet : BattlePets)
    {
        if (battlePet->GetDBState() == BATTLE_PET_DB_STATE_DELETE)
            continue;

        battlePets.push_back(battlePet);

        nicknameSize += battlePet->GetNickname().size();
    }

    bool HasJournalSlotsEnabled = true;

    WorldPacket data(SMSG_BATTLE_PET_JOURNAL, 6 + 2 + battlePets.size() * (2 * (1 + 8) + 2 + 1 + 4 + 2 + 4 + 4 + 4 + 2 + 4 + 2 + 4 + 4 + 4 + 4 + 2) +
                    BATTLE_PET_MAX_LOADOUT_SLOTS * (1 + 8 + 1 + 4 + 1) + nicknameSize);

    data.WriteBits(battlePets.size(), 19);

    for (BattlePet const* battlePet : battlePets)
    {
        ByteBuffer ownerData;

        ObjectGuid battlePetGUID = battlePet->GetGUID();

        uint8 Quality = battlePet->GetQuality();
        uint8 Breed = battlePet->GetBreed();
        uint32 Flags = battlePet->GetFlags();

        bool HasQuality = Quality != 0;
        bool HasBreed = Breed != 0;
        bool HasFlags = Flags != 0;

        bool HasNoRename = false;
        bool HasOwnerInfo = battlePet->GetOwner() && sDB2Manager->HasBattlePetSpeciesFlag(battlePet->GetSpecies(), BATTLE_PET_FLAG_NOT_ACCOUNT_BOUND);

        data.WriteBit(!HasFlags);

        data.WriteGuidMask(battlePetGUID, 3, 7);

        data.WriteBits(battlePet->GetNickname().size(), 7);

        data.WriteBit(HasOwnerInfo);

        if (HasOwnerInfo)
        {
            ObjectGuid ownerGuid = battlePet->GetOwner()->GetGUID();

            data.WriteGuidMask(ownerGuid, 4, 1, 6, 7, 0, 5, 2, 3);

            ownerData.WriteGuidBytes(ownerGuid, 1);

            ownerData << uint32(realmID);

            ownerData.WriteGuidBytes(ownerGuid, 4, 3);

            ownerData << uint32(realmID);

            ownerData.WriteGuidBytes(ownerGuid, 0, 6, 7, 2, 5);
        }

        data.WriteGuidMask(battlePetGUID, 0, 2, 6);

        data.WriteBit(HasNoRename);

        data.WriteGuidMask(battlePetGUID, 1, 5);

        data.WriteBit(!HasBreed);

        data.WriteGuidMask(battlePetGUID, 4);

        data.WriteBit(!HasQuality);

        if (HasQuality)
            journalData << uint8(Quality);

        journalData << uint32(battlePet->GetPower());

        journalData.WriteGuidBytes(battlePetGUID, 7);

        journalData << uint16(battlePet->GetLevel());

        if (HasOwnerInfo)
            journalData.append(ownerData);

        journalData << uint32(battlePet->GetCurrentHealth());

        if (HasBreed)
            journalData << uint16(Breed);

        journalData << uint32(battlePet->GetSpecies());

        journalData.WriteGuidBytes(battlePetGUID, 2);

        if (HasFlags)
            journalData << uint16(Flags);

        journalData << uint32(battlePet->GetNpc());

        journalData << uint32(battlePet->GetDisplayId());

        journalData << uint32(battlePet->GetSpeed());

        journalData.WriteString(battlePet->GetNickname());

        journalData.WriteGuidBytes(battlePetGUID, 6, 5);

        journalData << uint32(battlePet->GetMaxHealth());

        journalData.WriteGuidBytes(battlePetGUID, 4);

        journalData << uint16(battlePet->GetXp());

        journalData.WriteGuidBytes(battlePetGUID, 0, 1, 3);
    }

    data.WriteBit(HasJournalSlotsEnabled);

    data.WriteBits(BATTLE_PET_MAX_LOADOUT_SLOTS, 25);

    for (uint8 i = 0; i < BATTLE_PET_MAX_LOADOUT_SLOTS; i++)
    {
        ObjectGuid loadoutGUID = GetLoadoutSlot(i);

        bool Unk = false;
        bool HasCollar = false;
        bool HasSlotIndex = true;
        bool IsLocked = !HasLoadoutSlot(i);

        data.WriteBit(IsLocked);
        data.WriteBit(!HasCollar);
        data.WriteBit(!HasSlotIndex);
        data.WriteBit(!Unk);

        data.WriteGuidMask(loadoutGUID, 0, 1, 7, 6, 4, 2, 5, 3);

        loadoutData.WriteGuidBytes(loadoutGUID, 5, 1, 7, 2, 3, 0, 4, 6);

        if (HasCollar)
            loadoutData << uint32(0);   // CollarID

        if (HasSlotIndex)
            loadoutData << uint8(i);
    }

    data.append(loadoutData);
    data.append(journalData);

    data << uint16(0);  // TrapLevel

    m_owner->GetSession()->SendPacket(&data);
}

void BattlePetMgr::SendBattlePetUpdate(BattlePet* battlePet, bool notification)
{
    if (!battlePet)
        return;

    CreatureTemplate const* creatureTemplate = sObjectMgr->GetCreatureTemplate(sBattlePetSpeciesStore.LookupEntry(battlePet->GetSpecies())->NpcId);
    if (!creatureTemplate)
        return;

    ObjectGuid battlePetGUID = battlePet->GetGUID();

    uint8 Quality = battlePet->GetQuality();
    uint8 Breed = battlePet->GetBreed();
    uint32 Flags = battlePet->GetFlags();

    bool HasQuality = Quality != 0;
    bool HasBreed = Breed != 0;
    bool HasFlags = Flags != 0;

    bool HasNoRename = false;
    bool HasOwnerInfo = battlePet->GetOwner() && sDB2Manager->HasBattlePetSpeciesFlag(battlePet->GetSpecies(), BATTLE_PET_FLAG_NOT_ACCOUNT_BOUND);

    WorldPacket data(SMSG_BATTLE_PET_UPDATE, 1 + 8 + 4 + (HasOwnerInfo ? (1 + 8 + 4 + 4) : 0) + (HasBreed ? 2 : 0) + (HasQuality ? 1 : 0) + (HasFlags ? 2 : 0) +
                       battlePet->GetNickname().size() + 4 + 4 + 4 + 4 + 4 + 4 + 2 + 4 + 2);

    data.WriteBits(1, 19);

    data.WriteGuidMask(battlePetGUID, 4, 1, 7);

    data.WriteBit(!HasQuality);
    data.WriteBit(!HasBreed);

    data.WriteGuidMask(battlePetGUID, 5);

    data.WriteBit(HasNoRename);

    data.WriteGuidMask(battlePetGUID, 2);

    data.WriteBit(!HasFlags);

    data.WriteBit(HasOwnerInfo);

    data.WriteGuidMask(battlePetGUID, 6);

    if (HasOwnerInfo)
    {
        ObjectGuid ownerGuid = battlePet->GetOwner()->GetGUID();

        data.WriteGuidMask(ownerGuid, 5, 2, 4, 1, 6, 0, 7, 3);
    }

    data.WriteGuidMask(battlePetGUID, 3);

    data.WriteBits(battlePet->GetNickname().size(), 7);

    data.WriteGuidMask(battlePetGUID, 0);

    data.WriteBit(notification);

    data.FlushBits();

    if (HasOwnerInfo)
    {
        ObjectGuid ownerGuid = battlePet->GetOwner()->GetGUID();

        data.WriteGuidBytes(ownerGuid, 3);

        data << uint32(realmID);
        data << uint32(realmID);

        data.WriteGuidBytes(ownerGuid, 0, 4, 2, 6, 1, 7, 5);
    }

    data.WriteGuidBytes(battlePetGUID, 1);

    data.WriteString(battlePet->GetNickname());

    data << uint32(battlePet->GetCurrentHealth());
    data << uint32(battlePet->GetSpecies());

    data.WriteGuidBytes(battlePetGUID, 0);

    data << uint32(battlePet->GetPower());
    data << uint32(battlePet->GetSpeed());

    if (HasBreed)
        data << uint16(Breed);

    data.WriteGuidBytes(battlePetGUID, 4);

    data << uint32(creatureTemplate->Entry);
    data << uint32(battlePet->GetMaxHealth());

    data.WriteGuidBytes(battlePetGUID, 6);

    if (HasQuality)
        data << uint8(Quality);

    data.WriteGuidBytes(battlePetGUID, 2, 3);

    data << uint16(battlePet->GetXp());

    data.WriteGuidBytes(battlePetGUID, 7);

    if (HasFlags)
        data << uint16(Flags);

    data.WriteGuidBytes(battlePetGUID, 5);

    data << uint32(creatureTemplate->Modelid1);

    data << uint16(battlePet->GetLevel());

    m_owner->GetSession()->SendPacket(&data);
}

void BattlePetMgr::SendPetBattleSlotUpdate(bool notification, uint8 slot)
{
    bool IsAutoSlotted = false;
    bool Unk = false;
    bool HasCollar = false;
    bool HasSlotIndex = true;
    bool IsLocked = !HasLoadoutSlot(slot);

    ObjectGuid loadoutGUID = GetLoadoutSlot(slot);

    WorldPacket data(SMSG_BATTLE_PET_SLOT_UPDATE, 4 + 1 + 8 + (HasCollar ? 4 : 0) + (HasSlotIndex ? 1 : 0));

    data.WriteBits(1, 25);

    data.WriteBit(IsAutoSlotted);

    data.WriteBit(!HasSlotIndex);
    data.WriteBit(!HasCollar);
    data.WriteBit(IsLocked);
    data.WriteBit(!Unk);

    data.WriteGuidMask(loadoutGUID, 4, 5, 2, 1, 0, 3, 7, 6);

    data.WriteBit(notification);

    data.FlushBits();

    data.WriteGuidBytes(loadoutGUID, 0, 3, 2, 1, 6, 4, 5, 7);

    if (HasCollar)
        data << uint32(0);   // CollarID

    if (HasSlotIndex)
        data << uint8(slot);

    m_owner->GetSession()->SendPacket(&data);
}
