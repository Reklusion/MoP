/*
 * Copyright (C) 2015-2016 Theatre of Dreams <http://www.theatreofdreams.eu>
 * Copyright (C) 2015 Warmane <http://www.warmane.com/>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "BattlePetSpawnMgr.h"
#include "Creature.h"
#include "DatabaseEnv.h"
#include "DBCStores.h"
#include "DB2Stores.h"
#include "Log.h"
#include "ObjectAccessor.h"
#include "ObjectMgr.h"
#include "MapManager.h"

BattlePetSpawnMgr::BattlePetSpawnMgr()
{
    _battlePetInfoStore.clear();
    _wildBattlePetInfoStore.clear();
    _wildBattlePetZoneLimitsStore.clear();

    _spawnTimer = 60 * IN_MILLISECONDS;
}

BattlePetSpawnMgr::~BattlePetSpawnMgr()
{
    for (auto itr : _wildBattlePetInfoStore)
        delete itr;

    for (auto itr : _battlePetInfoStore)
        delete itr.second;

    _wildBattlePetInfoStore.clear();
    _battlePetInfoStore.clear();
    _wildBattlePetZoneLimitsStore.clear();
}

BattlePet* BattlePetSpawnMgr::GetWildBattlePet(ObjectGuid guid)
{
    auto itr = _battlePetInfoStore.find(guid);
    if (itr != _battlePetInfoStore.end())
        return itr->second;

    return nullptr;
}

void BattlePetSpawnMgr::AddWildCreature(Creature* creature, uint32 zoneId, int32 mapId)
{
    uint32 minLevel = 0;
    uint32 maxLevel = 0;
    if (AreaTableEntry const* area = sAreaStore.LookupEntry(zoneId))
    {
        minLevel = area->WildBattlePetLevelMin;
        maxLevel = area->WildBattlePetLevelMax;
    }

    // if minLevel ==0 and maxLevel = 0 means zone without battle pets
    if (!minLevel && !maxLevel)
        return;

    uint32 replacementEntry = sObjectMgr->GetBattlePetReplacementForCreature(creature->GetEntry());
    if (!replacementEntry)
        return;

    BattlePetSpeciesEntry const* speciesEntry = sDB2Manager->GetBattlePetSpeciesIdFromNpcEntry(replacementEntry);
    if (!speciesEntry)
        return;

    uint32 zoneLimit = sObjectMgr->GetBattlePetZoneLimitForFamily(zoneId, speciesEntry->FamilyId);

    WildBattlePetInfo* wildPet = new WildBattlePetInfo(creature->GetGUID(), speciesEntry->Id, zoneId, mapId, zoneLimit, minLevel, maxLevel);

    _wildBattlePetInfoStore.push_back(wildPet);
}

void BattlePetSpawnMgr::AddWildCreature(Creature* creature, uint32 zoneId)
{
    uint32 minLevel = 0;
    uint32 maxLevel = 0;
    if (AreaTableEntry const* area = sAreaStore.LookupEntry(zoneId))
    {
        minLevel = area->WildBattlePetLevelMin;
        maxLevel = area->WildBattlePetLevelMax;
    }

    // if minLevel ==0 and maxLevel = 0 means zone without battle pets
    if (!minLevel && !maxLevel)
        return;

    BattlePetSpeciesEntry const* speciesEntry = sDB2Manager->GetBattlePetSpeciesIdFromNpcEntry(creature->GetEntry());
    if (!speciesEntry)
        return;

    // generate battle pet information for replacement creature
    uint8 breed = sObjectMgr->GetBattlePetRandomBreedID(speciesEntry->Id);
    uint8 quality = sObjectMgr->GetBattlePetRandomQuality(speciesEntry->Id);
    uint8 level = Math::Rand(minLevel, maxLevel);

    BattlePet* battlePet = new BattlePet(ObjectGuid::Empty, speciesEntry->Id, speciesEntry->FamilyId, level, quality, breed);
    battlePet->InitializeAbilities(true);

    // apply random movement to replacement creature
    creature->SetRespawnRadius(12.0f);
    creature->SetDefaultMovementType(RANDOM_MOTION_TYPE);
    creature->GetMotionMaster()->Initialize();

    creature->SetUInt32Value(UNIT_WILD_BATTLE_PET_LEVEL, level);

    _battlePetInfoStore[creature->GetGUID()] = battlePet;
}

void BattlePetSpawnMgr::Update(uint32 diff)
{
    if (_spawnTimer <= diff)
    {
        for (WildBattlePetInfo* wildPet : _wildBattlePetInfoStore)
        {
            // do not spawn again battle pet
            if (!wildPet->WildPetGUID.IsEmpty())
                continue;

            SpawnWildBattlePet(wildPet);
        }

        _spawnTimer = 60 * IN_MILLISECONDS;
    }
    else
        _spawnTimer -= diff;
}

void BattlePetSpawnMgr::EnteredBattle(Creature* creature)
{
    creature->SetRespawnTime(MONTH);
    creature->DespawnOrUnsummon();
}

void BattlePetSpawnMgr::LeftBattle(Creature* creature, bool killed)
{
    // wild pet battle defeated player or pet battle was abandoned
    if (!killed)
    {
        BattlePet* battlePet = GetWildBattlePet(creature->GetGUID());
        if (!battlePet)
            return;

        battlePet->SetCurrentHealth(battlePet->GetMaxHealth());

        // respawn wild battle pet in the world
        creature->Respawn(true);

        creature->SetFlag64(UNIT_NPC_FLAGS, UNIT_NPC_FLAG_PETBATTLE);
        creature->SetUInt32Value(UNIT_WILD_BATTLE_PET_LEVEL, battlePet->GetLevel());
    }
    else
        RemoveWildBattlePet(creature);
}

void BattlePetSpawnMgr::SpawnWildBattlePet(WildBattlePetInfo* wildPet)
{
    BattlePetSpeciesEntry const* speciesEntry = sBattlePetSpeciesStore.LookupEntry(wildPet->SpeciesID);
    if (!speciesEntry)
        return;

    if (wildPet->ZoneLimit && wildPet->ZoneLimit >= _wildBattlePetZoneLimitsStore[wildPet->ZoneID][speciesEntry->FamilyId])
        return;

    Map* map = sMapMgr->FindMap(wildPet->MapID, 0);
    if (!map)
        return;

    Creature* oldCreature = map->GetCreature(wildPet->CreatureGUID);
    if (!oldCreature)
        return;

    if (!oldCreature->IsAlive())
        return;

    Position pos = oldCreature->GetPosition();

    // initialise replacement creature
    Creature* replacementCreature = new Creature();
    if (!replacementCreature->Create(map->GenerateLowGuid<HighGuid::Creature>(), map, speciesEntry->NpcId, 0, 
        pos.GetPositionX(), pos.GetPositionY(), pos.GetPositionZ(), pos.GetOrientation()))
    {
        delete replacementCreature;
        return;
    }

    if (!map->AddToMap(replacementCreature))
    {
        delete replacementCreature;
        return;
    }

    // generate battle pet information for replacement creature
    uint8 breed = sObjectMgr->GetBattlePetRandomBreedID(speciesEntry->Id);
    uint8 quality = sObjectMgr->GetBattlePetRandomQuality(speciesEntry->Id);
    uint8 level = Math::Rand(wildPet->MinLevel, wildPet->MaxLevel);

    // apply random movement to replacement creature
    replacementCreature->SetRespawnRadius(12.0f);
    replacementCreature->SetDefaultMovementType(RANDOM_MOTION_TYPE);
    replacementCreature->Respawn(true);

    replacementCreature->SetFlag64(UNIT_NPC_FLAGS, UNIT_NPC_FLAG_PETBATTLE);
    replacementCreature->SetUInt32Value(UNIT_WILD_BATTLE_PET_LEVEL, level);

    BattlePet* battlePet = new BattlePet(ObjectGuid::Empty, speciesEntry->Id, speciesEntry->FamilyId, level, quality, breed);
    battlePet->InitializeAbilities(true);

    // despawn replaced creature
    oldCreature->SetRespawnTime(MONTH);
    oldCreature->DespawnOrUnsummon();

    _battlePetInfoStore[replacementCreature->GetGUID()] = battlePet;

    ++_wildBattlePetZoneLimitsStore[wildPet->ZoneID][speciesEntry->FamilyId];

    wildPet->WildPetGUID = replacementCreature->GetGUID();
}

void BattlePetSpawnMgr::RemoveWildBattlePet(Creature* creature, bool deleteCreature)
{
    if (deleteCreature)
    {
        for (std::vector<WildBattlePetInfo*>::iterator itr = _wildBattlePetInfoStore.begin(); itr != _wildBattlePetInfoStore.end();)
        {
            if ((*itr)->CreatureGUID == creature->GetGUID())
            {
                delete (*itr);
                _wildBattlePetInfoStore.erase(itr);
                break;
            }
            else
                ++itr;
        }

        return;
    }

    // remove battle pet information
    BattlePet* battlePet = _battlePetInfoStore[creature->GetGUID()];
    if (!battlePet)
        return;

    delete battlePet;
    battlePet = nullptr;

    _battlePetInfoStore.erase(creature->GetGUID());

    for (WildBattlePetInfo* wildPet : _wildBattlePetInfoStore)
        if (wildPet->WildPetGUID == creature->GetGUID())
        {
            Map* map = sMapMgr->FindMap(wildPet->MapID, 0);
            if (!map)
                return;

            // remove replacemented creature
            creature->AddObjectToRemoveList();

            wildPet->WildPetGUID = ObjectGuid::Empty;

            Creature* oldCreature = map->GetCreature(wildPet->CreatureGUID);
            if (!oldCreature)
                return;

            // respawn old creature
            oldCreature->SetRespawnTime(oldCreature->GetCreatureData()->spawntimesecs);
            oldCreature->Respawn(true);

            break;
        }
}
