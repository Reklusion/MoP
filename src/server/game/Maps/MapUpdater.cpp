/*
 * Copyright (C) 2011-2015 Project SkyFire <http://www.projectskyfire.org/>
 * Copyright (C) 2008-2015 TrinityCore <http://www.trinitycore.org/>
 * Copyright (C) 2005-2015 MaNGOS <http://getmangos.com/>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 3 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "MapUpdater.h"

#include "Map.h"
#include "Player.h"
#include "MapManager.h"

#include <boost/asio/signal_set.hpp>
#include <boost/thread/futures/wait_for_all.hpp>

namespace Tod
{
    MapUpdater::MapUpdater( uint8_t workersCount )
    {
        //! will prevent io_service from quiting
        m_work = std::make_unique< boost::asio::io_service::work >( m_ioService );

        std::mutex lock;
        for ( uint8_t idx = 0; idx < workersCount; ++idx )
        {
            m_threadPool.create_thread( [ &, this ]
            {
                m_ioService.run();
            } );
        }
    }

    MapUpdater::~MapUpdater()
    {
        m_work.reset();

        m_ioService.stop();
        m_threadPool.join_all();
    }

    void TryMapRecovery( Map * map )
    {
        const MapRefManager & players = map->GetPlayers();
        for ( auto & it : players )
        {
            Player * player = it.GetSource();
            player->GetSession()->KickPlayer();
        }

        map->ForceUnloadTimer();
    }

    typedef std::pair< Map*, bool >        UpdateStatus;
    typedef std::promise< UpdateStatus >   UpdatePromise;

    void MapUpdater::Update( uint32_t diff )
    {
        std::vector< std::shared_ptr< UpdatePromise > > tasks;
        const auto PostMapUpdate = [ &tasks, this ]( Map * map, float diff )
        {
            auto result = std::make_shared< UpdatePromise >();
            tasks.push_back( result );

            const auto handler = [ result, map ]( const boost::system::error_code & error, int signal )
            {
                if ( !error )
                {
                    result->set_value( { map->IsInstance() ? map : nullptr, false } );
                }
            };

            m_ioService.post( [ result, map, diff, handler, this ]
            {
                boost::asio::signal_set signals( m_ioService, SIGSEGV );
                signals.async_wait( handler );

                map->Update( diff );

                result->set_value( { map, true } );

                signals.cancel();
            } );
        };

        auto & maps = sMapMgr->GetMaps();
        for ( auto & it : maps )
        {
            Map * map = it.second;
            if ( MapInstanced * instanced = map->ToMapInstanced() )
            {
                //! will cleanup old instances
                instanced->Update( diff );

                auto & instances = instanced->GetInstancedMaps();
                for ( auto & it : instances )
                {
                    Map * instance = it.second;
                    if ( !instance->CanUnload( 0 ) )
                    {
                        PostMapUpdate( instance, diff );
                    }
                }
            }
            else
            {
                PostMapUpdate( map, diff );
            }
        }

        for ( auto task : tasks )
        {
            Map * map = nullptr;
            bool success = false;

            std::tie( map, success ) = task->get_future().get();
            if ( success ) //! update successful
            {
                map->DelayedUpdate( diff );
            }
            else if ( map != nullptr ) //! SIGSEGV received while updating instanceable map
            {
                TC_LOG_FATAL( "map_updater", "Map: %u - %u crashed, trying to recover!", map->GetId(), map->GetInstanceId() );
                TryMapRecovery( map );
            }
            else //! SIGSEGV received while updating non instanceable map
            {
                TC_LOG_FATAL( "map_updater", "Map: %u crashed, we can't recover!", map->GetId() );
                ASSERT( false && "Non instanced map crashed! We won't even try to recovery from that.");
            }
        }
    }
}
