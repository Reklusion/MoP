/*
 * Copyright (C) 2011-2015 Project SkyFire <http://www.projectskyfire.org/>
 * Copyright (C) 2008-2015 TrinityCore <http://www.trinitycore.org/>
 * Copyright (C) 2005-2015 MaNGOS <http://getmangos.com/>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 3 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SF_SOCIALMGR_H
#define SF_SOCIALMGR_H

#include "DatabaseEnv.h"
#include "ObjectGuid.h"

#include "Singleton/Singleton.hpp"

class SocialMgr;
class PlayerSocial;
class Player;
class WorldPacket;

enum FriendStatus
{
    FRIEND_STATUS_OFFLINE   = 0x00,
    FRIEND_STATUS_ONLINE    = 0x01,
    FRIEND_STATUS_AFK       = 0x02,
    FRIEND_STATUS_DND       = 0x04,
    FRIEND_STATUS_RAF       = 0x08
};

enum SocialFlag
{
    SOCIAL_FLAG_FRIEND          = 0x01,
    SOCIAL_FLAG_IGNORED         = 0x02,
    SOCIAL_FLAG_MUTED           = 0x04,
    SOCIAL_FLAG_RECRUIT_A_FLAG  = 0x08,

    SOCIAL_FLAG_ALL             = SOCIAL_FLAG_FRIEND | SOCIAL_FLAG_IGNORED | SOCIAL_FLAG_MUTED
};

struct FriendInfo
{
    FriendStatus Status;
    uint8 Flags;
    uint32 Area;
    uint8 Level;
    uint8 Class;
    std::string Note;

    FriendInfo() : Status(FRIEND_STATUS_OFFLINE), Flags(0), Area(0), Level(0), Class(0), Note()
    { }

    FriendInfo(uint8 flags, std::string const& note) : Status(FRIEND_STATUS_OFFLINE), Flags(flags), Area(0), Level(0), Class(0), Note(note)
    { }
};

typedef std::map<ObjectGuid, FriendInfo> PlayerSocialMap;
typedef std::map<ObjectGuid, PlayerSocial> SocialMap;

/// Results of friend related commands
enum FriendsResult
{
    FRIEND_DB_ERROR             = 0,
    FRIEND_LIST_FULL            = 1,
    FRIEND_ONLINE               = 2,
    FRIEND_OFFLINE              = 3,
    FRIEND_NOT_FOUND            = 4,
    FRIEND_REMOVED              = 5,
    FRIEND_ADDED_ONLINE         = 6,
    FRIEND_ADDED_OFFLINE        = 7,
    FRIEND_ALREADY              = 8,
    FRIEND_SELF                 = 9,
    FRIEND_ENEMY                = 10,
    FRIEND_IGNORE_FULL          = 11,
    FRIEND_IGNORE_SELF          = 12,
    FRIEND_IGNORE_NOT_FOUND     = 13,
    FRIEND_IGNORE_ALREADY       = 14,
    FRIEND_IGNORE_ADDED         = 15,
    FRIEND_IGNORE_REMOVED       = 16,
    FRIEND_IGNORE_AMBIGUOUS     = 17,                         // That name is ambiguous, type more of the player's server name
    FRIEND_MUTE_FULL            = 18,
    FRIEND_MUTE_SELF            = 19,
    FRIEND_MUTE_NOT_FOUND       = 20,
    FRIEND_MUTE_ALREADY         = 21,
    FRIEND_MUTE_ADDED           = 22,
    FRIEND_MUTE_REMOVED         = 23,
    FRIEND_MUTE_AMBIGUOUS       = 24,                         // That name is ambiguous, type more of the player's server name
    FRIEND_UNKNOWN              = 28                          // Unknown friend response from server
};

#define SOCIALMGR_FRIEND_LIMIT  100
#define SOCIALMGR_IGNORE_LIMIT  50

class PlayerSocial
{
    friend class SocialMgr;
    public:
        PlayerSocial();

        // adding/removing
        bool AddToSocialList(ObjectGuid friendGuid, bool ignore);
        void RemoveFromSocialList(ObjectGuid friendGuid, bool ignore);
        void SetFriendNote(ObjectGuid friendGuid, std::string note);

        // Packet send's
        void SendSocialList(Player* player, uint32 flags);

        // Misc
        bool HasFriend(ObjectGuid friendGuid);
        bool HasIgnore(ObjectGuid ignoreGuid);
        ObjectGuid GetPlayerGUID() const { return m_playerGUID; }
        void SetPlayerGUID(ObjectGuid guid) { m_playerGUID = guid; }
        uint32 GetNumberOfSocialsWithFlag(SocialFlag flag);

    private:
        PlayerSocialMap m_playerSocialMap;
        ObjectGuid m_playerGUID;
};

class SocialMgr
{
    friend class Tod::Singleton<SocialMgr>;

    SocialMgr();
    ~SocialMgr();

    public:
        // Misc
        void RemovePlayerSocial(ObjectGuid guid) { m_socialMap.erase(guid); }

        void GetFriendInfo(Player* player, ObjectGuid friendGUID, FriendInfo &friendInfo);
        // Packet management
        void SendFriendStatus(Player* player, FriendsResult result, ObjectGuid friendGuid, bool broadcast);
        void BroadcastToFriendListers(Player* player, WorldPacket* packet);
        // Loading
        PlayerSocial* LoadFromDB(PreparedQueryResult result, ObjectGuid guid);

    private:
        SocialMap m_socialMap;
};

#define sSocialMgr Tod::Singleton<SocialMgr>::GetSingleton()

#endif
