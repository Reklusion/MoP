/*
 * Copyright (C) 2011-2015 Project SkyFire <http://www.projectskyfire.org/>
 * Copyright (C) 2008-2015 TrinityCore <http://www.trinitycore.org/>
 * Copyright (C) 2005-2015 MaNGOS <http://getmangos.com/>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 3 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "Common.h"
#include "Language.h"
#include "WorldPacket.h"
#include "WorldSession.h"
#include "World.h"
#include "ObjectMgr.h"
#include "GuildMgr.h"
#include "Log.h"
#include "Opcodes.h"
#include "Guild.h"
#include "GossipDef.h"
#include "SocialMgr.h"
#include "CharacterCache.h"
#include "PetitionMgr.h"

#define CHARTER_DISPLAY_ID 16161
#define GUILD_CHARTER 5863
#define GUILD_CHARTER_COST 1000
#define MAX_PETITION_CHOICE_TEXTS 10

void WorldSession::HandlePetitionBuyOpcode(WorldPacket& recvData)
{
    ObjectGuid guid;

    uint8 nameLength = 0;

    std::string name;

    recvData.ReadGuidMask(guid, 5, 2, 3);

    nameLength = recvData.ReadBits(7);

    recvData.ReadGuidMask(guid, 4, 1, 7, 0, 6);

    name = recvData.ReadString(nameLength);

    recvData.ReadGuidBytes(guid, 1, 7, 4, 6, 0, 5, 2, 3);

    // prevent cheating
    Creature* creature = GetPlayer()->GetNPCIfCanInteractWith(guid, UNIT_NPC_FLAG_PETITIONER);
    if (!creature)
    {
        TC_LOG_DEBUG("network", "WORLD: HandlePetitionBuyOpcode - %s not found or you can't interact with him.", guid.ToString().c_str());
        return;
    }

    // remove fake death
    if (GetPlayer()->HasUnitState(UNIT_STATE_DIED))
        GetPlayer()->RemoveAurasByType(SPELL_AURA_FEIGN_DEATH);

    uint32 charterid = GUILD_CHARTER;
    uint32 cost = GUILD_CHARTER_COST;

    // if tabard designer, then trying to buy a guild charter.
    // do not let if already in guild.
    if (_player->GetGuildId())
        return;

    if (sGuildMgr->GetGuildByName(name))
    {
        Guild::SendCommandResult(this, GUILD_COMMAND_CREATE, ERR_GUILD_NAME_EXISTS_S, name);
        return;
    }

    if (sObjectMgr->IsReservedName(name) || !ObjectMgr::IsValidCharterName(name))
    {
        Guild::SendCommandResult(this, GUILD_COMMAND_CREATE, ERR_GUILD_NAME_INVALID, name);
        return;
    }

    ItemTemplate const* pProto = sObjectMgr->GetItemTemplate(charterid);
    if (!pProto)
    {
        _player->SendBuyError(BUY_ERR_CANT_FIND_ITEM, NULL, charterid, 0);
        return;
    }

    if (!_player->HasEnoughMoney(uint64(cost)))
    {                                                       //player hasn't got enough money
        _player->SendBuyError(BUY_ERR_NOT_ENOUGHT_MONEY, creature, charterid, 0);
        return;
    }

    ItemPosCountVec dest;
    InventoryResult msg = _player->CanStoreNewItem(NULL_BAG, NULL_SLOT, dest, charterid, pProto->GetBuyCount());
    if (msg != EQUIP_ERR_OK)
    {
        _player->SendEquipError(msg, NULL, NULL, charterid);
        return;
    }

    _player->ModifyMoney(-(int32)cost);
    Item* charter = _player->StoreNewItem(dest, charterid, true);
    if (!charter)
        return;

    charter->SetUInt32Value(ITEM_ENCHANTMENT, charter->GetGUID().GetCounter());

    // ITEM_ENCHANTMENT is guild id
    // ITEM_ENCHANTMENT+1 is current signatures count (showed on item)
    charter->SetState(ITEM_CHANGED, _player);
    _player->SendNewItem(charter, 1, true, false);

    // a petition is invalid, if both the owner and the type matches
    // we checked above, if this player is in an arenateam, so this must be
    // datacorruption
    CharacterDatabase.EscapeString(name);
    if (Petition const* petition = sPetitionMgr->GetPetitionByOwner(_player->GetGUID()))
    {
        // clear from petition store
        sPetitionMgr->RemovePetition(petition->petitionGuid);
        TC_LOG_DEBUG("network", "Invalid petition GUID: %u", petition->petitionGuid.GetCounter());
    }

    // fill petition store
    sPetitionMgr->AddPetition(charter->GetGUID(), _player->GetGUID(), name, false);
}

void WorldSession::HandlePetitionShowSignOpcode(WorldPacket& recvData)
{
    uint8 playerCount = 0;

    ObjectGuid petitionGuid;

    recvData.ReadGuidMask(petitionGuid, 3, 7, 2, 4, 5, 6, 0, 1);
    recvData.ReadGuidBytes(petitionGuid, 2, 4, 5, 7, 1, 0, 3, 6);

    Petition const* petition = sPetitionMgr->GetPetition(petitionGuid);
    if (!petition)
    {
        TC_LOG_DEBUG("entities.player.items", "Petition %s is not found for player %u %s", petitionGuid.ToString().c_str(), GetPlayer()->GetGUID().GetCounter(), GetPlayer()->GetName().c_str());
        return;
    }

    // if guild petition and has guild => error, return;
    if (_player->GetGuildId())
        return;

    SendPetitionSigns(petition, _player);
}

void WorldSession::SendPetitionSigns(Petition const* petition, Player* sendTo)
{
    SignaturesVector const& signatures = petition->signatures;

    ObjectGuid playerGuid = petition->ownerGuid;
    ObjectGuid petitionGuid = petition->petitionGuid;

    ByteBuffer playerData;

    WorldPacket data(SMSG_PETITION_SHOW_SIGNATURES, 2 * (1 + 8) + 3 + signatures.size() * (1 + 8 + 4) + 4);

    data.WriteGuidMask(playerGuid, 1);

    data.WriteGuidMask(petitionGuid, 3);

    data.WriteGuidMask(playerGuid, 3);

    data.WriteGuidMask(petitionGuid, 4, 0);

    data.WriteGuidMask(playerGuid, 7, 5);

    data.WriteGuidMask(petitionGuid, 1, 5, 7);

    data.WriteGuidMask(playerGuid, 0, 6);

    data.WriteGuidMask(petitionGuid, 6);

    data.WriteGuidMask(playerGuid, 2, 4);

    data.WriteBits(signatures.size(), 21);

    for (Signature const& signature : signatures)
    {
        ObjectGuid singatureGuid = signature.second;

        data.WriteGuidMask(singatureGuid, 2, 0, 4, 7, 5, 1, 6, 3);

        playerData.WriteGuidBytes(singatureGuid, 6, 0, 1, 3, 2, 5, 7, 4);

        playerData << uint32(1); // Choice ??? Blizzard also stores declined players ???
    }

    data.WriteGuidMask(petitionGuid, 2);

    if (!playerData.empty())
        data.append(playerData);
    else
        data.FlushBits();

    data.WriteGuidBytes(petitionGuid, 6, 5, 4);

    data.WriteGuidBytes(playerGuid, 4);

    data.WriteGuidBytes(petitionGuid, 1);

    data << uint32(petition->petitionGuid.GetCounter());

    data.WriteGuidBytes(petitionGuid, 2, 3, 7);

    data.WriteGuidBytes(playerGuid, 5, 6, 3, 7, 1, 0);

    data.WriteGuidBytes(petitionGuid, 0);

    data.WriteGuidBytes(playerGuid, 2);

    sendTo->GetSession()->SendPacket(&data);
}

void WorldSession::HandlePetitionQueryOpcode(WorldPacket& recvData)
{
    uint32 guildGuid = 0;

    ObjectGuid petitionGuid;

    recvData >> guildGuid;

    recvData.ReadGuidMask(petitionGuid, 2, 3, 1, 0, 4, 7, 6, 5);
    recvData.ReadGuidBytes(petitionGuid, 0, 4, 7, 5, 1, 6, 3, 2);

    if (petitionGuid.GetCounter() != guildGuid)
        return;

    SendPetitionQueryOpcode(petitionGuid);
}

void WorldSession::SendPetitionQueryOpcode(ObjectGuid petitionGuid)
{
    ObjectGuid ownerGuid;
    std::string name = "NO_NAME_FOR_GUID";

    Petition const* petition = sPetitionMgr->GetPetition(petitionGuid);
    if (petition)
    {
        ownerGuid = petition->ownerGuid;
        name = petition->petitionName;
    }

    bool HasData = petition != nullptr;

    uint32 MinSignatures = sWorld->getIntConfig(CONFIG_REQUIRED_PETITION_SIGNS);
    uint32 MaxSignatures = sWorld->getIntConfig(CONFIG_REQUIRED_PETITION_SIGNS);
    uint32 DeadLine = time(NULL) + YEAR;
    uint32 IssueDate = 0;

    uint32 AllowedGuildID = 0;
    uint32 AllowedClasses = 0;
    uint32 AllowedRaces = 0;
    uint16 AllowedGender = 0;
    uint32 AllowedMinLevel = 0;
    uint32 AllowedMaxLevel = 0;

    uint32 NumChoices = 0;
    uint32 StaticType = 0;
    uint32 Muid = 0;
    uint32 Muid2 = 0;

    std::string BodyText;

    std::array<std::string, MAX_PETITION_CHOICE_TEXTS> ChoiceTexts;

    uint32 ChoicesLength = 0;
    for (uint8 i = 0; i < MAX_PETITION_CHOICE_TEXTS; ++i)
        ChoicesLength += ChoiceTexts[i].length();

    WorldPacket data(SMSG_PETITION_QUERY_RESPONSE, 4 + 1 + (HasData ? ((MAX_PETITION_CHOICE_TEXTS * 1) + 1 + 8 + 3 + name.length() +
                    BodyText.length() + 13 * (4) + ChoicesLength) : 0));

    data << uint32(petitionGuid.GetCounter());

    data.WriteBit(HasData);

    if (HasData)
    {
        for (uint8 i = 0; i < MAX_PETITION_CHOICE_TEXTS; ++i)
            data.WriteBits(ChoiceTexts[i].length(), 6);

        data.WriteGuidMask(ownerGuid, 2, 4);

        data.WriteBits(BodyText.length(), 12);

        data.WriteGuidMask(ownerGuid, 0, 7, 3, 6, 5);

        data.WriteBits(name.size(), 7);

        data.WriteGuidMask(ownerGuid, 1);
    }

    data.FlushBits();

    if (HasData)
    {
        data.WriteGuidBytes(ownerGuid, 5);

        data << uint32(StaticType);

        data.WriteString(name);

        data << uint32(IssueDate);

        data.WriteString(BodyText);

        data.WriteGuidBytes(ownerGuid, 4);

        data << uint32(DeadLine);

        data.WriteGuidBytes(ownerGuid, 6);

        data << uint32(AllowedClasses);
        data << uint32(MaxSignatures);

        for (uint8 i = 0; i < MAX_PETITION_CHOICE_TEXTS; ++i)
            data.WriteString(ChoiceTexts[i]);

        data.WriteGuidBytes(ownerGuid, 1, 7, 0);

        data << uint32(AllowedMaxLevel);
        data << uint32(NumChoices);

        data.WriteGuidBytes(ownerGuid, 2);

        data << uint32(MinSignatures);
        data << uint16(AllowedGender);
        data << uint32(Muid);

        data.WriteGuidBytes(ownerGuid, 3);

        data << uint32(AllowedRaces);
        data << uint32(AllowedMinLevel);
        data << uint32(AllowedGuildID);
        data << uint32(Muid2);
    }

    SendPacket(&data);
}

void WorldSession::HandlePetitionRenameOpcode(WorldPacket& recvData)
{
    ObjectGuid petitionGuid;

    uint32 nameLen = 0;

    std::string newName;

    nameLen = recvData.ReadBits(7);

    recvData.ReadGuidMask(petitionGuid, 7, 4, 6, 2, 0, 5, 3, 1);

    recvData.ReadGuidBytes(petitionGuid, 4, 1, 7);

    newName = recvData.ReadString(nameLen);

    recvData.ReadGuidBytes(petitionGuid, 0, 3, 2, 6, 5);

    Petition* petition = sPetitionMgr->GetPetition(petitionGuid);
    if (!petition)
    {
        TC_LOG_DEBUG("network", "CMSG_PETITION_QUERY failed for petition %s", petitionGuid.ToString().c_str());
        return;
    }

    if (sGuildMgr->GetGuildByName(newName))
    {
        Guild::SendCommandResult(this, GUILD_COMMAND_CREATE, ERR_GUILD_NAME_EXISTS_S, newName);
        return;
    }

    if (sObjectMgr->IsReservedName(newName) || !ObjectMgr::IsValidCharterName(newName))
    {
        Guild::SendCommandResult(this, GUILD_COMMAND_CREATE, ERR_GUILD_NAME_INVALID, newName);
        return;
    }

    CharacterDatabase.EscapeString(newName);

    // update petition storage
    petition->UpdateName(newName);

    TC_LOG_DEBUG("network", "Petition (%s) renamed to '%s'", petitionGuid.ToString().c_str(), newName.c_str());

    WorldPacket data(SMSG_PETITION_RENAME_RESULT, 1 + 8 + 1 + newName.length());

    data.WriteBits(newName.length(), 7);

    data.WriteGuidMask(petitionGuid, 0, 3, 4, 2, 6, 5, 7, 1);

    data.WriteGuidBytes(petitionGuid, 4, 3, 6, 0, 5, 2, 1, 7);

    data.WriteString(newName);

    SendPacket(&data);
}

void WorldSession::HandlePetitionSignOpcode(WorldPacket& recvData)
{
    ObjectGuid petitionGuid;

    uint8 Choice = 0;

    recvData >> Choice;

    recvData.ReadGuidMask(petitionGuid, 4, 2, 0, 1, 5, 3, 6, 7);
    recvData.ReadGuidBytes(petitionGuid, 6, 1, 7, 2, 5, 3, 0, 4);

    Player* player = GetPlayer();

    Petition* petition = sPetitionMgr->GetPetition(petitionGuid);
    if (!petition)
    {
        TC_LOG_DEBUG("network", "Petition %s is not found for player %u %s", petitionGuid.ToString().c_str(), player->GetGUID().GetCounter(), player->GetName().c_str());
        return;
    }

    ObjectGuid ownerGuid = petition->ownerGuid;
    uint8 signs = petition->signatures.size();

    ObjectGuid playerGuid = _player->GetGUID();
    if (ownerGuid == playerGuid)
    {
        SendPetitionSignResults(petitionGuid, player->GetGUID(), PETITION_SIGN_CANT_SIGN_OWN);
        return;
    }

    if (signs >= sWorld->getIntConfig(CONFIG_REQUIRED_PETITION_SIGNS))
    {
        SendPetitionSignResults(petitionGuid, player->GetGUID(), PETITION_SIGN_FULL);
        return;
    }

    // not let enemies sign guild charter
    if (!sWorld->getBoolConfig(CONFIG_ALLOW_TWO_SIDE_INTERACTION_GUILD) && player->GetTeam() != sCharacterCache->GetCharacterTeamByGuid(ownerGuid))
    {
        Guild::SendCommandResult(this, GUILD_COMMAND_CREATE, ERR_GUILD_NOT_ALLIED);
        return;
    }

    if (player->GetGuildId() || player->GetGuildIdInvited())
    {
        SendPetitionSignResults(petitionGuid, player->GetGUID(), PETITION_SIGN_ALREADY_IN_GUILD);
        return;
    }

    // Client doesn't allow to sign petition two times by one character, but not check sign by another character from same account
    // not allow sign another player from already sign player account
    bool isSigned = petition->IsPetitionSignedByAccount(GetAccountId());
    if (isSigned)
    {
        // close at signer side
        SendPetitionSignResults(petitionGuid, player->GetGUID(), PETITION_SIGN_ALREADY_SIGNED);
        return;
    }

    // fill petition store
    petition->AddSignature(petitionGuid, GetAccountId(), playerGuid, false);

    // close at signer side
    SendPetitionSignResults(petitionGuid, player->GetGUID(), PETITION_SIGN_OK);

    // update for owner if online
    if (Player* owner = ObjectAccessor::FindConnectedPlayer(ownerGuid))
        owner->GetSession()->SendPetitionSignResults(petitionGuid, player->GetGUID(), PETITION_SIGN_OK);
}

void WorldSession::HandlePetitionDeclineOpcode(WorldPacket& recvData)
{
    ObjectGuid petitionGuid;

    recvData.ReadGuidMask(petitionGuid, 5, 6, 4, 3, 1, 7, 0, 2);
    recvData.ReadGuidBytes(petitionGuid, 6, 2, 1, 5, 0, 7, 4, 3);

    TC_LOG_DEBUG("network", "Petition %s declined by %u", petitionGuid.ToString().c_str(), _player->GetGUID().GetCounter());

    // Disabled
    // Client does not handle decline petition result for petition's owner
}

void WorldSession::HandleOfferPetitionOpcode(WorldPacket& recvData)
{
    ObjectGuid petitionGuid;
    ObjectGuid playerGuid;

    uint32 Unk = 0;

    recvData >> Unk;

    recvData.ReadGuidMask(playerGuid, 4, 1);

    recvData.ReadGuidMask(petitionGuid, 2);

    recvData.ReadGuidMask(playerGuid, 6);

    recvData.ReadGuidMask(petitionGuid, 1);

    recvData.ReadGuidMask(playerGuid, 2);

    recvData.ReadGuidMask(petitionGuid, 4);

    recvData.ReadGuidMask(playerGuid, 3, 7);

    recvData.ReadGuidMask(petitionGuid, 0, 6);

    recvData.ReadGuidMask(playerGuid, 5, 0);

    recvData.ReadGuidMask(petitionGuid, 3, 5, 7);

    recvData.ReadGuidBytes(playerGuid, 7);

    recvData.ReadGuidBytes(petitionGuid, 1, 4, 2);

    recvData.ReadGuidBytes(playerGuid, 6);

    recvData.ReadGuidBytes(petitionGuid, 3, 0, 5);

    recvData.ReadGuidBytes(playerGuid, 0, 2, 5, 3, 4);

    recvData.ReadGuidBytes(petitionGuid, 7);

    recvData.ReadGuidBytes(playerGuid, 1);

    recvData.ReadGuidBytes(petitionGuid, 6);

    Petition const* petition = sPetitionMgr->GetPetition(petitionGuid);
    if (!petition)
        return;

    Player* player = ObjectAccessor::FindConnectedPlayer(playerGuid);
    if (!player)
        return;

    if (!sWorld->getBoolConfig(CONFIG_ALLOW_TWO_SIDE_INTERACTION_GUILD) && GetPlayer()->GetTeam() != player->GetTeam())
    {
        Guild::SendCommandResult(this, GUILD_COMMAND_CREATE, ERR_GUILD_NOT_ALLIED);
        return;
    }

    if (player->GetGuildId() || player->GetGuildIdInvited())
    {
        SendPetitionSignResults(petitionGuid, player->GetGUID(), PETITION_SIGN_ALREADY_IN_GUILD);
        return;
    }

    SendPetitionSigns(petition, player);
}

void WorldSession::HandleTurnInPetitionOpcode(WorldPacket& recvData)
{
    ObjectGuid petitionGuid;

    recvData.ReadGuidMask(petitionGuid, 1, 2, 3, 0, 5, 7, 4, 6);
    recvData.ReadGuidBytes(petitionGuid, 2, 1, 4, 6, 0, 7, 5, 3);

    // Check if player really has the required petition charter
    Item* item = _player->GetItemByGuid(petitionGuid);
    if (!item)
        return;

    TC_LOG_DEBUG("network", "Petition %s turned in by %u", petitionGuid.ToString().c_str(), _player->GetGUID().GetCounter());

    // Get petition data from db
    uint32 ownerGuidLow = 0;
    std::string name;

    Petition const* petition = sPetitionMgr->GetPetition(petitionGuid);
    if (petition)
    {
        ownerGuidLow = petition->ownerGuid.GetCounter();
        name = petition->petitionName;
    }

    // Only the petition owner can turn in the petition
    if (_player->GetGUID().GetCounter() != ownerGuidLow)
    {
        WorldPacket data(SMSG_TURN_IN_PETITION_RESULTS, 1);

        data.WriteBits(PETITION_TURN_GUILD_PERMISSIONS, 4);

        data.FlushBits();

        SendPacket(&data);

        return;
    }

    // Petition type (guild/arena) specific checks
    // Check if player is already in a guild
    if (_player->GetGuildId())
    {
        WorldPacket data(SMSG_TURN_IN_PETITION_RESULTS, 1);

        data.WriteBits(PETITION_TURN_ALREADY_IN_GUILD, 4);

        data.FlushBits();

        SendPacket(&data);

        return;
    }

    // Check if guild name is already taken
    if (sGuildMgr->GetGuildByName(name))
    {
        WorldPacket data(SMSG_TURN_IN_PETITION_RESULTS, 1);

        data.WriteBits(PETITION_TURN_GUILD_NAME_INVALID, 4);

        data.FlushBits();

        SendPacket(&data);

        return;
    }

    SignaturesVector const signatures = petition->signatures; // we need a copy, it will be removed on guild/arena remove
    uint32 requiredSignatures = sWorld->getIntConfig(CONFIG_REQUIRED_PETITION_SIGNS);

    // Notify player if signatures are missing
    if (signatures.size() < requiredSignatures)
    {
        WorldPacket data(SMSG_TURN_IN_PETITION_RESULTS, 1);

        data.WriteBits(PETITION_TURN_NEED_MORE_SIGNATURES, 4);

        data.FlushBits();

        SendPacket(&data);

        return;
    }

    // Proceed with guild creation

    // Delete charter item
    _player->DestroyItem(item->GetBagSlot(), item->GetSlot(), true);

    // Create guild
    Guild* guild = new Guild();
    if (!guild->Create(_player, name))
    {
        delete guild;
        return;
    }

    // Register guild and add guild master
    sGuildMgr->AddGuild(guild);

    Guild::SendCommandResult(this, GUILD_COMMAND_CREATE, ERR_GUILD_COMMAND_SUCCESS, name);

    // Add members from signatures
    for (Signature const& signature : signatures)
        guild->AddMember(signature.second);

    sPetitionMgr->RemovePetition(petitionGuid);

    // created
    TC_LOG_DEBUG("network", "Player %s (%s) turning in petition %s", _player->GetName().c_str(), _player->GetGUID().ToString().c_str(), petitionGuid.ToString().c_str());

    WorldPacket data(SMSG_TURN_IN_PETITION_RESULTS, 1);

    data.WriteBits(PETITION_TURN_OK, 4);

    data.FlushBits();

    SendPacket(&data);
}

void WorldSession::HandlePetitionShowListOpcode(WorldPacket& recvData)
{
    ObjectGuid guid;

    recvData.ReadGuidMask(guid, 1, 7, 2, 5, 4, 0, 3, 6);
    recvData.ReadGuidBytes(guid, 6, 3, 2, 4, 1, 7, 5, 0);

    SendPetitionShowList(guid);
}

void WorldSession::SendPetitionShowList(ObjectGuid guid)
{
    Creature* creature = GetPlayer()->GetNPCIfCanInteractWith(guid, UNIT_NPC_FLAG_PETITIONER);
    if (!creature)
    {
        TC_LOG_DEBUG("network", "WORLD: HandlePetitionShowListOpcode - %s not found or you can't interact with him.", guid.ToString().c_str());
        return;
    }

    WorldPacket data(SMSG_PETITION_SHOW_LIST, 1 + 8 + 4);

    data.WriteGuidMask(guid, 3, 5, 7, 6, 1, 0, 2, 4);

    data.WriteGuidBytes(guid, 6, 0, 1);

    data << uint32(GUILD_CHARTER_COST);

    data.WriteGuidBytes(guid, 4, 3, 5, 2, 7);

    SendPacket(&data);
}

void WorldSession::SendPetitionSignResults(ObjectGuid petitionGuid, ObjectGuid playerGuid, int8 result)
{
    WorldPacket data(SMSG_PETITION_SIGN_RESULTS, 2 * (1 + 8) + 1);

    data.WriteGuidMask(playerGuid, 2, 0);

    data.WriteGuidMask(petitionGuid, 0);

    data.WriteGuidMask(playerGuid, 1);

    data.WriteGuidMask(petitionGuid, 5, 2, 4);

    data.WriteGuidMask(playerGuid, 6);

    data.WriteGuidMask(petitionGuid, 1, 6);

    data.WriteGuidMask(playerGuid, 4);

    data.WriteGuidMask(petitionGuid, 3);

    data.WriteGuidMask(playerGuid, 5, 3);

    data.WriteGuidMask(petitionGuid, 7);

    data.WriteGuidMask(playerGuid, 7);

    data.WriteBits(result, 4);

    data.WriteGuidBytes(petitionGuid, 0, 5);

    data.WriteGuidBytes(playerGuid, 3, 0);

    data.WriteGuidBytes(petitionGuid, 3);

    data.WriteGuidBytes(playerGuid, 2, 6, 4, 1);

    data.WriteGuidBytes(petitionGuid, 6, 7);

    data.WriteGuidBytes(playerGuid, 7);

    data.WriteGuidBytes(petitionGuid, 2, 1);

    data.WriteGuidBytes(playerGuid, 5);

    data.WriteGuidBytes(petitionGuid, 4);

    SendPacket(&data);
}
