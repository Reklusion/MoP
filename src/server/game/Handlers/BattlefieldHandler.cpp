/*
 * Copyright (C) 2011-2015 Project SkyFire <http://www.projectskyfire.org/>
 * Copyright (C) 2008-2015 TrinityCore <http://www.trinitycore.org/>
 * Copyright (C) 2005-2015 MaNGOS <http://getmangos.com/>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 3 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "ObjectAccessor.h"
#include "ObjectMgr.h"
#include "Opcodes.h"
#include "Player.h"
#include "WorldPacket.h"
#include "WorldSession.h"
#include "Object.h"
#include "Battlefield.h"
#include "BattlefieldMgr.h"

void WorldSession::HandleBfQueueInviteResponse(WorldPacket& recvData)
{
    bool accepted = false;
    ObjectGuid guid;

    recvData.ReadGuidMask(guid, 3, 6, 0, 2);

    accepted = recvData.ReadBit();

    recvData.ReadGuidMask(guid, 1, 5, 7, 4);

    recvData.FlushBits();

    recvData.ReadGuidBytes(guid, 4, 1, 2, 6, 0, 7, 5, 3);

    TC_LOG_DEBUG("misc", "HandleQueueInviteResponse: GUID: %s, Accepted:%u", guid.ToString().c_str(), accepted);

    Battlefield* bf = sBattlefieldMgr->GetBattlefieldByQueueGUID(guid);
    if (!bf)
        return;

    if (accepted)
        bf->PlayerAcceptInviteToQueue(_player);
    else
    {
        if (_player->GetZoneId() == bf->GetZoneId())
            bf->KickPlayerFromBattlefield(_player->GetGUID());
        else
            bf->AskToLeaveQueue(_player);
    }
}

void WorldSession::HandleBfEntryInviteResponse(WorldPacket& recvData)
{
    bool accepted = false;
    ObjectGuid guid;

    recvData.ReadGuidMask(guid, 0, 7);

    accepted = recvData.ReadBit();

    recvData.ReadGuidMask(guid, 4, 3, 1, 6, 2, 5);

    recvData.FlushBits();

    recvData.ReadGuidBytes(guid, 1, 6, 2, 5, 3, 4, 7, 0);

    Battlefield* bf = sBattlefieldMgr->GetBattlefieldByQueueGUID(guid);
    if (!bf)
        return;

    if (accepted)
        bf->PlayerAcceptInviteToWar(_player);
    else
    {
        if (_player->GetZoneId() == bf->GetZoneId())
            bf->KickPlayerFromBattlefield(_player->GetGUID());
        else
            bf->AskToLeaveQueue(_player);
    }
}

void WorldSession::HandleBfExitRequest(WorldPacket& recvData)
{
    ObjectGuid guid;

    recvData.ReadGuidMask(guid, 3, 2, 4, 1, 7, 0, 5, 6);
    recvData.ReadGuidBytes(guid, 5, 6, 2, 3, 0, 4, 7, 1);

    TC_LOG_DEBUG("misc", "HandleBfExitRequest: GUID: %s ", guid.ToString().c_str());

    if (Battlefield* bf = sBattlefieldMgr->GetBattlefieldByQueueGUID(guid))
        bf->AskToLeaveQueue(_player);
}

void WorldSession::HandleBfQueueRequest(WorldPacket& recvData)
{
    Player* player = GetPlayer();
    if (!player)
        return;

    ObjectGuid guid;

    recvData.ReadGuidMask(guid, 5, 0, 6, 3, 7, 2, 4, 1);
    recvData.ReadGuidBytes(guid, 5, 4, 0, 1, 6, 2, 7, 3);

    TC_LOG_DEBUG("misc", "HandleBfQueueRequest: GUID %s", guid.ToString().c_str());

    if (Battlefield* bf = sBattlefieldMgr->GetBattlefieldByQueueGUID(guid))
        if (bf->IsWarTime())
        {
            if (bf->CanJoinTheBattle(player))
                bf->InvitePlayerToWar(player);
            else
                player->GetSession()->SendBfDeclineInviteToWar(guid, true);
        }
        else
        {
            uint32 timer = bf->GetTimer() / 1000;
            if (timer < 15 * MINUTE)
                bf->InvitePlayerToQueue(GetPlayer());
        }
}

void WorldSession::SendBfInvitePlayerToWar(ObjectGuid guid, uint32 zoneId, uint32 timer)
{
    WorldPacket data(SMSG_BATTLEFIELD_ENTRY_INVITE, 1 + 8 + 4 + 4);

    data.WriteGuidMask(guid, 0, 2, 7, 6, 1, 4, 5, 3);

    data.WriteGuidBytes(guid, 4, 2, 0, 1, 7, 3, 6, 5);

    data << uint32(zoneId);
    data << uint32(time(NULL) + timer);      

    SendPacket(&data);

    // invite sound
    _player->PlayDirectSound(BF_START_WAR_SOUND, _player);
}

void WorldSession::SendBfDeclineInviteToWar(ObjectGuid guid, bool OutsideWarZone)
{
    WorldPacket data(SMSG_BATTLEFIELD_EJECT_PENDING, 1 + 8 + 1);

    data.WriteGuidMask(guid, 5, 6, 0);

    data.WriteBit(OutsideWarZone);

    data.WriteGuidMask(guid, 4, 1, 2, 7, 3);

    data.FlushBits();

    data.WriteGuidBytes(guid, 0, 2, 6, 7, 1, 5, 3, 4);

    SendPacket(&data);
}

void WorldSession::SendBfInvitePlayerToQueue(ObjectGuid guid, int8 battleState)
{
    uint32 Timeout = 0;
    uint32 InstanceID = 0;

    int32 MinLevel = 0;
    int32 MaxLevel = 0;
    int32 MapID = -1;

    int8 Index = 0;

    bool IsTimeout = Timeout != 0;
    bool HasMinLevel = MinLevel != 0;
    bool HasMaxLevel = MaxLevel != 0;
    bool HasMapID = MapID != -1;
    bool HasInstanceID = InstanceID != 0;
    bool HasIndex = Index != 0;
    bool HasActiveState = battleState != BATTLEFIELD_INACTIVE;

    WorldPacket data(SMSG_BATTLEFIELD_QUEUE_INVITE, 1 + 8 + 1 + (IsTimeout ? 4 : 0) + (HasMinLevel ? 4 : 0) + (HasMaxLevel ? 4 : 0) +
                    (HasMapID ? 4 : 0) + (HasInstanceID ? 4 : 0) + (HasActiveState ? 1 : 0));

    data.WriteBit(HasIndex);
    data.WriteBit(!IsTimeout);

    data.WriteGuidMask(guid, 3, 6, 5, 4, 2);

    data.WriteBit(!HasInstanceID);

    data.WriteGuidMask(guid, 0, 7);

    data.WriteBit(!HasMaxLevel);
    data.WriteBit(!HasActiveState);
    data.WriteBit(!HasMinLevel);
    
    data.WriteGuidMask(guid, 1);
    
    data.WriteBit(HasMapID);
    
    data.FlushBits();

    if (HasMinLevel)
        data << uint32(MinLevel);

    data.WriteGuidBytes(guid, 5, 4);

    if (HasMapID)
        data << uint32(MapID);

    data.WriteGuidBytes(guid, 7);

    if (HasInstanceID)
        data << uint32(InstanceID);

    if (IsTimeout)
        data << uint32(Timeout);
    
    data.WriteGuidBytes(guid, 3);

    if (HasActiveState)
        data << uint8(battleState);

    data.WriteGuidBytes(guid, 6, 2, 0, 1);
    
    if (HasMaxLevel)
        data << uint32(MaxLevel);
    
    SendPacket(&data);
}

void WorldSession::SendBfQueueInviteResponse(ObjectGuid guid, uint32 zoneId, int8 battleStatus, bool canQueue /*=true*/, bool loggingIn /*=false*/)
{
    ObjectGuid playerFailedGuid;

    uint8 status = canQueue ? 1 : 0;

    WorldPacket data(SMSG_BATTLEFIELD_QUEUE_REQUEST_RESPONSE, 2 * (1 + 8) + 1 + 4 + 1 + 1);

    data.WriteBit(!playerFailedGuid.IsEmpty());
    
    data.WriteGuidMask(guid, 6);

    data.WriteGuidMask(playerFailedGuid, 5, 0, 4, 3, 7, 6, 1, 2);

    data.WriteGuidMask(guid, 4, 0);

    data.WriteBit(loggingIn);

    data.WriteGuidMask(guid, 1, 5, 3, 7, 2);
    
    data.FlushBits();

    data.WriteGuidBytes(playerFailedGuid, 1, 3, 7, 6, 5, 0, 2, 4);

    data.WriteGuidBytes(guid, 5, 3, 7, 4);

    data << uint32(zoneId);

    data.WriteGuidBytes(guid, 2, 1, 6, 0);
    
    data << uint8(status);
    data << uint8(battleStatus);
    
    SendPacket(&data);
}

void WorldSession::SendBfEntered(ObjectGuid guid, bool relocated, bool onOffense)
{
    WorldPacket data(SMSG_BATTLEFIELD_ENTERED, 1 + 8 + 1);

    bool isAFK = _player->IsAFK();
    
    data.WriteGuidMask(guid, 5, 0);

    data.WriteBit(isAFK);
    data.WriteBit(relocated);

    data.WriteGuidMask(guid, 7, 4, 2);

    data.WriteBit(onOffense);

    data.WriteGuidMask(guid, 1, 3, 6);

    data.FlushBits();

    data.WriteGuidBytes(guid, 2, 5, 0, 6, 7, 3, 4, 1);

    SendPacket(&data);
}

void WorldSession::SendBfLeaveMessage(ObjectGuid guid, int8 battleState, bool relocated, BFLeaveReason reason)
{
    WorldPacket data(SMSG_BATTLEFIELD_EJECTED, 1 + 8 + 1 + 1 + 1);

    data.WriteGuidMask(guid, 5);

    data.WriteBit(relocated);

    data.WriteGuidMask(guid, 4, 0, 2, 7, 1, 6, 3);

    data.FlushBits();

    data.WriteGuidBytes(guid, 0, 2, 3, 4, 6, 5);

    data << uint8(battleState);
    data << uint8(reason);

    data.WriteGuidBytes(guid, 7, 1);

    SendPacket(&data);
}
