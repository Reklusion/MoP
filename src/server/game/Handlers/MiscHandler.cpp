/*
 * Copyright (C) 2011-2015 Project SkyFire <http://www.projectskyfire.org/>
 * Copyright (C) 2008-2015 TrinityCore <http://www.trinitycore.org/>
 * Copyright (C) 2005-2015 MaNGOS <http://getmangos.com/>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 3 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "Common.h"
#include "Language.h"
#include "DatabaseEnv.h"
#include "WorldPacket.h"
#include "Opcodes.h"
#include "Log.h"
#include "Player.h"
#include "GameTime.h"
#include "GossipDef.h"
#include "World.h"
#include "ObjectMgr.h"
#include "GuildMgr.h"
#include "WorldSession.h"
#include "BigNumber.h"
#include "SHA1.h"
#include "UpdateData.h"
#include "LootMgr.h"
#include "Chat.h"
#include "zlib.h"
#include "ObjectAccessor.h"
#include "Object.h"
#include "Battleground.h"
#include "OutdoorPvP.h"
#include "Pet.h"
#include "CellImpl.h"
#include "AccountMgr.h"
#include "Vehicle.h"
#include "CreatureAI.h"
#include "DBCEnums.h"
#include "ScriptMgr.h"
#include "MapManager.h"
#include "InstanceScript.h"
#include "GameObjectAI.h"
#include "Group.h"
#include "AccountMgr.h"
#include "Spell.h"
#include "BattlegroundMgr.h"
#include "Battlefield.h"
#include "BattlefieldMgr.h"
#include "DB2Stores.h"
#include "ReputationMgr.h"
#include "RatedMgr.h"
#include "RatedInfo.h"

void WorldSession::HandleRepopRequestOpcode(WorldPacket& recvData)
{
    bool CheckInstance = false;
    CheckInstance = recvData.ReadBit();

    if (GetPlayer()->IsAlive() || GetPlayer()->HasFlag(PLAYER_FLAGS, PLAYER_FLAGS_GHOST))
        return;

    if (GetPlayer()->HasAuraType(SPELL_AURA_PREVENT_RESURRECTION))
        return; // silently return, client should display the error by itself

    // the world update order is sessions, players, creatures
    // the netcode runs in parallel with all of these
    // creatures can kill players
    // so if the server is lagging enough the player can
    // release spirit after he's killed but before he is updated
    if (GetPlayer()->GetDeathState() == JUST_DIED)
    {
        TC_LOG_DEBUG("network", "HandleRepopRequestOpcode: got request after player %s(%d) was killed and before he was updated",
            GetPlayer()->GetName().c_str(), GetPlayer()->GetGUID().GetCounter());
        GetPlayer()->KillPlayer();
    }

    GetPlayer()->UpdateCriteria(CRITERIA_TYPE_RELEASE_SPIRIT, 1);

    GetPlayer()->RemovePet(NULL, PET_SLOT_OTHER_PET, true);
    GetPlayer()->BuildPlayerRepop();
    GetPlayer()->RepopAtGraveyard();
}

void WorldSession::HandleWhoOpcode(WorldPacket& recvData)
{
    uint32 LevelMin = 0;
    uint32 LevelMax = 0;
    uint32 RaceMask = 0;
    uint32 ClassMask = 0;
    uint32 ZonesCount = 0;
    uint32 WordCount = 0;

    uint8 PlayerNameLen = 0;
    uint8 GuildNameLen = 0;
    uint8 RealmNameLen = 0;
    uint8 GuildRealmNameLen = 0;

    bool RequestServerInfo = false;
    bool ShowEnemies = false;
    bool ShowArenaPlayers = false;
    bool ExactName = false;

    std::string PlayerName;
    std::string GuildName;
    std::string RealmName;
    std::string GuildRealmName;

    recvData >> ClassMask;
    recvData >> RaceMask;
    recvData >> LevelMax;
    recvData >> LevelMin;

    ShowEnemies = recvData.ReadBit();
    ExactName = recvData.ReadBit();
    RequestServerInfo = recvData.ReadBit();

    GuildRealmNameLen = recvData.ReadBits(9);

    ShowArenaPlayers = recvData.ReadBit();

    PlayerNameLen = recvData.ReadBits(6);

    ZonesCount = recvData.ReadBits(4);

    // zones count, client limit = 10 (2.0.10)
    // can't be received from real client or broken packet
    if (ZonesCount > 10)
    {
        recvData.rfinish();
        return;
    }

    RealmNameLen = recvData.ReadBits(9);
    GuildNameLen = recvData.ReadBits(7);

    WordCount = recvData.ReadBits(3);

    // can't be received from real client or broken packet
    if (WordCount > 4)
    {
        recvData.rfinish();
        return;
    }

    std::vector<uint32> WordLens(WordCount);

    for (uint8 i = 0; i < WordCount; i++)
        WordLens[i] = recvData.ReadBits(7);

    std::vector<std::wstring> wWords(WordCount);
    for (uint32 i = 0; i < WordCount; ++i)
    {
        // user entered string, it used as universal search pattern(guild+player name)?
        std::string temp;
        recvData.ReadString(WordLens[i]);

        if (!Utf8toWStr(temp, wWords[i]))
            continue;

        wstrToLower(wWords[i]);
    }

    GuildRealmName = recvData.ReadString(GuildRealmNameLen);

    std::vector<uint32> ZoneIds(ZonesCount);
    for (uint32 i = 0; i < ZonesCount; ++i)
        recvData >> ZoneIds[i];                            // zone id, 0 if zone is unknown...

    PlayerName = recvData.ReadString(PlayerNameLen);
    RealmName = recvData.ReadString(RealmNameLen);
    GuildName = recvData.ReadString(GuildNameLen);

    uint32 VirtualRealmAddress = 0;
    int32 Faction = 0;
    int32 Locale = 0;

    if (RequestServerInfo)
    {
        recvData >> Locale;
        recvData >> VirtualRealmAddress;
        recvData >> Faction;
    }

    std::wstring wPlayerName, wGuildName;
    if (!(Utf8toWStr(PlayerName, wPlayerName) && Utf8toWStr(GuildName, wGuildName)))
        return;

    wstrToLower(wPlayerName);
    wstrToLower(wGuildName);

    // client send in case not set max level value 100 but Trinity supports 255 max level,
    // update it to show GMs with characters after 100 level
    if (LevelMax >= MAX_LEVEL)
        LevelMax = STRONG_MAX_LEVEL;

    uint32 team = _player->GetTeam();
    uint32 security = GetSecurity();
    uint32 gmLevelInWhoList  = sWorld->getIntConfig(CONFIG_GM_LEVEL_IN_WHO_LIST);

    uint32 GuildNameSize = 0;
    uint32 PlayerNameSize = 0;
    uint32 DeclinedNameSize = 0;

    std::vector<WhoListData> whoDataVector;
    std::unordered_map<ObjectGuid, StringVector> declinedStrings;

    boost::shared_lock<boost::shared_mutex> lock(*HashMapHolder<Player>::GetLock());
    HashMapHolder<Player>::MapType const& m = ObjectAccessor::GetPlayers();
    for (HashMapHolder<Player>::MapType::const_iterator itr = m.begin(); itr != m.end(); ++itr)
    {
        Player* target = itr->second;
        // player can see member of other team only if CONFIG_ALLOW_TWO_SIDE_WHO_LIST
        if (target->GetTeam() != team && !HasPermission(rbac::RBAC_PERM_TWO_SIDE_WHO_LIST))
            continue;

        // player can see MODERATOR, GAME MASTER, ADMINISTRATOR only if CONFIG_GM_IN_WHO_LIST
        if (!HasPermission(rbac::RBAC_PERM_WHO_SEE_ALL_SEC_LEVELS) && target->GetSession()->GetSecurity() > AccountTypes(gmLevelInWhoList))
            continue;

        // do not process players which are not in world
        if (!target->IsInWorld())
            continue;

        // check if target is globally visible for player
        if (!target->IsVisibleGloballyFor(_player))
            continue;

        // check if target's level is in level range
        uint8 level = target->GetLevel();
        if (level < LevelMin || level > LevelMax)
            continue;

        // check if class matches classmask
        uint8 class_ = target->GetClass();
        if (!(ClassMask & (1 << class_)))
            continue;

        // check if race matches racemask
        uint32 race = target->GetRace();
        if (!(RaceMask & (1 << race)))
            continue;

        uint32 zoneId = target->GetZoneId();
        uint8 gender = target->GetGender();

        if (!ZoneIds.empty())
        {
            bool z_show = true;
            for (uint32 ZoneID : ZoneIds)
                if (ZoneID == zoneId)
                {
                    z_show = true;
                    break;
                }

            if (!z_show)
                continue;
        }

        std::string pname = target->GetName();
        std::wstring wpname;
        if (!Utf8toWStr(pname, wpname))
            continue;

        wstrToLower(wpname);

        if (!(wPlayerName.empty() || wpname.find(wPlayerName) != std::wstring::npos))
            continue;

        std::string gname = sGuildMgr->GetGuildNameById(target->GetGuildId());
        std::wstring wgname;
        if (!Utf8toWStr(gname, wgname))
            continue;

        wstrToLower(wgname);

        if (!(wGuildName.empty() || wgname.find(wGuildName) != std::wstring::npos))
            continue;

        if (!wWords.empty())
        {
            std::string aName;
            if (AreaTableEntry const* areaEntry = sAreaStore.LookupEntry(zoneId))
                aName = areaEntry->AreaName;

            bool s_show = false;
            for (std::wstring& wWordString : wWords)
            {
                if (!wWordString.empty())
                {
                    if (wgname.find(wWordString) != std::wstring::npos ||
                        wpname.find(wWordString) != std::wstring::npos ||
                        Utf8FitTo(aName, wWordString))
                    {
                        s_show = true;
                        break;
                    }
                }
            }

            if (!s_show)
                continue;
        }

        // 49 is maximum player count sent to client - can be overridden
        // through config, but is unstable
        if (whoDataVector.size() >= sWorld->getIntConfig(CONFIG_MAX_WHO))
            continue;

        WhoListData whoData(target, gname, pname, class_, race, gender, level, zoneId);
        whoDataVector.push_back(whoData);

        GuildNameSize += gname.length();
        PlayerNameSize += pname.length();

        DeclinedNames const& names = target->GetDeclinedNames();

        declinedStrings[target->GetGUID()].resize(MAX_DECLINED_NAME_CASES);

        for (uint8 i = 0; i < MAX_DECLINED_NAME_CASES; ++i)
        {
            declinedStrings[target->GetGUID()][i] = names[i];

            DeclinedNameSize += names[i].length();
        }
    }

    size_t PacketSize = 0;
    if (!whoDataVector.empty())
    {
        PacketSize = whoDataVector.size() * (3 * (1 + 8) + 2 + MAX_DECLINED_NAME_CASES * (1) + 4 + 4 + 4 + 1 + 1 + 1 + 1 + 4) +
                    GuildNameSize + PlayerNameSize + DeclinedNameSize;
    }

    WorldPacket data(SMSG_WHO, 1 + PacketSize);

    ByteBuffer bytesData;

    data.WriteBits(whoDataVector.size(), 6);

    for (WhoListData const& whoData : whoDataVector)
    {
        Player* target = whoData.player;

        bool IsDeleted = false;
        bool IsGM = target->IsGameMaster();

        ObjectGuid playerGuid = target->GetGUID();
        ObjectGuid accountId = ObjectGuid::Create<HighGuid::Account>(target->GetSession()->GetAccountId());
        ObjectGuid guildGuid = target->GetGuild() ? target->GetGuild()->GetGUID() : ObjectGuid::Empty;

        data.WriteGuidMask(accountId, 2);

        data.WriteGuidMask(playerGuid, 2);

        data.WriteGuidMask(accountId, 7);

        data.WriteGuidMask(guildGuid, 5);

        data.WriteBits(whoData.GuildName.length(), 7);

        data.WriteGuidMask(accountId, 1, 5);

        data.WriteGuidMask(guildGuid, 7);

        data.WriteGuidMask(playerGuid, 5);

        data.WriteBit(IsDeleted);

        data.WriteGuidMask(guildGuid, 1);

        data.WriteGuidMask(playerGuid, 6);

        data.WriteGuidMask(guildGuid, 2);

        data.WriteGuidMask(playerGuid, 4);

        data.WriteGuidMask(guildGuid, 0, 3);

        data.WriteGuidMask(accountId, 6);

        data.WriteBit(IsGM);

        data.WriteGuidMask(playerGuid, 1);

        data.WriteGuidMask(guildGuid, 4);

        data.WriteGuidMask(accountId, 0);

        for (std::string& declinedName : declinedStrings[playerGuid])
            data.WriteBits(declinedName.size(), 7);

        data.WriteGuidMask(playerGuid, 3);

        data.WriteGuidMask(guildGuid, 6);

        data.WriteGuidMask(playerGuid, 0);

        data.WriteGuidMask(accountId, 4, 3);

        data.WriteGuidMask(playerGuid, 7);

        data.WriteBits(whoData.PlayerName.length(), 6);

        bytesData.WriteGuidBytes(playerGuid, 1);

        bytesData << uint32(realmID);

        bytesData.WriteGuidBytes(playerGuid, 7);

        bytesData << uint32(realmID);

        bytesData.WriteGuidBytes(playerGuid, 4);

        bytesData.WriteString(whoData.PlayerName);

        bytesData.WriteGuidBytes(guildGuid, 1);

        bytesData.WriteGuidBytes(playerGuid, 0);

        bytesData.WriteGuidBytes(guildGuid, 2, 0, 4);

        bytesData.WriteGuidBytes(playerGuid, 3);

        bytesData.WriteGuidBytes(guildGuid, 6);

        bytesData << uint32(target->GetSession()->GetAccountId());

        bytesData.WriteString(whoData.GuildName);

        bytesData.WriteGuidBytes(guildGuid, 3);

        bytesData.WriteGuidBytes(accountId, 4);

        bytesData << uint8(whoData.Class);

        bytesData.WriteGuidBytes(accountId, 7);

        bytesData.WriteGuidBytes(playerGuid, 6, 2);

        for (std::string& declinedName : declinedStrings[playerGuid])
            bytesData.WriteString(declinedName);

        bytesData.WriteGuidBytes(accountId, 2, 3);

        bytesData << uint8(whoData.Race);

        bytesData.WriteGuidBytes(guildGuid, 7);

        bytesData.WriteGuidBytes(accountId, 1, 5, 6);

        bytesData.WriteGuidBytes(playerGuid, 5);

        bytesData.WriteGuidBytes(accountId, 0);

        bytesData << int8(whoData.Gender);

        bytesData.WriteGuidBytes(guildGuid, 5);

        bytesData << uint8(whoData.Level);
        bytesData << int32(whoData.ZoneID);
    }

    if (whoDataVector.empty())
        data.FlushBits();
    else
        data.append(bytesData);

    SendPacket(&data);
}

void WorldSession::HandleLogoutRequestOpcode(WorldPacket& /*recvData*/)
{
    PlayerLootObjects& lootObjects = _player->GetLootObjects();
    if (!lootObjects.empty())
        for (auto & lootItr : lootObjects)
            if (Loot* loot = lootItr.second)
                DoLootRelease(loot);

    bool InstantLogout = (GetPlayer()->HasFlag(PLAYER_FLAGS, PLAYER_FLAGS_RESTING) && !GetPlayer()->IsInCombat()) ||
                         GetPlayer()->IsInFlight() || HasPermission(rbac::RBAC_PERM_INSTANT_LOGOUT);

    /// TODO: Possibly add RBAC permission to log out in combat
    bool CanLogoutInCombat = GetPlayer()->HasFlag(PLAYER_FLAGS, PLAYER_FLAGS_RESTING);

    uint32 reason = 0;
    if (GetPlayer()->IsInCombat() && !CanLogoutInCombat)
        reason = 1;
    else if (GetPlayer()->IsFalling())
        reason = 3;                                         // is jumping or falling
    else if (GetPlayer()->duel || GetPlayer()->HasAura(9454)) // is dueling or frozen by GM via freeze command
        reason = 2;                                         // FIXME - Need the correct value

    WorldPacket data(SMSG_LOGOUT_RESPONSE, 4 + 1);

    data << uint32(reason);

    data.WriteBit(InstantLogout);

    data.FlushBits();

    SendPacket(&data);

    if (reason)
    {
        LogoutRequest(0);
        return;
    }

    // instant logout in taverns/cities or on taxi or for admins, gm's, mod's if its enabled in worldserver.conf
    if (InstantLogout)
    {
        LogoutPlayer(true);
        return;
    }

    // not set flags if player can't free move to prevent lost state at logout cancel
    if (GetPlayer()->CanFreeMove())
    {
        if (GetPlayer()->GetStandState() == UNIT_STAND_STATE_STAND)
            GetPlayer()->SetStandState(UNIT_STAND_STATE_SIT);
        GetPlayer()->SetRooted(true);
        GetPlayer()->SetFlag(UNIT_FLAGS, UNIT_FLAG_STUNNED);
    }

    LogoutRequest(time(NULL));
}

void WorldSession::HandleLogoutCancelOpcode(WorldPacket& /*recvData*/)
{
    // Player have already logged out serverside, too late to cancel
    if (!GetPlayer())
        return;

    LogoutRequest(0);

    WorldPacket data(SMSG_LOGOUT_CANCEL_ACK, 0);
    SendPacket(&data);

    // not remove flags if can't free move - its not set in Logout request code.
    if (GetPlayer()->CanFreeMove())
    {
        //!we can move again
        GetPlayer()->SetRooted(false);

        //! Stand Up
        GetPlayer()->SetStandState(UNIT_STAND_STATE_STAND);

        //! DISABLE_ROTATE
        GetPlayer()->RemoveFlag(UNIT_FLAGS, UNIT_FLAG_STUNNED);
    }
}

void WorldSession::HandleSetPvP(WorldPacket& recvData)
{
    bool NewPvPStatus = recvData.ReadBit();

    GetPlayer()->ApplyModFlag(PLAYER_FLAGS, PLAYER_FLAGS_IN_PVP, NewPvPStatus);
    GetPlayer()->ApplyModFlag(PLAYER_FLAGS, PLAYER_FLAGS_PVP_TIMER, !NewPvPStatus);

    if (GetPlayer()->HasFlag(PLAYER_FLAGS, PLAYER_FLAGS_IN_PVP))
    {
        if (!GetPlayer()->IsPvP() || GetPlayer()->pvpInfo.EndTimer)
            GetPlayer()->UpdatePvP(true, true);
    }
    else
    {
        if (!GetPlayer()->pvpInfo.IsHostile && GetPlayer()->IsPvP())
            GetPlayer()->pvpInfo.EndTimer = time(nullptr); // start set-off
    }
}

void WorldSession::HandleTogglePvP(WorldPacket& /*recvData*/)
{
    bool InPvP = GetPlayer()->HasFlag(PLAYER_FLAGS, PLAYER_FLAGS_IN_PVP);

    GetPlayer()->ApplyModFlag(PLAYER_FLAGS, PLAYER_FLAGS_IN_PVP, !InPvP);
    GetPlayer()->ApplyModFlag(PLAYER_FLAGS, PLAYER_FLAGS_PVP_TIMER, InPvP);

    if (InPvP)
    {
        if (!GetPlayer()->IsPvP() || GetPlayer()->pvpInfo.EndTimer)
            GetPlayer()->UpdatePvP(true, true);
    }
    else
    {
        if (!GetPlayer()->pvpInfo.IsHostile && GetPlayer()->IsPvP())
            GetPlayer()->pvpInfo.EndTimer = time(nullptr); // start toggle-off
    }
}

void WorldSession::HandleRequestCemeteryList(WorldPacket& /*recvData*/)
{
    uint32 zoneId = _player->GetZoneId();
    uint32 team = _player->GetBGTeam();

    std::vector<uint32> graveyardIds;
    auto range = sObjectMgr->GraveYardStore.equal_range(zoneId);

    for (auto it = range.first; it != range.second && graveyardIds.size() < 16; ++it) // client max
        if (it->second.team == 0 || it->second.team == team)
            graveyardIds.push_back(it->first);

    if (graveyardIds.empty())
    {
        TC_LOG_DEBUG("network", "No graveyards found for zone %u for player %u (team %u) in CMSG_REQUEST_CEMETERY_LIST",
            zoneId, m_GUIDLow, team);
        return;
    }

   // WorldMapFrame.lua
    bool IsGossipTransferred = false;

    WorldPacket data(SMSG_REQUEST_CEMETERY_LIST_RESPONSE, 3 + 1 * graveyardIds.size() * (4));

    data.WriteBits(graveyardIds.size(), 22);

    data.WriteBit(IsGossipTransferred);

    for (uint32 GraveyardID : graveyardIds)
        data << uint32(GraveyardID);

    SendPacket(&data);
}

void WorldSession::HandleReturnToGraveyard(WorldPacket& /*recvData*/)
{
    if (GetPlayer()->IsAlive() || !GetPlayer()->HasFlag(PLAYER_FLAGS, PLAYER_FLAGS_GHOST))
        return;

    GetPlayer()->RepopAtGraveyard();
}

void WorldSession::HandleSetPreferedCemetery(WorldPacket& recvData)
{
    uint32 CemeteryID = 0;
    recvData >> CemeteryID;

    // ToDo: do sthg with this packet
}

void WorldSession::HandleSetSelectionOpcode(WorldPacket& recvData)
{
    ObjectGuid guid;

    recvData.ReadGuidMask(guid, 7, 6, 5, 4, 3, 2, 1, 0);
    recvData.ReadGuidBytes(guid, 0, 7, 3, 5, 1, 4, 6, 2);

    _player->SetSelection(guid);
}

void WorldSession::HandleStandStateChangeOpcode(WorldPacket& recvData)
{
    uint32 animstate = 0;
    recvData >> animstate;

    _player->SetStandState(animstate);
}

void WorldSession::HandleReclaimCorpseOpcode(WorldPacket& recvData)
{
    ObjectGuid guid;

    recvData.ReadGuidMask(guid, 1, 5, 7, 2, 6, 3, 0, 4);
    recvData.ReadGuidBytes(guid, 2, 5, 4, 6, 1, 0, 7, 3);

    if (_player->IsAlive())
        return;

    // do not allow corpse reclaim in arena
    if (_player->GetBattleground() && _player->GetBattleground()->IsArena())
        return;

    // body not released yet
    if (!GetPlayer()->HasFlag(PLAYER_FLAGS, PLAYER_FLAGS_GHOST))
        return;

    Corpse* corpse = _player->GetCorpse();
    if (!corpse)
        return;

    // prevent resurrect before 30-sec delay after body release not finished
    if (time_t(corpse->GetGhostTime() + _player->GetCorpseReclaimDelay(corpse->GetType() == CORPSE_RESURRECTABLE_PVP)) > time_t(time(NULL)))
        return;

    if (!_player->IsWithinDistInMap(corpse, CORPSE_RECLAIM_RADIUS, true))
        return;

    // resurrect
    _player->ResurrectPlayer(_player->InBattleground() ? 1.0f : 0.5f);

    // spawn bones
    _player->SpawnCorpseBones();
}

void WorldSession::HandleResurrectResponseOpcode(WorldPacket& recvData)
{
    ObjectGuid guid;
    uint32 status = 0;

    recvData >> status; // Accept = 0 Decline = 1 Timeout = 2

    recvData.ReadGuidMask(guid, 3, 0, 6, 4, 5, 2, 1, 7);
    recvData.ReadGuidBytes(guid, 7, 0, 1, 3, 4, 6, 2, 5);

    if (GetPlayer()->IsAlive())
        return;

    if (status == 1)
    {
        GetPlayer()->ClearResurrectRequestData();           // reject
        return;
    }

    if (!GetPlayer()->IsResurrectRequested())
        return;

    GetPlayer()->ResurrectUsingRequestData();
}

void WorldSession::HandleAreaTriggerOpcode(WorldPacket& recvData)
{
    uint32 TriggerID = 0;
    bool Entered = false;
    bool FromClient = false;

    recvData >> TriggerID;

    FromClient = recvData.ReadBit();
    Entered = recvData.ReadBit();

    Player* player = GetPlayer();
    if (player->IsInFlight())
    {
        TC_LOG_DEBUG("network", "HandleAreaTriggerOpcode: Player '%s' (GUID: %u) in flight, ignore Area Trigger ID:%u",
            player->GetName().c_str(), player->GetGUID().GetCounter(), TriggerID);
        return;
    }

    AreaTriggerEntry const* atEntry = sAreaTriggerStore.LookupEntry(TriggerID);
    if (!atEntry)
    {
        TC_LOG_DEBUG("network", "HandleAreaTriggerOpcode: Player '%s' (GUID: %u) send unknown (by DBC) Area Trigger ID:%u",
            player->GetName().c_str(), player->GetGUID().GetCounter(), TriggerID);
        return;
    }

    if (Entered && !player->IsInAreaTriggerRadius(atEntry))
    {
        TC_LOG_DEBUG("network", "HandleAreaTriggerOpcode: Player '%s' (GUID: %u) too far (trigger map: %u player map: %u), ignore Area Trigger ID: %u",
            player->GetName().c_str(), atEntry->MapID, player->GetMapId(), player->GetGUID().GetCounter(), TriggerID);
        return;
    }

    if (player->IsDebugAreaTriggers)
        ChatHandler(player->GetSession()).PSendSysMessage(Entered ? LANG_DEBUG_AREATRIGGER_ENTERED : LANG_DEBUG_AREATRIGGER_LEFT, TriggerID);

    if (sScriptMgr->OnAreaTrigger(player, atEntry, Entered))
        return;

    if (player->IsAlive())
        if (std::unordered_set<uint32> const* quests = sObjectMgr->GetQuestsForAreaTrigger(TriggerID))
            for (uint32 questId : *quests)
            {
                Quest const* qInfo = sObjectMgr->GetQuestTemplate(questId);
                if (qInfo && player->GetQuestStatus(questId) == QUEST_STATUS_INCOMPLETE)
                {
                    for (QuestObjective const& obj : qInfo->Objectives)
                        if (obj.Type == QUEST_OBJECTIVE_AREATRIGGER && !player->IsQuestObjectiveComplete(obj))
                        {
                            player->SetQuestObjectiveData(obj, 1);
                            player->SendQuestUpdateAddCreditSimple(obj);
                            break;
                        }

                    if (player->CanCompleteQuest(questId))
                        player->CompleteQuest(questId);
                }
            }

    if (sObjectMgr->IsTavernAreaTrigger(TriggerID))
    {
        // set resting flag we are in the inn
        if (Entered)
        {
            player->SetRestFlag(REST_FLAG_IN_TAVERN, atEntry->ID);

            if (sWorld->IsFFAPvPRealm())
                player->RemoveByteFlag(UNIT_BYTES_2, UNIT_BYTES_2_OFFSET_PVP_FLAG, UNIT_BYTE2_FLAG_FFA_PVP);
        }
        else if (player->HasRestFlag(REST_FLAG_IN_TAVERN))
            player->RemoveRestFlag(REST_FLAG_IN_TAVERN);

        return;
    }

    if (Battleground* bg = player->GetBattleground())
        bg->HandleAreaTrigger(player, TriggerID, Entered);

    if (OutdoorPvP* pvp = player->GetOutdoorPvP())
        if (pvp->HandleAreaTrigger(_player, TriggerID, Entered))
            return;

    AreaTriggerStruct const* at = sObjectMgr->GetAreaTrigger(TriggerID);
    if (!at)
        return;

    bool teleported = false;
    if (player->GetMapId() != at->target_mapId)
    {
        if (Map::EnterState denyReason = sMapMgr->PlayerCannotEnter(at->target_mapId, player, false))
        {
            bool reviveAtTrigger = false; // should we revive the player if he is trying to enter the correct instance?
            switch (denyReason)
            {
                case Map::CANNOT_ENTER_NO_ENTRY:
                    TC_LOG_DEBUG("maps", "MAP: Player '%s' attempted to enter map with id %d which has no entry", player->GetName().c_str(), at->target_mapId);
                    break;
                case Map::CANNOT_ENTER_UNINSTANCED_DUNGEON:
                    TC_LOG_DEBUG("maps", "MAP: Player '%s' attempted to enter dungeon map %d but no instance template was found", player->GetName().c_str(), at->target_mapId);
                    break;
                case Map::CANNOT_ENTER_DIFFICULTY_UNAVAILABLE:
                    TC_LOG_DEBUG("maps", "MAP: Player '%s' attempted to enter instance map %d but the requested difficulty was not found", player->GetName().c_str(), at->target_mapId);
                    if (MapEntry const* entry = sMapStore.LookupEntry(at->target_mapId))
                        player->SendTransferAborted(entry->ID, TRANSFER_ABORT_DIFFICULTY, player->GetDifficultyID(entry));
                    break;
                case Map::CANNOT_ENTER_NOT_IN_RAID:
                    TC_LOG_DEBUG("maps", "MAP: Player '%s' must be in a raid group to enter map %d", player->GetName().c_str(), at->target_mapId);
                    player->SendRaidGroupOnlyMessage(RAID_GROUP_ERR_ONLY, 0);
                    reviveAtTrigger = true;
                    break;
                case Map::CANNOT_ENTER_CORPSE_IN_DIFFERENT_INSTANCE:
                {
                    WorldPacket data(SMSG_CORPSE_NOT_IN_INSTANCE, 0);
                    player->SendDirectMessage(&data);
                    TC_LOG_DEBUG("maps", "MAP: Player '%s' does not have a corpse in instance map %d and cannot enter", player->GetName().c_str(), at->target_mapId);
                    break;
                }
                case Map::CANNOT_ENTER_INSTANCE_BIND_MISMATCH:
                    if (MapEntry const* entry = sMapStore.LookupEntry(at->target_mapId))
                    {
                        char const* mapName = entry->MapName;
                        TC_LOG_DEBUG("maps", "MAP: Player '%s' cannot enter instance map '%s' because their permanent bind is incompatible with their group's", player->GetName().c_str(), mapName);
                        // is there a special opcode for this?
                        // @todo figure out how to get player localized difficulty string (e.g. "10 player", "Heroic" etc)
                        ChatHandler(player->GetSession()).PSendSysMessage(player->GetSession()->GetTrinityString(LANG_INSTANCE_BIND_MISMATCH), mapName);
                    }
                    reviveAtTrigger = true;
                    break;
                case Map::CANNOT_ENTER_TOO_MANY_INSTANCES:
                    player->SendTransferAborted(at->target_mapId, TRANSFER_ABORT_TOO_MANY_INSTANCES);
                    TC_LOG_DEBUG("maps", "MAP: Player '%s' cannot enter instance map %d because he has exceeded the maximum number of instances per hour.", player->GetName().c_str(), at->target_mapId);
                    reviveAtTrigger = true;
                    break;
                case Map::CANNOT_ENTER_MAX_PLAYERS:
                    player->SendTransferAborted(at->target_mapId, TRANSFER_ABORT_MAX_PLAYERS);
                    reviveAtTrigger = true;
                    break;
                case Map::CANNOT_ENTER_ZONE_IN_COMBAT:
                    player->SendTransferAborted(at->target_mapId, TRANSFER_ABORT_ZONE_IN_COMBAT);
                    reviveAtTrigger = true;
                    break;
                default:
                    break;
            }

            if (reviveAtTrigger) // check if the player is touching the areatrigger leading to the map his corpse is on
                if (!player->IsAlive() && player->HasCorpse())
                    if (player->GetCorpseLocation().GetMapId() == at->target_mapId)
                    {
                        player->ResurrectPlayer(0.5f);
                        player->SpawnCorpseBones();
                    }

            return;
        }

        if (Group* group = player->GetGroup())
            if (group->IsLFGGroup() && player->GetMap()->IsDungeon())
                teleported = player->TeleportToBGEntryPoint();
    }

    if (!teleported)
    {
        WorldSafeLocsEntry const* entranceLocation = nullptr;
        InstanceSave* instanceSave = player->GetInstanceSave(at->target_mapId);
        if (instanceSave)
        {
            // Check if we can contact the instancescript of the instance for an updated entrance location
            if (Map* map = sMapMgr->FindMap(at->target_mapId, player->GetInstanceSave(at->target_mapId)->GetInstanceId()))
                if (InstanceMap* instanceMap = map->ToInstanceMap())
                    if (InstanceScript* instanceScript = instanceMap->GetInstanceScript())
                        entranceLocation = sWorldSafeLocsStore.LookupEntry(instanceScript->GetEntranceLocation());

            // Finally check with the instancesave for an entrance location if we did not get a valid one from the instancescript
            if (!entranceLocation)
                entranceLocation = sWorldSafeLocsStore.LookupEntry(instanceSave->GetEntranceLocation());
        }

        if (entranceLocation)
            player->TeleportTo(entranceLocation->MapID, entranceLocation->Loc.X, entranceLocation->Loc.Y, entranceLocation->Loc.Z, entranceLocation->Facing * M_PI / 180, TELE_TO_NOT_LEAVE_TRANSPORT);
        else
            player->TeleportTo(at->target_mapId, at->target_X, at->target_Y, at->target_Z, at->target_Orientation, TELE_TO_NOT_LEAVE_TRANSPORT);
    }
}

void WorldSession::HandleUpdateAccountData(WorldPacket& recvData)
{
    uint32 timestamp = 0;
    uint32 type = 0;
    uint32 decompressedSize = 0;
    uint32 compressedSize = 0;

    recvData >> decompressedSize;
    recvData >> timestamp;
    recvData >> compressedSize;

    if (decompressedSize == 0)                               // erase
    {
        type = recvData.ReadBits(3);
        recvData.rfinish();

        SetAccountData(AccountDataType(type), 0, "");

        return;
    }

    if (decompressedSize > 0xFFFF)
    {
        recvData.rfinish();                   // unnneded warning spam in this case
        TC_LOG_ERROR("network", "UAD: Account data packet too big, size %u", decompressedSize);
        return;
    }

    ByteBuffer dest;
    dest.resize(decompressedSize);

    uLongf realSize = decompressedSize;
    if (uncompress(dest.contents(), &realSize, recvData.contents() + recvData.rpos(), recvData.size() - recvData.rpos()) != Z_OK)
    {
        recvData.rfinish();                   // unnneded warning spam in this case
        TC_LOG_ERROR("network", "UAD: Failed to decompress account data");
        return;
    }

    type = recvData.ReadBits(3);
    if (type > NUM_ACCOUNT_DATA_TYPES)
        return;

    recvData.rfinish();                       // uncompress read (recvData.size() - recvData.rpos())

    std::string adata;
    dest >> adata;

    SetAccountData(AccountDataType(type), timestamp, adata);
}

void WorldSession::HandleRequestAccountData(WorldPacket& recvData)
{
    uint32 type = recvData.ReadBits(3);

    if (type > NUM_ACCOUNT_DATA_TYPES)
        return;

    AccountData* adata = GetAccountData(AccountDataType(type));

    uint32 size = adata->Data.size();

    uLongf destSize = compressBound(size);

    ByteBuffer dest;
    dest.resize(destSize);

    if (size && compress(dest.contents(), &destSize, (uint8 const*)adata->Data.c_str(), size) != Z_OK)
    {
        TC_LOG_DEBUG("network", "RAD: Failed to compress account data");
        return;
    }

    dest.resize(destSize);

    WorldPacket data(SMSG_UPDATE_ACCOUNT_DATA, 1 + 8 + 1 + 4 + 4 + 4 + dest.size());

    ObjectGuid guid = GetPlayer() ? GetPlayer()->GetGUID() : ObjectGuid::Empty;

    data.WriteBits(type, 3);

    data.WriteGuidMask(guid, 5, 1, 3, 7, 0, 4, 2, 6);

    data.WriteGuidBytes(guid, 3, 1, 5);

    data << uint32(size);
    data << uint32(destSize);

    data.append(dest);

    data.WriteGuidBytes(guid, 7, 4, 0, 6, 2);

    data << uint32(adata->Time);

    SendPacket(&data);
}

int32 WorldSession::HandleEnableNagleAlgorithm()
{
    // Instructs the server we wish to receive few amounts of large packets (SMSG_MULTIPLE_PACKETS?)
    // instead of large amount of small packets
    return 0;
}

void WorldSession::HandleSetActionButtonOpcode(WorldPacket& recvData)
{
    ObjectGuid buttonStream;
    uint8 slotId = 0;

    recvData >> slotId;

    recvData.ReadGuidMask(buttonStream, 7, 0, 5, 2, 1, 6, 3, 4);
    recvData.ReadGuidBytes(buttonStream, 6, 7, 3, 5, 2, 1, 4, 0);

    uint32 action = ACTION_BUTTON_ACTION(buttonStream.GetRawValue());
    uint8 type = ACTION_BUTTON_TYPE(buttonStream.GetRawValue());

    if (!action)
        GetPlayer()->RemoveActionButton(slotId);
    else
        GetPlayer()->AddActionButton(slotId, action, type);
}

void WorldSession::HandleCompleteCinematic(WorldPacket& /*recvData*/)
{
    int32 cinematic = _player->GetCinematic();
    if (cinematic <= 0)
        return;

    _player->SetCinematic(0);
    sScriptMgr->OnCinematicComplete(_player, cinematic);

    // If player has sight bound to visual waypoint NPC we should remove it
    GetPlayer()->GetCinematicMgr()->EndCinematic();
}

void WorldSession::HandleNextCinematicCamera(WorldPacket& /*recvData*/)
{
    // Sent by client when cinematic actually begun. So we begin the server side process
    GetPlayer()->GetCinematicMgr()->BeginCinematic();
}

void WorldSession::HandleCompleteMovie(WorldPacket& /*recvData*/)
{
    uint32 movie = _player->GetMovie();
    if (!movie)
        return;

    _player->SetMovie(0);
    sScriptMgr->OnMovieComplete(_player, movie);
}

void WorldSession::HandleSetActionBarToggles(WorldPacket& recvData)
{
    uint8 ActionBar = 0;
    recvData >> ActionBar;

    if (!GetPlayer())                                        // ignore until not logged (check needed because STATUS_AUTHED)
    {
        if (ActionBar != 0)
            TC_LOG_ERROR("network", "WorldSession::HandleSetActionBarToggles in not logged state with value: %u, ignored", uint32(ActionBar));
        return;
    }

    GetPlayer()->SetByteValue(PLAYER_BYTES1, PLAYER_BYTES1_OFFSET_ACTION_BAR_TOGGLES, ActionBar);
}

void WorldSession::HandleSendTimezoneInformation(WorldPacket& /*recvData*/)
{
    SendTimezoneInformation();
}

void WorldSession::HandlePlayedTime(WorldPacket& recvData)
{
    bool TriggerScriptEvent = false;
    TriggerScriptEvent = recvData.ReadBit();                // 0 or 1 expected

    WorldPacket data(SMSG_PLAYED_TIME, 4 + 4 + 1);

    data << uint32(_player->GetTotalPlayedTime());
    data << uint32(_player->GetLevelPlayedTime());

    data.WriteBit(TriggerScriptEvent);                      // 0 - will not show in chat frame

    data.FlushBits();

    SendPacket(&data);
}

void WorldSession::HandleInspectOpcode(WorldPacket& recvData)
{
    ObjectGuid playerGuid;

    recvData.ReadGuidMask(playerGuid, 0, 3, 7, 2, 5, 1, 4, 6);
    recvData.ReadGuidBytes(playerGuid, 3, 5, 2, 4, 1, 6, 0, 7);

    Player* player = ObjectAccessor::GetPlayer(*_player, playerGuid);
    if (!player)
    {
        TC_LOG_DEBUG("network", "CMSG_INSPECT: No player found from %s", playerGuid.ToString().c_str());
        return;
    }

    if (!GetPlayer()->IsWithinDistInMap(player, INSPECT_DISTANCE, false))
        return;

    if (GetPlayer()->IsValidAttackTarget(player))
        return;

    std::vector<uint16> talents;
    if (GetPlayer()->CanBeGameMaster() || sWorld->getIntConfig(CONFIG_TALENTS_INSPECTING) + (GetPlayer()->GetTeamId() == player->GetTeamId()) > 1)
    {
        for (auto itr : *player->GetTalentMap(player->GetActiveTalentGroup()))
        {
            if (itr.second->state == PLAYERSPELL_REMOVED)
                continue;

            SpellInfo const* spell = sSpellMgr->GetSpellInfo(itr.first);
            if (spell && spell->talentId)
                talents.push_back(spell->talentId);
        }
    }

    std::vector<uint16> glyphs;
    for (uint8 i = 0; i < MAX_GLYPH_SLOT_INDEX; ++i)
    {
        uint16 GlyphID = player->GetGlyph(0, i);
        if (!GlyphID)
            continue;

        glyphs.push_back(GlyphID);
    }

    std::unordered_map<ObjectGuid, std::vector<std::pair<uint8, uint32>>> itemEnchants;
    std::unordered_map<ObjectGuid, std::vector<uint32>> itemDynamics;

    uint32 enchantSize = 0;
    uint32 dynamicSize = 0;

    std::vector<Item*> items;
    for (uint32 i = 0; i < EQUIPMENT_SLOT_END; ++i)
    {
        Item* item = player->GetItemByPos(INVENTORY_SLOT_BAG_0, i);
        if (!item)
            continue;

        items.push_back(item);

        // Enchants
        for (uint32 j = 0; j < MAX_ENCHANTMENT_SLOT; ++j)
        {
            uint32 enchId = item->GetEnchantmentId(EnchantmentSlot(j));
            if (!enchId)
                continue;

            itemEnchants[item->GetGUID()].push_back(std::make_pair(j, enchId));
        }

        enchantSize += itemEnchants[item->GetGUID()].size();

        // Dynamic modifiers
        uint32 mask = item->GetUInt32Value(ITEM_MODIFIERS_MASK);

        itemDynamics[item->GetGUID()].push_back(mask);

        if (mask)
            for (uint8 i = 0; mask != 0; mask >>= 1, ++i)
                if ((mask & 1) != 0)
                    itemDynamics[item->GetGUID()].push_back(item->GetModifier(ItemModifier(i)));

        dynamicSize += itemDynamics[item->GetGUID()].size();
    }

    Guild* guild = sGuildMgr->GetGuildById(player->GetGuildId());
    bool HasGuild = guild != nullptr;

    WorldPacket data(SMSG_INSPECT, 1 + 8 + 9 + (HasGuild ? (1 + 8 + 4 + 8 + 4) : 0) + items.size() * (1 + 8 + 3 + 2 + 4 + 4 + 4 + 1) + 4 +
                    glyphs.size() * (2) + talents.size() * (2) + enchantSize * (1 + 4) + dynamicSize * (4));

    ByteBuffer guildData;
    ByteBuffer itemData;

    data.WriteBit(HasGuild);

    data.WriteGuidMask(playerGuid, 2);

    if (HasGuild)
    {
        ObjectGuid guildGuid = guild->GetGUID();

        data.WriteGuidMask(guildGuid, 7, 0, 5, 3, 2, 4, 6, 1);

        guildData.WriteGuidBytes(guildGuid, 6, 2, 5, 0);

        guildData << uint32(guild->GetMembersCount());

        guildData.WriteGuidBytes(guildGuid, 4, 7);

        guildData << uint64(guild->GetExperience());

        guildData.WriteGuidBytes(guildGuid, 1);

        guildData << uint32(guild->GetLevel());

        guildData.WriteGuidBytes(guildGuid, 3);
    }

    data.WriteGuidMask(playerGuid, 4, 3, 5, 7);

    data.WriteBits(items.size(), 20);

    data.WriteGuidMask(playerGuid, 0);

    for (Item* item : items)
    {
        ObjectGuid itemCreator = item->GetGuidValue(ITEM_CREATOR);

        bool HasItemSuffixFactor = item->GetItemSuffixFactor() != 0;
        bool HasItemPropertyID = item->GetItemRandomPropertyId() != 0;
        bool ItemIsUsable = true;

        data.WriteGuidMask(itemCreator, 1);

        data.WriteBit(HasItemSuffixFactor);
        data.WriteBit(ItemIsUsable);

        data.WriteGuidMask(itemCreator, 3);

        data.WriteBits(itemEnchants[item->GetGUID()].size(), 21);

        data.WriteGuidMask(itemCreator, 2, 6, 4);

        data.WriteBit(HasItemPropertyID);

        data.WriteGuidMask(itemCreator, 0, 5, 7);

        if (HasItemPropertyID)
            itemData << int16(item->GetItemRandomPropertyId());

        itemData.WriteGuidBytes(itemCreator, 3);

        /* START OF DYNAMIC FIELDS PART */

        size_t dynamicSize = itemDynamics[item->GetGUID()].size();

        // all dynamic modifiers sent as uint32
        itemData << uint32(dynamicSize * sizeof(uint32));

        // handle all dynamic modifiers
        for (uint32 modifier : itemDynamics[item->GetGUID()])
            itemData << uint32(modifier);

        /* END OF DYNAMIC FIELDS PART */

        for (auto & itr : itemEnchants[item->GetGUID()])
        {
            itemData << uint32(itr.second);
            itemData << uint8(itr.first);
        }

        itemData << uint32(item->GetEntry());

        itemData.WriteGuidBytes(itemCreator, 6, 4, 7, 2);

        if (HasItemSuffixFactor)
            itemData << uint32(item->GetItemSuffixFactor());

        itemData.WriteGuidBytes(itemCreator, 5);

        itemData << uint8(item->GetPos());

        itemData.WriteGuidBytes(itemCreator, 0, 1);
    }

    data.WriteBits(glyphs.size(), 23);
    data.WriteBits(talents.size(), 23);

    data.WriteGuidMask(playerGuid, 6, 1);

    data.WriteGuidBytes(playerGuid, 1, 4, 2);

    data.append(itemData);
    data.append(guildData);

    data.WriteGuidBytes(playerGuid, 5);

    for (uint16 GlyphID : glyphs)
        data << uint16(GlyphID);

    data.WriteGuidBytes(playerGuid, 0);

    data << uint32(player->GetSpecId(player->GetActiveTalentGroup()));

    for (uint16 TalentID : talents)
        data << uint16(TalentID);

    data.WriteGuidBytes(playerGuid, 7, 3, 6);

    SendPacket(&data);
}

void WorldSession::HandleInspectHonorStatsOpcode(WorldPacket& recvData)
{
    ObjectGuid playerGuid;

    recvData.ReadGuidMask(playerGuid, 4, 3, 6, 1, 0, 2, 5, 7);
    recvData.ReadGuidBytes(playerGuid, 0, 5, 1, 4, 2, 6, 7, 3);

    Player* player = ObjectAccessor::GetPlayer(*_player, playerGuid);
    if (!player)
    {
        TC_LOG_DEBUG("network", "CMSG_INSPECT_HONOR_STATS: No player found from GUID: %s", playerGuid.ToString().c_str());
        return;
    }

    WorldPacket data(SMSG_INSPECT_HONOR_STATS, 1 + 8 + 4 + 2 + 2 + 1);

    data << uint32(player->GetUInt32Value(PLAYER_LIFETIME_HONORABLE_KILLS));
    data << uint16(player->GetUInt16Value(PLAYER_KILLS, PLAYER_KILLS_OFFSET_YESTERDAY_KILLS));
    data << uint16(player->GetUInt16Value(PLAYER_KILLS, PLAYER_KILLS_OFFSET_TODAY_KILLS));
    data << uint8(player->GetByteValue(PLAYER_BYTES1, PLAYER_BYTES1_OFFSET_PVP_RANK));

    data.WriteGuidMask(playerGuid, 2, 1, 6, 4, 5, 3, 7, 0);
    data.WriteGuidBytes(playerGuid, 1, 3, 6, 7, 2, 4, 5, 0);

    SendPacket(&data);
}

void WorldSession::HandleInspectRatedBGStatsOpcode(WorldPacket& recvData)
{
    uint32 RealmId = 0;
    recvData >> RealmId;

    ObjectGuid playerGuid;

    recvData.ReadGuidMask(playerGuid, 0, 2, 5, 1, 6, 4, 3, 7);
    recvData.ReadGuidBytes(playerGuid, 7, 3, 1, 5, 4, 0, 2, 6);

    Player* player = ObjectAccessor::FindPlayer(playerGuid);
    if (!player)
    {
        TC_LOG_DEBUG("network", "CMSG_INSPECT_HONOR_STATS: No player found from %s", playerGuid.ToString().c_str());
        return;
    }

    if (!GetPlayer()->IsWithinDistInMap(player, INSPECT_DISTANCE, false))
        return;

    if (GetPlayer()->IsValidAttackTarget(player))
        return;

    std::unordered_map<uint8, StatsBySlot const*> ratedStats;

    RatedInfo* rInfo = sRatedMgr->GetRatedInfo(player->GetGUID());
    if (!rInfo)
        return;

    for (uint8 i = 0; i < MAX_RATED_SLOT; ++i)
    {
        RatedType ratedType = RatedInfo::GetRatedTypeBySlot(i);

        StatsBySlot const* stats = rInfo->GetStatsBySlot(ratedType);
        if (!stats)
            continue;

        ratedStats[i] = stats;
    }

    if (ratedStats.empty())
        return;

    WorldPacket data(SMSG_INSPECT_RATED_BG_STATS, 1 + 8 + 1 + ratedStats.size() * (4 + 4 + 4 + 4 + 4 + 4 + 4 + 1));

    data.WriteGuidMask(playerGuid, 4, 2, 3, 6, 0, 5, 7, 1);

    data.WriteBits(ratedStats.size(), 3);

    for (auto & itr : ratedStats)
    {
        uint8 slot = itr.first;
        StatsBySlot const* stats = itr.second;

        data << uint32(stats->SeasonWins);
        data << uint32(stats->SeasonBest);
        data << uint32(stats->WeekWins);
        data << uint32(stats->SeasonGames);
        data << uint32(RealmId);
        data << uint32(stats->WeekGames);
        data << uint32(stats->PersonalRating);
        data << uint8(slot);
    }

    data.WriteGuidBytes(playerGuid, 1, 7, 3, 2, 0, 5, 6, 4);

    SendPacket(&data);
}

void WorldSession::HandleWorldTeleportOpcode(WorldPacket& recvData)
{
    ObjectGuid transportGUID;

    uint32 MapID = 0;

    float PositionX = 0.0f;
    float PositionY = 0.0f;
    float PositionZ = 0.0f;
    float Orientation = 0.0f;

    recvData >> MapID;
    recvData >> Orientation;
    recvData >> PositionY;
    recvData >> PositionX;
    recvData >> PositionZ;

    recvData.ReadGuidMask(transportGUID, 0, 4, 2, 6, 1, 5, 7, 3);
    recvData.ReadGuidBytes(transportGUID, 2, 7, 3, 0, 4, 5, 1, 6);

    if (GetPlayer()->IsInFlight())
    {
        TC_LOG_DEBUG("network", "Player '%s' (GUID: %u) in flight, ignore worldport command.",
            GetPlayer()->GetName().c_str(), GetPlayer()->GetGUID().GetCounter());
        return;
    }

    if (HasPermission(rbac::RBAC_PERM_OPCODE_WORLD_TELEPORT))
        GetPlayer()->TeleportTo(MapID, PositionX, PositionY, PositionZ, Orientation);
    else
        SendNotification(LANG_YOU_NOT_HAVE_PERMISSION);
}

void WorldSession::HandleWhoisOpcode(WorldPacket& recvData)
{
    uint32 TextLength = recvData.ReadBits(6);
    std::string CharName = recvData.ReadString(TextLength);

    if (!HasPermission(rbac::RBAC_PERM_OPCODE_WHOIS))
    {
        SendNotification(LANG_YOU_NOT_HAVE_PERMISSION);
        return;
    }

    if (CharName.empty() || !normalizePlayerName(CharName))
    {
        SendNotification(LANG_NEED_CHARACTER_NAME);
        return;
    }

    Player* player = ObjectAccessor::FindConnectedPlayerByName(CharName);
    if (!player)
    {
        SendNotification(LANG_PLAYER_NOT_EXIST_OR_OFFLINE, CharName.c_str());
        return;
    }

    uint32 accid = player->GetSession()->GetAccountId();

    PreparedStatement* stmt = LoginDatabase.GetPreparedStatement(LOGIN_SEL_ACCOUNT_WHOIS);
    stmt->setUInt32(0, accid);
    PreparedQueryResult result = LoginDatabase.Query(stmt);

    if (!result)
    {
        SendNotification(LANG_ACCOUNT_FOR_PLAYER_NOT_FOUND, CharName.c_str());
        return;
    }

    Field* fields = result->Fetch();
    std::string acc = fields[0].GetString();
    if (acc.empty())
        acc = "Unknown";

    std::string email = fields[1].GetString();
    if (email.empty())
        email = "Unknown";

    std::string lastip = fields[2].GetString();
    if (lastip.empty())
        lastip = "Unknown";

    std::string msg = CharName + "'s " + "account is " + acc + ", e-mail: " + email + ", last ip: " + lastip;

    WorldPacket data(SMSG_WHO_IS, 2 + msg.length());

    data.WriteBits(msg.length(), 11);

    data.WriteString(msg);

    SendPacket(&data);
}

void WorldSession::HandleFarSightOpcode(WorldPacket& recvData)
{
    bool Apply = recvData.ReadBit();

    if (Apply)
    {
        if (WorldObject* target = _player->GetViewpoint())
            _player->SetSeer(target);
        else
            TC_LOG_DEBUG("network", "Player %s (%s) requests non-existing seer %s", _player->GetName().c_str(), _player->GetGUID().ToString().c_str(), _player->GetGuidValue(PLAYER_FARSIGHT_OBJECT).ToString().c_str());
    }
    else
        _player->SetSeer(_player);

    GetPlayer()->UpdateVisibilityForPlayer();
}

void WorldSession::HandleSetTitleOpcode(WorldPacket& recvData)
{
    int32 title = -1;
    recvData >> title;

    // -1 at none
    if (title > 0 && title < MAX_TITLE_INDEX)
    {
       if (!GetPlayer()->HasTitle(title))
            return;
    }
    else
        title = 0;

    GetPlayer()->SetUInt32Value(PLAYER_TITLE, title);
}

void WorldSession::SendTitleEarned(uint32 TitleIndex)
{
    WorldPacket data(SMSG_TITLE_EARNED, 4);

    data << uint32(TitleIndex);

    SendPacket(&data);
}

void WorldSession::SendTitleLost(uint32 TitleIndex)
{
    WorldPacket data(SMSG_TITLE_LOST, 4);

    data << uint32(TitleIndex);

    SendPacket(&data);
}

void WorldSession::HandleTimeSyncResp(WorldPacket& recvData)
{
    uint32 Counter = 0;
    uint32 ClientTicks = 0;

    recvData >> Counter;
    recvData >> ClientTicks;

    // Prevent crashing server if queue is empty
    if (_player->m_timeSyncQueue.empty())
    {
        TC_LOG_ERROR("network", "Received CMSG_TIME_SYNC_RESPONSE from player %s without requesting it (hacker?)", _player->GetName().c_str());
        return;
    }

    if (Counter != _player->m_timeSyncQueue.front())
        TC_LOG_ERROR("network", "Wrong time sync counter from player %s (cheater?)", _player->GetName().c_str());

    TC_LOG_DEBUG("network", "Time sync received for player %s: counter %u, client ticks %u, time since last sync %u", GetPlayer()->GetGUID().ToString().c_str(),
                Counter, ClientTicks, ClientTicks - _player->m_timeSyncClient);

    uint32 ourTicks = ClientTicks + (GameTime::GetGameTimeMS() - _player->m_timeSyncServer);

    // diff should be small
    TC_LOG_DEBUG("network", "Players' (%s) ticks: %u, diff %u, latency %u", GetPlayer()->GetGUID().ToString().c_str(), ourTicks, ourTicks - ClientTicks, GetLatency());

    _player->m_timeSyncClient = ClientTicks;
    _player->m_timeSyncQueue.pop();
}

void WorldSession::HandleResetInstancesOpcode(WorldPacket& /*recvData*/)
{
    if (Group* group = _player->GetGroup())
    {
        if (group->IsLeader(_player->GetGUID()))
            group->ResetInstances(INSTANCE_RESET_ALL, RESET_TYPE_NON_RAID, _player);
    }
    else
        _player->ResetInstances(INSTANCE_RESET_ALL, RESET_TYPE_NON_RAID);
}

void WorldSession::HandleSetDungeonDifficultyOpcode(WorldPacket& recvData)
{
    uint32 Mode = 0;
    recvData >> Mode;

    DifficultyEntry const* difficultyEntry = sDifficultyStore.LookupEntry(Mode);
    if (!difficultyEntry)
    {
        TC_LOG_DEBUG("network", "WorldSession::HandleSetDungeonDifficultyOpcode: %s sent an invalid instance mode %d!",
            _player->GetGUID().GetCounter(), Mode);
        return;
    }

    if (difficultyEntry->InstanceType != MAP_INSTANCE)
    {
        TC_LOG_DEBUG("network", "WorldSession::HandleSetDungeonDifficultyOpcode: %s sent an non-dungeon instance mode %d!",
            _player->GetGUID().GetCounter(), difficultyEntry->ID);
        return;
    }

    if (!(difficultyEntry->Flags & DIFFICULTY_FLAG_CAN_SELECT))
    {
        TC_LOG_DEBUG("network", "WorldSession::HandleSetDungeonDifficultyOpcode: %s sent unselectable instance mode %d!",
            _player->GetGUID().GetCounter(), difficultyEntry->ID);
        return;
    }

    Difficulty difficultyID = Difficulty(difficultyEntry->ID);
    if (difficultyID == _player->GetDungeonDifficultyID())
        return;

    // cannot reset while in an instance
    Map* map = _player->FindMap();
    if (map && map->IsDungeon())
    {
        TC_LOG_DEBUG("network", "WorldSession::HandleSetDungeonDifficultyOpcode: player (Name: %s, GUID: %u) tried to reset the instance while player is inside!",
            _player->GetName().c_str(), _player->GetGUID().GetCounter());
        return;
    }

    Group* group = _player->GetGroup();
    if (group)
    {
        if (group->IsLeader(_player->GetGUID()))
        {
            for (GroupReference* itr = group->GetFirstMember(); itr != NULL; itr = itr->next())
            {
                Player* groupGuy = itr->GetSource();
                if (!groupGuy)
                    continue;

                if (!groupGuy->IsInMap(groupGuy))
                    return;

                if (groupGuy->GetMap()->IsNonRaidDungeon())
                {
                    TC_LOG_DEBUG("network", "WorldSession::HandleSetDungeonDifficultyOpcode: player %d tried to reset the instance while group member (Name: %s, GUID: %u) is inside!",
                        _player->GetGUID().GetCounter(), groupGuy->GetName().c_str(), groupGuy->GetGUID().GetCounter());
                    return;
                }
            }

            // the difficulty is set even if the instances can't be reset
            group->ResetInstances(INSTANCE_RESET_CHANGE_DIFFICULTY, RESET_TYPE_INSTANCE, _player);
            group->SetDungeonDifficultyID(difficultyID);
            group->SendUpdate();
        }
    }
    else
    {
        _player->ResetInstances(INSTANCE_RESET_CHANGE_DIFFICULTY, RESET_TYPE_INSTANCE);
        _player->SetDungeonDifficultyID(difficultyID);
        _player->SendDungeonDifficulty();
    }
}

void WorldSession::HandleSetRaidDifficultyOpcode(WorldPacket& recvData)
{
    uint32 Mode = 0;
    recvData >> Mode;

    DifficultyEntry const* difficultyEntry = sDifficultyStore.LookupEntry(Mode);
    if (!difficultyEntry)
    {
        TC_LOG_DEBUG("network", "WorldSession::HandleSetRaidDifficultyOpcode: %s sent an invalid instance mode %u!",
            _player->GetGUID().GetCounter(), Mode);
        return;
    }

    if (difficultyEntry->InstanceType != MAP_RAID)
    {
        TC_LOG_DEBUG("network", "WorldSession::HandleSetRaidDifficultyOpcode: %s sent an non-dungeon instance mode %u!",
            _player->GetGUID().GetCounter(), difficultyEntry->ID);
        return;
    }

    if (!(difficultyEntry->Flags & DIFFICULTY_FLAG_CAN_SELECT))
    {
        TC_LOG_DEBUG("network", "WorldSession::HandleSetRaidDifficultyOpcode: %s sent unselectable instance mode %u!",
            _player->GetGUID().GetCounter(), difficultyEntry->ID);
        return;
    }

    Difficulty difficultyID = Difficulty(difficultyEntry->ID);
    if (difficultyID == _player->GetRaidDifficultyID())
        return;

    // cannot reset while in an instance
    Map* map = _player->FindMap();
    if (map && map->IsDungeon())
    {
        TC_LOG_DEBUG("network", "WorldSession::HandleSetRaidDifficultyOpcode: player %d tried to reset the instance while inside!", _player->GetGUID().GetCounter());
        return;
    }

    Group* group = _player->GetGroup();
    if (group)
    {
        if (group->IsLeader(_player->GetGUID()))
        {
            for (GroupReference* itr = group->GetFirstMember(); itr != NULL; itr = itr->next())
            {
                Player* groupGuy = itr->GetSource();
                if (!groupGuy)
                    continue;

                if (!groupGuy->IsInMap(groupGuy))
                    return;

                if (groupGuy->GetMap()->IsRaid())
                {
                    TC_LOG_DEBUG("network", "WorldSession::HandleSetRaidDifficultyOpcode: player %d tried to reset the instance while inside!", _player->GetGUID().GetCounter());
                    return;
                }
            }
            // the difficulty is set even if the instances can't be reset
            group->ResetInstances(INSTANCE_RESET_CHANGE_DIFFICULTY, RESET_TYPE_RAID, _player);
            group->SetRaidDifficultyID(difficultyID);
            group->SendUpdate();
        }
    }
    else
    {
        _player->ResetInstances(INSTANCE_RESET_CHANGE_DIFFICULTY, RESET_TYPE_RAID);
        _player->SetRaidDifficultyID(difficultyID);
        _player->SendRaidDifficulty();
    }
}

void WorldSession::HandleChangePlayerDifficulty(WorldPacket& recvData)
{
    TC_LOG_DEBUG("network", "WORLD: CMSG_CHANGEPLAYER_DIFFICULTY");
}

void WorldSession::HandleQueryInspectAchievements(WorldPacket& recvData)
{
    ObjectGuid guid;

    recvData.ReadGuidMask(guid, 2, 7, 1, 5, 4, 0, 3, 6);
    recvData.ReadGuidBytes(guid, 7, 2, 0, 4, 1, 5, 6, 3);

    Player* player = ObjectAccessor::GetPlayer(*_player, guid);
    if (!player)
    {
        TC_LOG_DEBUG("network", "CMSG_QUERY_INSPECT_ACHIEVEMENTS: Inspected Player %s", guid.ToString().c_str());
        return;
    }

    if (!GetPlayer()->IsWithinDistInMap(player, INSPECT_DISTANCE, false))
        return;

    if (GetPlayer()->IsValidAttackTarget(player))
        return;

    player->SendRespondInspectAchievements(_player);
}

void WorldSession::HandleWorldStateUITimerUpdate(WorldPacket& /*recvData*/)
{
    WorldPacket data(SMSG_WORLD_STATE_UI_TIMER_UPDATE, 4);

    data << uint32(time(NULL));

    SendPacket(&data);
}

void WorldSession::HandleReadyForAccountDataTimes(WorldPacket& /*recvData*/)
{
    SendAccountDataTimes(GLOBAL_CACHE_MASK);
}

void WorldSession::SendSetPhaseShift(std::set<uint32> const& phaseIds, std::set<uint32> const& terrainswaps, std::set<uint32> const& worldMapAreaSwaps)
{
    ObjectGuid guid = _player->GetGUID();

    WorldPacket data(SMSG_SET_PHASE_SHIFT, 1 + 8 + 4 + 4 + 4 + 4 + 4 + phaseIds.size() * (2) + terrainswaps.size() * (2) + worldMapAreaSwaps.size() * (2));

    data.WriteGuidMask(guid, 0, 3, 1, 4, 6, 2, 7, 5);

    data.WriteGuidBytes(guid, 4, 3, 2);

    data << uint32(phaseIds.size() * 2);
    for (uint16 PhaseID : phaseIds)
        data << uint16(PhaseID);

    data.WriteGuidBytes(guid, 0, 6);

    // Inactive terrain swaps
    data << uint32(0);
    //for (uint8 i = 0; i < inactiveSwapsCount; ++i)
    //    data << uint16(0);

    data.WriteGuidBytes(guid, 1, 7);

    data << uint32(worldMapAreaSwaps.size() * 2);
    for (uint16 AreaSwapID : worldMapAreaSwaps)
        data << uint16(AreaSwapID);

    data << uint32(terrainswaps.size() * 2);
    for (uint16 TerrainSwapID : terrainswaps)
        data << uint16(TerrainSwapID);

    data.WriteGuidBytes(guid, 5);

    // Flags
    data << uint32(0x08 | (!phaseIds.empty() ? 0x10 : 0));

    SendPacket(&data);
}

void WorldSession::HandleAreaSpiritHealerQueryOpcode(WorldPacket& recvData)
{
    ObjectGuid guid;

    recvData.ReadGuidMask(guid, 5, 6, 0, 4, 1, 2, 7, 3);
    recvData.ReadGuidBytes(guid, 0, 2, 6, 7, 1, 5, 3, 4);

    Creature* unit = GetPlayer()->GetMap()->GetCreature(guid);
    if (!unit)
        return;

    if (!unit->IsSpiritService())
        return;

    if (Battleground* bg = _player->GetBattleground())
        sBattlegroundMgr->SendAreaSpiritHealerQueryOpcode(_player, bg, guid);
    else if (Battlefield* bf = sBattlefieldMgr->GetBattlefieldByZoneId(_player->GetZoneId()))
        bf->SendAreaSpiritHealerQueryOpcode(_player, guid);
}

void WorldSession::HandleAreaSpiritHealerQueueOpcode(WorldPacket& recvData)
{
    ObjectGuid guid;

    recvData.ReadGuidMask(guid, 5, 4, 0, 2, 7, 1, 6, 3);
    recvData.ReadGuidBytes(guid, 1, 7, 6, 2, 4, 3, 0, 5);

    Creature* unit = GetPlayer()->GetMap()->GetCreature(guid);
    if (!unit)
        return;

    if (!unit->IsSpiritService())
        return;

    if (Battleground* bg = _player->GetBattleground())
        bg->AddPlayerToResurrectQueue(guid, _player->GetGUID());
    else if (Battlefield* bf = sBattlefieldMgr->GetBattlefieldByZoneId(_player->GetZoneId()))
        bf->AddPlayerToResurrectQueue(guid, _player->GetGUID());
}

void WorldSession::HandleHearthAndResurrect(WorldPacket& /*recvData*/)
{
    if (_player->IsInFlight())
        return;

    if (Battlefield* bf = sBattlefieldMgr->GetBattlefieldByZoneId(_player->GetZoneId()))
    {
        bf->PlayerAskToLeave(_player);
        return;
    }

    AreaTableEntry const* atEntry = sAreaStore.LookupEntry(_player->GetAreaId());
    if (!atEntry || !(atEntry->GetFlags() & AREA_FLAG_CAN_HEARTH_AND_RESURRECT))
        return;

    _player->BuildPlayerRepop();
    _player->ResurrectPlayer(1.0f);
    _player->TeleportTo(_player->m_homebindMapId, _player->m_homebindX, _player->m_homebindY, _player->m_homebindZ, _player->GetOrientation());
}

void WorldSession::HandleInstanceLockResponse(WorldPacket& recvData)
{
    bool Accept = recvData.ReadBit();

    if (!_player->HasPendingBind())
    {
        TC_LOG_DEBUG("network", "InstanceLockResponse: Player %s (guid %u) tried to bind himself/teleport to graveyard without a pending bind!",
            _player->GetName().c_str(), _player->GetGUID().GetCounter());
        return;
    }

    if (Accept)
        _player->BindToInstance();
    else
        _player->RepopAtGraveyard();

    _player->SetPendingBind(0, 0);
}

void WorldSession::HandleUpdateMissileTrajectory(WorldPacket& recvData)
{
    TC_LOG_DEBUG("network", "WORLD: CMSG_UPDATE_MISSILE_TRAJECTORY");

    ObjectGuid guid;
    uint32 spellId = 0;
    float elevation = 0.0f;
    float speed = 0.0f;
    float curX, curY, curZ;
    float targetX, targetY, targetZ;
    uint8 moveStop;

    //recvData >> guid >> spellId >> elevation >> speed;
    recvData >> curX >> curY >> curZ;
    recvData >> targetX >> targetY >> targetZ;
    recvData >> moveStop;

    Unit* caster = ObjectAccessor::GetUnit(*_player, guid);
    Spell* spell = caster ? caster->GetCurrentSpell(CURRENT_GENERIC_SPELL) : NULL;
    if (!spell || spell->m_spellInfo->Id != spellId || !spell->m_targets.HasDst() || !spell->m_targets.HasSrc())
    {
        recvData.rfinish();
        return;
    }

    Position pos = *spell->m_targets.GetSrcPos();
    pos.Relocate(curX, curY, curZ);
    spell->m_targets.ModSrc(pos);

    pos = *spell->m_targets.GetDstPos();
    pos.Relocate(targetX, targetY, targetZ);
    spell->m_targets.ModDst(pos);

    spell->m_targets.SetElevation(elevation);
    spell->m_targets.SetSpeed(speed);

    if (moveStop)
    {
        uint32 opcode;
        recvData >> opcode;
        recvData.SetOpcode(CMSG_MOVE_STOP); // always set to CMSG_MOVE_STOP in client SetOpcode
        HandleMovementOpcodes(recvData);
    }
}

void WorldSession::HandleViolenceLevel(WorldPacket& recvData)
{
    uint8 ViolenceLevel = 0;
    recvData >> ViolenceLevel;

    /*
    Client-side only.
    There are several files concerning the violence level.
    One file for this is UnitBloodLevels.dbc which have different textures for diferrent violence level.

    enum ViolenceLevels
    {
        VIOLENCELEVEL_NONE              = 0,
        VIOLENCELEVEL_MEDIUM            = 1,
        VIOLENCELEVEL_HIGH              = 2,
        VIOLENCELEVEL_NUMVIOLENCELEVELS = 3
    };

    Default is: VIOLENCELEVEL_HIGH
    */
}

void WorldSession::HandleObjectUpdateFailedOpcode(WorldPacket& recvData)
{
    ObjectGuid guid;

    recvData.ReadGuidMask(guid, 3, 5, 6, 0, 1, 2, 7, 4);
    recvData.ReadGuidBytes(guid, 0, 6, 5, 7, 2, 1, 3, 4);

    WorldObject* obj = ObjectAccessor::GetWorldObject(*GetPlayer(), guid);

    TC_LOG_ERROR("network", "Object update failed for object : guid %s (name: %s) for player %s (%s)", guid.ToString().c_str(),
                obj ? obj->GetName().c_str() : "object-not-found", GetPlayerName().c_str(), _player->GetGUID().ToString().c_str());

    // If create object failed for current player then client will be stuck on loading screen
    if (_player->GetGUID() == guid)
    {
        LogoutPlayer(true);
        return;
    }

    // Pretend we've never seen this object
    _player->m_clientGUIDs.erase(guid);
}

void WorldSession::HandleObjectUpdateRescuedOpcode(WorldPacket& recvData)
{
    ObjectGuid guid;

    recvData.ReadGuidMask(guid, 5, 3, 2, 0, 6, 4, 1, 7);
    recvData.ReadGuidBytes(guid, 2, 6, 3, 0, 4, 7, 5, 1);

    TC_LOG_ERROR("network", "Object update rescued for %s for player %s (%s)", guid.ToString().c_str(), GetPlayerName().c_str(), _player->GetGUID().ToString().c_str());

    // Client received values update after destroying object
    // re-register object in m_clientGUIDs to send DestroyObject on next visibility update
    _player->m_clientGUIDs.insert(guid);
}

void WorldSession::HandleSaveCUFProfiles(WorldPacket& recvData)
{
    uint32 Count = recvData.ReadBits(19);
    if (Count > MAX_CUF_PROFILES)
    {
        TC_LOG_ERROR("entities.player", "HandleSaveCUFProfiles - %s tried to save more than %i CUF profiles. Hacking attempt?", GetPlayerName().c_str(), MAX_CUF_PROFILES);
        recvData.rfinish();
        return;
    }

    CUFProfile* profiles[MAX_CUF_PROFILES];
    uint8 strlens[MAX_CUF_PROFILES];

    for (uint8 i = 0; i < Count; ++i)
    {
        profiles[i] = new CUFProfile();

        profiles[i]->BoolOptions.set(CUF_AUTO_ACTIVATE_SPEC_2            , recvData.ReadBit());
        profiles[i]->BoolOptions.set(CUF_DISPLAY_MAIN_TANK_AND_ASSIST    , recvData.ReadBit());
        profiles[i]->BoolOptions.set(CUF_DISPLAY_POWER_BAR               , recvData.ReadBit());
        profiles[i]->BoolOptions.set(CUF_AUTO_ACTIVATE_10_PLAYERS        , recvData.ReadBit());
        profiles[i]->BoolOptions.set(CUF_AUTO_ACTIVATE_3_PLAYERS         , recvData.ReadBit());
        profiles[i]->BoolOptions.set(CUF_LOCKED                          , recvData.ReadBit());
        profiles[i]->BoolOptions.set(CUF_AUTO_ACTIVATE_40_PLAYERS        , recvData.ReadBit());
        profiles[i]->BoolOptions.set(CUF_AUTO_ACTIVATE_2_PLAYERS         , recvData.ReadBit());
        profiles[i]->BoolOptions.set(CUF_KEEP_GROUPS_TOGETHER            , recvData.ReadBit());
        profiles[i]->BoolOptions.set(CUF_USE_CLASS_COLORS                , recvData.ReadBit());
        profiles[i]->BoolOptions.set(CUF_AUTO_ACTIVATE_25_PLAYERS        , recvData.ReadBit());
        profiles[i]->BoolOptions.set(CUF_SHOWN                           , recvData.ReadBit());

        strlens[i] = recvData.ReadBits(7);

        profiles[i]->BoolOptions.set(CUF_DISPLAY_PETS                    , recvData.ReadBit());
        profiles[i]->BoolOptions.set(CUF_AUTO_ACTIVATE_PVP               , recvData.ReadBit());
        profiles[i]->BoolOptions.set(CUF_DISPLAY_ONLY_DISPELLABLE_DEBUFFS, recvData.ReadBit());
        profiles[i]->BoolOptions.set(CUF_DISPLAY_NON_BOSS_DEBUFFS        , recvData.ReadBit());
        profiles[i]->BoolOptions.set(CUF_AUTO_ACTIVATE_15_PLAYERS        , recvData.ReadBit());
        profiles[i]->BoolOptions.set(CUF_DYNAMIC_POSITION                , recvData.ReadBit());
        profiles[i]->BoolOptions.set(CUF_DISPLAY_BORDER                  , recvData.ReadBit());
        profiles[i]->BoolOptions.set(CUF_DISPLAY_HORIZONTAL_GROUPS       , recvData.ReadBit());
        profiles[i]->BoolOptions.set(CUF_AUTO_ACTIVATE_SPEC_1            , recvData.ReadBit());
        profiles[i]->BoolOptions.set(CUF_AUTO_ACTIVATE_5_PLAYERS         , recvData.ReadBit());
        profiles[i]->BoolOptions.set(CUF_AUTO_ACTIVATE_PVE               , recvData.ReadBit());
        profiles[i]->BoolOptions.set(CUF_DISPLAY_HEAL_PREDICTION         , recvData.ReadBit());
        profiles[i]->BoolOptions.set(CUF_DISPLAY_AGGRO_HIGHLIGHT         , recvData.ReadBit());
    }

    for (uint8 i = 0; i < Count; ++i)
    {
        recvData >> profiles[i]->FrameHeight;
        recvData >> profiles[i]->LeftPoint;
        recvData >> profiles[i]->HealthText;
        recvData >> profiles[i]->FrameWidth;
        recvData >> profiles[i]->TopPoint;
        recvData >> profiles[i]->SortBy;
        recvData >> profiles[i]->BottomOffset;

        profiles[i]->ProfileName = recvData.ReadString(strlens[i]);

        recvData >> profiles[i]->BottomPoint;
        recvData >> profiles[i]->LeftOffset;
        recvData >> profiles[i]->TopOffset;

        GetPlayer()->SaveCUFProfile(i, profiles[i]);
    }

    for (uint8 i = Count; i < MAX_CUF_PROFILES; ++i)
        GetPlayer()->SaveCUFProfile(i, nullptr);
}

void WorldSession::SendLoadCUFProfiles()
{
    Player* player = GetPlayer();

    std::vector<CUFProfile*> profiles;
    uint32 stringCount = 0;

    for (uint8 i = 0; i < MAX_CUF_PROFILES; ++i)
    {
        CUFProfile* profile = player->GetCUFProfile(i);
        if (!profile)
            continue;

        stringCount += profile->ProfileName.size();

        profiles.push_back(profile);
    }

    ByteBuffer byteBuffer;

    WorldPacket data(SMSG_LOAD_CUF_PROFILES, 3 + profiles.size() * (32 + 15) + stringCount);

    data.WriteBits(profiles.size(), 19);

    for (auto & profile : profiles)
    {
        data.WriteBit(profile->BoolOptions[CUF_AUTO_ACTIVATE_SPEC_1]);
        data.WriteBit(profile->BoolOptions[CUF_AUTO_ACTIVATE_3_PLAYERS]);
        data.WriteBit(profile->BoolOptions[CUF_DYNAMIC_POSITION]);
        data.WriteBit(profile->BoolOptions[CUF_AUTO_ACTIVATE_10_PLAYERS]);
        data.WriteBit(profile->BoolOptions[CUF_AUTO_ACTIVATE_40_PLAYERS]);
        data.WriteBit(profile->BoolOptions[CUF_DISPLAY_BORDER]);
        data.WriteBit(profile->BoolOptions[CUF_USE_CLASS_COLORS]);
        data.WriteBit(profile->BoolOptions[CUF_KEEP_GROUPS_TOGETHER]);
        data.WriteBit(profile->BoolOptions[CUF_DISPLAY_POWER_BAR]);
        data.WriteBits(profile->ProfileName.size(), 7);
        data.WriteBit(profile->BoolOptions[CUF_DISPLAY_PETS]);
        data.WriteBit(profile->BoolOptions[CUF_DISPLAY_AGGRO_HIGHLIGHT]);
        data.WriteBit(profile->BoolOptions[CUF_SHOWN]);
        data.WriteBit(profile->BoolOptions[CUF_AUTO_ACTIVATE_PVP]);
        data.WriteBit(profile->BoolOptions[CUF_LOCKED]);
        data.WriteBit(profile->BoolOptions[CUF_DISPLAY_MAIN_TANK_AND_ASSIST]);
        data.WriteBit(profile->BoolOptions[CUF_DISPLAY_NON_BOSS_DEBUFFS]);
        data.WriteBit(profile->BoolOptions[CUF_DISPLAY_HORIZONTAL_GROUPS]);
        data.WriteBit(profile->BoolOptions[CUF_AUTO_ACTIVATE_SPEC_2]);
        data.WriteBit(profile->BoolOptions[CUF_DISPLAY_HEAL_PREDICTION]);
        data.WriteBit(profile->BoolOptions[CUF_DISPLAY_ONLY_DISPELLABLE_DEBUFFS]);
        data.WriteBit(profile->BoolOptions[CUF_AUTO_ACTIVATE_25_PLAYERS]);
        data.WriteBit(profile->BoolOptions[CUF_AUTO_ACTIVATE_PVE]);
        data.WriteBit(profile->BoolOptions[CUF_AUTO_ACTIVATE_5_PLAYERS]);
        data.WriteBit(profile->BoolOptions[CUF_AUTO_ACTIVATE_15_PLAYERS]);
        data.WriteBit(profile->BoolOptions[CUF_AUTO_ACTIVATE_2_PLAYERS]);

        byteBuffer << uint16(profile->LeftOffset);
        byteBuffer << uint16(profile->TopOffset);
        byteBuffer << uint8(profile->HealthText);
        byteBuffer.WriteString(profile->ProfileName);
        byteBuffer << uint8(profile->BottomPoint);
        byteBuffer << uint8(profile->LeftPoint);
        byteBuffer << uint16(profile->FrameHeight);
        byteBuffer << uint8(profile->TopPoint);
        byteBuffer << uint8(profile->SortBy);
        byteBuffer << uint16(profile->FrameWidth);
        byteBuffer << uint16(profile->BottomOffset);
    }

    data.append(byteBuffer);
    SendPacket(&data);
}

void WorldSession::HandleSelectFactionOpcode(WorldPacket& recvData)
{
    Player* player = GetPlayer();
    if (!player)
        return;

    if (player->GetRace() != RACE_PANDAREN_NEUTRAL)
        return;

    uint32 Choice = 0;
    recvData >> Choice;

    uint8 race = RACE_PANDAREN_HORDE;
    WorldLocation location = WorldLocation(1, 1357.62f, -4373.55f, 26.13f, 0.13f);;
    uint32 homebindLocation = 363;
    uint32 const* languageSpells = pandarenLanguageSpellsHorde;

    if (Choice)
    {
        race = RACE_PANDAREN_ALLIANCE;
        location = WorldLocation(0, -8960.02f, 516.10f, 96.36f, 0.67f);
        homebindLocation = 9;
        languageSpells = pandarenLanguageSpellsAlliance;
    }

    player->SetRace(race);
    player->SetFactionForRace(race);
    player->TeleportTo(location);
    player->SetHomebind(location, homebindLocation);

    for (uint8 i = 0; i < PANDAREN_FACTION_LANGUAGE_COUNT; i++)
        _player->LearnSpell(languageSpells[i], false);

    if (player->GetQuestStatus(31450) == QUEST_STATUS_INCOMPLETE)
        player->KilledMonsterCredit(64594);

    player->SaveToDB();

    bool ShowUI = true;

    WorldPacket data(SMSG_NEUTRAL_PLAYER_FACTION_SELECT_RESULT, 4 + 1);

    data << uint32(Choice);

    data.WriteBit(ShowUI);

    data.FlushBits();

    SendPacket(&data);

    player->GetReputationMgr().UpdateReputationFlags();
    player->GetReputationMgr().SendInitialReputations();

    player->SendFeatureSystemStatus();

    player->GetReputationMgr().SendForceReactions();

    player->SendMovieStart(116);
}

void WorldSession::HandleQueryCountdownTimer(WorldPacket& recvData)
{
    uint32 TimerType = 0;

    recvData >> TimerType;

    // ToDO: do something?
}
