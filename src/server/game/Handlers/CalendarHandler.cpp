/*
 * Copyright (C) 2011-2015 Project SkyFire <http://www.projectskyfire.org/>
 * Copyright (C) 2008-2015 TrinityCore <http://www.trinitycore.org/>
 * Copyright (C) 2005-2015 MaNGOS <http://getmangos.com/>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 3 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "CharacterCache.h"
#include "InstanceSaveMgr.h"
#include "Log.h"
#include "Opcodes.h"
#include "Player.h"
#include "SocialMgr.h"
#include "CalendarMgr.h"
#include "ObjectAccessor.h"
#include "DatabaseEnv.h"
#include "GuildMgr.h"
#include "WorldSession.h"

void WorldSession::HandleCalendarGetCalendar(WorldPacket& /*recvData*/)
{
    ObjectGuid guid = _player->GetGUID();

    time_t currTime = time(nullptr);

    CalendarInviteStore invites = sCalendarMgr->GetPlayerInvites(guid);
    CalendarEventStore playerEvents = sCalendarMgr->GetPlayerEvents(guid);
    ResetTimeByMapDifficultyMap const& resets = sInstanceSaveMgr->GetResetTimeMap();

    std::vector<InstanceSave const*> saves;
    for (uint8 i = 0; i < MAX_DIFFICULTY; ++i)
    {
        Player::BoundInstancesMap boundInstances = _player->GetBoundInstances(Difficulty(i));
        for (Player::BoundInstancesMap::const_iterator itr = boundInstances.begin(); itr != boundInstances.end(); ++itr)
            if (itr->second.perm)
                saves.push_back(itr->second.save);
    }

    std::unordered_map<int32, int32> sentMaps;
    for (ResetTimeByMapDifficultyMap::const_iterator itr = resets.begin(); itr != resets.end(); ++itr)
    {
        uint32 mapId = PAIR32_LOPART(itr->first);
        if (sentMaps.find(mapId) != sentMaps.end())
            continue;

        MapEntry const* mapEntry = sMapStore.LookupEntry(mapId);
        if (!mapEntry || !mapEntry->IsRaid())
            continue;

        sentMaps[mapId] = itr->second - currTime;
    }

    std::vector<std::pair<HolidaysEntry const*, CalendarHolidayDates const*>> holidays;

    uint32 holidaysStringSize = 0;
    for (HolidaysEntry const* holiday : sHolidaysStore)
    {
        CalendarHolidayDates const* holidayDates = sCalendarMgr->GetDatesForHoliday(holiday->ID);
        if (!holidayDates)
            continue;

        holidays.emplace_back(holiday, holidayDates);

        std::string TextureFilename = std::string(holiday->TextureFilename);
        holidaysStringSize += TextureFilename.length();
    }

    uint32 stringSize = 0;
    for (CalendarEvent* calendarEvent : playerEvents)
        stringSize += calendarEvent->GetTitle().size();

    ByteBuffer lockoutInfoBuffer;
    ByteBuffer invitesInfoBuffer;
    ByteBuffer eventsInfoBuffer;
    ByteBuffer holidaysInfoBuffer;

    WorldPacket data(SMSG_CALENDAR_SEND_CALENDAR, 11 + 4 + 4 + 4 + saves.size() * (1 + 8 + 4 + 4 + 4) + invites.size() * (1 + 8 + 8 + 1 + 8 + 1 + 1) +
                    playerEvents.size() * (2 * (1 + 8) + 1 + 4 + 4 + 4 + 8 + 2) + sentMaps.size() * (4 + 4 + 4) + stringSize + holidaysStringSize +
                    (holidays.size() * (1 + 51 * (4))));

    data.WriteBits(sentMaps.size(), 20);
    data.WriteBits(holidays.size(), 16);
    data.WriteBits(saves.size(), 20);

    for (InstanceSave const* save : saves)
    {
        ObjectGuid guid = ObjectGuid::Create<HighGuid::InstanceSave>(save->GetInstanceId());

        data.WriteGuidMask(guid, 6, 7, 2, 1, 5, 4, 0, 3);

        lockoutInfoBuffer << uint32(save->GetDifficultyID());

        lockoutInfoBuffer.WriteGuidBytes(guid, 3, 0, 1, 5);

        lockoutInfoBuffer << uint32(save->GetResetTime() - currTime);
        lockoutInfoBuffer << uint32(save->GetMapId());

        lockoutInfoBuffer.WriteGuidBytes(guid, 2, 7, 6, 4);
    }

    for (auto & itr : holidays)
    {
        HolidaysEntry const* holiday = itr.first;
        CalendarHolidayDates const& dates = *itr.second;

        std::string TextureFilename = std::string(holiday->TextureFilename);

        data.WriteBits(TextureFilename.length(), 6);

        holidaysInfoBuffer << uint32(dates[7]);
        holidaysInfoBuffer << uint32(dates[0]);
        holidaysInfoBuffer << uint32(holiday->CalendarFlags[8]);
        holidaysInfoBuffer << uint32(dates[12]);
        holidaysInfoBuffer << uint32(dates[16]);
        holidaysInfoBuffer << uint32(dates[21]);
        holidaysInfoBuffer << uint32(dates[14]);
        holidaysInfoBuffer << uint32(dates[8]);

        holidaysInfoBuffer << int32(holiday->CalendarFilterType);

        holidaysInfoBuffer << uint32(dates[24]);
        holidaysInfoBuffer << uint32(holiday->Duration[7]);
        holidaysInfoBuffer << uint32(dates[13]);
        holidaysInfoBuffer << uint32(holiday->Duration[2]);
        holidaysInfoBuffer << uint32(dates[10]);
        holidaysInfoBuffer << uint32(holiday->Duration[6]);
        holidaysInfoBuffer << uint32(dates[6]);
        holidaysInfoBuffer << uint32(dates[23]);

        holidaysInfoBuffer << int32(holiday->Priority);

        holidaysInfoBuffer << uint32(dates[19]);
        holidaysInfoBuffer << uint32(holiday->CalendarFlags[9]);
        holidaysInfoBuffer << uint32(holiday->Duration[8]);
        holidaysInfoBuffer << uint32(dates[9]);
        holidaysInfoBuffer << uint32(holiday->Duration[1]);
        holidaysInfoBuffer << uint32(holiday->CalendarFlags[6]);
        holidaysInfoBuffer << uint32(holiday->CalendarFlags[1]);
        holidaysInfoBuffer << uint32(dates[4]);
        holidaysInfoBuffer << uint32(holiday->Duration[0]);
        holidaysInfoBuffer << uint32(dates[20]);
        holidaysInfoBuffer << uint32(dates[18]);
        holidaysInfoBuffer << uint32(dates[2]);
        holidaysInfoBuffer << uint32(dates[25]);
        holidaysInfoBuffer << uint32(holiday->CalendarFlags[7]);

        holidaysInfoBuffer << int32(holiday->ID);

        holidaysInfoBuffer << uint32(holiday->Duration[4]);
        holidaysInfoBuffer << uint32(dates[22]);

        holidaysInfoBuffer.WriteString(TextureFilename);

        holidaysInfoBuffer << uint32(dates[1]);
        holidaysInfoBuffer << uint32(holiday->CalendarFlags[2]);
        holidaysInfoBuffer << uint32(holiday->Duration[9]);
        holidaysInfoBuffer << uint32(dates[3]);
        holidaysInfoBuffer << uint32(dates[15]);
        holidaysInfoBuffer << uint32(holiday->CalendarFlags[3]);
        holidaysInfoBuffer << uint32(holiday->Duration[3]);
        holidaysInfoBuffer << uint32(holiday->CalendarFlags[4]);
        holidaysInfoBuffer << uint32(dates[5]);
        holidaysInfoBuffer << uint32(dates[17]);
        holidaysInfoBuffer << uint32(holiday->CalendarFlags[5]);

        holidaysInfoBuffer << int32(holiday->Region);

        holidaysInfoBuffer << uint32(holiday->CalendarFlags[0]);
        holidaysInfoBuffer << uint32(dates[11]);

        holidaysInfoBuffer << int32(holiday->Looping);

        holidaysInfoBuffer << uint32(holiday->Duration[5]);
    }

    data.WriteBits(invites.size(), 19);

    for (CalendarInvite* invite : invites)
    {
        CalendarEvent* calendarEvent = sCalendarMgr->GetEvent(invite->GetEventId());
        ObjectGuid guid = invite->GetSenderGUID();

        data.WriteGuidMask(guid, 1, 2, 6, 7, 3, 0, 4, 5);

        invitesInfoBuffer.WriteGuidBytes(guid, 2);

        invitesInfoBuffer << uint64(invite->GetInviteId());
        invitesInfoBuffer << uint8(invite->GetStatus());

        invitesInfoBuffer.WriteGuidBytes(guid, 6, 3, 4, 1, 0);

        invitesInfoBuffer << uint64(invite->GetEventId());

        invitesInfoBuffer.WriteGuidBytes(guid, 7, 5);

        invitesInfoBuffer << uint8(invite->GetRank());
        invitesInfoBuffer << uint8((calendarEvent && calendarEvent->IsGuildEvent() && calendarEvent->GetGuildId() == _player->GetGuildId()) ? 1 : 0);
    }

    data.WriteBits(playerEvents.size(), 19);

    for (CalendarEvent* calendarEvent : playerEvents)
    {
        Guild* guild = sGuildMgr->GetGuildById(calendarEvent->GetGuildId());
        ObjectGuid guildGuid = guild ? guild->GetGUID() : ObjectGuid::Empty;
        ObjectGuid creatorGuid = calendarEvent->GetCreatorGUID();

        data.WriteGuidMask(creatorGuid, 2);

        data.WriteGuidMask(guildGuid, 1, 7);

        data.WriteGuidMask(creatorGuid, 4);

        data.WriteGuidMask(guildGuid, 5, 6, 3, 4);

        data.WriteGuidMask(creatorGuid, 7);

        data.WriteBits(calendarEvent->GetTitle().size(), 8);

        data.WriteGuidMask(creatorGuid, 1);

        data.WriteGuidMask(guildGuid, 2, 0);

        data.WriteGuidMask(creatorGuid, 0, 3, 6, 5);

        eventsInfoBuffer.WriteGuidBytes(creatorGuid, 5);

        eventsInfoBuffer.WriteGuidBytes(guildGuid, 3);

        eventsInfoBuffer.WriteString(calendarEvent->GetTitle());

        eventsInfoBuffer.WriteGuidBytes(guildGuid, 7);

        eventsInfoBuffer << int32(calendarEvent->GetDungeonId());

        eventsInfoBuffer.WriteGuidBytes(creatorGuid, 0, 4);

        eventsInfoBuffer.WriteGuidBytes(guildGuid, 2);

        eventsInfoBuffer.WriteGuidBytes(creatorGuid, 7, 2);

        eventsInfoBuffer.AppendPackedTime(calendarEvent->GetEventTime());

        eventsInfoBuffer.WriteGuidBytes(creatorGuid, 3, 1);

        eventsInfoBuffer.WriteGuidBytes(guildGuid, 6, 1);

        eventsInfoBuffer.WriteGuidBytes(creatorGuid, 6);

        eventsInfoBuffer << uint32(calendarEvent->GetFlags());

        eventsInfoBuffer.WriteGuidBytes(guildGuid, 4, 5, 0);

        eventsInfoBuffer << uint64(calendarEvent->GetEventId());
        eventsInfoBuffer << uint8(calendarEvent->GetType());
    }

    data.append(eventsInfoBuffer);
    data.append(lockoutInfoBuffer);
    data.append(holidaysInfoBuffer);
    data.append(invitesInfoBuffer);

    data.AppendPackedTime(currTime);

    for (auto & itr : sentMaps)
    {
        data << int32(itr.first);
        data << int32(itr.second);
        data << int32(0); // offset => found it different only once
    }

    data << uint32(1135753200); // Constant date, unk (28.12.2005 07:00)
    data << uint32(currTime);   // server time

    SendPacket(&data);
}

void WorldSession::HandleCalendarGetEvent(WorldPacket& recvData)
{
    uint64 eventId = 0;
    recvData >> eventId;

    if (CalendarEvent* calendarEvent = sCalendarMgr->GetEvent(eventId))
        sCalendarMgr->SendCalendarEvent(_player->GetGUID(), *calendarEvent, CALENDAR_SENDTYPE_GET);
    else
        sCalendarMgr->SendCalendarCommandResult(_player->GetGUID(), CALENDAR_ERROR_EVENT_INVALID);
}

void WorldSession::HandleCalendarGuildFilter(WorldPacket& recvData)
{
    uint8 minLevel = 0;
    uint8 maxLevel = 0;
    uint8 minRank = 0;

    recvData >> minLevel;
    recvData >> maxLevel;
    recvData >> minRank;

    if (Guild* guild = sGuildMgr->GetGuildById(_player->GetGuildId()))
        guild->MassInviteToEvent(this, minLevel, maxLevel, minRank);
}

void WorldSession::HandleCalendarAddEvent(WorldPacket& recvData)
{
    ObjectGuid guid = _player->GetGUID();

    std::string title;
    std::string description;

    uint8 type = 0;
    int32 dungeonId = -1;
    uint32 eventPackedTime = 0;
    uint32 maxInvites = 0;
    uint32 flags = 0;
    uint32 inviteeCount = 0;
    uint32 descriptionLength = 0;
    uint32 titleLength = 0;

    recvData >> maxInvites;
    recvData >> flags;
    recvData >> dungeonId;

    recvData.ReadPackedTime(eventPackedTime);

    recvData >> type;

    inviteeCount = recvData.ReadBits(22);
    descriptionLength = recvData.ReadBits(11);

    std::vector<CalendarInvitePacketInfo> calendarInviteList;
    for (uint32 i = 0; i < inviteeCount; ++i)
    {
        CalendarInvitePacketInfo info;

        recvData.ReadGuidMask(info.Guid, 7, 2, 6, 3, 5, 1, 0, 4);

        calendarInviteList.push_back(info);
    }

    titleLength = recvData.ReadBits(8);

    for (CalendarInvitePacketInfo& info : calendarInviteList)
    {
        recvData.ReadGuidBytes(info.Guid, 4, 2, 3, 1, 0, 6, 7);

        recvData >> info.Status;

        recvData.ReadGuidBytes(info.Guid, 5);

        recvData >> info.ModerationRank;
    }

    title = recvData.ReadString(titleLength);
    description = recvData.ReadString(descriptionLength);

    // prevent events in the past
    // To Do: properly handle timezones and remove the "- time_t(86400L)" hack
    if (time_t(eventPackedTime) < (time(NULL) - time_t(86400L)))
        return;

    CalendarEvent* calendarEvent = new CalendarEvent(sCalendarMgr->GetFreeEventId(), guid, 0, CalendarEventType(type), dungeonId,
        time_t(eventPackedTime), flags, title, description);

    if (calendarEvent->IsGuildEvent() || calendarEvent->IsGuildAnnouncement())
        calendarEvent->SetGuildId(GetPlayer()->GetGuildId());

    if (calendarEvent->IsGuildAnnouncement())
    {
        CalendarInvite* invite = new CalendarInvite(0, calendarEvent->GetEventId(), ObjectGuid::Empty, guid,
            CALENDAR_DEFAULT_RESPONSE_TIME, CALENDAR_STATUS_NOT_SIGNED_UP, CALENDAR_RANK_PLAYER, "");
        sCalendarMgr->AddInvite(calendarEvent, invite);
    }
    else
    {
        for (CalendarInvitePacketInfo& info : calendarInviteList)
        {
            CalendarInvite* invite = new CalendarInvite(sCalendarMgr->GetFreeInviteId(), calendarEvent->GetEventId(), ObjectGuid(info.Guid), guid,
                CALENDAR_DEFAULT_RESPONSE_TIME, CalendarInviteStatus(info.Status), CalendarModerationRank(info.ModerationRank), "");
            sCalendarMgr->AddInvite(calendarEvent, invite);
        }
     }

    sCalendarMgr->AddEvent(calendarEvent, CALENDAR_SENDTYPE_ADD);
}

void WorldSession::HandleCalendarUpdateEvent(WorldPacket& recvData)
{
    ObjectGuid guid = _player->GetGUID();

    time_t oldEventTime = 0;

    ObjectGuid eventId;
    ObjectGuid inviteId;

    std::string title;
    std::string description;

    uint8 type = 0;
    uint32 maxInvites = 0;
    int32 dungeonId = -1;
    uint32 eventPackedTime = 0;
    uint32 flags = 0;
    uint16 descriptionLength = 0;
    uint16 titleLength = 0;

    recvData >> maxInvites;
    recvData >> dungeonId;

    recvData.ReadPackedTime(eventPackedTime);

    recvData >> flags;
    recvData >> type;

    recvData.ReadGuidMask(eventId, 4, 5, 2);

    recvData.ReadGuidMask(inviteId, 4);

    recvData.ReadGuidMask(eventId, 7, 0);

    recvData.ReadGuidMask(inviteId, 5, 3);

    recvData.ReadGuidMask(eventId, 6, 1);

    recvData.ReadGuidMask(inviteId, 6, 2, 7, 1, 0);

    descriptionLength = recvData.ReadBits(11);
    titleLength = recvData.ReadBits(8);

    recvData.ReadGuidMask(eventId, 3);

    recvData.ReadGuidBytes(inviteId, 6);

    recvData.ReadGuidBytes(eventId, 0);

    recvData.ReadGuidBytes(inviteId, 7, 3);

    recvData.ReadGuidBytes(eventId, 6);

    recvData.ReadGuidBytes(inviteId, 1);

    recvData.ReadGuidBytes(eventId, 2);

    title = recvData.ReadString(titleLength);

    recvData.ReadGuidBytes(inviteId, 5, 4);

    recvData.ReadGuidBytes(eventId, 5, 3);

    recvData.ReadGuidBytes(inviteId, 0);

    recvData.ReadGuidBytes(eventId, 4);

    description = recvData.ReadString(descriptionLength);

    recvData.ReadGuidBytes(eventId, 1);

    recvData.ReadGuidBytes(inviteId, 2);

    recvData.ReadGuidBytes(eventId, 7);

    // prevent events in the past
    // To Do: properly handle timezones and remove the "- time_t(86400L)" hack
    if (time_t(eventPackedTime) < (time(NULL) - time_t(86400L)))
        return;

    if (CalendarEvent* calendarEvent = sCalendarMgr->GetEvent(eventId))
    {
        oldEventTime = calendarEvent->GetEventTime();

        calendarEvent->SetType(CalendarEventType(type));
        calendarEvent->SetFlags(flags);
        calendarEvent->SetEventTime(time_t(eventPackedTime));
        calendarEvent->SetDungeonId(dungeonId);
        calendarEvent->SetTitle(title);
        calendarEvent->SetDescription(description);

        sCalendarMgr->UpdateEvent(calendarEvent);
        sCalendarMgr->SendCalendarEventUpdateAlert(*calendarEvent, oldEventTime);
    }
    else
        sCalendarMgr->SendCalendarCommandResult(guid, CALENDAR_ERROR_EVENT_INVALID);
}

void WorldSession::HandleCalendarRemoveEvent(WorldPacket& recvData)
{
    ObjectGuid guid = _player->GetGUID();

    uint64 eventId = 0;
    uint64 inviteId = 0;
    uint32 flags = 0;

    recvData >> inviteId;
    recvData >> eventId;
    recvData >> flags;

    sCalendarMgr->RemoveEvent(eventId, guid);
}

void WorldSession::HandleCalendarCopyEvent(WorldPacket& recvData)
{
    ObjectGuid guid = _player->GetGUID();

    uint64 eventId = 0;
    uint64 inviteId = 0;
    uint32 eventTime = 0;

    recvData >> eventId;
    recvData >> inviteId;

    recvData.ReadPackedTime(eventTime);

    // prevent events in the past
    // To Do: properly handle timezones and remove the "- time_t(86400L)" hack
    if (time_t(eventTime) < (time(NULL) - time_t(86400L)))
        return;

    if (CalendarEvent* oldEvent = sCalendarMgr->GetEvent(eventId))
    {
        CalendarEvent* newEvent = new CalendarEvent(*oldEvent, sCalendarMgr->GetFreeEventId(), guid);
        newEvent->SetEventTime(time_t(time));
        CalendarInviteStore invites = sCalendarMgr->GetEventInvites(eventId);
        SQLTransaction trans;
        if (invites.size() > 1)
            trans = CharacterDatabase.BeginTransaction();

        for (CalendarInviteStore::const_iterator itr = invites.begin(); itr != invites.end(); ++itr)
            sCalendarMgr->AddInvite(newEvent, new CalendarInvite(**itr, sCalendarMgr->GetFreeInviteId(), newEvent->GetEventId(), guid), false);

        sCalendarMgr->AddEvent(newEvent, CALENDAR_SENDTYPE_COPY);
    }
    else
        sCalendarMgr->SendCalendarCommandResult(guid, CALENDAR_ERROR_EVENT_INVALID);
}

void WorldSession::HandleCalendarEventInvite(WorldPacket& recvData)
{
    ObjectGuid playerGuid = _player->GetGUID();

    uint64 eventId = 0;
    uint64 inviteId = 0;

    std::string name;

    uint16 length = 0;
    bool isPreInvite = false;
    bool isGuildEvent = false;

    ObjectGuid inviteeGuid;
    uint32 inviteeTeam = 0;
    uint32 inviteeGuildId = 0;

    recvData >> eventId;
    recvData >> inviteId;

    isPreInvite = recvData.ReadBit();

    length = recvData.ReadBits(9);

    isGuildEvent = recvData.ReadBit();

    name = recvData.ReadString(length);

    if (!normalizePlayerName(name))
        return;

    Player* player = ObjectAccessor::FindConnectedPlayerByName(name.c_str());
    if (player)
    {
        // Invitee is online
        inviteeGuid = player->GetGUID();
        inviteeTeam = player->GetTeam();
        inviteeGuildId = player->GetGuildId();
    }
    else
    {
        // Invitee offline, get data from storage
        ObjectGuid guid = sCharacterCache->GetCharacterGuidByName(name);
        if (!guid.IsEmpty())
        {
            if (CharacterCacheEntry const* characterInfo = sCharacterCache->GetCharacterCacheByGuid(guid))
            {
                inviteeGuid = guid;
                inviteeTeam = Player::TeamForRace(characterInfo->Race);
                inviteeGuildId = characterInfo->GuildId;
            }
        }
    }

    if (!inviteeGuid)
    {
        sCalendarMgr->SendCalendarCommandResult(playerGuid, CALENDAR_ERROR_PLAYER_NOT_FOUND);
        return;
    }

    if (_player->GetTeam() != inviteeTeam && !sWorld->getBoolConfig(CONFIG_ALLOW_TWO_SIDE_INTERACTION_CALENDAR))
    {
        sCalendarMgr->SendCalendarCommandResult(playerGuid, CALENDAR_ERROR_NOT_ALLIED);
        return;
    }

    if (QueryResult result = CharacterDatabase.PQuery("SELECT flags FROM character_social WHERE guid = " UI64FMTD " AND friend = " UI64FMTD, inviteeGuid.GetRawValue(), playerGuid.GetRawValue()))
    {
        Field* fields = result->Fetch();
        if (fields[0].GetUInt8() & SOCIAL_FLAG_IGNORED)
        {
            sCalendarMgr->SendCalendarCommandResult(playerGuid, CALENDAR_ERROR_IGNORING_YOU_S, name.c_str());
            return;
        }
    }

    if (!isPreInvite)
    {
        if (CalendarEvent* calendarEvent = sCalendarMgr->GetEvent(eventId))
        {
            if (calendarEvent->GetEventTime() < time(NULL))
            {
                sCalendarMgr->SendCalendarCommandResult(playerGuid, CALENDAR_ERROR_EVENT_PASSED);
                return;
            }

            // we probably can invite them, but we have to send them some different info about that invite
            if (calendarEvent->IsGuildEvent() && calendarEvent->GetGuildId() == inviteeGuildId)
            {
                sCalendarMgr->SendCalendarCommandResult(playerGuid, CALENDAR_ERROR_NO_GUILD_INVITES);
                return;
            }

            CalendarInvite* invite = new CalendarInvite(sCalendarMgr->GetFreeInviteId(), eventId, inviteeGuid, playerGuid, 0, CALENDAR_STATUS_INVITED, CALENDAR_RANK_PLAYER, "");
            sCalendarMgr->AddInvite(calendarEvent, invite, true, player && player->GetGuildId() != calendarEvent->GetGuildId());
        }
        else
            sCalendarMgr->SendCalendarCommandResult(playerGuid, CALENDAR_ERROR_EVENT_INVALID);
    }
    else
    {
        // we probably can invite them, but we have to send them some different info about that invite
        if (isGuildEvent && _player->GetGuildId() == inviteeGuildId)
        {
            sCalendarMgr->SendCalendarCommandResult(playerGuid, CALENDAR_ERROR_NO_GUILD_INVITES);
            return;
        }

        CalendarInvite invite(0, 0, inviteeGuid, playerGuid, 0, CALENDAR_STATUS_INVITED, CALENDAR_RANK_PLAYER, "");
        sCalendarMgr->SendCalendarEventInvite(invite);
    }
}

void WorldSession::HandleCalendarEventSignup(WorldPacket& recvData)
{
    ObjectGuid guid = _player->GetGUID();

    uint64 eventId = 0;
    bool tentative = false;

    recvData >> eventId;
    tentative = recvData.ReadBit();

    if (CalendarEvent* calendarEvent = sCalendarMgr->GetEvent(eventId))
    {
        if (calendarEvent->IsGuildEvent() && calendarEvent->GetGuildId() != _player->GetGuildId())
        {
            sCalendarMgr->SendCalendarCommandResult(guid, CALENDAR_ERROR_GUILD_PLAYER_NOT_IN_GUILD);
            return;
        }

        CalendarInviteStatus status = tentative ? CALENDAR_STATUS_TENTATIVE : CALENDAR_STATUS_SIGNED_UP;
        CalendarInvite* invite = new CalendarInvite(sCalendarMgr->GetFreeInviteId(), eventId, guid, guid, time(NULL), status, CALENDAR_RANK_PLAYER, "");
        sCalendarMgr->AddInvite(calendarEvent, invite);
        sCalendarMgr->SendCalendarClearPendingAction(guid);
    }
    else
        sCalendarMgr->SendCalendarCommandResult(guid, CALENDAR_ERROR_EVENT_INVALID);
}

void WorldSession::HandleCalendarEventRsvp(WorldPacket& recvData)
{
    ObjectGuid guid = _player->GetGUID();

    uint64 eventId = 0;
    uint64 inviteId = 0;
    uint8 status = 0;

    recvData >> eventId;
    recvData >> inviteId;
    recvData >> status;

    if (CalendarEvent* calendarEvent = sCalendarMgr->GetEvent(eventId))
    {
        // i think we still should be able to remove self from locked events
        if (status != CALENDAR_STATUS_REMOVED && calendarEvent->GetFlags() & CALENDAR_FLAG_INVITES_LOCKED)
        {
            sCalendarMgr->SendCalendarCommandResult(guid, CALENDAR_ERROR_EVENT_LOCKED);
            return;
        }

        if (CalendarInvite* invite = sCalendarMgr->GetInvite(inviteId))
        {
            invite->SetStatus(CalendarInviteStatus(status));
            invite->SetStatusTime(time(NULL));

            sCalendarMgr->UpdateInvite(invite);
            sCalendarMgr->SendCalendarEventStatus(*calendarEvent, *invite);
            sCalendarMgr->SendCalendarEventStatusAlert(*calendarEvent, *invite);
            sCalendarMgr->SendCalendarClearPendingAction(guid);
        }
        else
            sCalendarMgr->SendCalendarCommandResult(guid, CALENDAR_ERROR_NO_INVITE); // correct?
    }
    else
        sCalendarMgr->SendCalendarCommandResult(guid, CALENDAR_ERROR_EVENT_INVALID);
}

void WorldSession::HandleCalendarEventRemoveInvite(WorldPacket& recvData)
{
    ObjectGuid guid = _player->GetGUID();

    ObjectGuid invitee;

    uint64 eventId = 0;
    uint64 senderId = 0;
    uint64 inviteId = 0;

    recvData >> inviteId;
    recvData >> senderId;
    recvData >> eventId;

    recvData.ReadGuidMask(invitee, 6, 3, 2, 4, 5, 7, 0, 1);
    recvData.ReadGuidBytes(invitee, 0, 4, 7, 3, 5, 1, 2, 6);

    if (CalendarEvent* calendarEvent = sCalendarMgr->GetEvent(eventId))
    {
        if (calendarEvent->GetCreatorGUID() == invitee)
        {
            sCalendarMgr->SendCalendarCommandResult(guid, CALENDAR_ERROR_DELETE_CREATOR_FAILED);
            return;
        }

        sCalendarMgr->RemoveInvite(inviteId, eventId, guid);
    }
    else
        sCalendarMgr->SendCalendarCommandResult(guid, CALENDAR_ERROR_NO_INVITE);
}

void WorldSession::HandleCalendarEventStatus(WorldPacket& recvData)
{
    ObjectGuid guid = _player->GetGUID();

    ObjectGuid invitee;

    uint64 eventId = 0;
    uint64 inviteId = 0;
    uint64 senderId = 0;
    uint8 status = 0;

    recvData >> senderId;
    recvData >> eventId;
    recvData >> inviteId;
    recvData >> status;

    recvData.ReadGuidMask(invitee, 4, 3, 7, 6, 2, 0, 5, 1);
    recvData.ReadGuidBytes(invitee, 7, 5, 2, 1, 4, 6, 0, 3);

    if (CalendarEvent* calendarEvent = sCalendarMgr->GetEvent(eventId))
    {
        if (CalendarInvite* invite = sCalendarMgr->GetInvite(inviteId))
        {
            invite->SetStatus((CalendarInviteStatus)status);

            sCalendarMgr->UpdateInvite(invite);
            sCalendarMgr->SendCalendarEventStatus(*calendarEvent, *invite);
            sCalendarMgr->SendCalendarEventStatusAlert(*calendarEvent, *invite);
            sCalendarMgr->SendCalendarClearPendingAction(invitee);
        }
        else
            sCalendarMgr->SendCalendarCommandResult(guid, CALENDAR_ERROR_NO_INVITE); // correct?
    }
    else
        sCalendarMgr->SendCalendarCommandResult(guid, CALENDAR_ERROR_EVENT_INVALID);
}

void WorldSession::HandleCalendarEventModeratorStatus(WorldPacket& recvData)
{
    ObjectGuid guid = _player->GetGUID();

    ObjectGuid invitee;

    uint64 eventId = 0;
    uint64 inviteId = 0;
    uint64 senderId = 0;
    uint8 rank = 0;

    recvData >> rank;
    recvData >> eventId;
    recvData >> senderId;
    recvData >> inviteId;

    recvData.ReadGuidMask(invitee, 6, 5, 1, 3, 4, 7, 0, 2);
    recvData.ReadGuidBytes(invitee, 7, 5, 0, 4, 1, 3, 2, 6);

    if (CalendarEvent* calendarEvent = sCalendarMgr->GetEvent(eventId))
    {
        if (calendarEvent->IsGuildEvent())
        {
            sCalendarMgr->SendCalendarCommandResult(guid, CALENDAR_ERROR_NO_MODERATOR);
            return;
        }

        if (CalendarInvite* invite = sCalendarMgr->GetInvite(inviteId))
        {
            invite->SetRank(CalendarModerationRank(rank));
            sCalendarMgr->UpdateInvite(invite);
            sCalendarMgr->SendCalendarEventModeratorStatus(*calendarEvent, *invite);
        }
        else
            sCalendarMgr->SendCalendarCommandResult(guid, CALENDAR_ERROR_NO_INVITE); // correct?
    }
    else
        sCalendarMgr->SendCalendarCommandResult(guid, CALENDAR_ERROR_EVENT_INVALID);
}

void WorldSession::HandleCalendarComplain(WorldPacket& recvData)
{
    ObjectGuid guid = _player->GetGUID();

    ObjectGuid complainGUID;

    uint64 eventId = 0;
    uint64 inviteId = 0;

    recvData >> inviteId;
    recvData >> eventId;

    recvData.ReadGuidMask(complainGUID, 4, 6, 2, 7, 1, 5, 3, 0);
    recvData.ReadGuidBytes(complainGUID, 6, 7, 1, 0, 4, 2, 3, 5);

    // what to do with complains?
}

void WorldSession::HandleCalendarGetNumPending(WorldPacket& /*recvData*/)
{
    ObjectGuid guid = _player->GetGUID();

    uint32 pending = sCalendarMgr->GetPlayerNumPending(guid);

    WorldPacket data(SMSG_CALENDAR_SEND_NUM_PENDING, 4);

    data << uint32(pending);

    SendPacket(&data);
}

void WorldSession::HandleSetSavedInstanceExtend(WorldPacket& recvData)
{
    uint32 mapId, difficulty;
    uint8 toggleExtend;
    recvData >> mapId >> difficulty>> toggleExtend;
    TC_LOG_DEBUG("network", "CMSG_SET_SAVED_INSTANCE_EXTEND - MapId: %u, Difficulty: %u, ToggleExtend: %s", mapId, difficulty, toggleExtend ? "On" : "Off");

    if (Player* player = GetPlayer())
    {
        InstancePlayerBind* instanceBind = player->GetBoundInstance(mapId, Difficulty(difficulty), toggleExtend); // include expired instances if we are toggling extend on
        if (!instanceBind || !instanceBind->save || !instanceBind->perm)
            return;

        BindExtensionState newState;
        if (!toggleExtend || instanceBind->extendState == EXTEND_STATE_EXPIRED)
            newState = EXTEND_STATE_NORMAL;
        else
            newState = EXTEND_STATE_EXTENDED;

        player->BindToInstance(instanceBind->save, true, newState, false);
    }

    /*
    InstancePlayerBind* instanceBind = _player->GetBoundInstance(mapId, Difficulty(difficulty));
    if (!instanceBind || !instanceBind->save)
        return;

    InstanceSave* save = instanceBind->save;
    // http://www.wowwiki.com/Instance_Lock_Extension
    // SendCalendarRaidLockoutUpdated(save);
    */
}

// ----------------------------------- SEND ------------------------------------

void WorldSession::SendCalendarRaidLockout(InstanceSave const* save, bool add)
{
    time_t currTime = time(nullptr);

    ObjectGuid guid = save ? ObjectGuid::Create<HighGuid::InstanceSave>(save->GetInstanceId()) : ObjectGuid::Empty;
    if (add)
    {
        WorldPacket data(SMSG_CALENDAR_RAID_LOCKOUT_ADDED, 1 + 8 + 4 + 4 + 4 + 4);

        data.WriteGuidMask(guid, 3, 1, 2, 0, 4, 7, 5, 6);

        data.WriteGuidBytes(guid, 6);

        data << uint32(save->GetDifficultyID());

        data.WriteGuidBytes(guid, 0, 4, 5, 3, 2);

        data << uint32(save->GetMapId());

        data.WriteGuidBytes(guid, 1);

        data.AppendPackedTime(currTime);

        data << uint32(save->GetResetTime() - currTime);

        data.WriteGuidBytes(guid, 7);

        SendPacket(&data);
    }
    else
    {
        WorldPacket data(SMSG_CALENDAR_RAID_LOCKOUT_REMOVED, 4 + 4 + 8);

        data << uint32(save->GetDifficultyID());
        data << uint32(save->GetMapId());

        data.WriteGuidMask(guid, 2, 0, 4, 6, 5, 7, 3, 1);
        data.WriteGuidBytes(guid, 6, 1, 7, 3, 4, 5, 0, 2);

        SendPacket(&data);
    }
}

void WorldSession::SendCalendarRaidLockoutUpdated(InstanceSave const* save)
{
    if (!save)
        return;

    ObjectGuid guid = _player->GetGUID();

    time_t currTime = time(nullptr);

    WorldPacket data(SMSG_CALENDAR_RAID_LOCKOUT_UPDATED, 4 + 4 + 4 + 4 + 4);

    data << uint32(save->GetResetTime() - currTime); // New remaining time? => found only same values as the possible old time
    data << uint32(save->GetMapId());
    data << uint32(save->GetDifficultyID());
    data.AppendPackedTime(currTime);
    data << uint32(save->GetResetTime() - currTime); // Old remaining time? => found only same values as the possible new time
    SendPacket(&data);
}
