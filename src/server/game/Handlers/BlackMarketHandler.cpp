/*
 * Copyright (C) 2011-2015 Project SkyFire <http://www.projectskyfire.org/>
 * Copyright (C) 2008-2015 TrinityCore <http://www.trinitycore.org/>
 * Copyright (C) 2005-2015 MaNGOS <http://getmangos.com/>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 3 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "ObjectMgr.h"
#include "Opcodes.h"
#include "Player.h"
#include "Unit.h"
#include "World.h"
#include "WorldPacket.h"
#include "WorldSession.h"
#include "BlackMarketMgr.h"

void WorldSession::HandleBlackMarketOpen(WorldPacket& recvData)
{
    ObjectGuid guid;

    recvData.ReadGuidMask(guid, 4, 5, 2, 7, 0, 1, 3, 6);
    recvData.ReadGuidBytes(guid, 3, 5, 0, 6, 4, 1, 7, 2);

    Creature* unit = GetPlayer()->GetNPCIfCanInteractWith(guid, UNIT_NPC_FLAG_BLACK_MARKET);
    if (!unit)
    {
        TC_LOG_DEBUG("network", "WORLD: HandleBlackMarketHello - Unit (GUID: %s) not found or you can't interact with him.", guid.ToString().c_str());
        return;
    }

    if (GetPlayer()->HasUnitState(UNIT_STATE_DIED))
        GetPlayer()->RemoveAurasByType(SPELL_AURA_FEIGN_DEATH);

    SendBlackMarketOpenResult(guid, unit);
}

void WorldSession::SendBlackMarketOpenResult(ObjectGuid npcGuid, Creature* unit)
{
    WorldPacket data(SMSG_BLACK_MARKET_OPEN_RESULT, 1 + 8 + 1);

    data.WriteGuidMask(npcGuid, 2, 0, 4, 1, 3, 6, 5, 7);

    data.WriteBit(sBlackMarketMgr->IsEnabled());

    data.FlushBits();

    data.WriteGuidBytes(npcGuid, 6, 1, 2, 5, 0, 7, 4, 3);

    SendPacket(&data);
}

void WorldSession::HandleBlackMarketRequestItems(WorldPacket& recvData)
{
    if (!sBlackMarketMgr->IsEnabled())
        return;

    ObjectGuid guid;
    uint32 Timestamp;

    recvData >> Timestamp;

    recvData.ReadGuidMask(guid, 2, 6, 0, 3, 4, 5, 1, 7);
    recvData.ReadGuidBytes(guid, 6, 2, 3, 5, 7, 4, 1, 0);

    Creature* unit = GetPlayer()->GetNPCIfCanInteractWith(guid, UNIT_NPC_FLAG_BLACK_MARKET);
    if (!unit)
    {
        TC_LOG_DEBUG("network", "WORLD: HandleBlackMarketRequestItems - Unit (GUID: %s) not found or you can't interact with him.", guid.ToString().c_str());
        return;
    }

    sBlackMarketMgr->BuildItemsResponse(GetPlayer());
}

void WorldSession::HandleBlackMarketBidOnItem(WorldPacket& recvData)
{
    if (!sBlackMarketMgr->IsEnabled())
        return;

    ObjectGuid guid;
    uint32 Item, MarketID;
    uint64 BidAmount;

    recvData >> Item >> MarketID >> BidAmount;

    recvData.ReadGuidMask(guid, 0, 5, 4, 3, 7, 6, 1, 2);
    recvData.ReadGuidBytes(guid, 4, 3, 6, 5, 7, 1, 0, 2);

    Player* player = GetPlayer();
    Creature* unit = player->GetNPCIfCanInteractWith(guid, UNIT_NPC_FLAG_BLACK_MARKET);
    if (!unit)
    {
        TC_LOG_DEBUG("network", "WORLD: HandleBlackMarketBidOnItem - Unit (GUID: %s) not found or you can't interact with him.", guid.ToString().c_str());
        return;
    }

    BlackMarketEntry* entry = sBlackMarketMgr->GetAuctionByID(MarketID);
    if (!entry)
    {
        TC_LOG_DEBUG("network", "WORLD: HandleBlackMarketBidOnItem - Player (%s, name: %s) tried to bid on a nonexistent auction (MarketId: %i).", player->GetGUID().ToString().c_str(), player->GetName().c_str(), MarketID);
        SendBlackMarketBidOnItemResult(ERR_BMAH_ITEM_NOT_FOUND, MarketID, Item);
        return;
    }

    if (entry->GetBidder() == player->GetGUID().GetCounter())
    {
        TC_LOG_DEBUG("network", "WORLD: HandleBlackMarketBidOnItem - Player (%s, name: %s) tried to place a bid on an item he already bid on. (MarketId: %i).", player->GetGUID().ToString().c_str(), player->GetName().c_str(), MarketID);
        SendBlackMarketBidOnItemResult(ERR_BMAH_ALREADY_BID, MarketID, Item);
        return;
    }

    if (!entry->ValidateBid(BidAmount))
    {
        TC_LOG_DEBUG("network", "WORLD: HandleBlackMarketBidOnItem - Player (%s, name: %s) tried to place an invalid bid. Amount: %lu (MarketId: %i).", player->GetGUID().ToString().c_str(), player->GetName().c_str(), BidAmount, MarketID);
        SendBlackMarketBidOnItemResult(ERR_BMAH_HIGHER_BID, MarketID, Item);
        return;
    }

    if (!player->HasEnoughMoney(BidAmount))
    {
        TC_LOG_DEBUG("network", "WORLD: HandleBlackMarketBidOnItem - Player (%s, name: %s) does not have enough money to place bid. (MarketId: %i).", player->GetGUID().ToString().c_str(), player->GetName().c_str(), MarketID);
        SendBlackMarketBidOnItemResult(ERR_BMAH_NOT_ENOUGH_MONEY, MarketID, Item);
        return;
    }

    if (entry->GetSecondsRemaining() <= 0)
    {
        TC_LOG_DEBUG("network", "WORLD: HandleBlackMarketBidOnItem - Player (%s, name: %s) tried to bid on a completed auction. (MarketId: %i).", player->GetGUID().ToString().c_str(), player->GetName().c_str(), MarketID);
        SendBlackMarketBidOnItemResult(ERR_BMAH_DATABASE_ERROR, MarketID, Item);
        return;
    }

    SQLTransaction trans = CharacterDatabase.BeginTransaction();

    sBlackMarketMgr->SendAuctionOutbidMail(entry, trans);
    entry->PlaceBid(BidAmount, player, trans);

    CharacterDatabase.CommitTransaction(trans);

    SendBlackMarketBidOnItemResult(ERR_BMAH_OK, MarketID, Item);
}

void WorldSession::SendBlackMarketBidOnItemResult(int32 error, int32 MarketID, uint32 ItemID)
{
    WorldPacket data(SMSG_BLACK_MARKET_BID_ON_ITEM_RESULT, 4 + 4 + 4);

    data << int32(MarketID);
    data << int32(error);
    data << uint32(ItemID);

    SendPacket(&data);
}

void WorldSession::SendBlackMarketWonNotification(BlackMarketEntry const* entry, Item const* item)
{
    WorldPacket data(SMSG_BLACK_MARKET_BID_WON, 4 + 4 + 4);

    data << int32(item->GetItemRandomPropertyId());
    data << int32(entry->GetMarketId());
    data << uint32(item->GetEntry());

    SendPacket(&data);
}

void WorldSession::SendBlackMarketOutbidNotification(BlackMarketTemplate const* templ)
{
    WorldPacket data(SMSG_BLACK_MARKET_OUTBID, 4 + 4 + 4);

    data << int32(templ->MarketID);
    data << int32(0); // Random Properties
    data << uint32(templ->Item);

    SendPacket(&data);
}