/*
* Copyright (C) 2015-2016 Theatre of Dreams <http://theatreofdreams.eu/>
*
* This program is free software; you can redistribute it and/or modify it
* under the terms of the GNU General Public License as published by the
* Free Software Foundation; either version 3 of the License, or (at your
* option) any later version.
*
* This program is distributed in the hope that it will be useful, but WITHOUT
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
* more details.
*
* You should have received a copy of the GNU General Public License along
* with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef _TOD_AUTHSERVER_HPP_
#define _TOD_AUTHSERVER_HPP_

#include <memory>

#include <boost/thread.hpp>
#include <boost/asio/io_service.hpp>

#include "RealmAcceptor.h"

#include "ace/Reactor.h"
#include "Implementation/LoginDatabase.h"

namespace Tod
{
    class AuthServer
    {
    public:
        ~AuthServer();

        void                    Run();

    private:
        void                    PrintWelcomeInfo();
        bool                    CreatePidFile();

        bool                    StartDatabaseWorkers();
        void                    StopDatabaseWorkers();
        void                    SetProcessAffinity();
        bool                    SetupListenServer();

        void                    StartProcessLoop();

        boost::thread           m_thread;
        boost::asio::io_service m_ios;

        typedef std::unique_ptr< ACE_Reactor > ACE_ReactorPtr;
        ACE_ReactorPtr          m_reactor;
        RealmAcceptor           m_acceptor;
    };
}

#endif /* _TOD_AUTHSERVER_HPP_ */
