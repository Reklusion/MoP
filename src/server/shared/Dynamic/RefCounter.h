/*
 * Copyright (C) 2015-2016 Theatre of Dreams <http://www.theatreofdreams.eu/>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 3 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * Author: lukaasm <lukaasm@gmail.com>
*/

#ifndef TOD_REFCOUNTER_H
#define TOD_REFCOUNTER_H

#include "Define.h"
#include "Errors.h"

#include <atomic>

namespace Tod
{
    class RefCounter
    {
    public:
        RefCounter()
            : m_refCounter( 0 )
        {

        }

        virtual ~RefCounter()
        {
        };

        void AddRef()
        {
            m_refCounter.fetch_add( 1u, std::memory_order_relaxed );
        }

        void RemRef()
        {
            m_refCounter.fetch_sub( 1u, std::memory_order_relaxed );
        }

        uint32 GetRefCounter()
        {
            return m_refCounter.load();
        }

    private:
        std::atomic< uint32 > m_refCounter;
    };
}

#endif /* TOD_REFCOUNTER_H */
