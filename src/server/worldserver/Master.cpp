/*
 * Copyright (C) 2015-2016 Theatre of Dreams <http://theatreofdreams.eu/>
 * Copyright (C) 2011-2015 Project SkyFire <http://www.projectskyfire.org/>
 * Copyright (C) 2008-2015 TrinityCore <http://www.trinitycore.org/>
 * Copyright (C) 2005-2015 MaNGOS <http://getmangos.com/>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 3 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

 /** \file
     \ingroup Trinityd
 */

#include "Common.h"
#include "SystemConfig.h"
#include "ScriptLoader.h"
#include "ScriptMgr.h"
#include "World.h"
#include "WorldRunnable.h"
#include "WorldSocket.h"
#include "WorldSocketMgr.h"
#include "Configuration/Config.hpp"
#include "Database/DatabaseEnv.h"
#include "Database/DatabaseWorkerPool.h"

#include "Runable.hpp"
#include "CliRunnable.h"
#include "Log.h"
#include "Master.h"
#include "RARunnable.h"
#include "TCSoap.h"
#include "Timer.h"
#include "Util.h"
#include "RealmList.h"

#include "BigNumber.h"
#include "OpenSSLCrypto.h"

#ifdef _WIN32
#include "ServiceWin32.h"
extern int m_ServiceStatus;
#endif

#ifdef __linux__
#include <sched.h>
#include <sys/resource.h>
#endif

class FreezeDetectorRunnable : public Tod::Threading::Runnable
{
private:
    uint32 _loops;
    uint32 _lastChange;
    uint32 _delaytime;
public:
    FreezeDetectorRunnable()
    {
        _loops = 0;
        _lastChange = 0;
        _delaytime = 0;
    }

    void SetDelayTime( uint32 t ) {
        _delaytime = t;
    }

    virtual void Run() override
    {
        if ( !_delaytime )
            return;

        TC_LOG_INFO( "server.worldserver", "Starting up anti-freeze thread (%u seconds max stuck time)...", _delaytime / 1000 );
        _loops = 0;
        _lastChange = 0;
        while ( !World::IsStopped() && !m_stopRequested )
        {
            std::this_thread::sleep_for( std::chrono::seconds( 1 ) );
            uint32 curtime = getMSTime();
            // normal work
            uint32 worldLoopCounter = World::m_worldLoopCounter;
            if ( _loops != worldLoopCounter )
            {
                _lastChange = curtime;
                _loops = worldLoopCounter;
            }
            // possible freeze
            else if ( getMSTimeDiff( _lastChange, curtime ) > _delaytime )
            {
                TC_LOG_ERROR( "server.worldserver", "World Thread hangs, kicking out server!" );
                ASSERT( false );
            }
        }
        TC_LOG_INFO( "server.worldserver", "Anti-freeze thread exiting without problems." );
    }
};

/// Main function
int Master::Run()
{
    OpenSSLCrypto::threadsSetup();
    BigNumber seed1;
    seed1.SetRand( 16 * 8 );

    TC_LOG_INFO( "server.worldserver", "%s (worldserver-daemon)", _FULLVERSION );
    TC_LOG_INFO( "server.worldserver", "<Ctrl-C> to stop.\n" );

    TC_LOG_INFO( "server.worldserver", "  TTTTTTTTTTTTTTTTTTTTTTT                  DDDDDDDDDDDDD        " );
    TC_LOG_INFO( "server.worldserver", "  T:::::::::::::::::::::T                  D::::::::::::DDD     " );
    TC_LOG_INFO( "server.worldserver", "  T:::::::::::::::::::::T                  D:::::::::::::::DD   " );
    TC_LOG_INFO( "server.worldserver", "  T:::::TT:::::::TT:::::T                  DDD:::::DDDDD:::::D  " );
    TC_LOG_INFO( "server.worldserver", "  TTTTTT  T:::::T  TTTTT   ooooooooooo       D:::::D    D:::::D " );
    TC_LOG_INFO( "server.worldserver", "          T:::::T        oo:::::::::::oo     D:::::D     D:::::D" );
    TC_LOG_INFO( "server.worldserver", "          T:::::T       o:::::::::::::::o    D:::::D     D:::::D" );
    TC_LOG_INFO( "server.worldserver", "          T:::::T       o:::::ooooo:::::o    D:::::D     D:::::D" );
    TC_LOG_INFO( "server.worldserver", "          T:::::T       o::::o     o::::o    D:::::D     D:::::D" );
    TC_LOG_INFO( "server.worldserver", "          T:::::T       o::::o     o::::o    D:::::D     D:::::D" );
    TC_LOG_INFO( "server.worldserver", "          T:::::T       o::::o     o::::o    D:::::D     D:::::D" );
    TC_LOG_INFO( "server.worldserver", "          T:::::T       o::::o     o::::o    D:::::D    D:::::D " );
    TC_LOG_INFO( "server.worldserver", "        TT:::::::TT     o:::::ooooo:::::   DDD:::::DDDDD:::::D  " );
    TC_LOG_INFO( "server.worldserver", "        T:::::::::T     o:::::::::::::::   D:::::::::::::::DD   " );
    TC_LOG_INFO( "server.worldserver", "        T:::::::::T      oo:::::::::::oo   D::::::::::::DDD     " );
    TC_LOG_INFO( "server.worldserver", "        TTTTTTTTTTT        ooooooooooo     DDDDDDDDDDDDD        " );
    TC_LOG_INFO( "server.worldserver", "                                                          " );
    TC_LOG_INFO( "server.worldserver", "            Project Theatre of Dreams 2015(c)             " );
    TC_LOG_INFO( "server.worldserver", "       Based on <http://www.projectskyfire.org/> \n       " );

    /// worldserver PID file creation
    std::string pidFile = Tod::GetConfig().Get<std::string>( "PidFile", "" );
    if ( !pidFile.empty() )
    {
        if ( uint32 pid = CreatePIDFile( pidFile ) )
            TC_LOG_INFO( "server.worldserver", "Daemon PID: %u\n", pid );
        else
        {
            TC_LOG_ERROR( "server.worldserver", "Cannot create PID file %s.\n", pidFile.c_str() );
            return 1;
        }
    }

    ///- Start the databases
    if ( !_StartDB() )
        return 1;

    // set server offline (not connectable)
    LoginDatabase.DirectPExecute( "UPDATE realmlist SET flag = (flag & ~%u) | %u WHERE id = '%d'", REALM_FLAG_OFFLINE, REALM_FLAG_INVALID, realmID );

    ///- Initialize the World
    sScriptMgr->SetScriptLoader( AddScripts );
    sWorld->SetInitialWorldSettings();


    auto worldThread = std::make_unique< WorldRunnable >();
    worldThread->Start();

    std::vector< std::unique_ptr< Tod::Threading::Runnable > > threads;

    bool startConsole = Tod::GetConfig().Get( "Console.Enable", true );
    if ( startConsole )
    {
        auto runnable = std::make_unique< CliRunnable >();
        runnable->Start();

        threads.push_back( std::move( runnable ) );
    }

    threads.push_back( std::make_unique< RARunnable >() );

#if defined(_WIN32) || defined(__linux__)

    ///- Handle affinity for multiple processors and process priority
    uint32 affinity = Tod::GetConfig().Get( "UseProcessors", 0 );
    bool highPriority = Tod::GetConfig().Get( "ProcessPriority", false );

#ifdef _WIN32 // Windows

    HANDLE hProcess = GetCurrentProcess();

    if ( affinity > 0 )
    {
        ULONG_PTR appAff;
        ULONG_PTR sysAff;

        if ( GetProcessAffinityMask( hProcess, &appAff, &sysAff ) )
        {
            ULONG_PTR currentAffinity = affinity & appAff;            // remove non accessible processors

            if ( !currentAffinity )
                TC_LOG_ERROR( "server.worldserver", "Processors marked in UseProcessors bitmask (hex) %x are not accessible for the worldserver. Accessible processors bitmask (hex): %x", affinity, appAff );
            else if ( SetProcessAffinityMask( hProcess, currentAffinity ) )
                TC_LOG_INFO( "server.worldserver", "Using processors (bitmask, hex): %x", currentAffinity );
            else
                TC_LOG_ERROR( "server.worldserver", "Can't set used processors (hex): %x", currentAffinity );
        }
    }

    if ( highPriority )
    {
        if ( SetPriorityClass( hProcess, HIGH_PRIORITY_CLASS ) )
            TC_LOG_INFO( "server.worldserver", "worldserver process priority class set to HIGH" );
        else
            TC_LOG_ERROR( "server.worldserver", "Can't set worldserver process priority class." );
    }

#else // Linux

    if ( affinity > 0 )
    {
        cpu_set_t mask;
        CPU_ZERO( &mask );

        for ( unsigned int i = 0; i < sizeof( affinity ) * 8; ++i )
            if ( affinity & ( 1 << i ) )
                CPU_SET( i, &mask );

        if ( sched_setaffinity( 0, sizeof( mask ), &mask ) )
            TC_LOG_ERROR( "server.worldserver", "Can't set used processors (hex): %x, error: %s", affinity, strerror( errno ) );
        else
        {
            CPU_ZERO( &mask );
            sched_getaffinity( 0, sizeof( mask ), &mask );
            TC_LOG_INFO( "server.worldserver", "Using processors (bitmask, hex): %lx", *( __cpu_mask* )( &mask ) );
        }
    }

    if ( highPriority )
    {
        if ( setpriority( PRIO_PROCESS, 0, PROCESS_HIGH_PRIORITY ) )
            TC_LOG_ERROR( "server.worldserver", "Can't set worldserver process priority class, error: %s", strerror( errno ) );
        else
            TC_LOG_INFO( "server.worldserver", "worldserver process priority class set to %i", getpriority( PRIO_PROCESS, 0 ) );
    }

#endif
#endif

    bool startSoap = Tod::GetConfig().Get( "SOAP.Enabled", false );
    if ( startSoap )
    {
        std::string bindIP = Tod::GetConfig().Get< std::string >( "SOAP.IP", "127.0.0.1" );
        uint16 bindPort = Tod::GetConfig().Get<uint16>( "SOAP.Port", 7878 );

        auto runnable = std::make_unique< TCSoapRunnable >();
        runnable->SetListenArguments( bindIP, bindPort );
        runnable->Start();

        threads.push_back( std::move( runnable ) );
    }

    if ( uint32 freezeDelay = Tod::GetConfig().Get<int>( "MaxCoreStuckTime", 0 ) )
    {
        auto runnable = std::make_unique< FreezeDetectorRunnable >();
        runnable->SetDelayTime( freezeDelay * 1000 );
        runnable->Start();

        threads.push_back( std::move( runnable ) );
    }

    ///- Launch the world listener socket
    uint16 worldPort = uint16( sWorld->getIntConfig( CONFIG_PORT_WORLD ) );
    std::string bindIp = Tod::GetConfig().Get<std::string>( "BindIP", "0.0.0.0" );

    if ( sWorldSocketMgr->StartNetwork( worldPort, bindIp.c_str() ) == -1 )
    {
        TC_LOG_ERROR( "server.worldserver", "Failed to start network" );
        World::StopNow( ERROR_EXIT_CODE );
        // go down and shutdown the server
    }

    // set server online (allow connecting now)
    LoginDatabase.DirectPExecute( "UPDATE realmlist SET flag = flag & ~%u, population = 0 WHERE id = '%u'", REALM_FLAG_INVALID, realmID );

    TC_LOG_INFO( "server.worldserver", "%s (worldserver-daemon) ready...", _FULLVERSION );

    worldThread->Wait();

    // set server offline
    LoginDatabase.DirectPExecute( "UPDATE realmlist SET flag = flag | %u WHERE id = '%d'", REALM_FLAG_OFFLINE, realmID );

    ///- Clean database before leaving
    ClearOnlineAccounts();

    _StopDB();

    TC_LOG_INFO( "server.worldserver", "Halting process..." );

    for ( auto & runnable : threads )
    {
        runnable->Stop();
    }

    threads.clear();

    OpenSSLCrypto::threadsCleanup();
    return World::GetExitCode();
}

/// Initialize connection to the databases
bool Master::_StartDB()
{
    MySQL::Library_Init();

    std::string sqlUpdatesPath = Tod::GetConfig().Get<std::string>( "DatabaseUpdater.PathToUpdates", "" );
    if ( !sqlUpdatesPath.size() || ( *sqlUpdatesPath.rbegin() != '\\' && *sqlUpdatesPath.rbegin() != '/' ) )
#if PLATFORM == PLATFORM_WINDOWS
        sqlUpdatesPath += '\\';
#else
        sqlUpdatesPath += '/';
#endif

    std::string pathToBase = "";
    if (Tod::GetConfig().Get<bool>( "DatabaseUpdater.Enabled", false ) )
        pathToBase = sqlUpdatesPath + "hotfixes";

    std::string dbString;
    uint8 asyncThreads, synchThreads;

    dbString = Tod::GetConfig().Get<std::string>( "HotfixDatabaseInfo", "" );
    if ( dbString.empty() )
    {
        TC_LOG_ERROR( "server.worldserver", "Hotfixes database not specified in configuration file" );
        return false;
    }

    asyncThreads = uint8( Tod::GetConfig().Get<int>( "HotfixDatabase.WorkerThreads", 1 ) );
    if ( asyncThreads < 1 || asyncThreads > 32 )
    {
        TC_LOG_ERROR( "server.worldserver", "Hotfixes database: invalid number of worker threads specified. "
                      "Please pick a value between 1 and 32." );
        asyncThreads = 1;
    }

    synchThreads = uint8( Tod::GetConfig().Get<int>( "HotfixDatabase.SynchThreads", 1 ) );

    ///- Initialize the Hotfixes database
    if ( !HotfixDatabase.Open( dbString, uint8( asyncThreads ), uint8( synchThreads ), pathToBase ) )
    {
        TC_LOG_ERROR( "server.worldserver", "Cannot connect to Hotfixes database %s", dbString.c_str() );
        return false;
    }

    if ( !pathToBase.empty() )
        pathToBase = sqlUpdatesPath + "world";

    dbString = Tod::GetConfig().Get<std::string>( "WorldDatabaseInfo", "" );
    if ( dbString.empty() )
    {
        TC_LOG_ERROR( "server.worldserver", "World database not specified in configuration file" );
        return false;
    }

    asyncThreads = uint8( Tod::GetConfig().Get<int>( "WorldDatabase.WorkerThreads", 1 ) );
    if ( asyncThreads < 1 || asyncThreads > 32 )
    {
        TC_LOG_ERROR( "server.worldserver", "World database: invalid number of worker threads specified. "
                      "Please pick a value between 1 and 32." );
        return false;
    }

    synchThreads = uint8( Tod::GetConfig().Get<int>( "WorldDatabase.SynchThreads", 1 ) );
    ///- Initialize the world database
    if ( !WorldDatabase.Open( dbString, asyncThreads, synchThreads, pathToBase ) )
    {
        TC_LOG_ERROR( "server.worldserver", "Cannot connect to world database %s", dbString.c_str() );
        return false;
    }

    ///- Get character database info from configuration file
    if ( !pathToBase.empty() )
        pathToBase = sqlUpdatesPath + "characters";

    dbString = Tod::GetConfig().Get<std::string>( "CharacterDatabaseInfo", "" );
    if ( dbString.empty() )
    {
        TC_LOG_ERROR( "server.worldserver", "Character database not specified in configuration file" );
        return false;
    }

    asyncThreads = uint8( Tod::GetConfig().Get<int>( "CharacterDatabase.WorkerThreads", 1 ) );
    if ( asyncThreads < 1 || asyncThreads > 32 )
    {
        TC_LOG_ERROR( "server.worldserver", "Character database: invalid number of worker threads specified. "
                      "Please pick a value between 1 and 32." );
        return false;
    }

    synchThreads = uint8( Tod::GetConfig().Get<int>( "CharacterDatabase.SynchThreads", 2 ) );

    ///- Initialize the Character database
    if ( !CharacterDatabase.Open( dbString, asyncThreads, synchThreads, pathToBase ) )
    {
        TC_LOG_ERROR( "server.worldserver", "Cannot connect to Character database %s", dbString.c_str() );
        return false;
    }

    if ( !pathToBase.empty() )
        pathToBase = sqlUpdatesPath + "auth";

    ///- Get login database info from configuration file
    dbString = Tod::GetConfig().Get<std::string>( "LoginDatabaseInfo", "" );
    if ( dbString.empty() )
    {
        TC_LOG_ERROR( "server.worldserver", "Login database not specified in configuration file" );
        return false;
    }

    asyncThreads = uint8( Tod::GetConfig().Get<int>( "LoginDatabase.WorkerThreads", 1 ) );
    if ( asyncThreads < 1 || asyncThreads > 32 )
    {
        TC_LOG_ERROR( "server.worldserver", "Login database: invalid number of worker threads specified. "
                      "Please pick a value between 1 and 32." );
        return false;
    }

    synchThreads = uint8( Tod::GetConfig().Get<int>( "LoginDatabase.SynchThreads", 1 ) );
    ///- Initialise the login database
    if ( !LoginDatabase.Open( dbString, asyncThreads, synchThreads, pathToBase ) )
    {
        TC_LOG_ERROR( "server.worldserver", "Cannot connect to login database %s", dbString.c_str() );
        return false;
    }

    ///- Get the realm Id from the configuration file
    realmID = Tod::GetConfig().Get<int>( "RealmID", 0 );
    if ( !realmID )
    {
        TC_LOG_ERROR( "server.worldserver", "Realm ID not defined in configuration file" );
        return false;
    }

    TC_LOG_INFO( "server.worldserver", "Realm running as realm ID %d", realmID );

    ///- Clean the database before starting
    ClearOnlineAccounts();

    ///- Insert version info into DB
    WorldDatabase.PExecute( "UPDATE version SET core_version = '%s', core_revision = '%s'", _FULLVERSION, _HASH );        // One-time query

    sWorld->LoadDBVersion();

    TC_LOG_INFO( "server.worldserver", "Using World DB: %s", sWorld->GetDBVersion() );
    return true;
}

void Master::_StopDB()
{
    CharacterDatabase.Close();
    WorldDatabase.Close();
    LoginDatabase.Close();

    MySQL::Library_End();
}

/// Clear 'online' status for all accounts with characters in this realm
void Master::ClearOnlineAccounts()
{
    // Reset online status for all accounts with characters on the current realm
    LoginDatabase.DirectPExecute( "UPDATE account SET online = 0 WHERE online > 0 AND id IN (SELECT acctid FROM realmcharacters WHERE realmid = %d)", realmID );

    // Reset online status for all characters
    CharacterDatabase.DirectExecute( "UPDATE characters SET online = 0 WHERE online <> 0" );

    // Battleground instance ids reset at server restart
    CharacterDatabase.DirectExecute( "UPDATE character_battleground_data SET instanceId = 0" );
}
