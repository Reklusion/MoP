/*
 * Copyright (C) 2017-2017 Project Aurora
 * Copyright (C) 2008-2015 TrinityCore <http://www.trinitycore.org/>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * Scripts for spells with SPELLFAMILY_PRIEST and SPELLFAMILY_GENERIC spells used by priest players.
 * Ordered alphabetically using scriptname.
 * Scriptnames of files in this file should be prefixed with "spell_pri_".
 */

#include "Player.h"
#include "ScriptMgr.h"
#include "SpellScript.h"
#include "SpellAuraEffects.h"
#include "GridNotifiers.h"
#include "SpellHistory.h"
#include "EventProcessor.h"

enum PriestSpells
{
    SPELL_PRIEST_MANA_LEECH_PROC_SHADOWFIEND            = 34650,
    SPELL_PRIEST_MANA_LEECH_AURA_MINDBENDER             = 123050,
    SPELL_PRIEST_MANA_LEECH_PROC_MINDBENDER             = 123051,
    SPELL_PRIEST_GLYPH_OF_REFLECTIVE_SHIELD             = 33202,
    SPELL_PRIEST_REFLECTIVE_SHIELD_DAMAGE               = 33619,
    SPELL_PRIEST_GLYPH_OF_POWER_WORD_SHIELD             = 55672,
    SPELL_PRIEST_GLYPH_OF_POWER_WORD_SHIELD_HEAL        = 56160,
    SPELL_PRIEST_GLYPH_OF_BINDING_HEAL                  = 63248,
    SPELL_PRIEST_GLYPH_OF_MASS_DISPEL                   = 55691,
    SPELL_PRIEST_MASS_DISPEL_MECHANIC_IMMUNE_SHIELD     = 39897,
    SPELL_PRIEST_PRAYER_OF_MENDING_HEAL                 = 33110,
    SPELL_PRIEST_GLYPH_OF_PSYCHIC_SCREAM                = 55676,
    SPELL_PRIEST_RAPID_RENEWAL                          = 95649,
    SPELL_PRIEST_RAPID_RENEWAL_HEAL                     = 63544,
    SPELL_PRIEST_SHADOW_WORD_DEATH_CASTER_DAMAGE        = 32409,
    SPELL_PRIEST_IMPROVED_SHADOW_WORD_DEATH             = 44298,
    SPELL_PRIEST_T13_SHADOW_2P                          = 105843,
    SPELL_PRIEST_SHADOW_WORD_DEATH_SHADOW_ORB           = 125927,
    SPELL_PRIEST_SHADOW_WORD_DEATH_EXTRA_COOLDOWN       = 95652,
    SPELL_PRIEST_EVANGELISM_PROC                        = 81661,
    SPELL_PRIEST_SHADOWY_APPARITION_PROC                = 147193,
    SPELL_PRIEST_SHADOW_ORBS_VISUAL                     = 77487,
    SPELL_PRIEST_DEVOURING_PLAGUE_HEAL                  = 127626,
    SPELL_PRIEST_SOLACE_AND_INSANITY                    = 139139,
    SPELL_PRIEST_DEVOURING_PLAGUE                       = 2944,
    SPELL_PRIEST_EMPOWERED_SHADOWS                      = 145180,
    SPELL_PRIEST_DARK_FLAMES                            = 99158,
    SPELL_PRIEST_SHADOW_WORD_PAIN                       = 589,
    SPELL_PRIEST_VAMPIRIC_TOUCH                         = 34914,
    SPELL_PRIEST_SHADOWFIEND_SUMMON                     = 34433,
    SPELL_PRIEST_SURGE_OF_DARKNESS                      = 87160,
    SPELL_PRIEST_SURGE_OF_LIGHT                         = 114255,
    SPELL_PRIEST_VOID_SHIFT_CHANGE                      = 118594,
    SPELL_PRIEST_GLYPH_OF_SHADOW                        = 107906,
    SPELL_PRIEST_GLYPH_OF_SHADOWY_FRIENDS               = 126745,
    SPELL_PRIEST_SHADOWFORM_VISUAL_WITHOUT_GLYPH        = 107903,
    SPELL_PRIEST_SHADOWFORM_VISUAL_WITH_GLYPH           = 107904,
    SPELL_PRIEST_VAMPIRIC_EMBRACE_HEAL                  = 15290,
    SPELL_PRIEST_SHADOW_WORD_PAIN_MASTERY               = 124464,
    SPELL_PRIEST_DEVOURING_PLAGUE_MASTERY               = 124467,
    SPELL_PRIEST_MIND_FLAY_MASTERY                      = 124468,
    SPELL_PRIEST_VAMPIRIC_TOUCH_MASTERY                 = 124465,
    SPELL_PRIEST_MIND_SEAR_MASTERY                      = 124469,
    SPELL_PRIEST_RAPTURE_PASSIVE                        = 47536,
    SPELL_PRIEST_RAPTURE_ENERGIZE                       = 47755,
    SPELL_PRIEST_RAPTURE_COOLDOWN                       = 63853,
    SPELL_PRIEST_ATONEMENT_HEAL                         = 81751,
    SPELL_PRIEST_DIVINE_AEGIS_PASSIVE                   = 47515,
    SPELL_PRIEST_DIVINE_AEGIS_ABSORB                    = 47753,
    SPELL_PRIEST_INNER_FOCUS                            = 89485,
    SPELL_PRIEST_PENANCE                                = 47540,
    SPELL_PRIEST_PENANCE_DAMAGE                         = 47758,
    SPELL_PRIEST_PENANCE_HEAL                           = 47757,
    SPELL_PRIEST_MASTERY_SHIELD_DISCIPLINE              = 77484,
    SPELL_PRIEST_WEAKENED_SOUL                          = 6788,
    SPELL_PRIEST_LIGHTWELL_RENEW                        = 126154,
    SPELL_PRIEST_LIGHTWELL_CHARGES                      = 126150,
    SPELL_PRIEST_ECHO_OF_LIGHT                          = 77489,
    SPELL_PRIEST_SPIRIT_OF_REDEMPTION_SHAPESHIFT        = 27827,
    SPELL_PRIEST_HOLY_WORD_SANCTUARY_AREA               = 88685,
    SPELL_PRIEST_HOLY_WORD_SANCTUARY_HEAL               = 88686,
    SPELL_PRIEST_HOLY_WORD_CHASTISE                     = 88625,
    SPELL_PRIEST_MIND_FLAY_INSANITY                     = 129197,
    SPELL_PRIEST_GLYPH_MIND_FLAY                        = 120585,
    SPELL_PRIEST_GLYPH_MIND_FLAY_SPEED_BUFF             = 120587,
    SPELL_PRIEST_HOLY_NOVA                              = 132157,
    SPELL_PRIEST_HOLY_NOVA_HEAL                         = 23455,
    SPELL_PRIEST_GLYPH_OF_CIRCLE_OF_HEALING             = 55675,
    SPELL_PRIEST_GLYPH_OF_PURIFY                        = 55677,
    SPELL_PRIEST_GLYPH_OF_PURIFY_HEAL                   = 56131,
    SPELL_PRIEST_GLYPH_OF_DISPEL_MAGIC                  = 119864,
    SPELL_PRIEST_GLYPH_OF_DISPEL_MAGIC_DAMAGE           = 119856,
    SPELL_PRIEST_SPELL_WARDING                          = 91724,
    SPELL_PRIEST_GLYPH_OF_INNER_SANCTUM                 = 14771,
    SPELL_PRIEST_LEAP_OF_FAITH_EFFECT                   = 92832,
    SPELL_PRIEST_GLYPH_OF_LEAP_OF_FAITH                 = 119850,
    SPELL_PRIEST_LEVITATE_EFFECT                        = 111759,
    SPELL_PRIEST_GLYPH_OF_LEVITATE                      = 108939,
    SPELL_PRIEST_PATH_OF_THE_DEVOUT                     = 111757,
    SPELL_PRIEST_GLYPH_OF_SMITE                         = 55692,
    SPELL_PRIEST_GLYPH_OF_THE_HEAVENS                   = 120581,
    SPELL_PRIEST_GLYPH_OF_THE_HEAVENS_EFFECT            = 124433,
    SPELL_PRIEST_GLYPH_OF_INSPIRED_HYMNS                = 147072,
    SPELL_PRIEST_GLYPH_OF_INSPIRED_HYMNS_EFFECT         = 147065,
    SPELL_PRIEST_CONFESSION                             = 126123,
    SPELL_PRIEST_BODY_AND_SOUL                          = 64129,
    SPELL_PRIEST_BODY_AND_SOUL_SPEED                    = 65081,
    SPELL_PRIEST_HALO_HOLY                              = 120517,
    SPELL_PRIEST_HALO_HOLY_VISUAL                       = 120630,
    SPELL_PRIEST_HALO_SHADOW_VISUAL                     = 120643,
    SPELL_PRIEST_SPECTRAL_GUISE_CHARGES                 = 119030,
    SPELL_PRIEST_SPECTRAL_GUISE_SUMMON                  = 112833,
    SPELL_PRIEST_VOID_TENDRILS_SUMMON                   = 127665,
    SPELL_PRIEST_VOID_TENDRIL_GRASP                     = 114404,
    SPELL_PRIEST_DIVINE_STAR_HOLY_TRIGGER               = 58880,
    SPELL_PRIEST_DIVINE_STAR_HOLY_AURA                  = 110744,
    SPELL_PRIEST_DIVINE_STAR_SHADOW_AURA                = 122121,
    SPELL_PRIEST_PSYFIEND_PSYCHIC_TERROR                = 113792,
    SPELL_PRIEST_PSYFIEND_PROC_AURA                     = 114164,
    SPELL_PRIEST_DIVINE_INSIGHT_TALENT                  = 109175,
    SPELL_PRIEST_DIVINE_INSIGHT_DISCIPLINE              = 123266,
    SPELL_PRIEST_DIVINE_INSIGHT_HOLY                    = 123267,
    SPELL_PRIEST_MIND_BLAST                             = 8092,
    SPELL_PRIEST_CASCADE_ABILITY_HOLY                   = 121135,
    SPELL_PRIEST_CASCADE_MISSILE_HEAL_HOLY              = 121146,
    SPELL_PRIEST_CASCADE_MISSILE_HEAL_SHADOW            = 127627,
    SPELL_PRIEST_CASCADE_MISSILE_DAMAGE_HOLY            = 120785,
    SPELL_PRIEST_CASCADE_MISSILE_DAMAGE_SHADOW          = 127628,
    SPELL_PRIEST_CASCADE_HEAL_HOLY                      = 121148,
    SPELL_PRIEST_CASCADE_HEAL_SHADOW                    = 127629,
    SPELL_PRIEST_CASCADE_COOLDOWN_HOLY                  = 120840,
    SPELL_PRIEST_CASCADE_COOLDOWN_SHADOW                = 127631,
    SPELL_PRIEST_CASCADE_SEARCHER_HOLY                  = 120786,
    SPELL_PRIEST_CASCADE_SEARCHER_SHADOW                = 127630,
    SPELL_PRIEST_ANGELIC_BULWARK_COOLDOWN               = 114216,
    SPELL_PRIEST_POWER_WORD_SOLACE_HEAL                 = 140816,
    SPELL_PRIEST_POWER_WORD_SOLACE_ENERGIZE             = 129253,
    SPELL_PRIEST_ITEM_EFFICIENCY                        = 37595,
    SPELL_PRIEST_BLESSED_HEALING                        = 70772,
    SPELL_PRIEST_T11_HOLY_4P                            = 89911,
    SPELL_PRIEST_CHAKRA_FLOW                            = 89912,
    SPELL_PRIEST_INDULGENECE_OF_THE_PENITENT            = 89913,
    SPELL_PRIEST_SHADOWFLAME                            = 99155,
    SPELL_PRIEST_SHADOWFLAME_DAMAGE                     = 99156,
    SPELL_PRIEST_T13_HOLY_4P                            = 105832,
    SPELL_PRIEST_T15_SHADOW_2P                          = 138156,
    SPELL_PRIEST_VAMPIRIC_TOUCH_ENERGIZE                = 34919,
    SPELL_PRIEST_DEVOUT_FAITH                           = 145327,
    SPELL_PRIEST_SERENDIPITY                            = 63735,
    SPELL_PRIEST_T16_HEALER_2P                          = 145306,
    SPELL_PRIEST_PIOUS_HEALER                           = 145330,
    SPELL_PRIEST_ABSOLUTION                             = 145336,
    SPELL_PRIEST_SIN_AND_PUNISHMENT                     = 131556,
    SPELL_PRIEST_GUARDIAN_SPIRIT_HEAL                   = 48153,
    SPELL_PRIEST_T9_HEALING_2P                          = 67201
};

enum PriestSpellIcons
{
    PRIEST_ICON_ID_BORROWED_TIME                    = 2899,
    PRIEST_ICON_ID_MIND_SEAR                        = 2895
};

enum PriestMiscData
{
    SPELL_VISUAL_SHADOWY_APPARITION                 = 33590
};

class PowerCheck
{
    public:
        explicit PowerCheck(Powers const power) : _power(power) { }

        bool operator()(WorldObject* obj) const
        {
            if (Unit* target = obj->ToUnit())
                return target->GetPowerType() != _power;

            return true;
        }

    private:
        Powers const _power;
};

class RaidCheck
{
    public:
        explicit RaidCheck(Unit const* caster) : _caster(caster) { }

        bool operator()(WorldObject* obj) const
        {
            if (Unit* target = obj->ToUnit())
                return !_caster->IsInRaidWith(target);

            return true;
        }

    private:
        Unit const* _caster;
};

class PlayerDistanceCheck
{
    public:
        PlayerDistanceCheck(Position const& pos) : i_pos(pos) { }

        bool operator()(WorldObject* unit) const
        {
            return unit->GetDistance2d(i_pos.GetPositionX(), i_pos.GetPositionY()) > 4.0f;
        }

    private:
        Position const i_pos;
};

#define MAX_CONFESSIONS 89

// These are entries from broadcast db
static uint32 const Confessions[MAX_CONFESSIONS] =
{
    64978, 64991, 65005, 65006, 65007,
    65008, 65009, 65010, 65011, 65012,
    65013, 65014, 65016, 65018, 65019,
    65020, 65021, 65022, 65023, 65024,
    65025, 65026, 65027, 65028, 65029,
    65030, 65031, 65032, 65033, 65034,
    65035, 65036, 65037, 65038, 65039,
    65040, 65041, 65042, 65044, 65045,
    65046, 74300, 74301, 74302, 74303,
    74304, 74305, 74306, 74307, 74308,
    76092, 76093, 76094, 76095, 76096,
    76097, 76098, 82236, 82237, 82238,
    82239, 82240, 82241, 82242, 82243,
    82244, 82245, 82246, 82247, 82248,
    82249, 82250, 82267, 122773, 122774,
    122775, 122776, 122777, 122778, 122779,
    122780, 122781, 122781, 122783, 122784,
    122785, 122786, 122788, 122789
};

// 64844 - Divine Hymn
class spell_pri_divine_hymn : public SpellScriptLoader
{
    public:
        spell_pri_divine_hymn() : SpellScriptLoader("spell_pri_divine_hymn") { }

        class spell_pri_divine_hymn_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_pri_divine_hymn_SpellScript);

            void FilterTargets(std::vector<WorldObject*>& targets)
            {
                Unit* caster = GetCaster();

                targets.erase(std::remove_if(targets.begin(), targets.end(), (RaidCheck(caster))), targets.end());

                uint32 maxTargets = caster->GetMap()->Is25ManRaid() ? 12 : 5;

                if (targets.size() > maxTargets)
                {
                    std::sort(targets.begin(), targets.end(), Trinity::HealthPctOrderPred());
                    targets.resize(maxTargets);
                }
            }

            void Register() override
            {
                OnObjectAreaTargetSelect += SpellObjectAreaTargetSelectFn(spell_pri_divine_hymn_SpellScript::FilterTargets, EFFECT_ALL, TARGET_UNIT_SRC_AREA_ALLY);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_pri_divine_hymn_SpellScript();
        }
};

// 47788 - Guardian Spirit
class spell_pri_guardian_spirit : public SpellScriptLoader
{
    public:
        spell_pri_guardian_spirit() : SpellScriptLoader("spell_pri_guardian_spirit") { }

        class spell_pri_guardian_spirit_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_pri_guardian_spirit_AuraScript);

        public:
            spell_pri_guardian_spirit_AuraScript()
            {
                healPct = 0;
            }

        private:
            uint32 healPct;

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                if (!sSpellMgr->GetSpellInfo(SPELL_PRIEST_GUARDIAN_SPIRIT_HEAL))
                    return false;
                return true;
            }

            bool Load() override
            {
                healPct = GetSpellInfo()->GetEffect(EFFECT_1)->CalcValue();
                return true;
            }

            void CalculateAmount(AuraEffect const* /*aurEff*/, int32 & amount, bool & /*canBeRecalculated*/)
            {
                // Set absorbtion amount to unlimited
                amount = -1;
            }

            void Absorb(AuraEffect* /*aurEff*/, DamageInfo & dmgInfo, uint32 & absorbAmount)
            {
                Unit* target = GetTarget();
                if (dmgInfo.GetDamage() < target->GetHealth())
                    return;

                int32 healAmount = int32(target->CountPctFromMaxHealth(healPct));
                // remove the aura now, we don't want 40% healing bonus
                Remove(AURA_REMOVE_BY_ENEMY_SPELL);
                target->CastCustomSpell(target, SPELL_PRIEST_GUARDIAN_SPIRIT_HEAL, &healAmount, NULL, NULL, true);
                absorbAmount = dmgInfo.GetDamage();
            }

            void Register() override
            {
                DoEffectCalcAmount += AuraEffectCalcAmountFn(spell_pri_guardian_spirit_AuraScript::CalculateAmount, EFFECT_1, SPELL_AURA_SCHOOL_ABSORB);
                OnEffectAbsorb += AuraEffectAbsorbFn(spell_pri_guardian_spirit_AuraScript::Absorb, EFFECT_1);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_pri_guardian_spirit_AuraScript();
        }
};

// 8129 - Mana Burn
class spell_pri_mana_burn : public SpellScriptLoader
{
    public:
        spell_pri_mana_burn() : SpellScriptLoader("spell_pri_mana_burn") { }

        class spell_pri_mana_burn_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_pri_mana_burn_SpellScript);

            void HandleAfterHit()
            {
                if (Unit* unitTarget = GetHitUnit())
                    unitTarget->RemoveAurasWithMechanic((1 << MECHANIC_FEAR) | (1 << MECHANIC_POLYMORPH));
            }

            void Register() override
            {
                AfterHit += SpellHitFn(spell_pri_mana_burn_SpellScript::HandleAfterHit);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_pri_mana_burn_SpellScript;
        }
};










// 28305 - Mana Leech (Passive)
// 123050 - Mana Leech(Passive)
class spell_pri_mana_leech : public SpellScriptLoader
{
    public:
        spell_pri_mana_leech() : SpellScriptLoader("spell_pri_mana_leech") { }

        class spell_pri_mana_leech_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_pri_mana_leech_AuraScript);

        public:
            spell_pri_mana_leech_AuraScript()
            {
                _procTarget = nullptr;
            }

        private:
            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                if (!sSpellMgr->GetSpellInfo(SPELL_PRIEST_MANA_LEECH_PROC_SHADOWFIEND))
                    return false;
                if (!sSpellMgr->GetSpellInfo(SPELL_PRIEST_MANA_LEECH_PROC_MINDBENDER))
                    return false;
                return true;
            }

            bool CheckProc(ProcEventInfo& /*eventInfo*/)
            {
                _procTarget = GetTarget()->GetOwner();
                return _procTarget != nullptr;
            }

            void HandleProc(AuraEffect const* aurEff, ProcEventInfo& /*eventInfo*/)
            {
                PreventDefaultAction();

                Unit* target = GetTarget();

                if (m_scriptSpellId == SPELL_PRIEST_MANA_LEECH_AURA_MINDBENDER)
                {
                    int32 mana = int32(_procTarget->CountPctFromMaxPower(POWER_MANA, 7) / 4);
                    target->EnergizeBySpell(_procTarget, SPELL_PRIEST_MANA_LEECH_PROC_MINDBENDER, mana, POWER_MANA);
                }
                else
                    target->CastSpell(_procTarget, SPELL_PRIEST_MANA_LEECH_PROC_SHADOWFIEND, true, nullptr, aurEff);
            }

            void Register() override
            {
                DoCheckProc += AuraCheckProcFn(spell_pri_mana_leech_AuraScript::CheckProc);
                OnEffectProc += AuraEffectProcFn(spell_pri_mana_leech_AuraScript::HandleProc, EFFECT_0, SPELL_AURA_DUMMY);
            }

        private:
            Unit* _procTarget;
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_pri_mana_leech_AuraScript();
        }
};

// 48045 - Mind Sear
class spell_pri_mind_sear : public SpellScriptLoader
{
    public:
        spell_pri_mind_sear() : SpellScriptLoader("spell_pri_mind_sear") { }

        class spell_pri_mind_sear_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_pri_mind_sear_SpellScript);

            SpellCastResult CheckCast()
            {
                Unit* caster = GetCaster();
                Unit* target = GetExplTargetUnit();

                if (!target || !caster || target->GetGUID() == caster->GetGUID() || target->GetCreatureType() == CREATURE_TYPE_NON_COMBAT_PET)
                    return SPELL_FAILED_BAD_TARGETS;

                return SPELL_CAST_OK;
            }

            void Register() override
            {
                OnCheckCast += SpellCheckCastFn(spell_pri_mind_sear_SpellScript::CheckCast);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_pri_mind_sear_SpellScript();
        }
};

// 17 - Power Word: Shield
class spell_pri_power_word_shield : public SpellScriptLoader
{
    public:
        spell_pri_power_word_shield() : SpellScriptLoader("spell_pri_power_word_shield") { }

        class spell_pri_power_word_shield_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_pri_power_word_shield_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                if (!sSpellMgr->GetSpellInfo(SPELL_PRIEST_GLYPH_OF_REFLECTIVE_SHIELD))
                    return false;
                if (!sSpellMgr->GetSpellInfo(SPELL_PRIEST_REFLECTIVE_SHIELD_DAMAGE))
                    return false;
                if (!sSpellMgr->GetSpellInfo(SPELL_PRIEST_GLYPH_OF_POWER_WORD_SHIELD))
                    return false;
                if (!sSpellMgr->GetSpellInfo(SPELL_PRIEST_GLYPH_OF_POWER_WORD_SHIELD_HEAL))
                    return false;
                if (!sSpellMgr->GetSpellInfo(SPELL_PRIEST_RAPTURE_PASSIVE))
                    return false;
                if (!sSpellMgr->GetSpellInfo(SPELL_PRIEST_RAPTURE_COOLDOWN))
                    return false;
                if (!sSpellMgr->GetSpellInfo(SPELL_PRIEST_RAPTURE_ENERGIZE))
                    return false;
                if (!sSpellMgr->GetSpellInfo(SPELL_PRIEST_BODY_AND_SOUL))
                    return false;
                if (!sSpellMgr->GetSpellInfo(SPELL_PRIEST_BODY_AND_SOUL_SPEED))
                    return false;
                if (!sSpellMgr->GetSpellInfo(SPELL_PRIEST_T13_HOLY_4P))
                    return false;
                return true;
            }

            bool Load() override
            {
                return GetCaster()->GetTypeId() == TYPEID_PLAYER;
            }

            void CalculateAmount(AuraEffect const* aurEff, int32& amount, bool& canBeRecalculated)
            {
                canBeRecalculated = false;

                if (Unit* caster = GetCaster())
                {
                    // +187% from sp bonus
                    float bonus = 1.871f;

                    // Borrowed Time
                    if (AuraEffect const* borrowedTime = caster->GetDummyAuraEffect(SPELLFAMILY_PRIEST, PRIEST_ICON_ID_BORROWED_TIME, EFFECT_1))
                        bonus += CalculatePct(1.0f, borrowedTime->GetAmount());

                    bonus *= caster->SpellBaseHealingBonusDone(GetSpellInfo()->GetSchoolMask());
                    bonus *= caster->CalculateLevelPenalty(GetSpellInfo());

                    amount += int32(bonus);

                    // Divine Aegis
                    if (AuraEffect const* borrowedTime = caster->GetAuraEffect(SPELL_PRIEST_DIVINE_AEGIS_PASSIVE, EFFECT_0))
                    {
                        float spellCrit = caster->GetFloatValue(PLAYER_SPELL_CRIT_PERCENTAGE);
                        if (Math::RollUnder(spellCrit))
                            amount *= 2;
                    }

                    // Item - Priest T13 Healer 4P Bonus (Holy Word and Power Word: Shield)
                    if (Aura const* bonusAura = caster->GetAura(SPELL_PRIEST_T13_HOLY_4P))
                    {
                        int32 chance = bonusAura->GetEffect(EFFECT_0)->GetAmount();
                        if (Math::RollUnder(chance))
                            AddPct(amount, bonusAura->GetEffect(EFFECT_1)->GetAmount());
                    }
                }
            }

            void ReflectDamage(AuraEffect* aurEff, DamageInfo& dmgInfo, uint32& absorbAmount)
            {
                Unit* caster = GetCaster();
                Unit* target = GetTarget();

                if (dmgInfo.GetAttacker() == target)
                    return;

                // Glyph of Reflective Shield
                if (AuraEffect const* talentAurEff = caster->GetAuraEffect(SPELL_PRIEST_GLYPH_OF_REFLECTIVE_SHIELD, EFFECT_0))
                {
                    int32 bp = CalculatePct(absorbAmount, talentAurEff->GetAmount());
                    target->CastCustomSpell(dmgInfo.GetAttacker(), SPELL_PRIEST_REFLECTIVE_SHIELD_DAMAGE, &bp, nullptr, nullptr, true, nullptr, aurEff);
                }

                // Glyph of Power Word: Shield
                if (AuraEffect const* talentAurEff = caster->GetAuraEffect(SPELL_PRIEST_GLYPH_OF_POWER_WORD_SHIELD, EFFECT_0))
                {
                    int32 bp = CalculatePct(absorbAmount, talentAurEff->GetAmount());
                    target->CastCustomSpell(target, SPELL_PRIEST_GLYPH_OF_POWER_WORD_SHIELD_HEAL, &bp, nullptr, nullptr, true, nullptr, aurEff);
                }

                // Rapture
                if (aurEff->GetAmount() >= int32(absorbAmount))
                    if (!caster->HasAura(SPELL_PRIEST_RAPTURE_COOLDOWN))
                        if (AuraEffect const* talentAurEff = caster->GetAuraEffect(SPELL_PRIEST_RAPTURE_PASSIVE, EFFECT_0))
                        {
                            caster->CastSpell(caster, SPELL_PRIEST_RAPTURE_COOLDOWN, true);

                            int32 bp = talentAurEff->GetAmount();

                            // Item - Priest T13 Healer 4P Bonus (Holy Word and Power Word: Shield)
                            if (AuraEffect const* bonusAurEff = caster->GetAuraEffect(SPELL_PRIEST_T13_HOLY_4P, EFFECT_1))
                                AddPct(bp, bonusAurEff->GetAmount());

                            caster->CastCustomSpell(caster, SPELL_PRIEST_RAPTURE_ENERGIZE, &bp, nullptr, nullptr, true, nullptr, aurEff);
                        }
            }

            void HandleDispel(DispelInfo* dispelInfo)
            {
                // Rapture
                if (Unit* caster = GetCaster())
                    if (!caster->HasAura(SPELL_PRIEST_RAPTURE_COOLDOWN))
                        if (AuraEffect const* talentAurEff = caster->GetAuraEffect(SPELL_PRIEST_RAPTURE_PASSIVE, EFFECT_0))
                        {
                            caster->CastSpell(caster, SPELL_PRIEST_RAPTURE_COOLDOWN, true);

                            int32 bp = talentAurEff->GetAmount();

                            // Item - Priest T13 Healer 4P Bonus (Holy Word and Power Word: Shield)
                            if (AuraEffect const* bonusAurEff = caster->GetAuraEffect(SPELL_PRIEST_T13_HOLY_4P, EFFECT_1))
                                AddPct(bp, bonusAurEff->GetAmount());

                            caster->CastCustomSpell(caster, SPELL_PRIEST_RAPTURE_ENERGIZE, &bp, nullptr, nullptr, true, nullptr, nullptr);
                        }
            }

            void OnApply(AuraEffect const* /*aurEff*/, AuraEffectHandleModes /*mode*/)
            {
                Unit* caster = GetCaster();
                Unit* target = GetTarget();

                if (!caster || !target)
                    return;

                if (caster->GetAura(SPELL_PRIEST_BODY_AND_SOUL))
                    caster->CastSpell(target, SPELL_PRIEST_BODY_AND_SOUL_SPEED, true);
            }

            void Register() override
            {
                DoEffectCalcAmount += AuraEffectCalcAmountFn(spell_pri_power_word_shield_AuraScript::CalculateAmount, EFFECT_0, SPELL_AURA_SCHOOL_ABSORB);
                AfterEffectAbsorb += AuraEffectAbsorbFn(spell_pri_power_word_shield_AuraScript::ReflectDamage, EFFECT_0);
                AfterDispel += AuraDispelFn(spell_pri_power_word_shield_AuraScript::HandleDispel);
                OnEffectApply += AuraEffectApplyFn(spell_pri_power_word_shield_AuraScript::OnApply, EFFECT_0, SPELL_AURA_SCHOOL_ABSORB, AURA_EFFECT_HANDLE_REAL);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_pri_power_word_shield_AuraScript();
        }
};

// 32546 - Binding Heal
class spell_pri_binding_heal : public SpellScriptLoader
{
    public:
        spell_pri_binding_heal() : SpellScriptLoader("spell_pri_binding_heal") { }

        class spell_pri_binding_heal_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_pri_binding_heal_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                if (!sSpellMgr->GetSpellInfo(SPELL_PRIEST_GLYPH_OF_BINDING_HEAL))
                    return false;
                return true;
            }

            void FilterTargets(std::vector<WorldObject*>& targets)
            {
                Unit* caster = GetCaster();
                if (!caster)
                    return;

                Unit* explicitTarget = GetExplTargetUnit();

                // first remove explicit target from container to properly select other alliance target (in case of having glyph)
                targets.erase(std::remove(targets.begin(), targets.end(), explicitTarget), targets.end());

                std::vector<WorldObject*> tempTargets;
                if (caster->GetAuraEffect(SPELL_PRIEST_GLYPH_OF_BINDING_HEAL, EFFECT_1))
                    tempTargets.push_back(Trinity::Containers::SelectRandomContainerElement(targets));

                // Add explicit target to list again
                tempTargets.push_back(explicitTarget);

                targets.clear();
                targets = std::move(tempTargets);
            }

            void Register() override
            {
                OnObjectAreaTargetSelect += SpellObjectAreaTargetSelectFn(spell_pri_binding_heal_SpellScript::FilterTargets, EFFECT_1, TARGET_UNIT_DEST_AREA_ALLY);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_pri_binding_heal_SpellScript();
        }
};

// 64904 - Hymn of Hope
class spell_pri_hymn_of_hope : public SpellScriptLoader
{
    public:
        spell_pri_hymn_of_hope() : SpellScriptLoader("spell_pri_hymn_of_hope") { }

        class spell_pri_hymn_of_hope_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_pri_hymn_of_hope_SpellScript);

            void FilterTargets(std::vector<WorldObject*>& targets)
            {
                targets.erase(std::remove_if(targets.begin(), targets.end(), PowerCheck(POWER_MANA)), targets.end());
                targets.erase(std::remove_if(targets.begin(), targets.end(), RaidCheck(GetCaster())), targets.end());

                uint32 maxTargets = 3;

                if (targets.size() > maxTargets)
                {
                    std::sort(targets.begin(), targets.end(), Trinity::PowerPctOrderPred(POWER_MANA));

                    // Patch 5.4.0: Now prefers to target healers when it grants mana. Healers will always be picked before non-healers, regardless of how much mana they have.
                    std::vector<WorldObject*> healers;
                    for (std::vector<WorldObject*>::iterator itr = targets.begin(); itr != targets.end();)
                    {
                        if (Player* player = (*itr)->ToPlayer())
                            if (ChrSpecializationEntry const* chrSpec = sChrSpecializationStore.LookupEntry(player->GetSpecId(player->GetActiveTalentGroup())))
                                if (chrSpec->Role == CHAR_SPECIALIZATION_ROLE_HEALER)
                                {
                                    itr = targets.erase(itr);
                                    healers.insert(healers.end(), *itr);
                                    continue;
                                }

                        ++itr;
                    }

                    if (healers.empty())
                        targets.resize(maxTargets);
                    else
                    {
                        if (healers.size() > maxTargets)
                            healers.resize(maxTargets);

                        targets.clear();
                        targets = std::move(healers);
                    }
                }
            }

            void Register() override
            {
                OnObjectAreaTargetSelect += SpellObjectAreaTargetSelectFn(spell_pri_hymn_of_hope_SpellScript::FilterTargets, EFFECT_ALL, TARGET_UNIT_SRC_AREA_ALLY);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_pri_hymn_of_hope_SpellScript();
        }
};

// 32592 - Mass Dispel
class spell_pri_mass_dispel : public SpellScriptLoader
{
    public:
        spell_pri_mass_dispel() : SpellScriptLoader("spell_pri_mass_dispel") { }

        class spell_pri_mass_dispel_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_pri_mass_dispel_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                if (!sSpellMgr->GetSpellInfo(SPELL_PRIEST_GLYPH_OF_MASS_DISPEL))
                    return false;
                if (!sSpellMgr->GetSpellInfo(SPELL_PRIEST_MASS_DISPEL_MECHANIC_IMMUNE_SHIELD))
                    return false;
                return true;
            }

            void HandleAfterCast()
            {
                Unit* caster = GetCaster();
                if (!caster)
                    return;

                if (caster->GetAuraEffect(SPELL_PRIEST_GLYPH_OF_MASS_DISPEL, EFFECT_0))
                {
                    WorldLocation const* loc = GetExplTargetDest();
                    if (!loc)
                        return;

                    float x, y, z;
                    loc->GetPosition(x, y, z);

                    caster->CastSpell(x, y, z, SPELL_PRIEST_MASS_DISPEL_MECHANIC_IMMUNE_SHIELD, true);
                }
            }

            void Register() override
            {
                AfterCast += SpellCastFn(spell_pri_mass_dispel_SpellScript::HandleAfterCast);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_pri_mass_dispel_SpellScript();
        }
};

// 37558 - Improved Prayer of Mending
class spell_pri_improved_prayer_of_mending : public SpellScriptLoader
{
    public:
        spell_pri_improved_prayer_of_mending() : SpellScriptLoader("spell_pri_improved_prayer_of_mending") { }

        class spell_pri_improved_prayer_of_mending_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_pri_improved_prayer_of_mending_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                if (!sSpellMgr->GetSpellInfo(SPELL_PRIEST_PRAYER_OF_MENDING_HEAL))
                    return false;
                return true;
            }

            void HandleProc(AuraEffect const* /*aurEff*/, ProcEventInfo& eventInfo)
            {
                PreventDefaultAction();
                GetTarget()->CastSpell(eventInfo.GetProcTarget(), SPELL_PRIEST_PRAYER_OF_MENDING_HEAL, true);
            }

            void Register() override
            {
                OnEffectProc += AuraEffectProcFn(spell_pri_improved_prayer_of_mending_AuraScript::HandleProc, EFFECT_0, SPELL_AURA_PROC_TRIGGER_SPELL);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_pri_improved_prayer_of_mending_AuraScript();
        }
};

// 33110 - Prayer of Mending Heal
class spell_pri_prayer_of_mending_heal : public SpellScriptLoader
{
    public:
        spell_pri_prayer_of_mending_heal() : SpellScriptLoader("spell_pri_prayer_of_mending_heal") { }

        class spell_pri_prayer_of_mending_heal_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_pri_prayer_of_mending_heal_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                if (!sSpellMgr->GetSpellInfo(SPELL_PRIEST_T9_HEALING_2P))
                    return false;
                return true;
            }

            void HandleHeal(SpellEffIndex /*effIndex*/)
            {
                Unit* target = GetHitUnit();

                if (Unit* caster = GetOriginalCaster())
                    if (AuraEffect* aurEff = caster->GetAuraEffect(SPELL_PRIEST_T9_HEALING_2P, EFFECT_0))
                    {
                        int32 heal = GetHitHeal();

                        AddPct(heal, aurEff->GetAmount());

                        SetHitHeal(heal);
                    }
            }

            void Register() override
            {
                OnEffectHitTarget += SpellEffectFn(spell_pri_prayer_of_mending_heal_SpellScript::HandleHeal, EFFECT_0, SPELL_EFFECT_HEAL);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_pri_prayer_of_mending_heal_SpellScript();
        }
};

#define PsyfiendProcAuraScriptName "spell_pri_psyfiend_proc_aura"

// 114164 - Psyfiend Hit Me Driver
class spell_pri_psyfiend_proc_aura : public SpellScriptLoader
{
    public:
        spell_pri_psyfiend_proc_aura() : SpellScriptLoader(PsyfiendProcAuraScriptName) { }

        class spell_pri_psyfiend_proc_aura_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_pri_psyfiend_proc_aura_AuraScript);

            bool Load() override
            {
                _attacker = nullptr;
                return true;
            }

            void HandleProc(AuraEffect const* /*aurEff*/, ProcEventInfo& eventInfo)
            {
                PreventDefaultAction();

                if (Unit* attacker = eventInfo.GetDamageInfo()->GetAttacker())
                    if (!attacker->HasAura(SPELL_PRIEST_PSYFIEND_PSYCHIC_TERROR))
                        _attacker = attacker;
            }

            void Register() override
            {
                OnEffectProc += AuraEffectProcFn(spell_pri_psyfiend_proc_aura_AuraScript::HandleProc, EFFECT_0, SPELL_AURA_DUMMY);
            }

        public:
            WorldObject* GetAttacker() const
            {
                return _attacker;
            }

            void ResetAttacker()
            {
                _attacker = nullptr;
            }

        private:
            WorldObject* _attacker;
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_pri_psyfiend_proc_aura_AuraScript();
        }
};

typedef spell_pri_psyfiend_proc_aura::spell_pri_psyfiend_proc_aura_AuraScript PsyfiendProcAuraScript;

// 8122 - Psychic Scream
// 113792 - Psychic Terror
class spell_pri_psychic_screams : public SpellScriptLoader
{
    public:
        spell_pri_psychic_screams() : SpellScriptLoader("spell_pri_psychic_screams") { }

        class spell_pri_psychic_screams_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_pri_psychic_screams_SpellScript);

            bool Load() override
            {
                _targets.clear();
                return true;
            }

            void FilterTargets(std::vector<WorldObject*>& targets)
            {
                _targets.clear();

                Unit* caster = GetCaster();
                if (!caster)
                {
                    targets.clear();
                    return;
                }

                TempSummon* summon = caster->ToTempSummon();
                if (!summon)
                {
                    targets.clear();
                    return;
                }

                if (Unit* summoner = summon->GetSummoner())
                    if (Aura* psyfiendProcAura = summoner->GetAura(SPELL_PRIEST_PSYFIEND_PROC_AURA))
                        if (PsyfiendProcAuraScript* script = dynamic_cast<PsyfiendProcAuraScript*>(psyfiendProcAura->GetScriptByName(PsyfiendProcAuraScriptName)))
                            if (WorldObject* attacker = script->GetAttacker())
                            {
                                if (std::find_if(targets.begin(), targets.end(), [attacker](WorldObject* worldobject)
                                {
                                    return worldobject->GetGUID() == attacker->GetGUID();
                                }) != targets.end())
                                {
                                    targets.clear();
                                    targets.insert(targets.begin(), attacker);

                                    _targets.insert(_targets.end(), targets.begin(), targets.end());

                                    script->ResetAttacker();

                                    return;
                                }
                            }

                targets.erase(std::remove_if(targets.begin(), targets.end(), Trinity::UnitAuraCheck(true, m_scriptSpellId)), targets.end());

                if (targets.size() > 1)
                    targets.resize(1);

                _targets.insert(_targets.end(), targets.begin(), targets.end());
            }

            void SetTargets(std::vector<WorldObject*>& targets)
            {
                targets.clear();
                targets.insert(_targets.end(), _targets.begin(), _targets.end());
            }

            void Register() override
            {
                if (m_scriptSpellId == SPELL_PRIEST_PSYFIEND_PSYCHIC_TERROR)
                {
                    OnObjectAreaTargetSelect += SpellObjectAreaTargetSelectFn(spell_pri_psychic_screams_SpellScript::FilterTargets, EFFECT_0, TARGET_UNIT_SRC_AREA_ENEMY);
                    OnObjectAreaTargetSelect += SpellObjectAreaTargetSelectFn(spell_pri_psychic_screams_SpellScript::SetTargets, EFFECT_1, TARGET_UNIT_SRC_AREA_ENEMY);
                    OnObjectAreaTargetSelect += SpellObjectAreaTargetSelectFn(spell_pri_psychic_screams_SpellScript::SetTargets, EFFECT_2, TARGET_UNIT_SRC_AREA_ENEMY);
                }
            }

        private:
            std::vector<WorldObject*> _targets;
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_pri_psychic_screams_SpellScript();
        }

        class spell_pri_psychic_screams_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_pri_psychic_screams_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                if (!sSpellMgr->GetSpellInfo(SPELL_PRIEST_GLYPH_OF_PSYCHIC_SCREAM))
                    return false;
                return true;
            }

            void OnApply(AuraEffect const* /*aurEff*/, AuraEffectHandleModes /*mode*/)
            {
                if (Unit* caster = GetCaster())
                    if (Unit* owner = caster->GetCharmerOrOwnerOrSelf())
                        if (!owner->GetAuraEffect(SPELL_PRIEST_GLYPH_OF_PSYCHIC_SCREAM, EFFECT_0))
                            PreventDefaultAction();
            }

            void Register() override
            {
                OnEffectApply += AuraEffectApplyFn(spell_pri_psychic_screams_AuraScript::OnApply, EFFECT_1, SPELL_AURA_MOD_ROOT, AURA_EFFECT_HANDLE_REAL);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_pri_psychic_screams_AuraScript();
        }
};

// 139 - Renew
class spell_pri_renew : public SpellScriptLoader
{
    public:
        spell_pri_renew() : SpellScriptLoader("spell_pri_renew") { }

        class spell_pri_renew_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_pri_renew_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                if (!sSpellMgr->GetSpellInfo(SPELL_PRIEST_RAPID_RENEWAL))
                    return false;
                return true;
            }

            bool Load() override
            {
                return GetCaster() && GetCaster()->GetTypeId() == TYPEID_PLAYER;
            }

            void HandleApplyEffect(AuraEffect const* aurEff, AuraEffectHandleModes /*mode*/)
            {
                if (Unit* caster = GetCaster())
                {
                    // Rapid Renewal
                    if (AuraEffect const* empoweredRenewAurEff = caster->GetAuraEffect(SPELL_PRIEST_RAPID_RENEWAL, EFFECT_2))
                    {
                        uint32 heal = caster->SpellHealingBonusDone(GetTarget(), GetSpellInfo(), aurEff->GetAmount(), DOT, aurEff->GetSpellEffectInfo());
                        heal = GetTarget()->SpellHealingBonusTaken(caster, GetSpellInfo(), heal, DOT, aurEff->GetSpellEffectInfo());
                        int32 basepoints0 = CalculatePct(int32(heal) * aurEff->GetTotalTicks(), empoweredRenewAurEff->GetAmount());
                        caster->CastCustomSpell(GetTarget(), SPELL_PRIEST_RAPID_RENEWAL_HEAL, &basepoints0, nullptr, nullptr, true, nullptr, aurEff);
                    }
                }
            }

            void Register() override
            {
                OnEffectApply += AuraEffectApplyFn(spell_pri_renew_AuraScript::HandleApplyEffect, EFFECT_0, SPELL_AURA_PERIODIC_HEAL, AURA_EFFECT_HANDLE_REAL_OR_REAPPLY_MASK);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_pri_renew_AuraScript();
        }
};

// 32379 - Shadow Word: Death
class spell_pri_shadow_word_death : public SpellScriptLoader
{
    public:
        spell_pri_shadow_word_death() : SpellScriptLoader("spell_pri_shadow_word_death") { }

        class spell_pri_shadow_word_death_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_pri_shadow_word_death_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                if (!sSpellMgr->GetSpellInfo(SPELL_PRIEST_SHADOW_WORD_DEATH_SHADOW_ORB))
                    return false;
                if (!sSpellMgr->GetSpellInfo(SPELL_PRIEST_SHADOW_WORD_DEATH_EXTRA_COOLDOWN))
                    return false;
                return true;
            }

            void HandleShadowOrb()
            {
                Unit* caster = GetCaster();
                if (!caster || !caster->IsAlive())
                    return;

                Player* player = caster->ToPlayer();
                if (!player)
                    return;

                if (player->GetSpecId(player->GetActiveTalentGroup()) != CHAR_SPECIALIZATION_PRIEST_SHADOW)
                    return;

                if (!player->HasAura(SPELL_PRIEST_SHADOW_WORD_DEATH_EXTRA_COOLDOWN))
                {
                    player->CastSpell(player, SPELL_PRIEST_SHADOW_WORD_DEATH_SHADOW_ORB, true);

                    Unit* target = GetHitUnit();
                    if (!target)
                        return;

                    if (target->IsAlive())
                    {
                        player->CastSpell(player, SPELL_PRIEST_SHADOW_WORD_DEATH_EXTRA_COOLDOWN, true);
                        player->GetSpellHistory()->ResetCooldown(GetSpellInfo()->Id, true);
                    }
                }
            }

            void Register() override
            {
                AfterHit += SpellHitFn(spell_pri_shadow_word_death_SpellScript::HandleShadowOrb);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_pri_shadow_word_death_SpellScript();
        }
};

// 129176 - Shadow Word: Death
class spell_pri_glyphed_shadow_word_death : public SpellScriptLoader
{
    public:
        spell_pri_glyphed_shadow_word_death() : SpellScriptLoader("spell_pri_glyphed_shadow_word_death") { }

        class spell_pri_glyphed_shadow_word_death_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_pri_glyphed_shadow_word_death_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                if (!sSpellMgr->GetSpellInfo(SPELL_PRIEST_SHADOW_WORD_DEATH_EXTRA_COOLDOWN))
                    return false;
                if (!sSpellMgr->GetSpellInfo(SPELL_PRIEST_SHADOW_WORD_DEATH_CASTER_DAMAGE))
                    return false;
                if (!sSpellMgr->GetSpellInfo(SPELL_PRIEST_SHADOW_WORD_DEATH_SHADOW_ORB))
                    return false;
                return true;
            }

            bool Load() override
            {
                _damageIncreased = false;
                return true;
            }

            void RecalculateDamage()
            {
                Unit* caster = GetCaster();
                Unit* target = GetHitUnit();

                if (!caster || !target)
                    return;

                if (target->HealthBelowPct(20))
                {
                    _damageIncreased = true;
                    SetHitDamage(GetHitDamage() * 4.0f);
                }
            }

            void HandleShadowOrb()
            {
                Unit* caster = GetCaster();
                if (!caster || !caster->IsAlive())
                    return;

                Player* player = caster->ToPlayer();
                if (!player)
                    return;

                Unit* target = GetHitUnit();
                if (!target)
                    return;

                bool IsShadowSpec = player->GetSpecId(player->GetActiveTalentGroup()) == CHAR_SPECIALIZATION_PRIEST_SHADOW;

                if (_damageIncreased && IsShadowSpec)
                    player->CastSpell(player, SPELL_PRIEST_SHADOW_WORD_DEATH_SHADOW_ORB, true);

                Aura* shadowOrbCooldownAura = player->GetAura(SPELL_PRIEST_SHADOW_WORD_DEATH_EXTRA_COOLDOWN);

                if (target->IsAlive())
                {
                    if (_damageIncreased && !shadowOrbCooldownAura)
                    {
                        player->CastSpell(player, SPELL_PRIEST_SHADOW_WORD_DEATH_EXTRA_COOLDOWN, true);
                        player->GetSpellHistory()->ResetCooldown(GetSpellInfo()->Id, true);
                    }

                    player->CastSpell(player, SPELL_PRIEST_SHADOW_WORD_DEATH_CASTER_DAMAGE, true);
                }
                else if (IsShadowSpec && (!shadowOrbCooldownAura || shadowOrbCooldownAura->GetDuration() <= 3000))
                    player->CastSpell(player, SPELL_PRIEST_SHADOW_WORD_DEATH_SHADOW_ORB, true);
            }

            void Register() override
            {
                OnHit += SpellHitFn(spell_pri_glyphed_shadow_word_death_SpellScript::RecalculateDamage);
                AfterHit += SpellHitFn(spell_pri_glyphed_shadow_word_death_SpellScript::HandleShadowOrb);
            }

        private:
            bool _damageIncreased;
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_pri_glyphed_shadow_word_death_SpellScript();
        }
};

// 32409 - Shadow Word: Death
class spell_pri_shadow_word_death_caster_damage : public SpellScriptLoader
{
    public:
        spell_pri_shadow_word_death_caster_damage() : SpellScriptLoader("spell_pri_shadow_word_death_caster_damage") { }

        class spell_pri_shadow_word_death_caster_damage_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_pri_shadow_word_death_caster_damage_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                if (!sSpellMgr->GetSpellInfo(SPELL_PRIEST_IMPROVED_SHADOW_WORD_DEATH))
                    return false;
                if (!sSpellMgr->GetSpellInfo(SPELL_PRIEST_T13_SHADOW_2P))
                    return false;
                return true;
            }

            void RecalculateDamage()
            {
                Unit* caster = GetCaster();
                if (!caster)
                    return;

                int32 multiplier = 1;

                // Priest T13 Shadow 2P Bonus
                if (AuraEffect const* aurEff = caster->GetAuraEffect(SPELL_PRIEST_T13_SHADOW_2P, EFFECT_1))
                    AddPct(multiplier, -aurEff->GetAmount());

                // Improved Shadow Word: Death
                if (AuraEffect const* aurEff = caster->GetAuraEffect(SPELL_PRIEST_IMPROVED_SHADOW_WORD_DEATH, EFFECT_0))
                    AddPct(multiplier, -aurEff->GetAmount());

                SetHitDamage(GetHitDamage() * multiplier);
            }

            void Register() override
            {
                OnHit += SpellHitFn(spell_pri_shadow_word_death_caster_damage_SpellScript::RecalculateDamage);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_pri_shadow_word_death_caster_damage_SpellScript();
        }
};

// 45243 - Focused Will
class spell_pri_focused_will : public SpellScriptLoader
{
    public:
        spell_pri_focused_will() : SpellScriptLoader("spell_pri_focused_will") { }

        class spell_pri_focused_will_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_pri_focused_will_AuraScript);

            bool CheckProc(ProcEventInfo& eventInfo)
            {
                DamageInfo* damageInfo = eventInfo.GetDamageInfo();
                if (!damageInfo || !damageInfo->GetDamage())
                    return false;

                int32 pct = GetSpellInfo()->GetEffect(EFFECT_0)->CalcValue();
                uint32 health = eventInfo.GetActionTarget()->CountPctFromMaxHealth(pct);

                if (damageInfo->GetDamage() <= health && (eventInfo.GetHitMask() & PROC_HIT_CRITICAL) == 0)
                    return false;

                return true;
            }

            void Register() override
            {
                DoCheckProc += AuraCheckProcFn(spell_pri_focused_will_AuraScript::CheckProc);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_pri_focused_will_AuraScript();
        }
};

// 81662 - Evangelism (Discipline, Holy, Passive)
class spell_pri_evangelism : public SpellScriptLoader
{
    public:
        spell_pri_evangelism() : SpellScriptLoader("spell_pri_evangelism") { }

        class spell_pri_evangelism_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_pri_evangelism_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                if (!sSpellMgr->GetSpellInfo(SPELL_PRIEST_EVANGELISM_PROC))
                    return false;
                return true;
            }

            void HandleEffectProc(AuraEffect const* /*aurEff*/, ProcEventInfo& /*eventInfo*/)
            {
                PreventDefaultAction();

                GetCaster()->CastSpell(GetCaster(), SPELL_PRIEST_EVANGELISM_PROC, true);
            }

            void Register() override
            {
                OnEffectProc += AuraEffectProcFn(spell_pri_evangelism_AuraScript::HandleEffectProc, EFFECT_0, SPELL_AURA_DUMMY);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_pri_evangelism_AuraScript();
        }
};

// 78203 - Shadowy Apparitions
class spell_pri_shadowy_apparition : public SpellScriptLoader
{
    public:
        spell_pri_shadowy_apparition() : SpellScriptLoader("spell_pri_shadowy_apparition") { }

        class spell_pri_shadowy_apparition_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_pri_shadowy_apparition_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                if (!sSpellMgr->GetSpellInfo(SPELL_PRIEST_SHADOWY_APPARITION_PROC))
                    return false;
                return true;
            }

            void HandleEffectProc(AuraEffect const* /*aurEff*/, ProcEventInfo& eventInfo)
            {
                PreventDefaultAction();

                if (Unit* target = eventInfo.GetActionTarget())
                    GetCaster()->CastSpell(target, SPELL_PRIEST_SHADOWY_APPARITION_PROC, true);
            }

            void Register() override
            {
                OnEffectProc += AuraEffectProcFn(spell_pri_shadowy_apparition_AuraScript::HandleEffectProc, EFFECT_0, SPELL_AURA_DUMMY);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_pri_shadowy_apparition_AuraScript();
        }
};

// 147193 - Shadowy Apparition
class spell_pri_shadowy_apparition_missile : public SpellScriptLoader
{
    public:
        spell_pri_shadowy_apparition_missile() : SpellScriptLoader("spell_pri_shadowy_apparition_missile") { }

        class spell_pri_shadowy_apparition_missile_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_pri_shadowy_apparition_missile_SpellScript);

            void HandleLaunch(SpellEffIndex /*effIndex*/)
            {
                if (Unit* caster = GetCaster())
                    if (Unit* target = GetHitUnit())
                        caster->SendPlaySpellVisual(target->GetGUID(), SPELL_VISUAL_SHADOWY_APPARITION, GetSpellInfo()->Speed);
            }

            void Register() override
            {
                OnEffectLaunchTarget += SpellEffectFn(spell_pri_shadowy_apparition_missile_SpellScript::HandleLaunch, EFFECT_0, SPELL_EFFECT_TRIGGER_MISSILE);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_pri_shadowy_apparition_missile_SpellScript();
        }
};

// 95740 - Shadow Orbs
class spell_pri_shadow_orbs : public SpellScriptLoader
{
    public:
        spell_pri_shadow_orbs() : SpellScriptLoader("spell_pri_shadow_orbs") { }

        class spell_pri_shadow_orbs_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_pri_shadow_orbs_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                if (!sSpellMgr->GetSpellInfo(SPELL_PRIEST_SHADOW_ORBS_VISUAL))
                    return false;
                return true;
            }

            bool Load() override
            {
                _shadowOrbsCount = 0;
                return true;
            }

            void PeriodicTick(AuraEffect const* /*aurEff*/)
            {
                if (Unit* caster = GetCaster())
                    if (Player* player = caster->ToPlayer())
                        if (player->GetSpecId(player->GetActiveTalentGroup()) == CHAR_SPECIALIZATION_PRIEST_SHADOW)
                        {
                            int32 shadowOrbs = player->GetPower(POWER_SHADOW_ORBS);
                            if (shadowOrbs == _shadowOrbsCount)
                                return;

                            if (shadowOrbs > _shadowOrbsCount)
                                player->CastSpell(player, SPELL_PRIEST_SHADOW_ORBS_VISUAL, true);
                            else
                            {
                                if (!shadowOrbs)
                                    player->RemoveAurasDueToSpell(SPELL_PRIEST_SHADOW_ORBS_VISUAL);
                                else
                                    player->RemoveAuraFromStack(SPELL_PRIEST_SHADOW_ORBS_VISUAL);
                            }

                            _shadowOrbsCount = shadowOrbs;
                        }
            }

            void Register() override
            {
                OnEffectPeriodic += AuraEffectPeriodicFn(spell_pri_shadow_orbs_AuraScript::PeriodicTick, EFFECT_1, SPELL_AURA_PERIODIC_DUMMY);
            }

        private:
            int32 _shadowOrbsCount;

        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_pri_shadow_orbs_AuraScript();
        }
};

// 2944 - Devouring Plague
class spell_pri_devouring_plague : public SpellScriptLoader
{
    public:
        spell_pri_devouring_plague() : SpellScriptLoader("spell_pri_devouring_plague") { }

        class spell_pri_devouring_plague_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_pri_devouring_plague_SpellScript);

            void CalculateDamage(SpellEffIndex /*eff*/)
            {
                if (Unit* caster = GetCaster())
                    if (Player* player = caster->ToPlayer())
                        if (player->GetSpecId(player->GetActiveTalentGroup()) == CHAR_SPECIALIZATION_PRIEST_SHADOW)
                        {
                            int32 damage = GetEffectValue();

                            // First shadow orb consumed by spell cast
                            int32 shadowOrbs = player->GetPower(POWER_SHADOW_ORBS) + 1;

                            SetEffectValue(damage * shadowOrbs);
                        }
            }

            void Register() override
            {
                OnEffectLaunchTarget += SpellEffectFn(spell_pri_devouring_plague_SpellScript::CalculateDamage, EFFECT_0, SPELL_EFFECT_SCHOOL_DAMAGE);
            }
        };

        SpellScript* GetSpellScript() const
        {
            return new spell_pri_devouring_plague_SpellScript;
        }

        class spell_pri_devouring_plague_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_pri_devouring_plague_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                if (!sSpellMgr->GetSpellInfo(SPELL_PRIEST_DEVOURING_PLAGUE_HEAL))
                    return false;
                return true;
            }

            bool Load() override
            {
                _shadowOrbsCount = 0;
                return true;
            }

            void RecalculateDamage(AuraEffect const* /*auraEffect*/, int32& amount, bool& canBeRecalculated)
            {
                canBeRecalculated = false;

                if (Unit* caster = GetCaster())
                    if (Player* player = caster->ToPlayer())
                    {
                        // First shadow orb consumed by spell cast
                        int32 shadowOrbs = player->GetPower(POWER_SHADOW_ORBS) + 1;

                        _shadowOrbsCount = shadowOrbs;
                        amount *= _shadowOrbsCount;

                        player->SetPower(POWER_SHADOW_ORBS, 0);
                    }
            }

            void RecalculateConsumedOrbs(AuraEffect const* /*auraEffect*/, int32& amount, bool& canBeRecalculated)
            {
                canBeRecalculated = false;

                amount = _shadowOrbsCount;
            }

            void OnTick(AuraEffect const *aurEff)
            {
                if (Unit* caster = GetCaster())
                    caster->CastCustomSpell(caster, SPELL_PRIEST_DEVOURING_PLAGUE_HEAL, &_shadowOrbsCount, nullptr, nullptr, true);
            }

            void Register() override
            {
                DoEffectCalcAmount += AuraEffectCalcAmountFn(spell_pri_devouring_plague_AuraScript::RecalculateDamage, EFFECT_1, SPELL_AURA_PERIODIC_DAMAGE);
                DoEffectCalcAmount += AuraEffectCalcAmountFn(spell_pri_devouring_plague_AuraScript::RecalculateConsumedOrbs, EFFECT_2, SPELL_AURA_DUMMY);
                OnEffectPeriodic += AuraEffectPeriodicFn(spell_pri_devouring_plague_AuraScript::OnTick, EFFECT_1, SPELL_AURA_PERIODIC_DAMAGE);
            }

        private:
            int32 _shadowOrbsCount;

        };

        AuraScript* GetAuraScript() const
        {
            return new spell_pri_devouring_plague_AuraScript();
        }
};

// 15407 Mind Flay
// 129197: Mind Flay (Insanity)
class spell_pri_mind_flay : public SpellScriptLoader
{
    public:
        spell_pri_mind_flay() : SpellScriptLoader("spell_pri_mind_flay") { }

        class spell_pri_mind_flay_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_pri_mind_flay_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                if (!sSpellMgr->GetSpellInfo(SPELL_PRIEST_SOLACE_AND_INSANITY))
                    return false;
                if (!sSpellMgr->GetSpellInfo(SPELL_PRIEST_DEVOURING_PLAGUE))
                    return false;
                return true;
            }

            bool Load() override
            {
                _haveGlygh = false;
                return true;
            }

            void RecalculateDamage(AuraEffect const* auraEffect, int32& amount, bool& /*canBeRecalculated*/)
            {
                if (m_scriptSpellId == SPELL_PRIEST_MIND_FLAY_INSANITY)
                    if (Unit* caster = GetCaster())
                        if (Player* player = caster->ToPlayer())
                            if (player->GetSpecId(player->GetActiveTalentGroup()) == CHAR_SPECIALIZATION_PRIEST_SHADOW)
                                if (player->HasSpell(SPELL_PRIEST_SOLACE_AND_INSANITY))
                                    if (Unit* unit = auraEffect->GetBase()->GetUnitOwner())
                                        if (AuraEffect const* aurEff = unit->GetAuraEffect(SPELL_PRIEST_DEVOURING_PLAGUE, EFFECT_2, caster->GetGUID()))
                                            AddPct(amount, aurEff->GetAmount() * 33.0f); // Damage from your Mind Flay is increased by 33% per orb consumed
            }

            void RecalculateSpeed(AuraEffect const* /*auraEffect*/, int32& amount, bool& /*canBeRecalculated*/)
            {
                if (Unit* caster = GetCaster())
                    if (_haveGlygh = caster->HasAura(SPELL_PRIEST_GLYPH_MIND_FLAY))
                        amount = 0;
            }

            void OnTick(AuraEffect const* /*aurEff*/)
            {
                if (Unit* caster = GetCaster())
                    if (_haveGlygh)
                        caster->CastSpell(caster, SPELL_PRIEST_GLYPH_MIND_FLAY_SPEED_BUFF, true);
            }

            void Register() override
            {
                DoEffectCalcAmount += AuraEffectCalcAmountFn(spell_pri_mind_flay_AuraScript::RecalculateDamage, EFFECT_0, SPELL_AURA_PERIODIC_DAMAGE);
                DoEffectCalcAmount += AuraEffectCalcAmountFn(spell_pri_mind_flay_AuraScript::RecalculateSpeed, EFFECT_1, SPELL_AURA_MOD_DECREASE_SPEED);
                OnEffectPeriodic += AuraEffectPeriodicFn(spell_pri_mind_flay_AuraScript::OnTick, EFFECT_0, SPELL_AURA_PERIODIC_DAMAGE);
            }

            bool _haveGlygh;
        };

        AuraScript* GetAuraScript() const
        {
            return new spell_pri_mind_flay_AuraScript();
        }
};

// 58228 - Glyph of Dark Archangel (Shadow)
class spell_pri_glyph_of_dark_archangel : public SpellScriptLoader
{
    public:
        spell_pri_glyph_of_dark_archangel() : SpellScriptLoader("spell_pri_glyph_of_dark_archangel") { }

        class spell_pri_glyph_of_dark_archangel_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_pri_glyph_of_dark_archangel_AuraScript);

            bool CheckProc(ProcEventInfo& eventInfo)
            {
                DamageInfo* damageInfo = eventInfo.GetDamageInfo();
                if (!damageInfo || !damageInfo->GetDamage())
                    return false;

                SpellInfo const* spellInfo = damageInfo->GetSpellInfo();
                if (!spellInfo)
                    return false;

                if (spellInfo->SpellFamilyName != SPELLFAMILY_PRIEST)
                    return false;

                // Devouring Plague
                if ((spellInfo->SpellFamilyFlags[3] & 0x00000010) == 0)
                    return false;

                return true;
            }

            void Register() override
            {
                DoCheckProc += AuraCheckProcFn(spell_pri_glyph_of_dark_archangel_AuraScript::CheckProc);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_pri_glyph_of_dark_archangel_AuraScript();
        }
};

// 145179 - Priest T16 Shadow 4P Bonus
class spell_pri_itemset_t16_4p : public SpellScriptLoader
{
    public:
        spell_pri_itemset_t16_4p() : SpellScriptLoader("spell_pri_itemset_t16_4p") { }

        class spell_pri_itemset_t16_4p_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_pri_itemset_t16_4p_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                if (!sSpellMgr->GetSpellInfo(SPELL_PRIEST_DEVOURING_PLAGUE))
                    return false;
                if (!sSpellMgr->GetSpellInfo(SPELL_PRIEST_EMPOWERED_SHADOWS))
                    return false;
                return true;
            }

            bool CheckProc(ProcEventInfo& eventInfo)
            {
                DamageInfo* damageInfo = eventInfo.GetDamageInfo();
                if (!damageInfo || !damageInfo->GetDamage())
                    return false;

                SpellInfo const* spellInfo = damageInfo->GetSpellInfo();
                if (!spellInfo)
                    return false;

                if (spellInfo->SpellFamilyName != SPELLFAMILY_PRIEST)
                    return false;

                // Devouring Plague
                if ((spellInfo->SpellFamilyFlags[3] & 0x00000010) == 0)
                    return false;

                return true;
            }

            void HandleProc(AuraEffect const* aurEff, ProcEventInfo& eventInfo)
            {
                PreventDefaultAction();

                if (Unit* caster = GetCaster())
                    if (Unit* target = eventInfo.GetActionTarget())
                        if (AuraEffect const* devouringPlague = target->GetAuraEffect(SPELL_PRIEST_DEVOURING_PLAGUE, EFFECT_2))
                        {
                            int32 baseAmount = aurEff->GetAmount();
                            baseAmount *= devouringPlague->GetAmount();

                            caster->CastCustomSpell(caster, SPELL_PRIEST_EMPOWERED_SHADOWS, &baseAmount, nullptr, nullptr, true);
                        }
            }

            void Register() override
            {
                DoCheckProc += AuraCheckProcFn(spell_pri_itemset_t16_4p_AuraScript::CheckProc);
                OnEffectProc += AuraEffectProcFn(spell_pri_itemset_t16_4p_AuraScript::HandleProc, EFFECT_0, SPELL_AURA_PROC_TRIGGER_SPELL);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_pri_itemset_t16_4p_AuraScript();
        }
};

// 99157 - Item - Priest T12 Shadow 4P Bonus
class spell_pri_itemset_t12_4p : public SpellScriptLoader
{
    public:
        spell_pri_itemset_t12_4p() : SpellScriptLoader("spell_pri_itemset_t12_4p") { }

        class spell_pri_itemset_t12_4p_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_pri_itemset_t12_4p_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                if (!sSpellMgr->GetSpellInfo(SPELL_PRIEST_DARK_FLAMES))
                    return false;

                return true;
            }

            bool CheckProc(ProcEventInfo& eventInfo)
            {
                if (DamageInfo* damageInfo = eventInfo.GetDamageInfo())
                    if (SpellInfo const* spellInfo = damageInfo->GetSpellInfo())
                        if (spellInfo->SpellFamilyName == SPELLFAMILY_PRIEST)
                        {
                            Unit* caster = eventInfo.GetActor();
                            Unit* target = eventInfo.GetActionTarget();

                            switch (spellInfo->Id)
                            {
                                case SPELL_PRIEST_SHADOW_WORD_PAIN:
                                    if (target->HasAura(SPELL_PRIEST_VAMPIRIC_TOUCH, caster->GetGUID()))
                                        return true;
                                    break;
                                case SPELL_PRIEST_VAMPIRIC_TOUCH:
                                    if (target->HasAura(SPELL_PRIEST_SHADOW_WORD_PAIN, caster->GetGUID()))
                                        return true;
                                    break;
                                default:
                                    return false;
                            }
                        }

                return false;
            }

            void HandleProc(AuraEffect const* /*aurEff*/, ProcEventInfo& /*eventInfo*/)
            {
                PreventDefaultAction();

                if (Unit* caster = GetCaster())
                    caster->CastSpell(caster, SPELL_PRIEST_DARK_FLAMES, true);
            }

            void Register() override
            {
                DoCheckProc += AuraCheckProcFn(spell_pri_itemset_t12_4p_AuraScript::CheckProc);
                OnEffectProc += AuraEffectProcFn(spell_pri_itemset_t12_4p_AuraScript::HandleProc, EFFECT_0, SPELL_AURA_DUMMY);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_pri_itemset_t12_4p_AuraScript();
        }
};

// 87100 - Shadowfiend (Passive)
class spell_pri_shadowfiend_passive : public SpellScriptLoader
{
    public:
        spell_pri_shadowfiend_passive() : SpellScriptLoader("spell_pri_shadowfiend_passive") { }

        class spell_pri_shadowfiend_passive_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_pri_shadowfiend_passive_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                if (!sSpellMgr->GetSpellInfo(SPELL_PRIEST_SHADOWFIEND_SUMMON))
                    return false;

                return true;
            }

            void HandleProc(AuraEffect const* aurEff, ProcEventInfo& /*eventInfo*/)
            {
                PreventDefaultAction();

                if (Unit* caster = GetCaster())
                    if (Player* player = caster->ToPlayer())
                        player->GetSpellHistory()->ModifyCooldown(SPELL_PRIEST_SHADOWFIEND_SUMMON, -(aurEff->GetAmount() * IN_MILLISECONDS));
            }

            void Register() override
            {
                OnEffectProc += AuraEffectProcFn(spell_pri_shadowfiend_passive_AuraScript::HandleProc, EFFECT_0, SPELL_AURA_DUMMY);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_pri_shadowfiend_passive_AuraScript();
        }
};

// 73510 - Mind Spike
class spell_pri_mind_spike : public SpellScriptLoader
{
    public:
        spell_pri_mind_spike() : SpellScriptLoader("spell_pri_mind_spike") { }

        class spell_pri_mind_spike_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_pri_mind_spike_SpellScript);

            void CalculateDamage(SpellEffIndex /*eff*/)
            {
                if (Unit* caster = GetCaster())
                    if (Unit* target = GetHitUnit())
                    {
                        // Surge of Darkness
                        if (AuraEffect const* surgeOfDarkness = caster->GetAuraEffect(SPELL_PRIEST_SURGE_OF_DARKNESS, EFFECT_3))
                        {
                            int32 damage = GetEffectValue();
                            AddPct(damage, surgeOfDarkness->GetAmount());
                            SetEffectValue(damage);
                        }
                        else
                        {
                            // Mind Spike remove all DoT on the target's
                            target->RemoveAurasByType(SPELL_AURA_PERIODIC_DAMAGE, caster->GetGUID());
                        }
                    }
            }

            void OnHitTarget(SpellEffIndex effIndex)
            {
                if (Unit* caster = GetCaster())
                    if (Unit* target = GetHitUnit())
                    {
                        uint32 triggerSpellId = GetSpellInfo()->GetEffect(effIndex)->TriggerSpell;
                        caster->CastSpell(target, triggerSpellId, true);
                    }
            }

            void Register() override
            {
                OnEffectLaunchTarget += SpellEffectFn(spell_pri_mind_spike_SpellScript::CalculateDamage, EFFECT_0, SPELL_EFFECT_SCHOOL_DAMAGE);
                OnEffectHitTarget += SpellEffectFn(spell_pri_mind_spike_SpellScript::OnHitTarget, EFFECT_0, SPELL_EFFECT_SCHOOL_DAMAGE);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_pri_mind_spike_SpellScript();
        }
};

// 64044 - Psychic Horror
class spell_pri_psychic_horror : public SpellScriptLoader
{
    public:
        spell_pri_psychic_horror() : SpellScriptLoader("spell_pri_psychic_horror") { }

        class spell_pri_psychic_horror_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_pri_psychic_horror_AuraScript);

            void OnApply(AuraEffect const* /*aurEff*/, AuraEffectHandleModes /*mode*/)
            {
                if (Unit* caster = GetCaster())
                    if (Player* player = caster->ToPlayer())
                        if (player->GetSpecId(player->GetActiveTalentGroup()) == CHAR_SPECIALIZATION_PRIEST_SHADOW)
                        {
                            // First shadow orb consumed by spell cast
                            int32 shadowOrbs = player->GetPower(POWER_SHADOW_ORBS) + 1;

                            player->SetPower(POWER_SHADOW_ORBS, 0);

                            int32 maxDuration = GetMaxDuration();
                            int32 newDuration = maxDuration + shadowOrbs * IN_MILLISECONDS;

                            SetDuration(newDuration);

                            if (newDuration > maxDuration)
                                SetMaxDuration(newDuration);
                        }
            }

            void Register()
            {
                OnEffectApply += AuraEffectApplyFn(spell_pri_psychic_horror_AuraScript::OnApply, EFFECT_0, SPELL_AURA_MOD_STUN, AURA_EFFECT_HANDLE_REAL);
            }
        };

        AuraScript* GetAuraScript() const
        {
            return new spell_pri_psychic_horror_AuraScript();
        }
};

// 108968 - Void Shift
// 142723 - Void Shift
class spell_pri_void_shift : public SpellScriptLoader
{
    public:
        spell_pri_void_shift() : SpellScriptLoader("spell_pri_void_shift") { }

        class spell_pri_void_shift_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_pri_void_shift_SpellScript);

            bool Validate(SpellInfo const* /*spellEntry*/)
            {
                if (!sSpellMgr->GetSpellInfo(SPELL_PRIEST_VOID_SHIFT_CHANGE))
                    return false;
                return true;
            }

            SpellCastResult CheckTarget()
            {
                if (GetExplTargetUnit())
                    if (GetExplTargetUnit()->GetTypeId() != TYPEID_PLAYER)
                        return SPELL_FAILED_BAD_TARGETS;

                return SPELL_CAST_OK;
            }

            void HandleDummy(SpellEffIndex /*effIndex*/)
            {
                if (Unit* caster = GetCaster())
                    if (Unit* target = GetHitUnit())
                    {
                        float casterPct = caster->GetHealthPct();
                        float targetPct = target->GetHealthPct();

                        Unit* receiver = nullptr;
                        Unit* loser = nullptr;

                        int32 heal = 0;
                        int32 damage = 0;

                        if (casterPct < targetPct)
                        {
                            if (casterPct < 25.0f)
                                casterPct = 25.0f;

                            receiver = caster;
                            loser = target;

                            heal = int32(caster->CountPctFromMaxHealth(targetPct) - caster->GetHealth());
                            damage = int32(target->GetHealth() - target->CountPctFromMaxHealth(casterPct));
                        }
                        else if (casterPct > targetPct)
                        {
                            if (targetPct < 25.0f)
                                targetPct = 25.0f;

                            receiver = target;
                            loser = caster;

                            heal = int32(target->CountPctFromMaxHealth(casterPct) - target->GetHealth());
                            damage = int32(caster->GetHealth() - caster->CountPctFromMaxHealth(targetPct));
                        }

                        if (!heal && !damage)
                            return;

                        receiver->CastCustomSpell(loser, SPELL_PRIEST_VOID_SHIFT_CHANGE, &damage, &heal, nullptr, true);
                    }
            }

            void Register()
            {
                OnCheckCast += SpellCheckCastFn(spell_pri_void_shift_SpellScript::CheckTarget);
                OnEffectHitTarget += SpellEffectFn(spell_pri_void_shift_SpellScript::HandleDummy, EFFECT_0, SPELL_EFFECT_DUMMY);
            }
        };

        SpellScript* GetSpellScript() const
        {
            return new spell_pri_void_shift_SpellScript;
        }
};

// 15473 - Shadowform
class spell_pri_shadowform : public SpellScriptLoader
{
    public:
        spell_pri_shadowform() : SpellScriptLoader("spell_pri_shadowform") { }

        class spell_pri_shadowform_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_pri_shadowform_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                if (!sSpellMgr->GetSpellInfo(SPELL_PRIEST_SHADOWFORM_VISUAL_WITHOUT_GLYPH) ||
                    !sSpellMgr->GetSpellInfo(SPELL_PRIEST_SHADOWFORM_VISUAL_WITH_GLYPH))
                    return false;
                return true;
            }

            bool Load() override
            {
                _visualID = 0;
                return true;
            }

            void HandleEffectApply(AuraEffect const* /*aurEff*/, AuraEffectHandleModes /*mode*/)
            {
                Unit* target = GetTarget();
                if (!target)
                    return;

                _visualID = SPELL_PRIEST_SHADOWFORM_VISUAL_WITHOUT_GLYPH;
                if (target->HasAura(SPELL_PRIEST_GLYPH_OF_SHADOW))
                    _visualID = SPELL_PRIEST_SHADOWFORM_VISUAL_WITH_GLYPH;

                target->CastSpell(target, _visualID, true);

                if (target->HasAura(SPELL_PRIEST_GLYPH_OF_SHADOWY_FRIENDS))
                    for (Unit* controlled : target->m_Controlled)
                        if (controlled->GetGUID() == target->GetCritterGUID())
                            controlled->CastSpell(controlled, _visualID, true);
            }

            void HandleEffectRemove(AuraEffect const* /*aurEff*/, AuraEffectHandleModes /*mode*/)
            {
                Unit* target = GetTarget();
                if (!target)
                    return;

                target->RemoveAurasDueToSpell(_visualID);

                for (Unit* controlled : target->m_Controlled)
                {
                    controlled->RemoveAurasDueToSpell(SPELL_PRIEST_SHADOWFLAME);

                    if (target->HasAura(SPELL_PRIEST_GLYPH_OF_SHADOWY_FRIENDS))
                        if (controlled->GetGUID() == target->GetCritterGUID())
                            controlled->RemoveAurasDueToSpell(_visualID);
                }
            }

            void Register() override
            {
                AfterEffectApply += AuraEffectApplyFn(spell_pri_shadowform_AuraScript::HandleEffectApply, EFFECT_0, SPELL_AURA_MOD_SHAPESHIFT, AURA_EFFECT_HANDLE_REAL_OR_REAPPLY_MASK);
                AfterEffectRemove += AuraEffectRemoveFn(spell_pri_shadowform_AuraScript::HandleEffectRemove, EFFECT_0, SPELL_AURA_MOD_SHAPESHIFT, AURA_EFFECT_HANDLE_REAL_OR_REAPPLY_MASK);
            }

        private:
            uint32 _visualID;
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_pri_shadowform_AuraScript();
        }
};

// 15286 - Vampiric Embrace
class spell_pri_vampiric_embrace : public SpellScriptLoader
{
    public:
        spell_pri_vampiric_embrace() : SpellScriptLoader("spell_pri_vampiric_embrace") { }

        class spell_pri_vampiric_embrace_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_pri_vampiric_embrace_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                if (!sSpellMgr->GetSpellInfo(SPELL_PRIEST_VAMPIRIC_EMBRACE_HEAL))
                    return false;
                return true;
            }

            bool Load() override
            {
                _damage = 0;
                return true;
            }

            void HandleEffectProc(AuraEffect const* aurEff, ProcEventInfo& eventInfo)
            {
                PreventDefaultAction();

                Unit* caster = GetCaster();
                if (!caster)
                    return;

                _damage += int32(CalculatePct(eventInfo.GetDamageInfo()->GetDamage(), aurEff->GetAmount()));
            }

            void HandlePeriodicTick(AuraEffect const* aurEff)
            {
                PreventDefaultAction();

                Unit* caster = GetCaster();
                if (!caster)
                    return;

                if (!_damage)
                    return;

                caster->CastCustomSpell(caster, SPELL_PRIEST_VAMPIRIC_EMBRACE_HEAL, &_damage, nullptr, nullptr, true, nullptr, aurEff);

                _damage = 0;
            }

            void Register() override
            {
                OnEffectProc += AuraEffectProcFn(spell_pri_vampiric_embrace_AuraScript::HandleEffectProc, EFFECT_0, SPELL_AURA_PERIODIC_DUMMY);
                OnEffectPeriodic += AuraEffectPeriodicFn(spell_pri_vampiric_embrace_AuraScript::HandlePeriodicTick, EFFECT_0, SPELL_AURA_PERIODIC_DUMMY);
            }

        private:
            int32 _damage;
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_pri_vampiric_embrace_AuraScript();
        }
};

// 15290 - Vampiric Embrace (heal)
class spell_pri_vampiric_embrace_heal : public SpellScriptLoader
{
    public:
        spell_pri_vampiric_embrace_heal() : SpellScriptLoader("spell_pri_vampiric_embrace_heal") { }

        class spell_pri_vampiric_embrace_heal_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_pri_vampiric_embrace_heal_SpellScript);

            bool Load() override
            {
                _raidSize = 0;
                return true;
            }

            void FilterTargets(std::vector<WorldObject*>& unitList)
            {
                // temporary remove caster to properly limit allies
                unitList.erase(std::remove(unitList.begin(), unitList.end(), GetCaster()), unitList.end());

                uint32 limit = 6;
                if (GetCaster()->GetMap()->Is25ManRaid())
                    limit = 15;

                if (unitList.size() > limit)
                    Trinity::Containers::RandomResize(unitList, limit);

                // again put caster to the list
                unitList.push_back(GetCaster());

                _raidSize = unitList.size();
            }

            void HandleHeal(SpellEffIndex /*effIndex*/)
            {
                if (Unit* caster = GetOriginalCaster())
                {
                    int32 heal = GetHitHeal();

                    heal /= _raidSize;

                    SetHitHeal(heal);
                }
            }

            void Register() override
            {
                OnObjectAreaTargetSelect += SpellObjectAreaTargetSelectFn(spell_pri_vampiric_embrace_heal_SpellScript::FilterTargets, EFFECT_0, TARGET_UNIT_CASTER_AREA_RAID);
                OnEffectHitTarget += SpellEffectFn(spell_pri_vampiric_embrace_heal_SpellScript::HandleHeal, EFFECT_0, SPELL_EFFECT_HEAL);
            }

        private:
            uint32 _raidSize;
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_pri_vampiric_embrace_heal_SpellScript();
        }
};

// 77486 - Mastery: Shadowy Recall
class spell_pri_shadowy_recall_mastery : public SpellScriptLoader
{
    public:
        spell_pri_shadowy_recall_mastery() : SpellScriptLoader("spell_pri_shadowy_recall_mastery") { }

        class spell_pri_shadowy_recall_mastery_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_pri_shadowy_recall_mastery_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                if (!sSpellMgr->GetSpellInfo(SPELL_PRIEST_SHADOW_WORD_PAIN_MASTERY))
                    return false;
                if (!sSpellMgr->GetSpellInfo(SPELL_PRIEST_DEVOURING_PLAGUE_MASTERY))
                    return false;
                if (!sSpellMgr->GetSpellInfo(SPELL_PRIEST_MIND_FLAY_MASTERY))
                    return false;
                if (!sSpellMgr->GetSpellInfo(SPELL_PRIEST_VAMPIRIC_TOUCH_MASTERY))
                    return false;
                if (!sSpellMgr->GetSpellInfo(SPELL_PRIEST_MIND_SEAR_MASTERY))
                    return false;
                return true;
            }

            bool CheckProc(ProcEventInfo& eventInfo)
            {
                DamageInfo* damageInfo = eventInfo.GetDamageInfo();
                if (!damageInfo || !damageInfo->GetDamage())
                    return false;

                SpellInfo const* spellInfo = damageInfo->GetSpellInfo();
                if (!spellInfo)
                    return false;

                if ((spellInfo->SpellFamilyFlags[0] & 0x00008000) == 0 &&
                    (spellInfo->SpellFamilyFlags[1] & 0x00000400) == 0 &&
                    (spellInfo->SpellFamilyFlags[2] & 0x00000040) == 0 &&
                    (spellInfo->SpellFamilyFlags[3] & 0x00000010) == 0 &&
                    spellInfo->SpellIconID != PRIEST_ICON_ID_MIND_SEAR)
                    return false;

                return true;
            }

            void HandleEffectProc(AuraEffect const* aurEff, ProcEventInfo& eventInfo)
            {
                PreventDefaultAction();

                Unit* caster = GetCaster();
                if (!caster)
                    return;

                int32 chance = aurEff->GetAmount();
                if (!Math::RollUnder(chance))
                    return;

                DamageInfo* damageInfo = eventInfo.GetDamageInfo();
                SpellInfo const* spellInfo = eventInfo.GetDamageInfo()->GetSpellInfo();

                uint32 spellId = 0;

                if ((spellInfo->SpellFamilyFlags[0] & 0x00008000) != 0) // Shadow Word : Pain
                    spellId = SPELL_PRIEST_SHADOW_WORD_PAIN_MASTERY;
                else if ((spellInfo->SpellFamilyFlags[1] & 0x00000400) != 0) // Vampiric Touch
                    spellId = SPELL_PRIEST_VAMPIRIC_TOUCH_MASTERY;
                else if ((spellInfo->SpellFamilyFlags[2] & 0x00000040) != 0) // Mind Flay
                    spellId = SPELL_PRIEST_MIND_FLAY_MASTERY;
                else if ((spellInfo->SpellFamilyFlags[3] & 0x00000010) != 0) // Devouring Plague
                    spellId = SPELL_PRIEST_DEVOURING_PLAGUE_MASTERY;
                else if (spellInfo->SpellIconID == PRIEST_ICON_ID_MIND_SEAR) // Mind Sear
                    spellId = SPELL_PRIEST_MIND_SEAR_MASTERY;

                if (spellId)
                {
                    int32 damage = int32(damageInfo->GetDamage());
                    Unit* target = damageInfo->GetVictim();

                    caster->CastCustomSpell(target, spellId, &damage, nullptr, nullptr, true, nullptr, aurEff);
                }
            }

            void Register() override
            {
                DoCheckProc += AuraCheckProcFn(spell_pri_shadowy_recall_mastery_AuraScript::CheckProc);
                OnEffectProc += AuraEffectProcFn(spell_pri_shadowy_recall_mastery_AuraScript::HandleEffectProc, EFFECT_0, SPELL_AURA_DUMMY);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_pri_shadowy_recall_mastery_AuraScript();
        }
};

// 81700 - Archangel
class spell_pri_archangel : public SpellScriptLoader
{
    public:
        spell_pri_archangel() : SpellScriptLoader("spell_pri_archangel") { }

        class spell_pri_archangel_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_pri_archangel_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                if (!sSpellMgr->GetSpellInfo(SPELL_PRIEST_PIOUS_HEALER))
                    return false;
                return true;
            }

            void CalculateAmount(AuraEffect const* aurEff, int32& amount, bool& canBeRecalculated)
            {
                canBeRecalculated = false;

                if (Unit* caster = GetCaster())
                    if (Aura* aura = caster->GetAura(SPELL_PRIEST_EVANGELISM_PROC, caster->GetGUID()))
                    {
                        amount *= aura->GetStackAmount();
                        caster->RemoveAurasDueToSpell(SPELL_PRIEST_EVANGELISM_PROC);
                    }
            }

            void OnApply(AuraEffect const* /*aurEff*/, AuraEffectHandleModes /*mode*/)
            {
                Unit* caster = GetCaster();
                if (!caster)
                    return;

                if (caster->HasAura(SPELL_PRIEST_T16_HEALER_2P))
                    caster->CastSpell(caster, SPELL_PRIEST_PIOUS_HEALER, true);
            }

            void Register() override
            {
                DoEffectCalcAmount += AuraEffectCalcAmountFn(spell_pri_archangel_AuraScript::CalculateAmount, EFFECT_0, SPELL_AURA_MOD_HEALING_DONE_PERCENT);
                OnEffectApply += AuraEffectApplyFn(spell_pri_archangel_AuraScript::OnApply, EFFECT_0, SPELL_AURA_MOD_HEALING_DONE_PERCENT, AURA_EFFECT_HANDLE_REAL);
            }
        };

        AuraScript* GetAuraScript() const
        {
            return new spell_pri_archangel_AuraScript;
        }
};

// 81749 - Atonement
class spell_pri_atonement : public SpellScriptLoader
{
    public:
        spell_pri_atonement() : SpellScriptLoader("spell_pri_atonement") { }

        class spell_pri_atonement_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_pri_atonement_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                if (!sSpellMgr->GetSpellInfo(SPELL_PRIEST_ATONEMENT_HEAL))
                    return false;
                return true;
            }

            void HandleEffectProc(AuraEffect const* aurEff, ProcEventInfo& eventInfo)
            {
                PreventDefaultAction();

                Unit* caster = GetCaster();
                if (!caster)
                    return;

                DamageInfo* damageInfo = eventInfo.GetDamageInfo();
                if (!damageInfo || !damageInfo->GetDamage())
                    return;

                int32 basePct = aurEff->GetAmount();
                int32 heal = damageInfo->GetDamage() * basePct;

                caster->CastCustomSpell(damageInfo->GetVictim(), SPELL_PRIEST_ATONEMENT_HEAL, &heal, nullptr, nullptr, true, nullptr, aurEff);
            }

            void Register() override
            {
                OnEffectProc += AuraEffectProcFn(spell_pri_atonement_AuraScript::HandleEffectProc, EFFECT_0, SPELL_AURA_DUMMY);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_pri_atonement_AuraScript();
        }
};

// 81751 - Atonement
class spell_pri_atonement_heal : public SpellScriptLoader
{
    public:
        spell_pri_atonement_heal() : SpellScriptLoader("spell_pri_atonement_heal") { }

        class spell_pri_atonement_heal_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_pri_atonement_heal_SpellScript);

            bool Load() override
            {
                return GetCaster()->GetTypeId() == TYPEID_PLAYER;
            }

            void FilterTargets(std::vector<WorldObject*>& targets)
            {
                ObjectGuid casterGUID = GetCaster()->GetGUID();

                targets.erase(std::remove_if(targets.begin(), targets.end(), [&casterGUID](WorldObject* object)
                {
                    if (Unit* unit = object->ToUnit())
                    {
                        bool remove = unit->GetMaxHealth() <= unit->GetHealth();

                        if (remove && unit->GetGUID() == casterGUID)
                            casterGUID.Clear();

                        return remove;
                    }

                    return true;
                }), targets.end());

                if (targets.size() > 1)
                {
                    std::sort(targets.begin(), targets.end(), Trinity::HealthPctOrderPred());
                    targets.resize(1);
                }
            }

            void HandleHeal(SpellEffIndex /*effIndex*/)
            {
                Unit* caster = GetCaster();
                Unit* target = GetHitUnit();

                if (caster->GetGUID() == target->GetGUID())
                    SetHitHeal(GetHitHeal() / 2);
            }

            void Register() override
            {
                OnObjectAreaTargetSelect += SpellObjectAreaTargetSelectFn(spell_pri_atonement_heal_SpellScript::FilterTargets, EFFECT_0, TARGET_UNIT_DEST_AREA_ALLY);
                OnEffectHitTarget += SpellEffectFn(spell_pri_atonement_heal_SpellScript::HandleHeal, EFFECT_0, SPELL_EFFECT_HEAL);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_pri_atonement_heal_SpellScript();
        }
};

// 47515 - Divine Aegis (Passive)
class spell_pri_divine_aegis : public SpellScriptLoader
{
    public:
        spell_pri_divine_aegis() : SpellScriptLoader("spell_pri_divine_aegis") { }

        class spell_pri_divine_aegis_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_pri_divine_aegis_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                if (!sSpellMgr->GetSpellInfo(SPELL_PRIEST_DIVINE_AEGIS_ABSORB))
                    return false;
                return true;
            }

            bool CheckProc(ProcEventInfo& eventInfo)
            {
                HealInfo* healInfo = eventInfo.GetHealInfo();
                if (!healInfo || !healInfo->GetEffectiveHeal())
                    return false;

                SpellInfo const* spellInfo = healInfo->GetSpellInfo();
                if (!spellInfo)
                    return false;

                if (spellInfo->SpellFamilyName != SPELLFAMILY_PRIEST)
                    return false;

                if ((eventInfo.GetHitMask() & PROC_HIT_CRITICAL) == 0)
                    return false;

                if ((eventInfo.GetSpellTypeMask() & PROC_SPELL_TYPE_HEAL) == 0)
                    return false;

                return true;
            }

            void HandleEffectProc(AuraEffect const* aurEff, ProcEventInfo& eventInfo)
            {
                PreventDefaultAction();

                Unit* caster = GetCaster();
                if (!caster)
                    return;

                Unit* target = eventInfo.GetHealInfo()->GetTarget();

                int32 heal = int32(eventInfo.GetHealInfo()->GetEffectiveHeal());
                int32 maxHeal = caster->CountPctFromMaxHealth(40);

                if (AuraEffect* aegisEff = target->GetAuraEffect(SPELL_PRIEST_DIVINE_AEGIS_ABSORB, EFFECT_0, caster->GetGUID()))
                {
                    int32 currentAmount = aegisEff->GetAmount();
                    heal = std::min(heal + currentAmount, maxHeal);

                    aegisEff->ChangeAmount(heal);
                    aegisEff->GetBase()->RefreshDuration();
                }
                else
                    caster->CastCustomSpell(target, SPELL_PRIEST_DIVINE_AEGIS_ABSORB, &heal, nullptr, nullptr, true, nullptr, aurEff);
            }

            void Register() override
            {
                DoCheckProc += AuraCheckProcFn(spell_pri_divine_aegis_AuraScript::CheckProc);
                OnEffectProc += AuraEffectProcFn(spell_pri_divine_aegis_AuraScript::HandleEffectProc, EFFECT_0, SPELL_AURA_DUMMY);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_pri_divine_aegis_AuraScript();
        }
};

// 92297 - Train of Thought
class spell_pri_train_of_thought : public SpellScriptLoader
{
    public:
        spell_pri_train_of_thought() : SpellScriptLoader("spell_pri_train_of_thought") { }

        class spell_pri_train_of_thought_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_pri_train_of_thought_AuraScript);

            bool Load() override
            {
                return GetCaster()->GetTypeId() == TYPEID_PLAYER;
            }

            void HandleEffectProc(AuraEffect const* aurEff, ProcEventInfo& eventInfo)
            {
                PreventDefaultAction();

                Unit* caster = GetCaster();
                if (!caster)
                    return;

                uint32 spellId = 0;
                int32 cooldown = aurEff->GetAmount();

                switch (aurEff->GetEffIndex())
                {
                    case EFFECT_0:
                        if (HealInfo* healInfo = eventInfo.GetHealInfo())
                            if (healInfo->GetEffectiveHeal())
                            {
                                spellId = SPELL_PRIEST_INNER_FOCUS;
                                cooldown *= IN_MILLISECONDS;
                            }
                        break;
                    case EFFECT_2:
                        if (DamageInfo* damageInfo = eventInfo.GetDamageInfo())
                            if (damageInfo->GetDamage())
                                spellId = SPELL_PRIEST_PENANCE;
                        break;
                    default:
                        break;
                }

                if (spellId)
                    caster->ToPlayer()->GetSpellHistory()->ModifyCooldown(spellId, cooldown);
            }

            void Register() override
            {
                OnEffectProc += AuraEffectProcFn(spell_pri_train_of_thought_AuraScript::HandleEffectProc, EFFECT_0, SPELL_AURA_DUMMY);
                OnEffectProc += AuraEffectProcFn(spell_pri_train_of_thought_AuraScript::HandleEffectProc, EFFECT_2, SPELL_AURA_DUMMY);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_pri_train_of_thought_AuraScript();
        }
};

class spell_pri_strength_of_soul : public SpellScriptLoader
{
    public:
        spell_pri_strength_of_soul() : SpellScriptLoader("spell_pri_strength_of_soul") { }

        class spell_pri_strength_of_soul_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_pri_strength_of_soul_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                if (!sSpellMgr->GetSpellInfo(SPELL_PRIEST_WEAKENED_SOUL))
                    return false;
                return true;
            }

            void HandleScript(SpellEffIndex /*effIndex*/)
            {
                if (Unit* unit = GetHitUnit())
                    if (Aura* aura = unit->GetAura(SPELL_PRIEST_WEAKENED_SOUL))
                    {
                        int32 newDuration = aura->GetDuration() - (GetEffectValue() * IN_MILLISECONDS);

                        if (newDuration > 0)
                            aura->SetDuration(newDuration);
                        else
                            aura->Remove();
                    }
            }

            void Register() override
            {
                OnEffectHitTarget += SpellEffectFn(spell_pri_strength_of_soul_SpellScript::HandleScript, EFFECT_0, SPELL_EFFECT_SCRIPT_EFFECT);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_pri_strength_of_soul_SpellScript();
        }
};

// 126137 - Lightspring Trigger
class spell_pri_lightspring_trigger : public SpellScriptLoader
{
    public:
        spell_pri_lightspring_trigger() : SpellScriptLoader("spell_pri_lightspring_trigger") { }

        class spell_pri_lightspring_trigger_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_pri_lightspring_trigger_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                if (!sSpellMgr->GetSpellInfo(SPELL_PRIEST_LIGHTWELL_RENEW))
                    return false;
                return true;
            }

            bool Load() override
            {
                _consumedCharges = 0;
                return true;
            }

            void FilterTargets(std::vector<WorldObject*>& targets)
            {
                Unit* caster = GetCaster();

                targets.erase(std::remove_if(targets.begin(), targets.end(), [caster](WorldObject* target) -> bool
                {
                    if (Unit* unit = target->ToUnit())
                        return unit->GetHealthPct() > 50.0f || unit->GetAura(SPELL_PRIEST_LIGHTWELL_RENEW, caster->GetGUID());
                    return true;
                }), targets.end());
            }

            void HandleScript(SpellEffIndex /*effIndex*/)
            {
                Unit* caster = GetCaster();

                if (Unit* unit = GetHitUnit())
                {
                    caster->CastSpell(unit, SPELL_PRIEST_LIGHTWELL_RENEW, false);
                    ++_consumedCharges;
                }
            }

            void HandleAfterCast()
            {
                Unit* caster = GetCaster();

                if (_consumedCharges)
                    if (Aura* aura = caster->GetAura(SPELL_PRIEST_LIGHTWELL_CHARGES))
                        aura->ModStackAmount(-_consumedCharges);
            }

            void Register() override
            {
                OnObjectAreaTargetSelect += SpellObjectAreaTargetSelectFn(spell_pri_lightspring_trigger_SpellScript::FilterTargets, EFFECT_0, TARGET_UNIT_CASTER_AREA_RAID);
                OnEffectHitTarget += SpellEffectFn(spell_pri_lightspring_trigger_SpellScript::HandleScript, EFFECT_0, SPELL_EFFECT_DUMMY);
                AfterCast += SpellCastFn(spell_pri_lightspring_trigger_SpellScript::HandleAfterCast);
            }

        private:
            uint8 _consumedCharges;
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_pri_lightspring_trigger_SpellScript();
        }
};

// 59907, 126150 - Lightwell Charges
class spell_pri_lightwell_charges : public SpellScriptLoader
{
    public:
        spell_pri_lightwell_charges() : SpellScriptLoader("spell_pri_lightwell_charges") { }

        class spell_pri_lightwell_charges_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_pri_lightwell_charges_AuraScript);

            void HandleRemove(AuraEffect const* /*aurEff*/, AuraEffectHandleModes /*mode*/)
            {
                if (Unit* caster = GetCaster())
                    if (Creature* creature = caster->ToCreature())
                        creature->DespawnOrUnsummon();
            }

            void AfterProcEffect(AuraEffect const* /*aurEff*/, ProcEventInfo& /*eventInfo*/)
            {
                PreventDefaultAction();

                ModStackAmount(-1);
            }

            void Register() override
            {
                AfterEffectRemove += AuraEffectRemoveFn(spell_pri_lightwell_charges_AuraScript::HandleRemove, EFFECT_0, SPELL_AURA_DUMMY, AURA_EFFECT_HANDLE_REAL);
                AfterEffectProc += AuraEffectProcFn(spell_pri_lightwell_charges_AuraScript::AfterProcEffect, EFFECT_0, SPELL_AURA_DUMMY);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_pri_lightwell_charges_AuraScript();
        }
};

// 7001, 126154 - Lightwell Renew
class spell_pri_lightwell_renew : public SpellScriptLoader
{
    public:
        spell_pri_lightwell_renew() : SpellScriptLoader("spell_pri_lightwell_renew") { }

        class spell_pri_lightwell_renew_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_pri_lightwell_renew_AuraScript);

            void InitializeAmount(AuraEffect const* /*aurEff*/, AuraEffectHandleModes /*mode*/)
            {
                // Attacks done to you equal to 30% of your total health will cancel the effect
                _remainingAmount = GetTarget()->CountPctFromMaxHealth(30);
            }

            void HandleProc(AuraEffect const* /*aurEff*/, ProcEventInfo& eventInfo)
            {
                DamageInfo* damageInfo = eventInfo.GetDamageInfo();
                if (!damageInfo)
                    return;

                if (!_remainingAmount)
                    return;

                uint32 damage = damageInfo->GetDamage();
                if (_remainingAmount <= damage)
                {
                    DropCharge(AURA_REMOVE_BY_ENEMY_SPELL);
                    return;
                }

                _remainingAmount -= damage;
            }

            void Register() override
            {
                AfterEffectApply += AuraEffectApplyFn(spell_pri_lightwell_renew_AuraScript::InitializeAmount, EFFECT_0, SPELL_AURA_PERIODIC_HEAL, AURA_EFFECT_HANDLE_REAL_OR_REAPPLY_MASK);
                OnEffectProc += AuraEffectProcFn(spell_pri_lightwell_renew_AuraScript::HandleProc, EFFECT_0, SPELL_AURA_PERIODIC_HEAL);
            }

            uint32 _remainingAmount = 0;
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_pri_lightwell_renew_AuraScript();
        }
};

// 77485 - Mastery: Echo of Light
class spell_pri_mastery_echo_of_light : public SpellScriptLoader
{
    public:
        spell_pri_mastery_echo_of_light() : SpellScriptLoader("spell_pri_mastery_echo_of_light") { }

        class spell_pri_mastery_echo_of_light_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_pri_mastery_echo_of_light_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                if (!sSpellMgr->GetSpellInfo(SPELL_PRIEST_ECHO_OF_LIGHT))
                    return false;
                return true;
            }

            bool CheckProc(ProcEventInfo& eventInfo)
            {
                HealInfo* healInfo = eventInfo.GetHealInfo();
                if (!healInfo || !healInfo->GetEffectiveHeal())
                    return false;

                return true;
            }

            void HandleEffectProc(AuraEffect const* aurEff, ProcEventInfo& eventInfo)
            {
                PreventDefaultAction();

                Unit* caster = GetCaster();
                Unit* target = eventInfo.GetActionTarget();

                int32 baseAmount = eventInfo.GetHealInfo()->GetEffectiveHeal() / 6;
                AddPct(baseAmount, aurEff->GetAmount());
                baseAmount += target->GetRemainingPeriodicAmount(caster->GetGUID(), SPELL_PRIEST_ECHO_OF_LIGHT, SPELL_AURA_PERIODIC_HEAL);

                caster->CastCustomSpell(target, SPELL_PRIEST_ECHO_OF_LIGHT, &baseAmount, nullptr, nullptr, true, nullptr, aurEff);
            }

            void Register() override
            {
                DoCheckProc += AuraCheckProcFn(spell_pri_mastery_echo_of_light_AuraScript::CheckProc);
                OnEffectProc += AuraEffectProcFn(spell_pri_mastery_echo_of_light_AuraScript::HandleEffectProc, EFFECT_0, SPELL_AURA_DUMMY);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_pri_mastery_echo_of_light_AuraScript();
        }
};

// 20711 - Spirit of Redemption
class spell_pri_spirit_of_redemption : public SpellScriptLoader
{
    public:
        spell_pri_spirit_of_redemption() : SpellScriptLoader("spell_pri_spirit_of_redemption") { }

        class spell_pri_spirit_of_redemption_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_pri_spirit_of_redemption_AuraScript);

            void Absorb(AuraEffect* /*aurEff*/, DamageInfo& dmgInfo, uint32& absorbAmount)
            {
                absorbAmount = 0;

                if (Unit* caster = GetCaster())
                {
                    if (dmgInfo.GetDamage() < caster->GetHealth())
                        return;

                    if (caster->HasAuraType(SPELL_AURA_SPIRIT_OF_REDEMPTION))
                        return;

                    dmgInfo.GetAttacker()->Kill(caster, true, true);

                    caster->CastSpell(caster, SPELL_PRIEST_SPIRIT_OF_REDEMPTION_SHAPESHIFT, true);

                    int32 healthAbsorbed = caster->GetHealth() - 1;

                    absorbAmount = std::max(healthAbsorbed, 1);
                }
            }

            void Register() override
            {
                OnEffectAbsorb += AuraEffectAbsorbFn(spell_pri_spirit_of_redemption_AuraScript::Absorb, EFFECT_0);
            }
        };

        AuraScript* GetAuraScript() const
        {
            return new spell_pri_spirit_of_redemption_AuraScript();
        }
};

// 34861 - Circle of Healing
class spell_pri_circle_of_healing : public SpellScriptLoader
{
    public:
        spell_pri_circle_of_healing() : SpellScriptLoader("spell_pri_circle_of_healing") { }

        class spell_pri_circle_of_healing_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_pri_circle_of_healing_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                if (!sSpellMgr->GetSpellInfo(SPELL_PRIEST_GLYPH_OF_CIRCLE_OF_HEALING))
                    return false;
                return true;
            }

            void FilterTargets(std::vector<WorldObject*>& targets)
            {
                targets.erase(std::remove_if(targets.begin(), targets.end(), RaidCheck(GetCaster())), targets.end());

                uint32 const maxTargets = GetCaster()->HasAura(SPELL_PRIEST_GLYPH_OF_CIRCLE_OF_HEALING) ? 6 : 5; // Glyph of Circle of Healing

                if (targets.size() > maxTargets)
                {
                    std::sort(targets.begin(), targets.end(), Trinity::HealthPctOrderPred());
                    targets.resize(maxTargets);
                }
            }

            void Register() override
            {
                OnObjectAreaTargetSelect += SpellObjectAreaTargetSelectFn(spell_pri_circle_of_healing_SpellScript::FilterTargets, EFFECT_0, TARGET_UNIT_DEST_AREA_ALLY);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_pri_circle_of_healing_SpellScript();
        }
};

// 81208 - Chakra: Serenity
class spell_pri_chakra_serenity : public SpellScriptLoader
{
    public:
        spell_pri_chakra_serenity() : SpellScriptLoader("spell_pri_chakra_serenity") { }

        class spell_pri_chakra_serenity_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_pri_chakra_serenity_AuraScript);

            void HandleEffectProc(AuraEffect const* /*aurEff*/, ProcEventInfo& eventInfo)
            {
                PreventDefaultAction();

                Unit* caster = GetCaster();
                Unit* target = eventInfo.GetActionTarget();

                // Renew
                if (AuraEffect* aurEff = target->GetAuraEffect(SPELL_AURA_PERIODIC_HEAL, SPELLFAMILY_PRIEST, 0x00000040, 0x00000000, 0x00000000, 0x00000000, caster->GetGUID()))
                    aurEff->GetBase()->RefreshDuration();
            }

            void Register() override
            {
                OnEffectProc += AuraEffectProcFn(spell_pri_chakra_serenity_AuraScript::HandleEffectProc, EFFECT_1, SPELL_AURA_DUMMY);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_pri_chakra_serenity_AuraScript();
        }
};

// 88685 - Holy Word : Sanctuary
class spell_pri_holy_word_sanctuary : public SpellScriptLoader
{
    public:
        spell_pri_holy_word_sanctuary() : SpellScriptLoader("spell_pri_holy_word_sanctuary") { }

        class spell_pri_holy_word_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_pri_holy_word_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                if (!sSpellMgr->GetSpellInfo(SPELL_PRIEST_HOLY_WORD_SANCTUARY_AREA))
                    return false;
                return true;
            }

            void HandleOnHit(SpellEffIndex /*effIndex*/)
            {
                if (Unit* caster = GetCaster())
                    caster->RemoveDynObject(SPELL_PRIEST_HOLY_WORD_SANCTUARY_AREA);
            }

            void Register() override
            {
                OnEffectHit += SpellEffectFn(spell_pri_holy_word_SpellScript::HandleOnHit, EFFECT_0, SPELL_EFFECT_PERSISTENT_AREA_AURA);
            }
        };

        SpellScript* GetSpellScript() const
        {
            return new spell_pri_holy_word_SpellScript();
        }

        class spell_pri_holy_word_sanctuary_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_pri_holy_word_sanctuary_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                if (!sSpellMgr->GetSpellInfo(SPELL_PRIEST_HOLY_WORD_SANCTUARY_AREA))
                    return false;
                return true;
            }

            void OnTick(AuraEffect const* /*aurEff*/)
            {
                if (DynamicObject* dynObj = GetCaster()->GetDynObject(SPELL_PRIEST_HOLY_WORD_SANCTUARY_AREA))
                    GetCaster()->CastSpell(dynObj->GetPositionX(), dynObj->GetPositionY(), dynObj->GetPositionZ(), SPELL_PRIEST_HOLY_WORD_SANCTUARY_HEAL, true);
            }

            void Register() override
            {
                OnEffectPeriodic += AuraEffectPeriodicFn(spell_pri_holy_word_sanctuary_AuraScript::OnTick, EFFECT_1, SPELL_AURA_PERIODIC_DUMMY);
            }
        };

        AuraScript* GetAuraScript() const
        {
            return new spell_pri_holy_word_sanctuary_AuraScript();
        }
};

// 88686 - Holy Word : Sanctuary
class spell_pri_holy_word_sanctuary_heal : public SpellScriptLoader
{
    public:
        spell_pri_holy_word_sanctuary_heal() : SpellScriptLoader("spell_pri_holy_word_sanctuary_heal") { }

        class spell_pri_holy_word_sanctuary_heal_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_pri_holy_word_sanctuary_heal_SpellScript);

            bool Load() override
            {
                _targetCount = 0;
                return true;
            }

            void HandleHeal(SpellEffIndex /*effIndex*/)
            {
                if (Unit* target = GetHitUnit())
                    if (++_targetCount > 6)
                    {
                        uint32 heal = GetHitHeal();

                        heal *= (6 / _targetCount);

                        SetHitHeal(heal);
                    }

            }

            void Register() override
            {
                OnEffectHitTarget += SpellEffectFn(spell_pri_holy_word_sanctuary_heal_SpellScript::HandleHeal, EFFECT_0, SPELL_EFFECT_HEAL);
            }

        private:
            uint32 _targetCount;
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_pri_holy_word_sanctuary_heal_SpellScript();
        }
};

// 81209 - Chakra: Chastise
class spell_pri_chakra_chastise : public SpellScriptLoader
{
    public:
        spell_pri_chakra_chastise() : SpellScriptLoader("spell_pri_chakra_chastise") { }

        class spell_pri_chakra_chastise_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_pri_chakra_chastise_AuraScript);

            void OnProc(AuraEffect const* /*aurEff*/, ProcEventInfo& /*eventInfo*/)
            {
                PreventDefaultAction();

                if (Player* player = GetCaster()->ToPlayer())
                    player->GetSpellHistory()->ResetCooldown(SPELL_PRIEST_HOLY_WORD_CHASTISE, true);
            }

            void Register() override
            {
                OnEffectProc += AuraEffectProcFn(spell_pri_chakra_chastise_AuraScript::OnProc, EFFECT_1, SPELL_AURA_DUMMY);
            }
        };

        AuraScript* GetAuraScript() const
        {
            return new spell_pri_chakra_chastise_AuraScript();
        }
};

// 125045 - Glyph of Holy Nova
class spell_pri_glyph_of_holy_nova : public SpellScriptLoader
{
    public:
        spell_pri_glyph_of_holy_nova() : SpellScriptLoader("spell_pri_glyph_of_holy_nova") { }

        class spell_pri_glyph_of_holy_nova_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_pri_glyph_of_holy_nova_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                if (!sSpellMgr->GetSpellInfo(SPELL_PRIEST_HOLY_NOVA))
                    return false;
                return true;
            }

            void OnApply(AuraEffect const* /*aurEff*/, AuraEffectHandleModes /*mode*/)
            {
                if (Player* player = GetTarget()->ToPlayer())
                    player->LearnSpell(SPELL_PRIEST_HOLY_NOVA, false);
            }

            void OnRemove(AuraEffect const* /*aurEff*/, AuraEffectHandleModes /*mode*/)
            {
                if (Player* player = GetTarget()->ToPlayer())
                    player->RemoveSpell(SPELL_PRIEST_HOLY_NOVA, false, false);
            }

            void Register() override
            {
                OnEffectApply += AuraEffectApplyFn(spell_pri_glyph_of_holy_nova_AuraScript::OnApply, EFFECT_0, SPELL_AURA_DUMMY, AURA_EFFECT_HANDLE_REAL);
                OnEffectRemove += AuraEffectRemoveFn(spell_pri_glyph_of_holy_nova_AuraScript::OnRemove, EFFECT_0, SPELL_AURA_DUMMY, AURA_EFFECT_HANDLE_REAL);
            }
        };

        AuraScript* GetAuraScript() const
        {
            return new spell_pri_glyph_of_holy_nova_AuraScript();
        }
};

// 132157 - Holy Nova
class spell_pri_holy_nova : public SpellScriptLoader
{
    public:
        spell_pri_holy_nova() : SpellScriptLoader("spell_pri_holy_nova") { }

        class spell_pri_holy_nova_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_pri_holy_nova_SpellScript);

            void HandleHit(SpellEffIndex /*effIndex*/)
            {
                if (Unit* caster = GetCaster())
                    caster->CastCustomSpell(SPELL_PRIEST_HOLY_NOVA_HEAL, SPELLVALUE_MAX_TARGETS, 5, caster);
            }

            void Register() override
            {
                OnEffectHit += SpellEffectFn(spell_pri_holy_nova_SpellScript::HandleHit, EFFECT_0, SPELL_EFFECT_SCHOOL_DAMAGE);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_pri_holy_nova_SpellScript();
        }
};

// 527 - Purify
class spell_pri_purify : public SpellScriptLoader
{
    public:
        spell_pri_purify() : SpellScriptLoader("spell_pri_purify") { }

        class spell_pri_purify_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_pri_purify_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                if (!sSpellMgr->GetSpellInfo(SPELL_PRIEST_GLYPH_OF_PURIFY))
                    return false;
                if (!sSpellMgr->GetSpellInfo(SPELL_PRIEST_GLYPH_OF_PURIFY_HEAL))
                    return false;
                return true;
            }

            bool Load() override
            {
                _alreadyDispeled = false;
                return true;
            }

            void OnSuccessfulDispel(SpellEffIndex /*effIndex*/)
            {
                if (!_alreadyDispeled)
                    if (Unit* unit = GetHitUnit())
                        if (AuraEffect const* aurEff = unit->GetAuraEffect(SPELL_PRIEST_GLYPH_OF_PURIFY, EFFECT_0))
                        {
                            int32 heal = GetHitUnit()->CountPctFromMaxHealth(aurEff->GetAmount());
                            GetCaster()->CastCustomSpell(SPELL_PRIEST_GLYPH_OF_PURIFY_HEAL, SPELLVALUE_BASE_POINT0, heal, unit);
                            _alreadyDispeled = true;
                        }
            }

            void Register() override
            {
                OnEffectSuccessfulDispel += SpellEffectFn(spell_pri_purify_SpellScript::OnSuccessfulDispel, EFFECT_0, SPELL_EFFECT_DISPEL);
                OnEffectSuccessfulDispel += SpellEffectFn(spell_pri_purify_SpellScript::OnSuccessfulDispel, EFFECT_1, SPELL_EFFECT_DISPEL);
            }

        private:
            bool _alreadyDispeled;
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_pri_purify_SpellScript();
        }
};

// 528 - Dispel Magic
class spell_pri_dispel_magic : public SpellScriptLoader
{
    public:
        spell_pri_dispel_magic() : SpellScriptLoader("spell_pri_dispel_magic") { }

        class spell_pri_dispel_magic_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_pri_dispel_magic_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                if (!sSpellMgr->GetSpellInfo(SPELL_PRIEST_GLYPH_OF_DISPEL_MAGIC))
                    return false;
                if (!sSpellMgr->GetSpellInfo(SPELL_PRIEST_GLYPH_OF_DISPEL_MAGIC_DAMAGE))
                    return false;
                return true;
            }

            void OnSuccessfulDispel(SpellEffIndex /*effIndex*/)
            {
                if (Unit* unit = GetHitUnit())
                    if (AuraEffect const* aurEff = unit->GetAuraEffect(SPELL_PRIEST_GLYPH_OF_DISPEL_MAGIC, EFFECT_0))
                        GetCaster()->CastSpell(unit, SPELL_PRIEST_GLYPH_OF_DISPEL_MAGIC_DAMAGE, true);
            }

            void Register() override
            {
                OnEffectSuccessfulDispel += SpellEffectFn(spell_pri_dispel_magic_SpellScript::OnSuccessfulDispel, EFFECT_0, SPELL_EFFECT_DISPEL);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_pri_dispel_magic_SpellScript();
        }
};

// 588 - Inner Fire
class spell_pri_inner_fire : public SpellScriptLoader
{
    public:
        spell_pri_inner_fire() : SpellScriptLoader("spell_pri_inner_fire") { }

        class spell_pri_inner_fire_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_pri_inner_fire_SpellScript);

            bool Validate(SpellInfo const* /*spellEntry*/)
            {
                if (!sSpellMgr->GetSpellInfo(SPELL_PRIEST_SPELL_WARDING))
                    return false;
                return true;
            }

            void HandleOnHit()
            {
                if (Player* player = GetCaster()->ToPlayer())
                    if (AuraEffect const* aurEff = player->GetAuraEffect(SPELL_PRIEST_GLYPH_OF_INNER_SANCTUM, EFFECT_0))
                        player->CastCustomSpell(SPELL_PRIEST_SPELL_WARDING, SPELLVALUE_BASE_POINT0, aurEff->GetAmount(), player);
            }

            void Register() override
            {
                OnHit += SpellHitFn(spell_pri_inner_fire_SpellScript::HandleOnHit);
            }
        };

        SpellScript* GetSpellScript() const
        {
            return new spell_pri_inner_fire_SpellScript;
        }
};

// 92833 - Leap of Faith
class spell_pri_leap_of_faith_effect_trigger : public SpellScriptLoader
{
    public:
        spell_pri_leap_of_faith_effect_trigger() : SpellScriptLoader("spell_pri_leap_of_faith_effect_trigger") { }

        class spell_pri_leap_of_faith_effect_trigger_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_pri_leap_of_faith_effect_trigger_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                if (!sSpellMgr->GetSpellInfo(SPELL_PRIEST_LEAP_OF_FAITH_EFFECT))
                    return false;
                return true;
            }

            void HandleEffectDummy(SpellEffIndex /*effIndex*/)
            {
                Unit* unit = GetHitUnit();
                Unit* caster = GetCaster();

                if (!caster || !unit)
                    return;

                Position destPos = GetHitDest()->GetPosition();

                SpellCastTargets targets;
                targets.SetDst(destPos);
                targets.SetUnitTarget(caster);
                unit->CastSpell(targets, sSpellMgr->GetSpellInfo(GetEffectValue()), nullptr);

                // Glyph of Leap of Faith
                if (caster->HasAura(SPELL_PRIEST_GLYPH_OF_LEAP_OF_FAITH))
                    unit->RemoveMovementImpairingAuras(true);

                // Body And Soul
                if (caster->GetAura(SPELL_PRIEST_BODY_AND_SOUL))
                    caster->CastSpell(unit, SPELL_PRIEST_BODY_AND_SOUL_SPEED, true);
            }

            void Register() override
            {
                OnEffectHitTarget += SpellEffectFn(spell_pri_leap_of_faith_effect_trigger_SpellScript::HandleEffectDummy, EFFECT_0, SPELL_EFFECT_DUMMY);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_pri_leap_of_faith_effect_trigger_SpellScript();
        }
};

// 1706 - Levitate
class spell_pri_levitate : public SpellScriptLoader
{
    public:
        spell_pri_levitate() : SpellScriptLoader("spell_pri_levitate") { }

        class spell_pri_levitate_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_pri_levitate_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                if (!sSpellMgr->GetSpellInfo(SPELL_PRIEST_LEVITATE_EFFECT))
                    return false;
                return true;
            }

            void HandleDummy(SpellEffIndex /*effIndex*/)
            {
                GetCaster()->CastSpell(GetHitUnit(), SPELL_PRIEST_LEVITATE_EFFECT, true);
            }

            void Register() override
            {
                OnEffectHitTarget += SpellEffectFn(spell_pri_levitate_SpellScript::HandleDummy, EFFECT_0, SPELL_EFFECT_DUMMY);
            }
        };

        SpellScript* GetSpellScript() const
        {
            return new spell_pri_levitate_SpellScript;
        }
};

// 111759 - Levitate
class spell_pri_levitate_effect : public SpellScriptLoader
{
    public:
        spell_pri_levitate_effect() : SpellScriptLoader("spell_pri_levitate_effect") { }

        class spell_pri_levitate_effect_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_pri_levitate_effect_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                if (!sSpellMgr->GetSpellInfo(SPELL_PRIEST_GLYPH_OF_LEVITATE))
                    return false;
                if (!sSpellMgr->GetSpellInfo(SPELL_PRIEST_PATH_OF_THE_DEVOUT))
                    return false;
                if (!sSpellMgr->GetSpellInfo(SPELL_PRIEST_GLYPH_OF_THE_HEAVENS))
                    return false;
                if (!sSpellMgr->GetSpellInfo(SPELL_PRIEST_GLYPH_OF_THE_HEAVENS_EFFECT))
                    return false;
                return true;
            }

            bool Load() override
            {
                _haveGlyph1 = false;
                _haveGlyph2 = false;
                return true;
            }

            void OnApply(AuraEffect const* /*aurEff*/, AuraEffectHandleModes /*mode*/)
            {
                Unit* caster = GetCaster();
                Unit* target = GetTarget();

                if (_haveGlyph1 = caster->HasAura(SPELL_PRIEST_GLYPH_OF_LEVITATE))
                    caster->CastSpell(target, SPELL_PRIEST_PATH_OF_THE_DEVOUT, true);

                if (_haveGlyph2 = caster->HasAura(SPELL_PRIEST_GLYPH_OF_THE_HEAVENS))
                    caster->CastSpell(target, SPELL_PRIEST_GLYPH_OF_THE_HEAVENS_EFFECT, true);
            }

            void OnRemove(AuraEffect const* /*aurEff*/, AuraEffectHandleModes /*mode*/)
            {
                if (Unit* target = GetTarget())
                {
                    if (_haveGlyph1)
                        if (Aura* aura = target->GetAura(SPELL_PRIEST_PATH_OF_THE_DEVOUT, GetCaster()->GetGUID()))
                            aura->SetDuration(10 * IN_MILLISECONDS);

                    if (_haveGlyph2)
                        target->RemoveAurasDueToSpell(SPELL_PRIEST_GLYPH_OF_THE_HEAVENS_EFFECT);
                }
            }

            void Register() override
            {
                OnEffectApply += AuraEffectApplyFn(spell_pri_levitate_effect_AuraScript::OnApply, EFFECT_1, SPELL_AURA_HOVER, AURA_EFFECT_HANDLE_REAL);
                OnEffectRemove += AuraEffectRemoveFn(spell_pri_levitate_effect_AuraScript::OnRemove, EFFECT_1, SPELL_AURA_HOVER, AURA_EFFECT_HANDLE_REAL);
            }

        private:
            bool _haveGlyph1;
            bool _haveGlyph2;
        };

        AuraScript* GetAuraScript() const
        {
            return new spell_pri_levitate_effect_AuraScript();
        }
};

// 585 - Smite
class spell_pri_smite : public SpellScriptLoader
{
    public:
        spell_pri_smite() : SpellScriptLoader("spell_pri_smite") { }

        class spell_pri_smite_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_pri_smite_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                if (!sSpellMgr->GetSpellInfo(SPELL_PRIEST_GLYPH_OF_SMITE))
                    return false;
                return true;
            }

            void RecalculateDamage()
            {
                Unit* caster = GetCaster();
                if (!caster)
                    return;

                Unit* target = GetHitUnit();

                int32 damage = GetHitDamage();

                // Glyph of Smite
                if (AuraEffect* aurEff = caster->GetAuraEffect(SPELL_PRIEST_GLYPH_OF_SMITE, EFFECT_0))
                    if (target->GetAuraEffect(SPELL_AURA_PERIODIC_DAMAGE, SPELLFAMILY_PRIEST, 0x00100000, 0x00000000, 0x00000000, 0x00000000, caster->GetGUID()))
                        AddPct(damage, aurEff->GetAmount());

                SetHitDamage(damage);
            }

            void Register() override
            {
                OnHit += SpellHitFn(spell_pri_smite_SpellScript::RecalculateDamage);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_pri_smite_SpellScript();
        }
};

// 64843 - Divine Hymn
// 64901 - Hymn of Hope
class spell_pri_hymns_channel : public SpellScriptLoader
{
    public:
        spell_pri_hymns_channel() : SpellScriptLoader("spell_pri_hymns_channel") { }

        class spell_pri_hymns_channel_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_pri_hymns_channel_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                if (!sSpellMgr->GetSpellInfo(SPELL_PRIEST_GLYPH_OF_INSPIRED_HYMNS))
                    return false;
                if (!sSpellMgr->GetSpellInfo(SPELL_PRIEST_GLYPH_OF_INSPIRED_HYMNS_EFFECT))
                    return false;
                return true;
            }

            bool Load() override
            {
                _haveGlyph = false;
                return true;
            }

            void OnApply(AuraEffect const* /*aurEff*/, AuraEffectHandleModes /*mode*/)
            {
                Unit* caster = GetCaster();

                if (_haveGlyph = caster->HasAura(SPELL_PRIEST_GLYPH_OF_INSPIRED_HYMNS))
                    caster->CastSpell(caster, SPELL_PRIEST_GLYPH_OF_INSPIRED_HYMNS_EFFECT, true);
            }

            void OnRemove(AuraEffect const* /*aurEff*/, AuraEffectHandleModes /*mode*/)
            {
                if (_haveGlyph)
                    if (Unit* caster = GetCaster())
                        caster->RemoveAurasDueToSpell(SPELL_PRIEST_GLYPH_OF_INSPIRED_HYMNS_EFFECT);
            }

            void Register() override
            {
                OnEffectApply += AuraEffectApplyFn(spell_pri_hymns_channel_AuraScript::OnApply, EFFECT_0, SPELL_AURA_PERIODIC_TRIGGER_SPELL, AURA_EFFECT_HANDLE_REAL);
                OnEffectRemove += AuraEffectRemoveFn(spell_pri_hymns_channel_AuraScript::OnRemove, EFFECT_0, SPELL_AURA_PERIODIC_TRIGGER_SPELL, AURA_EFFECT_HANDLE_REAL);
            }

        private:
            bool _haveGlyph;
        };

        AuraScript* GetAuraScript() const
        {
            return new spell_pri_hymns_channel_AuraScript();
        }
};

// 126152 - Glyph of Confession
class spell_pri_glyph_of_confession : public SpellScriptLoader
{
    public:
        spell_pri_glyph_of_confession() : SpellScriptLoader("spell_pri_glyph_of_confession") { }

        class spell_pri_glyph_of_confession_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_pri_glyph_of_confession_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                if (!sSpellMgr->GetSpellInfo(SPELL_PRIEST_CONFESSION))
                    return false;
                return true;
            }

            void OnApply(AuraEffect const* /*aurEff*/, AuraEffectHandleModes /*mode*/)
            {
                if (Player* player = GetTarget()->ToPlayer())
                    player->LearnSpell(SPELL_PRIEST_CONFESSION, false);
            }

            void OnRemove(AuraEffect const* /*aurEff*/, AuraEffectHandleModes /*mode*/)
            {
                if (Player* player = GetTarget()->ToPlayer())
                    player->RemoveSpell(SPELL_PRIEST_CONFESSION, false, false);
            }

            void Register() override
            {
                OnEffectApply += AuraEffectApplyFn(spell_pri_glyph_of_confession_AuraScript::OnApply, EFFECT_0, SPELL_AURA_DUMMY, AURA_EFFECT_HANDLE_REAL);
                OnEffectRemove += AuraEffectRemoveFn(spell_pri_glyph_of_confession_AuraScript::OnRemove, EFFECT_0, SPELL_AURA_DUMMY, AURA_EFFECT_HANDLE_REAL);
            }
        };

        AuraScript* GetAuraScript() const
        {
            return new spell_pri_glyph_of_confession_AuraScript();
        }
};

// 126123 - Confession
class spell_pri_confession : public SpellScriptLoader
{
    public:
        spell_pri_confession() : SpellScriptLoader("spell_pri_confession") { }

        class spell_pri_confession_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_pri_confession_SpellScript);

            SpellCastResult CheckCast()
            {
                Unit* caster = GetCaster();
                Unit* target = GetExplTargetUnit();

                if (!target || !caster || target->GetGUID() == caster->GetGUID())
                    return SPELL_FAILED_BAD_TARGETS;

                if (target->GetTypeId() != TYPEID_PLAYER)
                    return SPELL_FAILED_TARGET_NOT_PLAYER;

                return SPELL_CAST_OK;
            }

            void HandleScriptEffect(SpellEffIndex /*effIndex*/)
            {
                if (Unit* unit = GetHitUnit())
                {
                    uint32 confessID = Confessions[Math::Rand(0, MAX_CONFESSIONS)];

                    unit->MonsterTextEmote(confessID, unit);
                }
            }

            void Register() override
            {
                OnCheckCast += SpellCheckCastFn(spell_pri_confession_SpellScript::CheckCast);
                OnEffectHitTarget += SpellEffectFn(spell_pri_confession_SpellScript::HandleScriptEffect, EFFECT_0, SPELL_EFFECT_SCRIPT_EFFECT);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_pri_confession_SpellScript();
        }
};

// 120517 - Halo
// 120644 - Halo
class spell_pri_halo : public SpellScriptLoader
{
    public:
        spell_pri_halo() : SpellScriptLoader("spell_pri_halo") { }

        class spell_pri_halo_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_pri_halo_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                if (!sSpellMgr->GetSpellInfo(SPELL_PRIEST_HALO_HOLY_VISUAL))
                    return false;
                if (!sSpellMgr->GetSpellInfo(SPELL_PRIEST_HALO_SHADOW_VISUAL))
                    return false;
                return true;
            }

            void CastVisual()
            {
                Unit* caster = GetCaster();
                if (!caster)
                    return;

                if (m_scriptSpellId == SPELL_PRIEST_HALO_HOLY)
                    caster->CastSpell(caster, SPELL_PRIEST_HALO_HOLY_VISUAL, false);
                else
                    caster->CastSpell(caster, SPELL_PRIEST_HALO_SHADOW_VISUAL, false);
            }

            void Register() override
            {
                AfterCast += SpellCastFn(spell_pri_halo_SpellScript::CastVisual);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_pri_halo_SpellScript();
        }
};

// 120692 - Halo
// 120696 - Halo
class spell_pri_halo_damage_and_heal : public SpellScriptLoader
{
    public:
        spell_pri_halo_damage_and_heal() : SpellScriptLoader("spell_pri_halo_damage_and_heal") { }

        class spell_pri_halo_damage_and_heal_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_pri_halo_damage_and_heal_SpellScript);

            void PreventEffects(SpellMissInfo missInfo)
            {
                if (missInfo != SPELL_MISS_NONE)
                    return;

                Unit* caster = GetCaster();
                Unit* target = GetHitUnit();

                if (!caster || !target)
                    return;

                if (!caster->IsValidAssistTarget(target))
                    PreventHitHeal();

                if (!caster->IsValidAttackTarget(target))
                    PreventHitDamage();
            }

            void RecalculateEffects()
            {
                Unit* caster = GetCaster();
                Unit* target = GetHitUnit();

                if (!caster || !target)
                    return;

                int32 damage = GetHitDamage();
                int32 heal = GetHitHeal();

                if (!damage && !heal)
                    return;

                float distance = caster->GetDistance(target);

                float multiplier = 0.5f * pow(1.01f, -1 * pow(((distance - 25) / 2), 4)) + 0.1f + 0.015f * distance;

                if (damage)
                    SetHitDamage(damage * multiplier);
                else if (heal)
                    SetHitHeal(heal * multiplier);
            }

            void Register() override
            {
                BeforeHit += BeforeSpellHitFn(spell_pri_halo_damage_and_heal_SpellScript::PreventEffects);
                OnHit += SpellHitFn(spell_pri_halo_damage_and_heal_SpellScript::RecalculateEffects);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_pri_halo_damage_and_heal_SpellScript();
        }
};

// 119030 - Spectral Guise
class spell_pri_spectral_guise_charges : public SpellScriptLoader
{
    public:
        spell_pri_spectral_guise_charges() : SpellScriptLoader("spell_pri_spectral_guise_charges") { }

        class spell_pri_spectral_guise_charges_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_pri_spectral_guise_charges_AuraScript);

            void OnRemove(AuraEffect const* /*aurEff*/, AuraEffectHandleModes /*mode*/)
            {
                if (GetTargetApplication()->GetRemoveMode() == AURA_REMOVE_BY_CANCEL)
                    return;

                if (Unit* caster = GetCaster())
                    if (Creature* creature = caster->ToCreature())
                        creature->DespawnOrUnsummon();
            }

            void Register() override
            {
                OnEffectRemove += AuraEffectRemoveFn(spell_pri_spectral_guise_charges_AuraScript::OnRemove, EFFECT_0, SPELL_AURA_DUMMY, AURA_EFFECT_HANDLE_REAL);
            }
        };

        AuraScript* GetAuraScript() const
        {
            return new spell_pri_spectral_guise_charges_AuraScript();
        }
};

// 119032 - Spectral Guise
class spell_pri_spectral_guise_stealth : public SpellScriptLoader
{
    public:
        spell_pri_spectral_guise_stealth() : SpellScriptLoader("spell_pri_spectral_guise_stealth") { }

        class spell_pri_spectral_guise_stealth_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_pri_spectral_guise_stealth_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                if (!sSpellMgr->GetSpellInfo(SPELL_PRIEST_SPECTRAL_GUISE_CHARGES))
                    return false;
                return true;
            }

            bool CheckProc(ProcEventInfo& eventInfo)
            {
                SpellInfo const* spellInfo = eventInfo.GetSpellInfo();
                if (!spellInfo)
                    return false;

                // do not proc from 112833 - Spectral Guise
                if (spellInfo->Id == SPELL_PRIEST_SPECTRAL_GUISE_SUMMON)
                    return false;

                return true;
            }

            void OnRemove(AuraEffect const* /*aurEff*/, AuraEffectHandleModes /*mode*/)
            {
                if (Unit* caster = GetCaster())
                    caster->RemoveAurasDueToSpell(SPELL_PRIEST_SPECTRAL_GUISE_CHARGES);
            }

            void Register() override
            {
                DoCheckProc += AuraCheckProcFn(spell_pri_spectral_guise_stealth_AuraScript::CheckProc);
                OnEffectRemove += AuraEffectRemoveFn(spell_pri_spectral_guise_stealth_AuraScript::OnRemove, EFFECT_0, SPELL_AURA_MOD_STEALTH, AURA_EFFECT_HANDLE_REAL);
            }
        };

        AuraScript* GetAuraScript() const
        {
            return new spell_pri_spectral_guise_stealth_AuraScript();
        }
};

// 108920 - Void Tendrils
class spell_pri_void_tendrils : public SpellScriptLoader
{
    public:
        spell_pri_void_tendrils() : SpellScriptLoader("spell_pri_void_tendrils") { }

        class spell_pri_void_tendrils_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_pri_void_tendrils_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                if (!sSpellMgr->GetSpellInfo(SPELL_PRIEST_VOID_TENDRILS_SUMMON))
                    return false;
                return true;
            }

            void OnApply(AuraEffect const* aurEff, AuraEffectHandleModes /*mode*/)
            {
                Unit* caster = GetCaster();
                Unit* target = GetTarget();

                if (!caster || !target)
                    return;

                caster->CastSpell(target, SPELL_PRIEST_VOID_TENDRILS_SUMMON, true, nullptr, aurEff, target->GetGUID());
            }

            void Register() override
            {
                OnEffectApply += AuraEffectApplyFn(spell_pri_void_tendrils_AuraScript::OnApply, EFFECT_0, SPELL_AURA_MOD_ROOT, AURA_EFFECT_HANDLE_REAL);
            }
        };

        AuraScript* GetAuraScript() const
        {
            return new spell_pri_void_tendrils_AuraScript();
        }
};

// 114404 - Void Tendril's Grasp
class spell_pri_void_tendrils_grasp : public SpellScriptLoader
{
    public:
        spell_pri_void_tendrils_grasp() : SpellScriptLoader("spell_pri_void_tendrils_grasp") { }

        class spell_pri_void_tendrils_grasp_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_pri_void_tendrils_grasp_AuraScript);

            void OnRemove(AuraEffect const* /*aurEff*/, AuraEffectHandleModes /*mode*/)
            {
                if (GetTargetApplication()->GetRemoveMode() != AURA_REMOVE_BY_DEATH)
                    return;

                if (Unit* caster = GetCaster())
                    if (Creature* creature = caster->ToCreature())
                        creature->DespawnOrUnsummon();
            }

            void Register() override
            {
                OnEffectRemove += AuraEffectRemoveFn(spell_pri_void_tendrils_grasp_AuraScript::OnRemove, EFFECT_0, SPELL_AURA_MOD_ROOT, AURA_EFFECT_HANDLE_REAL);
            }
        };

        AuraScript* GetAuraScript() const
        {
            return new spell_pri_void_tendrils_grasp_AuraScript();
        }
};

// 127665 - Void Tendrils
class spell_pri_void_tendrils_summon : public SpellScriptLoader
{
    public:
        spell_pri_void_tendrils_summon() : SpellScriptLoader("spell_pri_void_tendrils_summon") { }

        class spell_pri_void_tendrils_summon_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_pri_void_tendrils_summon_SpellScript);

            void HandleSummon(SpellEffIndex effIndex)
            {
                PreventHitDefaultEffect(effIndex);

                Unit* caster = GetCaster();
                Unit* target = GetOriginalCaster();

                if (!caster || !target)
                    return;

                uint32 entry = uint32(GetSpellInfo()->GetEffect(effIndex)->MiscValue);
                SummonPropertiesEntry const* properties = sSummonPropertiesStore.LookupEntry(uint32(GetSpellInfo()->GetEffect(effIndex)->MiscValueB));
                uint32 duration = uint32(GetSpellInfo()->GetDuration());

                if (Creature* summon = caster->GetMap()->SummonCreature(entry, *target, properties, duration, caster, GetSpellInfo()->Id))
                    summon->CastSpell(target, SPELL_PRIEST_VOID_TENDRIL_GRASP, false);
            }

            void Register() override
            {
                OnEffectHit += SpellEffectFn(spell_pri_void_tendrils_summon_SpellScript::HandleSummon, EFFECT_0, SPELL_EFFECT_SUMMON);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_pri_void_tendrils_summon_SpellScript();
        }
};

#define DivineStarAuraScriptName "spell_pri_divine_star_aura"

// 110744 - Divine Star
// 122121 - Divine Star
class spell_pri_divine_star_aura : public SpellScriptLoader
{
    public:
        spell_pri_divine_star_aura() : SpellScriptLoader(DivineStarAuraScriptName) { }

        class spell_pri_divine_star_aura_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_pri_divine_star_aura_AuraScript);

            bool Load() override
            {
                _missileSpell = nullptr;
                return true;
            }

            void Register() override { }

        public:
            void SetMissileSpell(Spell* spell)
            {
                _missileSpell = spell;
            }

            Spell* GetMissileSpell() const { return _missileSpell; }

        private:
            Spell* _missileSpell;
        };

        AuraScript* GetAuraScript() const
        {
            return new spell_pri_divine_star_aura_AuraScript();
        }

        class spell_pri_divine_star_aura_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_pri_divine_star_aura_SpellScript);

            void HandleTriggerSpellHit(SpellEffIndex effIndex)
            {
                if (Unit* target = GetHitUnit())
                    target->CastSpell(target, GetEffectInfo(effIndex)->TriggerSpell, true);
            }

            void Register() override
            {
                OnEffectHitTarget += SpellEffectFn(spell_pri_divine_star_aura_SpellScript::HandleTriggerSpellHit, EFFECT_1, SPELL_EFFECT_DUMMY);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_pri_divine_star_aura_SpellScript();
        }

};

typedef spell_pri_divine_star_aura::spell_pri_divine_star_aura_AuraScript DivineStarAuraScript;

// 58880 - Divine Star
// 122127 - Divine Star
class spell_pri_divine_star : public SpellScriptLoader
{
    public:
        spell_pri_divine_star() : SpellScriptLoader("spell_pri_divine_star") { }

        class spell_pri_divine_star_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_pri_divine_star_SpellScript);

            void SetDest(SpellDestination& dest)
            {
                Unit* caster = GetCaster();
                Unit* originalCaster = GetOriginalCaster();

                // first is casted by player - should adjust destination
                if (caster->GetGUID() == originalCaster->GetGUID())
                {
                    float dist = 24.0f;
                    float angle = 0.0f;

                    Position pos = dest._position;

                    caster->MovePositionToFirstCollision(pos, dist, angle);

                    dest.Relocate(pos);
                }

                uint32 auraID = SPELL_PRIEST_DIVINE_STAR_SHADOW_AURA;
                if (m_scriptSpellId == SPELL_PRIEST_DIVINE_STAR_HOLY_TRIGGER)
                    auraID = SPELL_PRIEST_DIVINE_STAR_HOLY_AURA;

                if (Aura* divineAura = originalCaster->GetAura(auraID))
                    if (DivineStarAuraScript* script = dynamic_cast<DivineStarAuraScript*>(divineAura->GetScriptByName(DivineStarAuraScriptName)))
                        script->SetMissileSpell(GetSpell());
            }

            void CastTrigger(SpellEffIndex /*index*/)
            {
                Unit* caster = GetCaster();
                Unit* originalCaster = GetOriginalCaster();

                if (caster->GetGUID() != originalCaster->GetGUID())
                {
                    uint32 auraID = SPELL_PRIEST_DIVINE_STAR_SHADOW_AURA;
                    if (m_scriptSpellId == SPELL_PRIEST_DIVINE_STAR_HOLY_TRIGGER)
                        auraID = SPELL_PRIEST_DIVINE_STAR_HOLY_AURA;

                    if (Aura* divineAura = originalCaster->GetAura(auraID))
                        divineAura->Remove();
                }
                else
                {
                    // cast back to caster's current destination
                    if (Creature* summonTrigger = caster->SummonCreature(WORLD_TRIGGER, *GetExplTargetDest(), TEMPSUMMON_TIMED_DESPAWN, 10 * IN_MILLISECONDS))
                        summonTrigger->CastSpell(caster->GetPositionX(), caster->GetPositionY(), caster->GetPositionZ(), m_scriptSpellId, true, nullptr, nullptr, caster->GetGUID());
                }
            }

            void Register() override
            {
                OnDestinationTargetSelect += SpellDestinationTargetSelectFn(spell_pri_divine_star_SpellScript::SetDest, EFFECT_0, TARGET_DEST_DEST_ANY);
                OnEffectHit += SpellEffectFn(spell_pri_divine_star_SpellScript::CastTrigger, EFFECT_0, SPELL_EFFECT_DUMMY);
            }
        };

        SpellScript* GetSpellScript() const
        {
            return new spell_pri_divine_star_SpellScript();
        }
};

// 110745 - Divine Star
// 122128 - Divine Star
class spell_pri_divine_star_aoe : public SpellScriptLoader
{
    public:
        spell_pri_divine_star_aoe() : SpellScriptLoader("spell_pri_divine_star_aoe") { }

        class spell_pri_divine_star_aoe_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_pri_divine_star_aoe_SpellScript);

            bool Load() override
            {
                _setTargets = false;
                return true;
            }

            void FilterTargets(std::vector<WorldObject*>& targets)
            {
                _setTargets = false;

                Unit* caster = GetCaster();
                if (!caster)
                {
                    targets.clear();
                    return;
                }

                Spell* divineStarSpell = nullptr;
                if (Aura* divineAura = caster->GetAura(GetTriggeringSpell()->Id))
                    if (DivineStarAuraScript* script = dynamic_cast<DivineStarAuraScript*>(divineAura->GetScriptByName(DivineStarAuraScriptName)))
                        divineStarSpell = script->GetMissileSpell();

                if (!divineStarSpell)
                {
                    targets.clear();
                    return;
                }

                Unit* triggerSpellCaster = divineStarSpell->GetCaster();
                SpellEvent const* spellEvent = divineStarSpell->GetSpellEvent();
                if (!triggerSpellCaster || !spellEvent)
                {
                    targets.clear();
                    return;
                }

                uint64 executionTimer = spellEvent->GetExecTime();
                uint64 currentTimer = triggerSpellCaster->m_Events.GetTimer();
                uint64 timerDiff = executionTimer - currentTimer;

                float speed = divineStarSpell->GetSpellInfo()->Speed;
                float distance = timerDiff * speed / IN_MILLISECONDS;

                currentPos.Relocate(divineStarSpell->m_targets.GetDst()->_position);

                currentPos.m_positionX -= distance * std::cos(currentPos.GetOrientation());
                currentPos.m_positionY -= distance * std::sin(currentPos.GetOrientation());

                Trinity::NormalizeMapCoord(currentPos.m_positionX);
                Trinity::NormalizeMapCoord(currentPos.m_positionY);

                targets.erase(std::remove_if(targets.begin(), targets.end(), PlayerDistanceCheck(currentPos)), targets.end());

                _setTargets = true;
            }

            void SetTargets(std::vector<WorldObject*>& targets)
            {
                if (!_setTargets)
                    targets.clear();
                else
                    targets.erase(std::remove_if(targets.begin(), targets.end(), PlayerDistanceCheck(currentPos)), targets.end());
            }

            void Register() override
            {
                OnObjectAreaTargetSelect += SpellObjectAreaTargetSelectFn(spell_pri_divine_star_aoe_SpellScript::FilterTargets, EFFECT_0, TARGET_UNIT_DEST_AREA_ENEMY);
                OnObjectAreaTargetSelect += SpellObjectAreaTargetSelectFn(spell_pri_divine_star_aoe_SpellScript::SetTargets, EFFECT_1, TARGET_UNIT_DEST_AREA_ALLY);
            }

        private:
            Position currentPos;
            bool _setTargets;
        };

        SpellScript* GetSpellScript() const
        {
            return new spell_pri_divine_star_aoe_SpellScript();
        }
};

// 47540 - Penance
class spell_pri_penance : public SpellScriptLoader
{
    public:
        spell_pri_penance() : SpellScriptLoader("spell_pri_penance") { }

        class spell_pri_penance_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_pri_penance_SpellScript);

            bool Load() override
            {
                return GetCaster()->GetTypeId() == TYPEID_PLAYER;
            }

            bool Validate(SpellInfo const* spellInfo) override
            {
                SpellInfo const* firstRankSpellInfo = sSpellMgr->GetSpellInfo(SPELL_PRIEST_PENANCE);
                if (!firstRankSpellInfo)
                    return false;

                // can't use other spell than this penance due to spell_ranks dependency
                if (!spellInfo->IsRankOf(firstRankSpellInfo))
                    return false;

                uint8 rank = spellInfo->GetRank();
                if (!sSpellMgr->GetSpellWithRank(SPELL_PRIEST_PENANCE_DAMAGE, rank, true))
                    return false;
                if (!sSpellMgr->GetSpellWithRank(SPELL_PRIEST_PENANCE_HEAL, rank, true))
                    return false;
                if (!sSpellMgr->GetSpellInfo(SPELL_PRIEST_DIVINE_INSIGHT_TALENT))
                    return false;
                if (!sSpellMgr->GetSpellInfo(SPELL_PRIEST_DIVINE_INSIGHT_DISCIPLINE))
                    return false;
                if (!sSpellMgr->GetSpellInfo(SPELL_PRIEST_T11_HOLY_4P))
                    return false;
                if (!sSpellMgr->GetSpellInfo(SPELL_PRIEST_CHAKRA_FLOW))
                    return false;
                if (!sSpellMgr->GetSpellInfo(SPELL_PRIEST_INDULGENECE_OF_THE_PENITENT))
                    return false;

                return true;
            }

            void HandleDummy(SpellEffIndex /*effIndex*/)
            {
                Unit* caster = GetCaster();
                if (Unit* unitTarget = GetHitUnit())
                {
                    if (!unitTarget->IsAlive())
                        return;

                    uint8 rank = GetSpellInfo()->GetRank();

                    if (caster->IsFriendlyTo(unitTarget))
                    {
                        caster->CastSpell(unitTarget, sSpellMgr->GetSpellWithRank(SPELL_PRIEST_PENANCE_HEAL, rank), TRIGGERED_DISALLOW_PROC_EVENTS);

                        // Item - Priest T11 Healer 4P Bonus
                        if (caster->HasAura(SPELL_PRIEST_T11_HOLY_4P))
                            if (Player* player = caster->ToPlayer())
                            {
                                uint32 spellID = 0;

                                uint32 spec = player->GetSpecId(player->GetActiveTalentGroup());
                                switch (spec)
                                {
                                    case CHAR_SPECIALIZATION_PRIEST_DISCIPLINE:
                                        spellID = SPELL_PRIEST_INDULGENECE_OF_THE_PENITENT;
                                        break;
                                    case CHAR_SPECIALIZATION_PRIEST_HOLY:
                                        // Chakra
                                        if (caster->GetAuraEffect(SPELL_AURA_ADD_PCT_MODIFIER, SPELLFAMILY_PRIEST, 0x00000000, 0x00001000, 0x00000000, 0x00000000))
                                            spellID = SPELL_PRIEST_CHAKRA_FLOW;
                                        break;
                                    default:
                                        break;
                                    }

                                if (spellID)
                                    player->CastSpell(player, spellID, true);
                            }
                    }
                    else
                        caster->CastSpell(unitTarget, sSpellMgr->GetSpellWithRank(SPELL_PRIEST_PENANCE_DAMAGE, rank), TRIGGERED_DISALLOW_PROC_EVENTS);

                    // Divine Insight (Discipline)
                    if (Player* player = caster->ToPlayer())
                        if (player->GetSpecId(player->GetActiveTalentGroup()) == CHAR_SPECIALIZATION_PRIEST_DISCIPLINE)
                            if (AuraEffect const* aurEff = player->GetAuraEffect(SPELL_PRIEST_DIVINE_INSIGHT_TALENT, EFFECT_2))
                                if (Math::RollUnder(aurEff->GetAmount()))
                                    player->CastSpell(player, SPELL_PRIEST_DIVINE_INSIGHT_DISCIPLINE, true, nullptr, aurEff);
                }
            }

            SpellCastResult CheckCast()
            {
                Player* caster = GetCaster()->ToPlayer();
                if (Unit* target = GetExplTargetUnit())
                {
                    if (!caster->IsFriendlyTo(target))
                    {
                        if (!caster->IsValidAttackTarget(target))
                            return SPELL_FAILED_BAD_TARGETS;

                        if (!caster->isInFront(target))
                            return SPELL_FAILED_UNIT_NOT_INFRONT;
                    }
                    else if (!caster->isInFront(target))
                        return SPELL_FAILED_UNIT_NOT_INFRONT;
                }

                return SPELL_CAST_OK;
            }

            void Register() override
            {
                OnEffectHitTarget += SpellEffectFn(spell_pri_penance_SpellScript::HandleDummy, EFFECT_0, SPELL_EFFECT_DUMMY);
                OnCheckCast += SpellCheckCastFn(spell_pri_penance_SpellScript::CheckCast);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_pri_penance_SpellScript;
        }
};

// 596 - Prayer of Healing
// 2060 - Greater Heal
// 30604 - Prayer of Healing
// 78966 - Prayer of Healing
class spell_pri_greater_heal_or_prayer_of_healing : public SpellScriptLoader
{
    public:
        spell_pri_greater_heal_or_prayer_of_healing() : SpellScriptLoader("spell_pri_greater_heal_or_prayer_of_healing") { }

        class spell_pri_greater_heal_or_prayer_of_healing_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_pri_greater_heal_or_prayer_of_healing_SpellScript);

            bool Validate(SpellInfo const* spellInfo) override
            {
                if (!sSpellMgr->GetSpellInfo(SPELL_PRIEST_DIVINE_INSIGHT_TALENT))
                    return false;
                if (!sSpellMgr->GetSpellInfo(SPELL_PRIEST_DIVINE_INSIGHT_HOLY))
                    return false;

                return true;
            }

            void HandleHeal(SpellEffIndex /*effIndex*/)
            {
                // Divine Insight (Discipline)
                if (Unit* caster = GetCaster())
                    if (Player* player = caster->ToPlayer())
                        if (player->GetSpecId(player->GetActiveTalentGroup()) == CHAR_SPECIALIZATION_PRIEST_HOLY)
                            if (AuraEffect const* aurEff = player->GetAuraEffect(SPELL_PRIEST_DIVINE_INSIGHT_TALENT, EFFECT_0))
                                if (Math::RollUnder(aurEff->GetAmount()))
                                    player->CastSpell(player, SPELL_PRIEST_DIVINE_INSIGHT_HOLY, true, nullptr, aurEff);
            }

            void Register() override
            {
                OnEffectHitTarget += SpellEffectFn(spell_pri_greater_heal_or_prayer_of_healing_SpellScript::HandleHeal, EFFECT_0, SPELL_EFFECT_HEAL);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_pri_greater_heal_or_prayer_of_healing_SpellScript;
        }
};

// 109175 - Divine Insight
class spell_pri_divine_insight : public SpellScriptLoader
{
    public:
        spell_pri_divine_insight() : SpellScriptLoader("spell_pri_divine_insight") { }

        class spell_pri_divine_insight_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_pri_divine_insight_AuraScript);

            bool Load() override
            {
                return GetCaster()->GetTypeId() == TYPEID_PLAYER;
            }

            void HandleEffectProc(AuraEffect const* /*aurEff*/, ProcEventInfo& /*eventInfo*/)
            {
                Unit* caster = GetCaster();
                if (!caster)
                    return;

                if (Player* player = caster->ToPlayer())
                    if (player->GetSpecId(player->GetActiveTalentGroup()) == CHAR_SPECIALIZATION_PRIEST_SHADOW)
                        player->GetSpellHistory()->ResetCooldown(SPELL_PRIEST_MIND_BLAST);
            }

            void Register() override
            {
                OnEffectProc += AuraEffectProcFn(spell_pri_divine_insight_AuraScript::HandleEffectProc, EFFECT_1, SPELL_AURA_PROC_TRIGGER_SPELL);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_pri_divine_insight_AuraScript();
        }
};

// 121135 - Cascade
// 127632 - Cascade
class spell_pri_cascade_ability : public SpellScriptLoader
{
    public:
        spell_pri_cascade_ability() : SpellScriptLoader("spell_pri_cascade_ability") { }

        class spell_pri_cascade_ability_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_pri_cascade_ability_SpellScript);

            bool Validate(SpellInfo const* spellInfo) override
            {
                if (!sSpellMgr->GetSpellInfo(SPELL_PRIEST_CASCADE_MISSILE_HEAL_HOLY))
                    return false;
                if (!sSpellMgr->GetSpellInfo(SPELL_PRIEST_CASCADE_MISSILE_HEAL_SHADOW))
                    return false;
                if (!sSpellMgr->GetSpellInfo(SPELL_PRIEST_CASCADE_MISSILE_DAMAGE_HOLY))
                    return false;
                if (!sSpellMgr->GetSpellInfo(SPELL_PRIEST_CASCADE_MISSILE_DAMAGE_SHADOW))
                    return false;
                return true;
            }

            void HandleTriggerSpellHit(SpellEffIndex effIndex)
            {
                Unit* caster = GetCaster();
                if (!caster)
                    return;

                if (Unit* target = GetHitUnit())
                {
                    uint32 spellID = 0;

                    bool IsEnemyTarget = true;
                    if (caster->IsValidAssistTarget(target))
                    {
                        IsEnemyTarget = false;
                        spellID = m_scriptSpellId == SPELL_PRIEST_CASCADE_ABILITY_HOLY ? SPELL_PRIEST_CASCADE_MISSILE_HEAL_HOLY : SPELL_PRIEST_CASCADE_MISSILE_HEAL_SHADOW;
                    }
                    else if (caster->IsValidAttackTarget(target))
                        spellID = m_scriptSpellId == SPELL_PRIEST_CASCADE_ABILITY_HOLY ? SPELL_PRIEST_CASCADE_MISSILE_DAMAGE_HOLY : SPELL_PRIEST_CASCADE_MISSILE_DAMAGE_SHADOW;

                    if (!spellID)
                        return;

                    int32 charges = GetEffectValue();

                    CustomSpellValues values;
                    values.AddSpellMod(SPELLVALUE_BASE_POINT1, int32(IsEnemyTarget));
                    values.AddSpellMod(SPELLVALUE_AURA_CHARGES, charges);

                    caster->CastCustomSpell(spellID, values, target);
                }
            }

            void Register() override
            {
                OnEffectHitTarget += SpellEffectFn(spell_pri_cascade_ability_SpellScript::HandleTriggerSpellHit, EFFECT_0, SPELL_EFFECT_DUMMY);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_pri_cascade_ability_SpellScript();
        }
};

// 121146 - Cascade
// 127627 - Cascade
// 127628 - Cascade
// 120785 - Cascade
class spell_pri_cascade_missile : public SpellScriptLoader
{
    public:
        spell_pri_cascade_missile() : SpellScriptLoader("spell_pri_cascade_missile") { }

        class spell_pri_cascade_missile_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_pri_cascade_missile_SpellScript);

            bool Validate(SpellInfo const* spellInfo) override
            {
                if (!sSpellMgr->GetSpellInfo(SPELL_PRIEST_CASCADE_HEAL_HOLY))
                    return false;
                if (!sSpellMgr->GetSpellInfo(SPELL_PRIEST_CASCADE_HEAL_SHADOW))
                    return false;
                if (!sSpellMgr->GetSpellInfo(SPELL_PRIEST_CASCADE_SEARCHER_HOLY))
                    return false;
                if (!sSpellMgr->GetSpellInfo(SPELL_PRIEST_CASCADE_SEARCHER_SHADOW))
                    return false;
                return true;
            }

            void HandleScript(SpellEffIndex /*effIndex*/)
            {
                Unit* caster = GetCaster();
                if (!caster)
                    return;

                if (Unit* target = GetHitUnit())
                {
                    uint32 spellID = SPELL_PRIEST_CASCADE_HEAL_HOLY;
                    if (m_scriptSpellId == SPELL_PRIEST_CASCADE_MISSILE_HEAL_SHADOW)
                        spellID = SPELL_PRIEST_CASCADE_HEAL_SHADOW;

                    caster->CastSpell(target, spellID, true);
                }
            }

            void HandleCooldown(SpellEffIndex /*effIndex*/)
            {
                Unit* caster = GetCaster();
                if (!caster)
                    return;

                if (Unit* target = GetHitUnit())
                {
                    uint32 cooldownSpellID = SPELL_PRIEST_CASCADE_COOLDOWN_HOLY;
                    if (m_scriptSpellId == SPELL_PRIEST_CASCADE_MISSILE_HEAL_SHADOW || m_scriptSpellId == SPELL_PRIEST_CASCADE_MISSILE_DAMAGE_SHADOW)
                        cooldownSpellID = SPELL_PRIEST_CASCADE_COOLDOWN_SHADOW;

                    caster->CastSpell(target, cooldownSpellID, true);
                }
            }

            void HandleTrigger()
            {
                Unit* caster = GetCaster();
                if (!caster)
                    return;

                if (Unit* target = GetHitUnit())
                {
                    if (!target->IsAlive())
                        return;

                    SpellValue const* spellValue = GetSpellValue();
                    if (!spellValue)
                        return;

                    int8 charges = spellValue->AuraCharges;
                    if (!charges)
                        return;

                    --charges;

                    Unit* originalCaster = GetOriginalCaster();
                    if (!originalCaster)
                        return;

                    uint32 searcherSpellID = SPELL_PRIEST_CASCADE_SEARCHER_HOLY;
                    if (m_scriptSpellId == SPELL_PRIEST_CASCADE_MISSILE_HEAL_SHADOW || m_scriptSpellId == SPELL_PRIEST_CASCADE_MISSILE_DAMAGE_SHADOW)
                        searcherSpellID = SPELL_PRIEST_CASCADE_SEARCHER_SHADOW;

                    CustomSpellValues values;
                    values.AddSpellMod(SPELLVALUE_BASE_POINT1, spellValue->EffectBasePoints[EFFECT_1]);
                    values.AddSpellMod(SPELLVALUE_AURA_CHARGES, charges);

                    target->CastCustomSpell(searcherSpellID, values, target, TRIGGERED_FULL_MASK, nullptr, nullptr, originalCaster->GetGUID());
                }
            }

            void Register() override
            {
                if (m_scriptSpellId == SPELL_PRIEST_CASCADE_MISSILE_HEAL_HOLY || m_scriptSpellId == SPELL_PRIEST_CASCADE_MISSILE_HEAL_SHADOW)
                    OnEffectHitTarget += SpellEffectFn(spell_pri_cascade_missile_SpellScript::HandleScript, EFFECT_0, SPELL_EFFECT_SCRIPT_EFFECT);
                OnEffectHitTarget += SpellEffectFn(spell_pri_cascade_missile_SpellScript::HandleCooldown, EFFECT_1, SPELL_EFFECT_APPLY_AURA);
                AfterHit += SpellHitFn(spell_pri_cascade_missile_SpellScript::HandleTrigger);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_pri_cascade_missile_SpellScript();
        }
};

// 120786 - Cascade
// 127630 - Cascade
class spell_pri_cascade_searcher : public SpellScriptLoader
{
    public:
        spell_pri_cascade_searcher() : SpellScriptLoader("spell_pri_cascade_searcher") { }

        class spell_pri_cascade_searcher_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_pri_cascade_searcher_SpellScript);

            bool Validate(SpellInfo const* spellInfo) override
            {
                if (!sSpellMgr->GetSpellInfo(SPELL_PRIEST_CASCADE_MISSILE_HEAL_HOLY))
                    return false;
                if (!sSpellMgr->GetSpellInfo(SPELL_PRIEST_CASCADE_MISSILE_HEAL_SHADOW))
                    return false;
                if (!sSpellMgr->GetSpellInfo(SPELL_PRIEST_CASCADE_MISSILE_DAMAGE_HOLY))
                    return false;
                if (!sSpellMgr->GetSpellInfo(SPELL_PRIEST_CASCADE_MISSILE_DAMAGE_SHADOW))
                    return false;
                return true;
            }

            bool Load() override
            {
                if (SpellValue const* spellValue = GetSpellValue())
                    _isEnemy = spellValue->EffectBasePoints[EFFECT_1] != 0;
                return true;
            }

            void FilterTargets(std::vector<WorldObject*>& targets)
            {
                Unit* caster = GetOriginalCaster();
                Unit* originalCaster = GetOriginalCaster();
                SpellValue const* spellValue = GetSpellValue();
                if (!spellValue || !originalCaster || !caster || !caster->IsAlive())
                {
                    targets.clear();
                    return;
                }

                uint32 cooldownSpellID = SPELL_PRIEST_CASCADE_COOLDOWN_HOLY;
                if (m_scriptSpellId == SPELL_PRIEST_CASCADE_SEARCHER_SHADOW)
                    cooldownSpellID = SPELL_PRIEST_CASCADE_COOLDOWN_SHADOW;

                targets.erase(std::remove_if(targets.begin(), targets.end(), Trinity::UnitAuraCheck(true, cooldownSpellID)), targets.end());

                bool IsEnemyTarget = _isEnemy;

                targets.erase(std::remove_if(targets.begin(), targets.end(), [originalCaster, IsEnemyTarget](WorldObject* obj)
                {
                    if (Unit* unit = obj->ToUnit())
                        if (unit->IsAlive())
                        {
                            if (IsEnemyTarget)
                                return !originalCaster->IsValidAttackTarget(unit);
                            else
                                return !originalCaster->IsValidAssistTarget(unit);
                        }

                    return true;
                }), targets.end());

                if (!targets.empty())
                {
                    std::sort(targets.begin(), targets.end(), Trinity::DistanceCompareOrderPred(caster, false));

                    if (targets.size() > 2)
                        targets.resize(2);
                }
            }

            void HandleDummy(SpellEffIndex effIndex)
            {
                SpellValue const* spellValue = GetSpellValue();
                if (!spellValue)
                    return;

                int8 charges = spellValue->AuraCharges;
                if (!charges)
                    return;

                Unit* caster = GetCaster();
                Unit* originalCaster = GetOriginalCaster();
                if (!caster ||  !caster->IsAlive() || !originalCaster)
                    return;

                if (Unit* target = GetHitUnit())
                {
                    uint32 spellID = 0;

                    if (_isEnemy)
                        spellID = m_scriptSpellId == SPELL_PRIEST_CASCADE_SEARCHER_HOLY ? SPELL_PRIEST_CASCADE_MISSILE_DAMAGE_HOLY : SPELL_PRIEST_CASCADE_MISSILE_DAMAGE_SHADOW;
                    else
                        spellID = m_scriptSpellId == SPELL_PRIEST_CASCADE_SEARCHER_HOLY ? SPELL_PRIEST_CASCADE_MISSILE_HEAL_HOLY : SPELL_PRIEST_CASCADE_MISSILE_HEAL_SHADOW;

                    if (!spellID)
                        return;

                    CustomSpellValues values;
                    values.AddSpellMod(SPELLVALUE_BASE_POINT1, GetEffectValue());
                    values.AddSpellMod(SPELLVALUE_AURA_CHARGES, charges);

                    caster->CastCustomSpell(spellID, values, target, TRIGGERED_FULL_MASK, nullptr, nullptr, GetOriginalCaster()->GetGUID());
                }
            }

            void Register() override
            {
                OnObjectAreaTargetSelect += SpellObjectAreaTargetSelectFn(spell_pri_cascade_searcher_SpellScript::FilterTargets, EFFECT_1, TARGET_UNIT_DEST_AREA_ENTRY);
                OnEffectHitTarget += SpellEffectFn(spell_pri_cascade_searcher_SpellScript::HandleDummy, EFFECT_1, SPELL_EFFECT_DUMMY);
            }

        private:
            bool _isEnemy;
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_pri_cascade_searcher_SpellScript();
        }
};

// 108945 - Angelic Bulwark
class spell_pri_angelic_bulwark : public SpellScriptLoader
{
    public:
        spell_pri_angelic_bulwark() : SpellScriptLoader("spell_pri_angelic_bulwark") { }

        class spell_pri_angelic_bulwark_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_pri_angelic_bulwark_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                if (!sSpellMgr->GetSpellInfo(SPELL_PRIEST_ANGELIC_BULWARK_COOLDOWN))
                    return false;
                return true;
            }

            bool CheckProc(ProcEventInfo& eventInfo)
            {
                Unit* target = GetTarget();
                if (!target)
                    return false;

                DamageInfo* damageInfo = eventInfo.GetDamageInfo();
                if (!damageInfo || !damageInfo->GetDamage())
                    return false;

                if (target->GetHealthPct() >= GetEffect(EFFECT_0)->GetAmount())
                    return false;

                return true;
            }

            void HandleEffectProc(AuraEffect const* aurEff, ProcEventInfo& /*eventInfo*/)
            {
                Unit* target = GetTarget();
                if (!target)
                    return;

                target->CastSpell(target, SPELL_PRIEST_ANGELIC_BULWARK_COOLDOWN, true, nullptr, aurEff);
            }

            void Register() override
            {
                DoCheckProc += AuraCheckProcFn(spell_pri_angelic_bulwark_AuraScript::CheckProc);
                OnEffectProc += AuraEffectProcFn(spell_pri_angelic_bulwark_AuraScript::HandleEffectProc, EFFECT_0, SPELL_AURA_PROC_TRIGGER_SPELL);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_pri_angelic_bulwark_AuraScript();
        }
};

// 114214 - Angelic Bulwark
class spell_pri_angelic_bulwark_absorb : public SpellScriptLoader
{
    public:
        spell_pri_angelic_bulwark_absorb() : SpellScriptLoader("spell_pri_angelic_bulwark_absorb") { }

        class spell_pri_angelic_bulwark_absorb_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_pri_angelic_bulwark_absorb_AuraScript);

            void CalculateAmount(AuraEffect const* /*aurEff*/, int32& amount, bool& canBeRecalculated)
            {
                canBeRecalculated = false;

                if (Unit* caster = GetCaster())
                    amount = int32(CalculatePct(caster->GetMaxHealth(), 20.0f));
            }

            void Register() override
            {
                DoEffectCalcAmount += AuraEffectCalcAmountFn(spell_pri_angelic_bulwark_absorb_AuraScript::CalculateAmount, EFFECT_0, SPELL_AURA_SCHOOL_ABSORB);
            }
        };

        AuraScript* GetAuraScript() const
        {
            return new spell_pri_angelic_bulwark_absorb_AuraScript;
        }
};

// 129250 - Power Word: Solace
class spell_pri_power_word_solace : public SpellScriptLoader
{
    public:
        spell_pri_power_word_solace() : SpellScriptLoader("spell_pri_power_word_solace") { }

        class spell_pri_power_word_solace_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_pri_power_word_solace_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                if (!sSpellMgr->GetSpellInfo(SPELL_PRIEST_POWER_WORD_SOLACE_ENERGIZE))
                    return false;
                return true;
            }

            bool Load() override
            {
                _solaceHealSpellInfo = sSpellMgr->GetSpellInfo(SPELL_PRIEST_POWER_WORD_SOLACE_HEAL);
                return _solaceHealSpellInfo != nullptr;
            }

            void HandleDummy(SpellEffIndex /*effIndex*/)
            {
                if (Unit* caster = GetCaster())
                {
                    caster->CastSpell(caster, SPELL_PRIEST_POWER_WORD_SOLACE_ENERGIZE, true);

                    if (Unit* target = GetHitUnit())
                    {
                        int32 damage = GetHitDamage();

                        SpellCastTargets targets;
                        targets.SetDst(target->GetPositionX(), target->GetPositionY(), target->GetPositionZ(), caster->GetOrientation());

                        CustomSpellValues values;
                        values.AddSpellMod(SPELLVALUE_BASE_POINT0, damage);

                        caster->CastSpell(targets, _solaceHealSpellInfo, &values, TRIGGERED_FULL_MASK, nullptr, nullptr, ObjectGuid::Empty);
                    }
                }
            }

            void Register()
            {
                OnEffectHitTarget += SpellEffectFn(spell_pri_power_word_solace_SpellScript::HandleDummy, EFFECT_0, SPELL_EFFECT_SCHOOL_DAMAGE);
            }

        private:
            SpellInfo const* _solaceHealSpellInfo;
        };

        SpellScript* GetSpellScript() const
        {
            return new spell_pri_power_word_solace_SpellScript();
        }
};

// 140816 - Power Word: Solace
class spell_pri_power_word_solace_heal : public SpellScriptLoader
{
    public:
        spell_pri_power_word_solace_heal() : SpellScriptLoader("spell_pri_power_word_solace_heal") { }

        class spell_pri_power_word_solace_heal_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_pri_power_word_solace_heal_SpellScript);

            void FilterTargets(std::vector<WorldObject*>& targets)
            {
                std::sort(targets.begin(), targets.end(), Trinity::HealthPctOrderPred());

                targets.resize(1);
            }

            void RecalculateHeal(SpellEffIndex /*effIndex*/)
            {
                if (Unit* caster = GetCaster())
                    if (Unit* target = GetHitUnit())
                        if (caster->GetGUID() == target->GetGUID())
                            SetHitDamage(GetHitDamage() / 2);
            }

            void Register() override
            {
                OnObjectAreaTargetSelect += SpellObjectAreaTargetSelectFn(spell_pri_power_word_solace_heal_SpellScript::FilterTargets, EFFECT_0, TARGET_UNIT_DEST_AREA_ALLY);
                OnEffectHitTarget += SpellEffectFn(spell_pri_power_word_solace_heal_SpellScript::RecalculateHeal, EFFECT_0, SPELL_EFFECT_HEAL);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_pri_power_word_solace_heal_SpellScript();
        }
};

// 109186 - From Darkness, Comes Light
class spell_pri_from_darkness_comes_light : public SpellScriptLoader
{
    public:
        spell_pri_from_darkness_comes_light() : SpellScriptLoader("spell_pri_from_darkness_comes_light") { }

        class spell_pri_from_darkness_comes_light_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_pri_from_darkness_comes_light_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                if (!sSpellMgr->GetSpellInfo(SPELL_PRIEST_SURGE_OF_DARKNESS) ||
                    !sSpellMgr->GetSpellInfo(SPELL_PRIEST_SURGE_OF_LIGHT))
                    return false;
                return true;
            }

            bool Load() override
            {
                _isShadowSurge = false;
                return true;
            }

            bool CheckProc(ProcEventInfo& eventInfo)
            {
                Unit* caster = GetCaster();
                if (!caster)
                    return false;

                Player* player = caster->ToPlayer();
                if (!player)
                    return false;

                if (player->GetClass() != CLASS_PRIEST)
                    return false;

                SpellInfo const* spellInfo = eventInfo.GetSpellInfo();
                if (!spellInfo)
                    return false;

                DamageInfo* damageInfo = eventInfo.GetDamageInfo();
                HealInfo* healInfo = eventInfo.GetHealInfo();

                if (player->GetSpecId(player->GetActiveTalentGroup()) == CHAR_SPECIALIZATION_PRIEST_SHADOW)
                {
                    if (!damageInfo || !damageInfo->GetDamage())
                        return false;

                    // Vampiric Touch
                    if ((spellInfo->SpellFamilyFlags[0] & 0x00000400) == 0)
                        return false;

                    if (damageInfo->GetDamageType() != DOT)
                        return false;

                    _isShadowSurge = true;
                }
                else
                {
                    // Damage from Smite
                    if ((!damageInfo || !damageInfo->GetDamage() || (spellInfo->SpellFamilyFlags[0] & 0x00000040) == 0) && (!healInfo || !healInfo->GetEffectiveHeal()))
                        return false;
                }

                return true;
            }

            void HandleEffectProc(AuraEffect const* aurEff, ProcEventInfo& eventInfo)
            {
                PreventDefaultAction();

                Unit* caster = GetCaster();
                if (!caster)
                    return;

                uint32 chance = 15;
                uint32 spellID = SPELL_PRIEST_SURGE_OF_LIGHT;

                if (_isShadowSurge)
                {
                    chance = 20;
                    spellID = SPELL_PRIEST_SURGE_OF_DARKNESS;
                }

                if (Math::RollUnder(chance))
                    caster->CastCustomSpell(spellID, SPELLVALUE_AURA_STACK, 2, caster, TRIGGERED_FULL_MASK);
            }

            void Register() override
            {
                DoCheckProc += AuraCheckProcFn(spell_pri_from_darkness_comes_light_AuraScript::CheckProc);
                OnEffectProc += AuraEffectProcFn(spell_pri_from_darkness_comes_light_AuraScript::HandleEffectProc, EFFECT_0, SPELL_AURA_DUMMY);
            }

        private:
            bool _isShadowSurge;
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_pri_from_darkness_comes_light_AuraScript();
        }
};

// 37594 - Greater Heal Refund
class spell_pri_item_greater_heal_refund : public SpellScriptLoader
{
    public:
        spell_pri_item_greater_heal_refund() : SpellScriptLoader("spell_pri_item_greater_heal_refund") { }

        class spell_pri_item_greater_heal_refund_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_pri_item_greater_heal_refund_AuraScript);

            bool CheckProc(ProcEventInfo& eventInfo)
            {
                Unit* caster = GetCaster();
                if (!caster)
                    return false;

                HealInfo* healInfo = eventInfo.GetHealInfo();
                if (!healInfo || !healInfo->GetEffectiveHeal())
                    return false;

                Unit* target = healInfo->GetTarget();
                if (!target)
                    return false;

                if (target->GetHealth() < target->GetMaxHealth())
                    return false;

                return true;
            }

            void Register() override
            {
                DoCheckProc += AuraCheckProcFn(spell_pri_item_greater_heal_refund_AuraScript::CheckProc);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_pri_item_greater_heal_refund_AuraScript();
        }
};

// 70770 - Item - Priest T10 Healer 2P Bonus
class spell_pri_t10_2p_healer_bonus : public SpellScriptLoader
{
    public:
        spell_pri_t10_2p_healer_bonus() : SpellScriptLoader("spell_pri_t10_2p_healer_bonus") { }

        class spell_pri_t10_2p_healer_bonus_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_pri_t10_2p_healer_bonus_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                if (!sSpellMgr->GetSpellInfo(SPELL_PRIEST_BLESSED_HEALING))
                    return false;
                return true;
            }

            bool CheckProc(ProcEventInfo& eventInfo)
            {
                Unit* caster = GetCaster();
                if (!caster)
                    return false;

                HealInfo* healInfo = eventInfo.GetHealInfo();
                if (!healInfo || !healInfo->GetEffectiveHeal())
                    return false;

                SpellInfo const* spellInfo = healInfo->GetSpellInfo();
                if (!spellInfo)
                    return false;

                // Flash Heal
                if (spellInfo->SpellFamilyFlags[0] != 0x10000800)
                    return false;

                return true;
            }

            void HandleEffectProc(AuraEffect const* aurEff, ProcEventInfo& eventInfo)
            {
                PreventDefaultAction();

                Unit* caster = GetCaster();
                if (!caster)
                    return;

                HealInfo* healInfo = eventInfo.GetHealInfo();
                SpellInfo const* spellInfo = sSpellMgr->GetSpellInfo(SPELL_PRIEST_BLESSED_HEALING);

                int32 heal = CalculatePct(healInfo->GetEffectiveHeal(), aurEff->GetAmount());
                heal /= spellInfo->GetMaxDuration() / spellInfo->GetEffect(EFFECT_0)->Amplitude;

                caster->CastCustomSpell(healInfo->GetTarget(), SPELL_PRIEST_BLESSED_HEALING, &heal, nullptr, nullptr, true, nullptr, aurEff);
            }

            void Register() override
            {
                DoCheckProc += AuraCheckProcFn(spell_pri_t10_2p_healer_bonus_AuraScript::CheckProc);
                OnEffectProc += AuraEffectProcFn(spell_pri_t10_2p_healer_bonus_AuraScript::HandleEffectProc, EFFECT_0, SPELL_AURA_DUMMY);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_pri_t10_2p_healer_bonus_AuraScript();
        }
};

// 99155 Shadowflame
class spell_pri_shadowflame : public SpellScriptLoader
{
    public:
        spell_pri_shadowflame() : SpellScriptLoader("spell_pri_shadowflame") { }

        class spell_pri_shadowflame_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_pri_shadowflame_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                if (!sSpellMgr->GetSpellInfo(SPELL_PRIEST_SHADOWFLAME_DAMAGE))
                    return false;
                return true;
            }

            void HandleEffectProc(AuraEffect const* aurEff, ProcEventInfo& eventInfo)
            {
                PreventDefaultAction();

                Unit* caster = GetCaster();
                if (!caster)
                    return;

                DamageInfo* damageInfo = eventInfo.GetDamageInfo();
                if (!damageInfo)
                    return;

                uint32 damage = damageInfo->GetDamage();
                if (!damage)
                    return;

                int32 dmg = CalculatePct(damage, aurEff->GetAmount());

                caster->CastCustomSpell(damageInfo->GetVictim(), SPELL_PRIEST_SHADOWFLAME_DAMAGE, &dmg, nullptr, nullptr, true, nullptr, aurEff);
            }

            void Register() override
            {
                OnEffectProc += AuraEffectProcFn(spell_pri_shadowflame_AuraScript::HandleEffectProc, EFFECT_0, SPELL_AURA_DUMMY);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_pri_shadowflame_AuraScript();
        }
};

// 138301 - Item - Priest T15 Healer 4P Bonus
class spell_pri_t15_4p_healer_bonus : public SpellScriptLoader
{
    public:
        spell_pri_t15_4p_healer_bonus() : SpellScriptLoader("spell_pri_t15_4p_healer_bonus") { }

        class spell_pri_t15_4p_healer_bonus_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_pri_t15_4p_healer_bonus_AuraScript);

            bool CheckProc(ProcEventInfo& eventInfo)
            {
                Unit* caster = GetCaster();
                if (!caster)
                    return false;

                HealInfo* healInfo = eventInfo.GetHealInfo();
                if (!healInfo || !healInfo->GetEffectiveHeal())
                    return false;

                SpellInfo const* spellInfo = healInfo->GetSpellInfo();
                if (!spellInfo)
                    return false;

                // Penance and Circle of Healing
                if ((spellInfo->SpellFamilyFlags[1] & 0x00018000) == 0 && spellInfo->SpellFamilyFlags[0] != 0x10000000)
                    return false;

                return true;
            }

            void Register() override
            {
                DoCheckProc += AuraCheckProcFn(spell_pri_t15_4p_healer_bonus_AuraScript::CheckProc);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_pri_t15_4p_healer_bonus_AuraScript();
        }
};

// 138302 - Golden Apparition
class spell_pri_golden_apparition : public SpellScriptLoader
{
    public:
        spell_pri_golden_apparition() : SpellScriptLoader("spell_pri_golden_apparition") { }

        class spell_pri_golden_apparition_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_pri_golden_apparition_SpellScript);

            void FilterTargets(std::vector<WorldObject*>& targets)
            {
                Unit* caster = GetCaster();

                targets.erase(std::remove_if(targets.begin(), targets.end(), [caster](WorldObject* object) -> bool
                {
                    if (Unit* unit = object->ToUnit())
                        if (unit->IsInPartyWith(caster) && unit->GetHealthPct() < 100.0f)
                            return false;
                    return true;
                }), targets.end());

                if (targets.size() > 1)
                {
                    std::sort(targets.begin(), targets.end(), Trinity::HealthPctOrderPred());
                    targets.resize(1);
                }
            }

            void Register() override
            {
                OnObjectAreaTargetSelect += SpellObjectAreaTargetSelectFn(spell_pri_golden_apparition_SpellScript::FilterTargets, EFFECT_0, TARGET_UNIT_DEST_AREA_ALLY);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_pri_golden_apparition_SpellScript();
        }
};

// 148859 - Shadowy Apparition
class spell_pri_shadowy_apparition_damage : public SpellScriptLoader
{
    public:
        spell_pri_shadowy_apparition_damage() : SpellScriptLoader("spell_pri_shadowy_apparition_damage") { }

        class spell_pri_shadowy_apparition_damage_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_pri_shadowy_apparition_damage_SpellScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                if (!sSpellMgr->GetSpellInfo(SPELL_PRIEST_T15_SHADOW_2P))
                    return false;
                return true;
            }

            void HandleHit(SpellEffIndex /*effIndex*/)
            {
                if (Unit* caster = GetCaster())
                    if (Unit* target = GetHitUnit())
                    {
                        // Item - Priest T15 Shadow 2P Bonus
                        if (Aura const* aura = caster->GetAura(SPELL_PRIEST_T15_SHADOW_2P))
                        {
                            AuraEffect const* aurEff1 = aura->GetEffect(EFFECT_0);
                            AuraEffect const* aurEff2 = aura->GetEffect(EFFECT_1);

                            if (aurEff1 && aurEff2)
                                if (Math::RollUnder(aurEff1->GetAmount()))
                                {
                                    // Shadow Word: Pain
                                    if (AuraEffect* shadowWord = target->GetAuraEffect(SPELL_AURA_PERIODIC_DAMAGE, SPELLFAMILY_PRIEST, 0x00008000, 0x0, 0x0, 0x0, caster->GetGUID()))
                                        if (Aura* base = shadowWord->GetBase())
                                            base->SetDuration(base->GetDuration() + (aurEff2->GetAmount() * IN_MILLISECONDS));

                                    // Vampiric Touch
                                    if (AuraEffect* vampiricTouch = target->GetAuraEffect(SPELL_AURA_PERIODIC_DAMAGE, SPELLFAMILY_PRIEST, 0x0, 0x00000400, 0x0, 0x0, caster->GetGUID()))
                                        if (Aura* base = vampiricTouch->GetBase())
                                            base->SetDuration(base->GetDuration() + (aurEff2->GetAmount() * IN_MILLISECONDS));
                                }
                        }
                    }
            }

            void Register() override
            {
                OnEffectHitTarget += SpellEffectFn(spell_pri_shadowy_apparition_damage_SpellScript::HandleHit, EFFECT_0, SPELL_EFFECT_TRIGGER_MISSILE);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_pri_shadowy_apparition_damage_SpellScript();
        }
};

// 34914 - Vampiric Touch
class spell_pri_vampiric_touch : public SpellScriptLoader
{
    public:
        spell_pri_vampiric_touch() : SpellScriptLoader("spell_pri_vampiric_touch") { }

        class spell_pri_vampiric_touch_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_pri_vampiric_touch_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                if (!sSpellMgr->GetSpellInfo(SPELL_PRIEST_VAMPIRIC_TOUCH_ENERGIZE))
                    return false;
                if (!sSpellMgr->GetSpellInfo(SPELL_PRIEST_SIN_AND_PUNISHMENT))
                    return false;
                return true;
            }

            void HandleEffectProc(AuraEffect const* /*aurEff*/, ProcEventInfo& eventInfo)
            {
                PreventDefaultAction();

                if (Unit* caster = GetCaster())
                    if (DamageInfo* damageInfo = eventInfo.GetDamageInfo())
                        if (damageInfo->GetDamage())
                            caster->CastSpell(caster, SPELL_PRIEST_VAMPIRIC_TOUCH_ENERGIZE, true);
            }

            void HandleDispel(DispelInfo* dispelInfo)
            {
                if (Unit* caster = GetCaster())
                    caster->CastSpell(dispelInfo->GetDispeller(), SPELL_PRIEST_SIN_AND_PUNISHMENT, true);
            }

            void Register() override
            {
                OnEffectProc += AuraEffectProcFn(spell_pri_vampiric_touch_AuraScript::HandleEffectProc, EFFECT_0, SPELL_AURA_DUMMY);
                AfterDispel += AuraDispelFn(spell_pri_vampiric_touch_AuraScript::HandleDispel);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_pri_vampiric_touch_AuraScript();
        }
};

// 138158 - Item - Priest T15 Shadow 4P Bonus
class spell_pri_t15_4p_shadow_bonus : public SpellScriptLoader
{
    public:
        spell_pri_t15_4p_shadow_bonus() : SpellScriptLoader("spell_pri_t15_4p_shadow_bonus") { }

        class spell_pri_t15_4p_shadow_bonus_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_pri_t15_4p_shadow_bonus_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                if (!sSpellMgr->GetSpellInfo(SPELL_PRIEST_SHADOWY_APPARITION_PROC))
                    return false;
                return true;
            }

            void HandleEffectProc(AuraEffect const* /*aurEff*/, ProcEventInfo& eventInfo)
            {
                PreventDefaultAction();

                if (Unit* target = eventInfo.GetActionTarget())
                    GetCaster()->CastSpell(target, SPELL_PRIEST_SHADOWY_APPARITION_PROC, true);
            }

            void Register() override
            {
                OnEffectProc += AuraEffectProcFn(spell_pri_t15_4p_shadow_bonus_AuraScript::HandleEffectProc, EFFECT_0, SPELL_AURA_DUMMY);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_pri_t15_4p_shadow_bonus_AuraScript();
        }
};

// 145306 - Item - Priest T16 Healer 2P Bonus
class spell_pri_t16_2p_healer_bonus : public SpellScriptLoader
{
    public:
        spell_pri_t16_2p_healer_bonus() : SpellScriptLoader("spell_pri_t16_2p_healer_bonus") { }

        class spell_pri_t16_2p_healer_bonus_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_pri_t16_2p_healer_bonus_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                if (!sSpellMgr->GetSpellInfo(SPELL_PRIEST_DEVOUT_FAITH))
                    return false;
                return true;
            }

            void HandleEffectProc(AuraEffect const* /*aurEff*/, ProcEventInfo& /*eventInfo*/)
            {
                PreventDefaultAction();

                if (Unit* caster = GetCaster())
                    if (Aura* serendipity = caster->GetAura(SPELL_PRIEST_SERENDIPITY))
                    {
                        uint8 stacks = serendipity->GetStackAmount();

                        caster->CastCustomSpell(SPELL_PRIEST_DEVOUT_FAITH, SPELLVALUE_AURA_STACK, stacks, caster, true);
                    }
            }

            void Register() override
            {
                AfterEffectProc += AuraEffectProcFn(spell_pri_t16_2p_healer_bonus_AuraScript::HandleEffectProc, EFFECT_0, SPELL_AURA_DUMMY);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_pri_t16_2p_healer_bonus_AuraScript();
        }
};

// 145334 Item - Priest T16 Healer 4P Bonus
class spell_pri_t16_4p_healer_bonus : public SpellScriptLoader
{
    public:
        spell_pri_t16_4p_healer_bonus() : SpellScriptLoader("spell_pri_t16_4p_healer_bonus") { }

        class spell_pri_t16_4p_healer_bonus_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_pri_t16_4p_healer_bonus_AuraScript);

            bool Validate(SpellInfo const* /*spellInfo*/) override
            {
                if (!sSpellMgr->GetSpellInfo(SPELL_PRIEST_ABSOLUTION))
                    return false;
                return true;
            }

            bool CheckProc(ProcEventInfo& eventInfo)
            {
                Unit* caster = GetCaster();
                if (!caster)
                    return false;

                HealInfo* healInfo = eventInfo.GetHealInfo();
                if (!healInfo || !healInfo->GetEffectiveHeal())
                    return false;

                SpellInfo const* spellInfo = healInfo->GetSpellInfo();
                if (!spellInfo)
                    return false;

                // Circle of Healing and Prayer of Mending
                if (spellInfo->SpellFamilyFlags[0] != 0x10000000 && (spellInfo->SpellFamilyFlags[1] & 0x00000020) == 0)
                    return false;

                return true;
            }

            void HandleEffectProc(AuraEffect const* /*aurEff*/, ProcEventInfo& /*eventInfo*/)
            {
                PreventDefaultAction();

                if (Unit* caster = GetCaster())
                    caster->CastSpell(caster, SPELL_PRIEST_ABSOLUTION, true);
            }

            void Register() override
            {
                DoCheckProc += AuraCheckProcFn(spell_pri_t16_4p_healer_bonus_AuraScript::CheckProc);
                OnEffectProc += AuraEffectProcFn(spell_pri_t16_4p_healer_bonus_AuraScript::HandleEffectProc, EFFECT_0, SPELL_AURA_DUMMY);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_pri_t16_4p_healer_bonus_AuraScript();
        }
};

// 3153 - Angelic Feather areatrigger
class areatrigger_pri_angelic_feather : public AreaTriggerEntityScript
{
    public:
        areatrigger_pri_angelic_feather() : AreaTriggerEntityScript("areatrigger_pri_angelic_feather") { }

        struct areatrigger_pri_angelic_featherAI : AreaTriggerAI
        {
            areatrigger_pri_angelic_featherAI(AreaTrigger* areatrigger) : AreaTriggerAI(areatrigger) { }

            void OnInitialize() override
            {
                if (Unit* caster = at->GetCaster())
                {
                    std::vector<AreaTrigger*> areaTriggers = caster->GetAreaTriggers(at->GetSpellId());

                    if (areaTriggers.size() >= 3)
                        areaTriggers.front()->SetDuration(0);
                }
            }
        };

        AreaTriggerAI* GetAI(AreaTrigger* areatrigger) const override
        {
            return new areatrigger_pri_angelic_featherAI(areatrigger);
        }
};

void AddSC_priest_spell_scripts()
{
    new spell_pri_divine_hymn();
    new spell_pri_guardian_spirit();
    new spell_pri_mana_burn();
    new spell_pri_mana_leech();
    new spell_pri_mind_sear();
    new spell_pri_power_word_shield();
    new spell_pri_binding_heal();
    new spell_pri_hymn_of_hope();
    new spell_pri_mass_dispel();
    new spell_pri_improved_prayer_of_mending();
    new spell_pri_prayer_of_mending_heal();
    new spell_pri_psyfiend_proc_aura();
    new spell_pri_psychic_screams();
    new spell_pri_renew();
    new spell_pri_shadow_word_death();
    new spell_pri_glyphed_shadow_word_death();
    new spell_pri_shadow_word_death_caster_damage();
    new spell_pri_focused_will();
    new spell_pri_evangelism();
    new spell_pri_shadowy_apparition();
    new spell_pri_shadowy_apparition_missile();
    new spell_pri_shadow_orbs();
    new spell_pri_devouring_plague();
    new spell_pri_mind_flay();
    new spell_pri_glyph_of_dark_archangel();
    new spell_pri_itemset_t16_4p();
    new spell_pri_itemset_t12_4p();
    new spell_pri_shadowfiend_passive();
    new spell_pri_mind_spike();
    new spell_pri_psychic_horror();
    new spell_pri_shadowform();
    new spell_pri_vampiric_embrace();
    new spell_pri_vampiric_embrace_heal();
    new spell_pri_shadowy_recall_mastery();
    new spell_pri_archangel();
    new spell_pri_atonement();
    new spell_pri_atonement_heal();
    new spell_pri_divine_aegis();
    new spell_pri_train_of_thought();
    new spell_pri_strength_of_soul();
    new spell_pri_lightspring_trigger();
    new spell_pri_lightwell_charges();
    new spell_pri_lightwell_renew();
    new spell_pri_mastery_echo_of_light();
    new spell_pri_spirit_of_redemption();
    new spell_pri_circle_of_healing();
    new spell_pri_chakra_serenity();
    new spell_pri_holy_word_sanctuary();
    new spell_pri_holy_word_sanctuary_heal();
    new spell_pri_chakra_chastise();
    new spell_pri_glyph_of_holy_nova();
    new spell_pri_purify();
    new spell_pri_dispel_magic();
    new spell_pri_inner_fire();
    new spell_pri_leap_of_faith_effect_trigger();
    new spell_pri_levitate();
    new spell_pri_levitate_effect();
    new spell_pri_smite();
    new spell_pri_glyph_of_confession();
    new spell_pri_confession();
    new spell_pri_halo();
    new spell_pri_halo_damage_and_heal();
    new spell_pri_spectral_guise_charges();
    new spell_pri_spectral_guise_stealth();
    new spell_pri_void_tendrils();
    new spell_pri_void_tendrils_grasp();
    new spell_pri_void_tendrils_summon();
    new spell_pri_divine_star_aura();
    new spell_pri_divine_star();
    new spell_pri_divine_star_aoe();
    new spell_pri_penance();
    new spell_pri_greater_heal_or_prayer_of_healing();
    new spell_pri_divine_insight();
    new spell_pri_cascade_ability();
    new spell_pri_cascade_missile();
    new spell_pri_cascade_searcher();
    new spell_pri_angelic_bulwark();
    new spell_pri_angelic_bulwark_absorb();
    new spell_pri_power_word_solace();
    new spell_pri_power_word_solace_heal();
    new spell_pri_from_darkness_comes_light();
    new spell_pri_item_greater_heal_refund();
    new spell_pri_t10_2p_healer_bonus();
    new spell_pri_shadowflame();
    new spell_pri_t15_4p_healer_bonus();
    new spell_pri_golden_apparition();
    new spell_pri_shadowy_apparition_damage();
    new spell_pri_vampiric_touch();
    new spell_pri_t15_4p_shadow_bonus();
    new spell_pri_t16_2p_healer_bonus();
    new spell_pri_t16_4p_healer_bonus();

    new areatrigger_pri_angelic_feather();
}
