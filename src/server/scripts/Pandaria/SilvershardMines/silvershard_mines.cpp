/*
* Copyright (C) 2015-2016 Theatre of Dreams <http://theatreofdreams.eu/>
*
* This program is free software; you can redistribute it and/or modify it
* under the terms of the GNU General Public License as published by the
* Free Software Foundation; either version 2 of the License, or (at your
* option) any later version.
*
* This program is distributed in the hope that it will be useful, but WITHOUT
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
* more details.
*
* You should have received a copy of the GNU General Public License along
* with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include "BattlegroundMgr.h"
#include "BattlegroundSM.h"
#include "ScriptMgr.h"
#include "ScriptedCreature.h"
#include "ScriptedEscortAI.h"
#include "SpellScript.h"
#include "SpellAuraEffects.h"

const Position Mine1BasePath[6] =
{
    { 739.892f, 171.675f, 319.368f, 4.31998f },
    { 734.821f, 162.103f, 319.031f, 4.24929f },
    { 726.766f, 148.577f, 319.760f, 4.17075f },
    { 721.845f, 137.475f, 319.594f, 4.30034f },
    { 718.639f, 126.858f, 319.617f, 4.43385f },
    { 717.141f, 113.589f, 320.762f, 4.64983f }
};

const Position Mine1Path1[36] =
{
    { 716.898f, 108.888f, 320.551f, 4.75191f },
    { 717.971f, 104.512f, 320.669f, 4.98753f },
    { 717.971f, 104.512f, 320.669f, 4.98753f },
    { 720.024f, 101.336f, 320.897f, 5.40378f },
    { 723.369f, 98.5218f, 321.493f, 5.73365f },
    { 729.090f, 96.0629f, 322.517f, 5.84360f },
    { 737.882f, 93.6786f, 325.393f, 6.05958f },
    { 733.325f, 94.6599f, 323.807f, 6.05565f },
    { 744.444f, 92.5406f, 327.611f, 6.09099f },
    { 750.125f, 91.4387f, 329.511f, 6.06351f },
    { 757.317f, 89.8598f, 331.928f, 6.05958f },
    { 762.877f, 87.7404f, 334.065f, 5.91821f },
    { 768.713f, 85.1259f, 336.430f, 5.85145f },
    { 773.373f, 82.5389f, 338.509f, 5.75328f },
    { 778.724f, 80.5440f, 340.774f, 6.00460f },
    { 791.644f, 78.5275f, 345.082f, 6.14204f },
    { 798.440f, 76.7713f, 347.838f, 5.93392f },
    { 803.257f, 74.6932f, 349.593f, 5.85146f },
    { 807.246f, 72.4051f, 350.672f, 5.74936f },
    { 812.704f, 68.0557f, 352.386f, 5.63469f },
    { 816.826f, 65.5221f, 353.674f, 5.80748f },
    { 819.006f, 63.9890f, 354.243f, 6.04309f },
    { 824.235f, 62.7089f, 355.585f, 6.04309f },
    { 828.808f, 62.2600f, 356.732f, 0.23978f },
    { 833.855f, 64.1138f, 357.820f, 0.71101f },
    { 838.657f, 68.0927f, 358.823f, 0.65211f },
    { 843.138f, 70.5184f, 359.805f, 0.48718f },
    { 847.602f, 71.6128f, 360.778f, 0.07484f },
    { 852.628f, 71.2957f, 361.598f, 6.11063f },
    { 857.191f, 69.9747f, 362.323f, 5.88287f },
    { 864.533f, 65.7985f, 363.372f, 5.75328f },
    { 873.080f, 58.3648f, 364.351f, 5.53338f },
    { 879.148f, 51.9385f, 364.316f, 5.40772f },
    { 882.286f, 47.5070f, 363.942f, 5.33703f },
    { 893.711f, 32.6301f, 363.885f, 5.38808f },
    { 896.174f, 25.6250f, 363.927f, 5.01895f }
};

const Position Mine1Path2[15] =
{
    { 715.775f, 109.201f, 320.465f, 4.42009f },
    { 713.263f, 103.857f, 320.011f, 4.12165f },
    { 709.314f, 100.551f, 319.224f, 3.77607f },
    { 703.863f, 97.7755f, 317.988f, 3.48548f },
    { 698.113f, 96.5760f, 316.682f, 3.28520f },
    { 688.010f, 95.8471f, 314.500f, 3.25378f },
    { 682.851f, 94.9335f, 313.412f, 3.35196f },
    { 676.983f, 93.1351f, 311.358f, 3.51689f },
    { 672.364f, 91.1811f, 309.715f, 3.60721f },
    { 662.336f, 85.2153f, 305.855f, 3.68575f },
    { 653.546f, 82.4786f, 303.386f, 3.44228f },
    { 641.982f, 81.3818f, 299.822f, 3.16739f },
    { 632.831f, 80.5024f, 298.629f, 3.25378f },
    { 624.665f, 79.9769f, 298.466f, 3.19881f },
    { 618.559f, 79.3252f, 298.240f, 3.18624f }
};

const Position Mine2BasePath[25] =
{
    { 736.719f, 206.841f, 319.453f, 2.34568f },
    { 732.870f, 212.803f, 319.872f, 2.09828f },
    { 730.385f, 218.579f, 320.495f, 1.87444f },
    { 729.269f, 225.360f, 320.801f, 1.72914f },
    { 728.085f, 230.747f, 320.842f, 1.86266f },
    { 726.511f, 235.145f, 321.051f, 1.96869f },
    { 724.248f, 239.234f, 320.726f, 2.12184f },
    { 716.060f, 250.566f, 321.081f, 2.18074f },
    { 711.230f, 260.242f, 320.745f, 2.00010f },
    { 708.382f, 267.842f, 320.557f, 1.91371f },
    { 703.525f, 278.092f, 320.465f, 2.02367f },
    { 696.545f, 290.101f, 320.580f, 2.16504f },
    { 686.895f, 301.002f, 320.943f, 2.32212f },
    { 673.842f, 312.105f, 321.949f, 2.49098f },
    { 661.171f, 323.271f, 324.429f, 2.39673f },
    { 650.565f, 331.679f, 327.493f, 2.47920f },
    { 636.036f, 344.291f, 333.462f, 2.45956f },
    { 633.303f, 345.558f, 334.660f, 2.94651f },
    { 627.289f, 345.887f, 336.985f, 3.18605f },
    { 611.482f, 341.935f, 342.122f, 3.35099f },
    { 601.369f, 340.163f, 344.520f, 3.33528f },
    { 593.060f, 339.451f, 345.887f, 3.21354f },
    { 583.250f, 337.605f, 346.248f, 3.29994f },
    { 575.833f, 336.993f, 346.454f, 3.19784f },
    { 566.000f, 337.000f, 346.739f, 3.13893f }
};

const Position Mine3BasePath[15] =
{
    { 762.102f, 199.402f, 319.583f, 0.63211f },
    { 765.323f, 202.277f, 319.913f, 0.76956f },
    { 768.118f, 205.748f, 320.482f, 1.00125f },
    { 770.554f, 210.020f, 321.275f, 1.06016f },
    { 776.046f, 220.860f, 322.751f, 1.14263f },
    { 778.564f, 227.716f, 324.035f, 1.28007f },
    { 780.879f, 235.361f, 326.096f, 1.27222f },
    { 785.627f, 246.052f, 329.068f, 1.14263f },
    { 789.657f, 253.186f, 332.540f, 1.04052f },
    { 792.414f, 257.171f, 334.903f, 0.87166f },
    { 796.528f, 262.171f, 337.855f, 0.88344f },
    { 803.217f, 269.550f, 341.092f, 0.81668f },
    { 821.324f, 287.412f, 345.477f, 0.76170f },
    { 828.740f, 293.419f, 346.322f, 0.65960f },
    { 834.990f, 299.877f, 346.818f, 0.86381f }
};

const Position Mine3Path1[29] =
{
    { 837.858f, 302.789f, 346.862f, 0.97531f },
    { 840.166f, 306.129f, 346.898f, 0.98316f },
    { 844.767f, 314.414f, 347.018f, 1.10097f },
    { 847.510f, 320.506f, 347.148f, 1.20700f },
    { 848.723f, 325.981f, 346.819f, 1.44262f },
    { 848.601f, 330.584f, 347.099f, 1.72144f },
    { 846.521f, 341.073f, 347.174f, 1.74107f },
    { 844.997f, 346.973f, 347.094f, 1.93349f },
    { 838.042f, 361.449f, 346.914f, 2.05916f },
    { 835.583f, 365.708f, 346.876f, 2.15341f },
    { 833.606f, 368.664f, 346.857f, 2.16126f },
    { 830.397f, 375.453f, 346.747f, 1.97669f },
    { 828.058f, 381.737f, 346.651f, 1.91386f },
    { 821.547f, 400.412f, 346.438f, 1.92171f },
    { 818.739f, 406.779f, 347.504f, 2.02382f },
    { 816.956f, 412.358f, 348.553f, 1.86281f },
    { 816.700f, 416.648f, 349.418f, 1.60755f },
    { 816.840f, 422.783f, 350.911f, 1.55258f },
    { 816.415f, 427.900f, 352.612f, 1.76463f },
    { 814.117f, 434.835f, 354.671f, 1.95313f },
    { 811.780f, 440.313f, 356.095f, 2.00811f },
    { 807.655f, 456.170f, 358.182f, 1.77641f },
    { 809.880f, 446.718f, 357.215f, 4.86697f },
    { 806.533f, 465.571f, 358.668f, 1.73323f },
    { 804.100f, 471.824f, 359.006f, 1.94529f },
    { 800.592f, 477.500f, 359.209f, 2.20447f },
    { 794.669f, 484.600f, 359.332f, 2.27123f },
    { 786.551f, 492.209f, 359.300f, 2.40475f },
    { 778.158f, 501.375f, 359.237f, 2.33013f }
};

const Position Mine3Path2[48] =
{
    { 837.773f, 302.314f, 346.859f, 0.49798f },
    { 840.932f, 303.387f, 346.882f, 0.24665f },
    { 847.814f, 303.783f, 346.931f, 0.00318f },
    { 854.226f, 303.156f, 346.991f, 6.15678f },
    { 859.770f, 301.475f, 347.044f, 5.92509f },
    { 866.662f, 298.078f, 346.974f, 5.72482f },
    { 872.025f, 292.932f, 347.097f, 5.55596f },
    { 876.060f, 288.615f, 347.074f, 5.39889f },
    { 881.257f, 279.966f, 346.815f, 5.21825f },
    { 886.729f, 269.054f, 346.008f, 5.16327f },
    { 890.301f, 259.174f, 345.645f, 5.05724f },
    { 891.924f, 254.189f, 345.524f, 4.99441f },
    { 893.594f, 247.363f, 345.518f, 4.92766f },
    { 894.693f, 241.557f, 345.759f, 4.89232f },
    { 895.093f, 235.801f, 347.025f, 4.80592f },
    { 896.541f, 230.283f, 348.504f, 4.97871f },
    { 898.524f, 224.891f, 350.028f, 5.11222f },
    { 902.493f, 215.504f, 352.780f, 5.11615f },
    { 904.915f, 208.650f, 354.803f, 5.01798f },
    { 909.608f, 189.108f, 360.515f, 4.91981f },
    { 912.477f, 170.838f, 365.938f, 4.82949f },
    { 912.123f, 165.460f, 366.284f, 4.62137f },
    { 911.073f, 159.744f, 366.380f, 4.47607f },
    { 909.527f, 155.157f, 366.423f, 4.36219f },
    { 902.984f, 143.702f, 366.397f, 4.15799f },
    { 901.478f, 141.268f, 366.334f, 4.18941f },
    { 899.940f, 139.334f, 366.307f, 4.02055f },
    { 896.203f, 135.818f, 366.265f, 3.91059f },
    { 888.025f, 125.569f, 366.097f, 4.08338f },
    { 885.237f, 119.963f, 365.898f, 4.31114f },
    { 884.161f, 116.839f, 365.773f, 4.47214f },
    { 883.031f, 112.043f, 365.564f, 4.52319f },
    { 882.122f, 104.614f, 365.263f, 4.64885f },
    { 883.003f, 99.0903f, 365.048f, 4.95908f },
    { 884.827f, 94.6253f, 364.916f, 5.19077f },
    { 888.429f, 89.5945f, 364.789f, 5.35962f },
    { 889.526f, 87.6692f, 364.748f, 5.13186f },
    { 891.118f, 82.0393f, 364.598f, 4.94730f },
    { 891.067f, 76.4689f, 364.473f, 4.67634f },
    { 890.180f, 70.5729f, 364.540f, 4.53497f },
    { 890.291f, 63.4279f, 364.488f, 4.80200f },
    { 891.844f, 55.7610f, 364.166f, 4.94337f },
    { 893.070f, 51.6872f, 363.821f, 5.03762f },
    { 893.401f, 46.6265f, 363.706f, 4.77059f },
    { 893.224f, 40.4971f, 363.674f, 4.61351f },
    { 893.311f, 35.6355f, 363.790f, 4.77058f },
    { 893.414f, 32.8909f, 363.869f, 4.65278f },
    { 896.116f, 26.4357f, 363.984f, 5.04155f }
};

enum SilvershardMinesEnum
{
    DATA_TRACKER_AURA           = 0
};

class npc_silvershard_mine_cart : public CreatureScript
{
public:
    npc_silvershard_mine_cart() : CreatureScript("npc_silvershard_mine_cart") { }

    struct npc_silvershard_mine_cartAI : public npc_escortAI
    {
        npc_silvershard_mine_cartAI(Creature* creature) : npc_escortAI(creature)
        {
            Initialize();
        }

        uint32 BgInstanceId;
        uint32 BgTypeId;

        uint32 PausePoint;
        uint32 DepotEntry;

        void Initialize()
        {
            BgInstanceId = 0;
            BgTypeId = 0;

            DepotEntry = 0;
            PausePoint = 0;

            SetDespawnAtEnd(false);
            SetDespawnAtFar(false);

            switch (me->GetEntry())
            {
                case NPC_MINE_CART_1:
                {
                    for (uint32 i = 0; i < 6; ++i)
                        AddWaypoint(i, Mine1BasePath[i].GetPositionX(), Mine1BasePath[i].GetPositionY(), Mine1BasePath[i].GetPositionZ());

                    PausePoint = 6;
                    DepotEntry = BG_SM_OBJECT_LAVA_DEPOT;
                    break;
                }
                case NPC_MINE_CART_2:
                {
                    for (uint32 i = 0; i < 25; ++i)
                        AddWaypoint(i, Mine2BasePath[i].GetPositionX(), Mine2BasePath[i].GetPositionY(), Mine2BasePath[i].GetPositionZ());

                    DepotEntry = BG_SM_OBJECT_WATERFALL_DEPOT;
                    SetDespawnAtEnd(true);
                    break;
                }
                case NPC_MINE_CART_3:
                {
                    for (uint32 i = 0; i < 15; ++i)
                        AddWaypoint(i, Mine3BasePath[i].GetPositionX(), Mine3BasePath[i].GetPositionY(), Mine3BasePath[i].GetPositionZ());

                    DepotEntry = BG_SM_OBJECT_TROLL_DEPOT;
                    PausePoint = 15;
                    break;
                }
                default:
                    break;
            }

            Start(false);
        }

        void SetData(uint32 id, uint32 value) override
        {
            BgInstanceId = id;
            BgTypeId = value;
        }

        void WaypointReached(uint32 waypointId) override
        {
            if (!PausePoint)
                return;

            uint32 TrackPoint = PausePoint - 1;
            if (TrackPoint == waypointId)
            {
                Battleground* bg = sBattlegroundMgr->GetBattleground(BgInstanceId, BattlegroundTypeId(BgTypeId));
                if (!bg)
                    return;

                uint32 SwitchTrackNPC = (me->GetEntry() == NPC_MINE_CART_1) ? SM_TRACK_SWITCH_EAST : SM_TRACK_SWITCH_NORTH;

                Creature* tracker = bg->GetBGCreature(SwitchTrackNPC, false);
                if (!tracker)
                    return;

                uint32 TrackerAura = tracker->GetAI()->GetData(DATA_TRACKER_AURA);

                switch (me->GetEntry())
                {
                    case NPC_MINE_CART_1:
                    {
                        if (TrackerAura == BG_SM_TRACK_SWITCH_CLOSED)
                        {
                            for (uint32 i = 0; i < 36; ++i)
                                AddWaypoint(i + PausePoint, Mine1Path1[i].GetPositionX(), Mine1Path1[i].GetPositionY(), Mine1Path1[i].GetPositionZ());

                            DepotEntry = BG_SM_OBJECT_DIAMOND_DEPOT;
                        }
                        else
                        {
                            for (uint32 i = 0; i < 15; ++i)
                                AddWaypoint(i + PausePoint, Mine1Path2[i].GetPositionX(), Mine1Path2[i].GetPositionY(), Mine1Path2[i].GetPositionZ());
                        }
                        break;
                    }
                    case NPC_MINE_CART_3:
                    {
                        if (TrackerAura == BG_SM_TRACK_SWITCH_CLOSED)
                        {
                            for (uint32 i = 0; i < 29; ++i)
                                AddWaypoint(i + PausePoint, Mine3Path1[i].GetPositionX(), Mine3Path1[i].GetPositionY(), Mine3Path1[i].GetPositionZ());
                        }
                        else
                        {
                            for (uint32 i = 0; i < 48; ++i)
                                AddWaypoint(i + PausePoint, Mine3Path2[i].GetPositionX(), Mine3Path2[i].GetPositionY(), Mine3Path2[i].GetPositionZ());

                            DepotEntry = BG_SM_OBJECT_DIAMOND_DEPOT;
                        }
                        break;
                    }
                    default:
                        break;
                }

                SetDespawnAtEnd(true);
            }
        }

        void CorpseRemoved(uint32& /*respawnDelay*/) override
        {
            Battleground* bg = sBattlegroundMgr->GetBattleground(BgInstanceId, BattlegroundTypeId(BgTypeId));
            if (!bg)
                return;

            bg->EventBgCustomEvent(DepotEntry, me->GetEntry());
        }
    };

    CreatureAI* GetAI(Creature* creature) const override
    {
        return new npc_silvershard_mine_cartAI(creature);
    }
};

class npc_silvershard_mine_track_switch : public CreatureScript
{
public:
    npc_silvershard_mine_track_switch() : CreatureScript("npc_silvershard_mine_track_switch") { }

    struct npc_silvershard_mine_track_switchAI : public ScriptedAI
    {
        npc_silvershard_mine_track_switchAI(Creature* creature) : ScriptedAI(creature)
        {
            Initialize();
        }

        uint32 auraID;

        void Initialize()
        {
            auraID = 0;
            DoCast(BG_SM_FEIGN_DEATH_STUN);
            DoCast(BG_SM_TRACK_SWITCH_OPENED);
        }

        void OnSpellClick(Unit* clicker, bool& result) override
        {
            if (!result)
                return;

            if (Player* player = clicker->ToPlayer())
                if (Battleground* bg = player->GetBattleground())
                    for (uint8 i = SM_TRACK_SWITCH_EAST; i <= SM_TRACK_SWITCH_NORTH; ++i)
                        if (Creature* tracker = bg->GetBGCreature(i, false))
                            if (tracker->GetGUID() == me->GetGUID())
                            {
                                if (i == SM_TRACK_SWITCH_EAST)
                                    bg->SendMessageToAll(LANG_BG_SM_EAST_DIRECTION_CHANGED, CHAT_MSG_BG_SYSTEM_NEUTRAL);
                                else
                                    bg->SendMessageToAll(LANG_BG_SM_NORTH_DIRECTION_CHANGED, CHAT_MSG_BG_SYSTEM_NEUTRAL);

                                break;
                            }
        }

        void SpellHit(Unit* caster, SpellInfo const* spellInfo) override
        {
            if (spellInfo->Id != BG_SM_TRACK_SWITCH_OPENED && spellInfo->Id != BG_SM_TRACK_SWITCH_CLOSED)
                return;

            auraID = spellInfo->Id;
            DoCast(BG_SM_PREVENTION_AURA);

            // force update spellclick flag
            me->ForceValuesUpdateAtIndex(UNIT_NPC_FLAGS);
        }

        uint32 GetData(uint32 type) const override
        {
            if (type == DATA_TRACKER_AURA)
                return auraID;

            return 0;
        }
    };

    CreatureAI* GetAI(Creature* creature) const override
    {
        return new npc_silvershard_mine_track_switchAI(creature);
    }
};

// 135846 Advice Prevention Aura
class spell_gen_silvershard_mines_prevention_aura : public SpellScriptLoader
{
public:
    spell_gen_silvershard_mines_prevention_aura() : SpellScriptLoader("spell_gen_silvershard_mines_prevention_aura") { }

    class spell_gen_silvershard_mines_prevention_aura_AuraScript : public AuraScript
    {
        PrepareAuraScript(spell_gen_silvershard_mines_prevention_aura_AuraScript);

        void OnRemove(AuraEffect const* /*aurEff*/, AuraEffectHandleModes /*mode*/)
        {
            // force update spellclick flag
            if (Unit* target = GetTarget())
                target->ForceValuesUpdateAtIndex(UNIT_NPC_FLAGS);
        }

        void Register() override
        {
            OnEffectRemove += AuraEffectRemoveFn(spell_gen_silvershard_mines_prevention_aura_AuraScript::OnRemove, EFFECT_0, SPELL_AURA_DUMMY, AURA_EFFECT_HANDLE_REAL);
        }
    };

    AuraScript* GetAuraScript() const override
    {
        return new spell_gen_silvershard_mines_prevention_aura_AuraScript();
    }
};

void AddSC_silvershard_mines()
{
    new npc_silvershard_mine_cart();
    new npc_silvershard_mine_track_switch();

    new spell_gen_silvershard_mines_prevention_aura();
}
