-- Class Priest

DELETE FROM `spell_group` WHERE `id`=5000;
INSERT INTO `spell_group` VALUES
(5000,588),
(5000,73413);

DELETE FROM `spell_group_stack_rules` WHERE `group_id`=5000;
INSERT INTO `spell_group_stack_rules` VALUES
(5000,1);

DELETE FROM `spell_proc` WHERE `SpellId` IN (81662,78203,87100,15286,77486,87195,114164,28809,28802,37600,37604,37603,99134,99135,99155,105827,138301,
38158,145306,34914,145334,145336,92711);
INSERT INTO `spell_proc` VALUES
(81662,0,6,0x00100080,0x00018000,0x00000000,0x00000000,0x00010000,0x0000000,0x0000000,0x0000000,0x0000000,0,0,0,0),
(78203,0,6,0x00008000,0x000100000,0x00000000,0x00000000,0x00040000,0x0000000,0x0000000,0x0000002,0x0000000,0,0,0,0),
(87100,0,6,0x00000000,0x000000000,0x00000040,0x00000000,0x00040000,0x0000000,0x0000000,0x0000002,0x0000000,0,0,0,0),
(15286,32,6,0x00000000,0x000000000,0x00000000,0x00000000,0x00050000,0x0000000,0x0000000,0x0000000,0x0000000,0,0,0,0),
(77486,0,6,0x00000000,0x000000000,0x00000000,0x00000000,0x00040000,0x0000000,0x0000000,0x0000000,0x0000000,0,0,0,0),
(87195,0,6,0x00002000,0x000000000,0x00000000,0x00000000,0x00010000,0x0000000,0x0000000,0x0000002,0x0000000,0,0,0,0),
(114164,0,0,0x00000000,0x000000000,0x00000000,0x00000000,0x000A20A8,0x0000000,0x0000000,0x0000000,0x0000000,0,0,0,0),
(28809,0,6,0x00001000,0x000000000,0x00000000,0x00000000,0x00000000,0x0000000,0x0000000,0x0000002,0x0000000,0,0,0,0),
(28802,0,6,0x00000000,0x000000000,0x00000000,0x00000000,0x00000000,0x0000000,0x0000000,0x0000000,0x0000000,0,0,0,0),
(37600,0,6,0x00000000,0x000000000,0x00000000,0x00000000,0x00000000,0x0000000,0x0000000,0x0000000,0x0000000,0,0,0,0),
(37604,0,6,0x00000000,0x000000000,0x00000000,0x00000000,0x00000000,0x0000000,0x0000000,0x0000000,0x0000000,0,0,0,0),
(37603,0,6,0x00008000,0x000000000,0x00000000,0x00000000,0x00000000,0x0000000,0x0000000,0x0000000,0x0000000,0,0,0,0),
(99134,0,6,0x00001800,0x000000020,0x00000000,0x00000000,0x00000000,0x0000000,0x0000000,0x0000000,0x0000000,0,0,0,0),
(99135,0,6,0x00000000,0x000000000,0x00000000,0x00000000,0x00000000,0x0000000,0x0000000,0x0000000,0x0000000,0,0,0,0),
(99155,0,6,0x00000000,0x000000000,0x00000000,0x00000000,0x00000000,0x0000000,0x0000000,0x0000000,0x0000000,0,0,0,0),
(105827,0,6,0x00000000,0x20400000,0x00000000,0x00000000,0x00004000,0x0000000,0x0000001,0x0000000,0x0000000,0,0,0,0),
(138301,0,6,0x00000000,0x00000000,0x00000000,0x00000000,0x00000000,0x0000000,0x0000000,0x0000000,0x0000000,0,0,0,0),
(38158,0,6,0x00000000,0x00000400,0x00000000,0x00000000,0x00000000,0x0000000,0x0000000,0x0000000,0x0000000,0,0,0,0),
(145306,0,6,0x00001200,0x00000000,0x00000000,0x00000000,0x00000000,0x0000000,0x0000000,0x0000000,0x0000000,0,0,0,0),
(34914,0,6,0x00000000,0x00000400,0x00000000,0x00000000,0x00040000,0x0000001,0x0000000,0x0000000,0x0000000,0,0,100,0),
(145334,0,6,0x00000000,0x00000000,0x00000000,0x00000000,0x00004000,0x0000000,0x0000001,0x0000000,0x0000000,0,0,0,0),
(145336,0,6,0x00200000,0x00000000,0x00000000,0x00000000,0x0000C000,0x0000000,0x0000000,0x0000000,0x0000000,0,0,100,1),
(92711,0,6,0x00000000,0x00040000,0x00000000,0x00000000,0x00000000,0x0000000,0x0000000,0x0000000,0x0000000,0,0,0,0);

UPDATE `spell_script_names` SET `spell_id`=48045 WHERE `spell_id`=49821 ;

DELETE FROM `spell_linked_spell` WHERE `spell_trigger` IN (47585,89485,-588,-14771,-108939,-81206,-81208,-81209);
INSERT INTO `spell_linked_spell` VALUES
(47585,60069,2,'Dispersion - Dispersion Mana Regen'),
(89485,96267,2,'Inner Focus - Inner Focus Silence'),
(-588,-96267,0,'Inner Fire - Spell Warding'),
(-14771,-96267,0,'Glyph of Inner Sanctum - Spell Warding'),
(-108939,-111757,0,'Glyph Glyph of Levitate - Path of the Devout'),
(-81206,-89912,0,'Chakra: Sanctuary - Chakra Flow'),
(-81208,-89912,0,'Chakra: Serenity - Chakra Flow'),
(-81209,-89912,0,'Chakra: Chastise - Chakra Flow');

DELETE FROM `spell_areatrigger` WHERE `SpellMiscID` IN (1489,658,657,337);
INSERT INTO `spell_areatrigger` VALUES
(1489,5802,0,0,0,0,0,0,18414),
(658,3921,0,0,0,0,0,0,18414),
(657,3919,0,0,0,0,0,0,18414),
(337,3153,0,0,0,0,0,0,18414);
 
DELETE FROM `areatrigger_template` WHERE `Id` IN (5802,3921,3919,3153);
INSERT INTO `areatrigger_template` VALUES
(5802,0,0,6.5,6.5,0,0,0,0,'',18414),
(3921,0,2,1,30,0,0,0,0,'',18414),
(3919,0,2,1,30,0,0,0,0,'',18414),
(3153,0,0,3,3,0,0,0,0,'areatrigger_pri_angelic_feather',18414);

DELETE FROM `areatrigger_template_actions` WHERE `AreaTriggerId` IN (5802,3921,3153);
INSERT INTO `areatrigger_template_actions` VALUES
(5802,2,81782,1),
(3921,0,120692,0),
(3153,2,121557,1);

UPDATE `creature_template` SET `ScriptName`='npc_pet_pri_lightwell' WHERE `entry`=64571;
UPDATE `creature_template` SET `ScriptName`='npc_pet_pri_spectral_guise' WHERE `entry`=59607;
UPDATE `creature_template` SET `ScriptName`='npc_pet_pri_void_tentril' WHERE `entry`=65282;
UPDATE `creature_template` SET `ScriptName`='npc_pet_pri_psyfiend' WHERE `entry`=59190;
UPDATE `creature_template` SET `ScriptName`='npc_pet_pri_shadowfiend_or_mindbender' WHERE `entry`=19668;

UPDATE `creature_template` SET `dmgschool`=5, `BaseAttackTime`=1500, `RangeAttackTime`=2000, `unit_class`=2, `unit_flags2`=2048, `resistance1`=350, `resistance2`=500,
`resistance3`=500, `resistance4`=500, `resistance5`=500, `resistance6`=500, `spell1`=63619, `ScriptName`='npc_pet_pri_shadowfiend_or_mindbender' WHERE `entry`=67235;

UPDATE `creature_template` SET `dmgschool`=5, `BaseAttackTime`=1500, `RangeAttackTime`=2000, `unit_class`=2, `unit_flags2`=2048, `resistance1`=350, `resistance2`=500,
`resistance3`=500, `resistance4`=500, `resistance5`=500, `resistance6`=500, `ScriptName`='npc_pet_pri_shadowfiend_or_mindbender' WHERE `entry` IN (62982,67236);

DELETE FROM `creature_template_addon` WHERE `entry` IN (67235,62982,67236,53475);
INSERT INTO `creature_template_addon` VALUES
(67235,0,0,0,10241,0,0,0,0,'28305'),
(62982,0,0,0,10241,0,0,0,0,'123050'),
(67236,0,0,0,10241,0,0,0,0,'123050'),
(53475,0,0,0,10241,0,0,0,0,'99153');

DELETE FROM `spell_script_names` WHERE `spell_id` IN (32546,32592,37558,8122,113792,129176,32409,45243,81662,78203,147193,95740,2944,129197,58228,145179,
99157,87100,73510,64044,108968,142723,15290,15407,77486,81700,81749,81751,47515,92297,89490,7001,126137,59907,126150,126154,77485,20711,81208,88685,88686,
81209,125045,132157,34861,527,528,588,111759,585,64843,64901,126152,126123,120517,120644,120692,120696,119030,119032,108920,114404,127665,110745,122128,
58880,122127,110744,122121,114164,123050,596,2060,30604,78966,109175,121135,127632,121146,127627,127628,120785,120786,127630,108945,114214,129250,140816,
109186,70770,99155,138301,138302,148859,138158,145306,145334);
INSERT INTO `spell_script_names` VALUES
(32546,'spell_pri_binding_heal'),
(32592,'spell_pri_mass_dispel'),
(37558,'spell_pri_improved_prayer_of_mending'),
(8122,'spell_pri_psychic_screams'),
(113792,'spell_pri_psychic_screams'),
(129176,'spell_pri_glyphed_shadow_word_death'),
(32409,'spell_pri_shadow_word_death_caster_damage'),
(45243,'spell_pri_focused_will'),
(81662,'spell_pri_evangelism'),
(78203,'spell_pri_shadowy_apparition'),
(147193,'spell_pri_shadowy_apparition_missile'),
(95740,'spell_pri_shadow_orbs'),
(2944,'spell_pri_devouring_plague'),
(15407,'spell_pri_mind_flay'),
(129197,'spell_pri_mind_flay'),
(58228,'spell_pri_glyph_of_dark_archangel'),
(145179,'spell_pri_itemset_t16_4p'),
(99157,'spell_pri_itemset_t12_4p'),
(87100,'spell_pri_shadowfiend_passive'),
(73510,'spell_pri_mind_spike'),
(64044,'spell_pri_psychic_horror'),
(108968,'spell_pri_void_shift'),
(142723,'spell_pri_void_shift'),
(15290,'spell_pri_vampiric_embrace_heal'),
(77486,'spell_pri_shadowy_recall_mastery'),
(81700,'spell_pri_archangel'),
(81749,'spell_pri_atonement'),
(81751,'spell_pri_atonement_heal'),
(47515,'spell_pri_divine_aegis'),
(92297,'spell_pri_train_of_thought'),
(89490,'spell_pri_strength_of_soul'),
(126137,'spell_pri_lightspring_trigger'),
(126150,'spell_pri_lightwell_charges'),
(59907,'spell_pri_lightwell_charges'),
(126154,'spell_pri_lightwell_renew'),
(7001,'spell_pri_lightwell_renew'),
(77485,'spell_pri_mastery_echo_of_light'),
(20711,'spell_pri_spirit_of_redemption'),
(81208,'spell_pri_chakra_serenity'),
(88685,'spell_pri_holy_word_sanctuary'),
(88686,'spell_pri_holy_word_sanctuary_heal'),
(81209,'spell_pri_chakra_chastise'),
(125045,'spell_pri_glyph_of_holy_nova'),
(132157,'spell_pri_holy_nova'),
(34861,'spell_pri_circle_of_healing'),
(527,'spell_pri_purify'),
(528,'spell_pri_dispel_magic'),
(588,'spell_pri_inner_fire'),
(111759,'spell_pri_levitate_effect'),
(585,'spell_pri_smite'),
(64843,'spell_pri_hymns_channel'),
(64901,'spell_pri_hymns_channel'),
(126152,'spell_pri_glyph_of_confession'),
(126123,'spell_pri_confession'),
(120517,'spell_pri_halo'),
(120644,'spell_pri_halo'),
(120692,'spell_pri_halo_damage_and_heal'),
(120696,'spell_pri_halo_damage_and_heal'),
(119030,'spell_pri_spectral_guise_charges'),
(119032,'spell_pri_spectral_guise_stealth'),
(108920,'spell_pri_void_tendrils'),
(114404,'spell_pri_void_tendrils_grasp'),
(127665,'spell_pri_void_tendrils_summon'),
(110744,'spell_pri_divine_star_aura'),
(122121,'spell_pri_divine_star_aura'),
(110745,'spell_pri_divine_star_aoe'),
(122128,'spell_pri_divine_star_aoe'),
(58880,'spell_pri_divine_star'),
(122127,'spell_pri_divine_star'),
(114164,'spell_pri_psyfiend_proc_aura'),
(123050,'spell_pri_mana_leech'),
(596,'spell_pri_greater_heal_or_prayer_of_healing'),
(2060,'spell_pri_greater_heal_or_prayer_of_healing'),
(30604,'spell_pri_greater_heal_or_prayer_of_healing'),
(78966,'spell_pri_greater_heal_or_prayer_of_healing'),
(109175,'spell_pri_divine_insight'),
(121135,'spell_pri_cascade_ability'),
(127632,'spell_pri_cascade_ability'),
(121146,'spell_pri_cascade_missile'),
(127627,'spell_pri_cascade_missile'),
(127628,'spell_pri_cascade_missile'),
(120785,'spell_pri_cascade_missile'),
(120786,'spell_pri_cascade_searcher'),
(127630,'spell_pri_cascade_searcher'),
(108945,'spell_pri_angelic_bulwark'),
(114214,'spell_pri_angelic_bulwark_absorb'),
(129250,'spell_pri_power_word_solace'),
(140816,'spell_pri_power_word_solace_heal'),
(109186,'spell_pri_from_darkness_comes_light'),
(70770,'spell_pri_t10_2p_healer_bonus'),
(99155,'spell_pri_shadowflame'),
(138301,'spell_pri_t15_4p_healer_bonus'),
(138302,'spell_pri_golden_apparition'),
(148859,'spell_pri_shadowy_apparition_damage'),
(138158,'spell_pri_t15_4p_shadow_bonus'),
(145306,'spell_pri_t16_2p_healer_bonus'),
(145334,'spell_pri_t16_4p_healer_bonus');
