DELETE FROM `playercreateinfo_cast_spell` WHERE `spell` IN (48266, 79597, 79598, 79593, 79602, 79600, 79603, 79599, 79595, 79594, 79601, 79596, 2457, 688, 73523);
INSERT INTO `playercreateinfo_cast_spell` (`racemask`, `classmask`, `spell`, `note`) VALUES
-- Death Knight
(0, 32, 48266, 'Death Knight - Blood Presence'),
-- Hunter
(1, 4, 79597, 'Human - Hunter - Young Wolf'),
(2, 4, 79598, 'Orc - Hunter - Young Boar'),
(4, 4, 79593, 'Dwarf - Hunter - Young Bear'),
(8, 4, 79602, 'Night Elf - Hunter - Young Cat'),
(16, 4, 79600, 'Undead - Hunter - Young Widow'),
(32, 4, 79603, 'Tauren - Hunter - Young Tallstrider'),
(128, 4, 79599, 'Troll - Hunter - Young Raptor'),
(256, 4, 79595, 'Goblin - Hunter - Young Crab'),
(512, 4, 79594, 'Blood Elf - Hunter - Young Dragonhawk'),
(1024, 4, 79601, 'Draenei - Hunter - Young Moth'),
(2097152, 4, 79596, 'Worgen - Hunter - Young Mastiff'),
-- Warrior
(0, 1, 2457, 'Warrior - Battle Stance'),
-- Warlock
(0, 256, 688, 'Warlock - Summon Imp'),
-- Quest
(16, 0, 73523, 'Undead - Rigor Mortis'),
-- Pandaren Neutral Starting Quests
(24, 1, 107922, 'Warrior - Launch Starting Quest, Warrior'),
(24, 3, 107917, 'Hunter - Launch Starting Quest, Hunter'),
(24, 4, 107920, 'Rogue - Launch Starting Quest, Rogue'),
(24, 5, 107919, 'Priest - Launch Starting Quest, Priest'),
(24, 7, 107921, 'Shaman - Launch Starting Quest, Shaman'),
(24, 8, 107916, 'Mage - Launch Starting Quest, Mage'),
(24, 10, 107915, 'Monk - Launch Starting Quest, Monk'),
-- Pandaren Neutral Remove Weapon
(24, 1, 108059, 'Warrior - Launch Starting Quest, Warrior'),
(24, 3, 108061, 'Hunter - Launch Starting Quest, Hunter'),
(24, 4, 108058, 'Rogue - Launch Starting Quest, Rogue'),
(24, 5, 108057, 'Priest - Launch Starting Quest, Priest'),
(24, 7, 108056, 'Shaman - Launch Starting Quest, Shaman'),
(24, 8, 108055, 'Mage - Launch Starting Quest, Mage'),
(24, 10, 108060, 'Monk - Launch Starting Quest, Monk');
