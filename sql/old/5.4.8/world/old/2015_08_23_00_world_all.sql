-- Fix rigor mortis being caster upon DK creation which shouldn't
-- Don't excluding just DK class because original value was 0 (all)
UPDATE `playercreateinfo_cast_spell` SET `classMask`=925 WHERE `raceMask`=16 AND `spell`=73523;
UPDATE `creature_template` SET `npcflag` = 2 WHERE `entry` IN (69742, 70166, 70551, 72373, 72755, 74888, 75081, 75120, 75469, 75644, 75913, 76067, 76447, 76452, 76616, 76746, 76904, 77161, 77775, 77777, 77778, 77781, 77791, 77792, 77831, 77857, 78207, 78470, 78495, 78520, 78538, 78561, 78562, 78792, 78810, 78821, 78937, 79315, 79316, 79337, 79604, 79608, 79814, 79817, 79820, 79830, 79831, 79833, 79863, 79897, 80159, 80746, 80987, 81123, 81193, 81292, 81324, 81412, 81415, 81600, 81674, 81913, 81915, 81948, 82070, 82214, 82227, 82252, 82375, 82509, 82612, 82622, 82664, 82669, 82720, 83820, 84259, 84261, 84270, 84492, 84728, 84892, 84973, 84975, 85130, 85190, 85206, 85378, 85379, 85749, 85840, 86366, 86696, 88173, 88193, 88392, 88570, 89400, 90644, 91029, 91030, 91031, 91033, 91034, 92317, 93658, 93659);

DELETE FROM `creature_questender` WHERE `id` IN (69741, 69742, 70166, 70296, 70301, 70320, 70551, 70552, 72373, 72755, 73136, 74888, 75081, 75120, 75469, 75644, 75913, 76067, 76447, 76452, 76616, 76746, 76904, 77161, 77775, 77777, 77778, 77781, 77791, 77792, 77831, 77857, 78207, 78470, 78495, 78520, 78538, 78561, 78562, 78792, 78810, 78821, 78937, 79315, 79316, 79337, 79604, 79608, 79814, 79817, 79820, 79830, 79831, 79833, 79863, 79897, 80159, 80746, 80987, 81123, 81193, 81292, 81324, 81412, 81415, 81600, 81674, 81913, 81915, 81948, 82070, 82214, 82227, 82252, 82375, 82509, 82612, 82622, 82664, 82669, 82720, 83820, 84259, 84261, 84270, 84492, 84728, 84892, 84973, 84975, 85130, 85190, 85206, 85378, 85379, 85484, 85749, 85840, 85849, 86366, 86696, 88173, 88193, 88392, 88570, 89400, 90644, 91029, 91030, 91031, 91033, 91034, 92317, 93658, 93659);
INSERT INTO `creature_questender` VALUES
(69741, 32652), -- Lady Jaina Proudmoore, To the Skies!
(69742, 32277), -- Lor'themar Theron, To the Skies!
(70166, 32325), -- Jubeka Shadowbreaker, Infiltrating the Black Temple
(70296, 32665), -- Mei Lin, Learn To Ride
(70301, 32667), -- Softpaws, Learn To Ride
(70320, 32296), -- Taoshi, Treasures of the Thunder King
(70551, 32292), -- Scout Captain Elsia, Forge Ahead!
(70551, 32722), -- Scout Captain Elsia, Forge Ahead!
(70552, 32587), -- Scout Captain Daelin, Forge Ahead!
(70552, 32724), -- Scout Captain Daelin, Forge Ahead!
(72373, 32993), -- Karg Bloodfury, The Strength of Our Bonds
(72755, 32976), -- Rolo's Treasure, Rolo's Riddle
(73136, 33138), -- Lorewalker Cho, Why Do We Fight?
(74888, 33513), -- Koristrasza, Blackthorn's Lieutenants
(74888, 33514), -- Koristrasza, The Ritual
(75081, 33525), -- Young Orc Woman, Treasure: Orc Couple
(75120, 33531), -- Clumsy Cragmaul Brute, Treasure: Fallen Ogre
(75469, 33579), -- Raksi, What the Draenei Found
(75644, 33649), -- Iron Scout, Treasure: Torched Iron Horde Scout
(75913, 33761), -- Crystal-Shaper Barum, Barum's Notes
(75913, 33740), -- Crystal-Shaper Barum, Burning Sky
(75913, 33734), -- Crystal-Shaper Barum, Pieces of Us
(76067, 33081), -- Yrel, Escape From Shaz'gul
(76447, 33833), -- Eremor, Wanted: Kuu'rat's Tusks
(76452, 34729), -- Weaponsmith Na'Shra, Blood Oath of Na'Shra
(76616, 33816), -- Farseer Drek'Thar, Honor Has Its Rewards
(76746, 33919), -- Der'shway, Wanted: Gutsmash the Destroyer
(76904, 33931), -- Deceptia, This Is Not a Quest
(77161, 34033), -- Thaelin Darkanvil, And The Mole You Rode In On
(77161, 34030), -- Thaelin Darkanvil, The Captive Engineer
(77775, 36644), -- Kaya Solasen, Your First Jewelcrafting Work Order
(77775, 36842), -- Kaya Solasen, Your Second Jewelcrafting Work Order
(77777, 36647), -- Kurt Broadoak, Your First Inscription Work Order
(77777, 36841), -- Kurt Broadoak, Your Second Inscription Work Order
(77778, 36643), -- Kaylie Macdonald, Your First Tailoring Work Order
(77778, 36845), -- Kaylie Macdonald, Your Second Tailoring Work Order
(77781, 36645), -- Garm, Your First Enchanting Work Order
(77781, 36839), -- Garm, Your Second Enchanting Work Order
(77791, 36641), -- Peter Kearie, Your First Alchemy Work Order
(77791, 36838), -- Peter Kearie, Your Second Alchemy Work Order
(77792, 35168), -- Yulia Samras, Your First Blacksmithing Work Order
(77792, 35172), -- Yulia Samras, Your Second Blacksmithing Work Order
(77831, 36646), -- Helayn Whent, Your First Engineering Work Order
(77831, 36840), -- Helayn Whent, Your Second Engineering Work Order
(77857, 35009), -- Ka'alu, Call of the Raven Mother
(78207, 36642), -- Marianne Levine, Your First Leatherworking Work Order
(78207, 36844), -- Marianne Levine, Your Second Leatherworking Work Order
(78470, 34362), -- Owynn Graddock, The Shadow Gate
(78495, 36692), -- Shadow Hunter Ukambe, Assault on Darktide Roost
(78495, 36690), -- Shadow Hunter Ukambe, Assault on Lost Veil Anzu
(78495, 36697), -- Shadow Hunter Ukambe, Assault on Magnarok
(78495, 36693), -- Shadow Hunter Ukambe, Assault on Mok'gol Watchpost
(78495, 36689), -- Shadow Hunter Ukambe, Assault on Pillars of Fate
(78495, 36667), -- Shadow Hunter Ukambe, Assault on Shattrath Harbor
(78495, 36688), -- Shadow Hunter Ukambe, Assault on Skettis
(78495, 36691), -- Shadow Hunter Ukambe, Assault on Socrethar's Rise
(78495, 36669), -- Shadow Hunter Ukambe, Assault on Stonefury Cliffs
(78495, 36694), -- Shadow Hunter Ukambe, Assault on the Broken Precipice
(78495, 36695), -- Shadow Hunter Ukambe, Assault on the Everbloom Wilds
(78495, 36699), -- Shadow Hunter Ukambe, Assault on the Heart of Shattrath
(78495, 36696), -- Shadow Hunter Ukambe, Assault on the Iron Siegeworks
(78495, 36701), -- Shadow Hunter Ukambe, Assault on the Pit
(78495, 36698), -- Shadow Hunter Ukambe, Battle in Ashran
(78495, 36700), -- Shadow Hunter Ukambe, Challenge at the Ring of Blood
(78495, 38182), -- Shadow Hunter Ukambe, Missive: Assault on Darktide Roost
(78495, 38184), -- Shadow Hunter Ukambe, Missive: Assault on Lost Veil Anzu
(78495, 38177), -- Shadow Hunter Ukambe, Missive: Assault on Magnarok
(78495, 38181), -- Shadow Hunter Ukambe, Missive: Assault on Mok'gol Watchpost
(78495, 38185), -- Shadow Hunter Ukambe, Missive: Assault on Pillars of Fate
(78495, 38187), -- Shadow Hunter Ukambe, Missive: Assault on Shattrath Harbor
(78495, 38186), -- Shadow Hunter Ukambe, Missive: Assault on Skettis
(78495, 38183), -- Shadow Hunter Ukambe, Missive: Assault on Socrethar's Rise
(78495, 38176), -- Shadow Hunter Ukambe, Missive: Assault on Stonefury Cliffs
(78495, 38180), -- Shadow Hunter Ukambe, Missive: Assault on the Broken Precipice
(78495, 38179), -- Shadow Hunter Ukambe, Missive: Assault on the Everbloom Wilds
(78495, 38178), -- Shadow Hunter Ukambe, Missive: Assault on the Iron Siegeworks
(78520, 34351), -- Soulbinder Tuulani, We Must Construct Additional Pylons
(78538, 34447), -- Vindicator Doruu, Kaelynara Sunchaser
(78538, 34448), -- Vindicator Doruu, Kaelynara Sunchaser
(78538, 34399), -- Vindicator Doruu, Trouble In The Mine
(78561, 34429), -- Archmage Khadgar, Kill Your Hundred
(78562, 34436), -- Archmage Khadgar, Keli'dan the Breaker
(78562, 34741), -- Archmage Khadgar, Keli'dan the Breaker
(78792, 34381), -- Shadow Hunter Bwu'ja, The Shadow Gate
(78810, 34289), -- Owynn Graddock, Soulgrinder Survivor
(78821, 34319), -- Shadow Hunter Bwu'ja, Soulgrinder Survivor
(78937, 34450), -- Rexxar, Rylak Rescue
(79315, 34739), -- Olin Umberhide, The Shadowmoon Clan
(79316, 34432), -- Qiana Moonshadow, The Shadowmoon Clan
(79337, 35883), -- Pitfighter Vaandaam, The Fists of Vaandaam
(79604, 33731), -- Durotan, The Battle for Shattrath
(79608, 34099), -- Yrel, The Battle for Shattrath
(79814, 37568), -- Keyana Tone, Your First Alchemy Work Order
(79814, 36838), -- Keyana Tone, Your Second Alchemy Work Order
(79817, 37569), -- Kinja, Your First Blacksmithing Work Order
(79817, 35172), -- Kinja, Your Second Blacksmithing Work Order
(79820, 37570), -- Garra, Your First Enchanting Work Order
(79820, 36839), -- Garra, Your Second Enchanting Work Order
(79830, 37573), -- Elrondir Surrion, Your First Jewelcrafting Work Order
(79830, 36842), -- Elrondir Surrion, Your Second Jewelcrafting Work Order
(79831, 37572), -- Y'rogg, Your First Inscription Work Order
(79831, 36841), -- Y'rogg, Your Second Inscription Work Order
(79833, 37574), -- Yanney, Your First Leatherworking Work Order
(79833, 36844), -- Yanney, Your Second Leatherworking Work Order
(79863, 37575), -- Turga, Your First Tailoring Work Order
(79863, 36845), -- Turga, Your Second Tailoring Work Order
(79897, 34516), -- Bazwix, My Precious!
(80159, 33461), -- Arsenio Zerep, Gloomshade Game Hunter
(80746, 34924), -- Vakora of the Flock, The Egg Thieves
(80746, 36790), -- Vakora of the Flock, The Initiate's Revenge
(80987, 35262), -- Rangari Kaalya, Service of Rangari Kaalya
(80987, 35834), -- Rangari Kaalya, Wake of the Genesaur
(81123, 35068), -- Captain "Victorious" Chong, If They Won't Surrender...
(81123, 35067), -- Captain "Victorious" Chong, Silence the War Machines
(81123, 35069), -- Captain "Victorious" Chong, Terror of Nagrand
(81193, 35100), -- Blood Guard Ehanes, If They Won't Surrender...
(81193, 35099), -- Blood Guard Ehanes, Silence the War Machines
(81193, 35101), -- Blood Guard Ehanes, Terror of Nagrand
(81193, 35271), -- Blood Guard Ehanes, The Warsong Threat
(81292, 33834), -- Dyuna, Wanted: Kliaa's Stinger
(81324, 37332), -- Rangari Sheera, Fungal Bundle
(81412, 35169), -- Vindicator Yrel, And Justice for Thrall
(81415, 35171), -- Durotan, And Justice for Thrall
(81600, 35208), -- Burrian Coalpart, Dark Iron Down
(81674, 35870), -- Nisha, Basilisk Butcher
(81674, 35026), -- Nisha, On the Mend
(81913, 35275), -- Shadow Hunter Ukambe, Inspecting the Troops
(81915, 35276), -- Jasper Fel, Inspecting the Troops
(81948, 34646), -- Qiana Moonshadow, Qiana Moonshadow
(82070, 35317), -- Farseer Drek'Thar, The Dark Heart of Oshu'gun
(82214, 35396), -- Vindicator Nobundo, The Dark Heart of Oshu'gun
(82227, 33836), -- Orrin, Wanted: Maa'run's Hoof
(82252, 35386), -- Captain Washburn, The Warsong Threat
(82375, 35482), -- Admiral Taylor, Admiral Taylor
(82375, 36183), -- Admiral Taylor, Admiral Taylor
(82509, 35634), -- Darkscryer Raastok, Control is King
(82612, 35672), -- Hutou Featherwind, Wanted: Venombarb
(82622, 35670), -- Killga, Wanted: Spineslicer's Husk
(82664, 35675), -- Quartermaster Jolie, Wanted: Spineslicer's Husk
(82669, 35676), -- Falrogh the Drunk, Wanted: Venombarb
(82720, 35671), -- Shadow-Sage Iskar, A Gathering of Shadows
(83820, 36037), -- High Centurion Tormmok, A Centurion Without a Cause
(84259, 36166), -- Lunzul, No Time to Waste
(84261, 36165), -- Kolrigg Stoktron, No Time to Waste
(84270, 36160), -- Baros Alexston, Garrison Campaign: Every Rose Has Its Thorn
(84492, 36185), -- Garaal, Fair Trade
(84728, 36241), -- Ardule D'na, The Power of Preservation
(84892, 36296), -- Phylarch the Evergreen, Phylarch the Evergreen
(84973, 36164), -- Exarch Akama, The Trial of Courage
(84975, 36167), -- Exarch Naielle, The Trial of Heart
(85130, 35654), -- Glirin, Chapter I: Plant Food
(85130, 35651), -- Glirin, Chapter II: The Harvest
(85130, 35650), -- Glirin, Chapter III: Ritual of the Charred
(85130, 35652), -- Glirin, Growing Wood
(85190, 36375), -- Sethekk Idol, Spires - Treasure 026 - Sethekk Idol
(85206, 36377), -- Rukhmar's Image, Spires - Treasure 049 - Rukhmar's Image
(85378, 36429), -- Weldon Barov, The Rise and Fall of Barov Industries: Weldon Barov
(85379, 36427), -- Alexi Barov, The Rise and Fall of Barov Industries: Alexi Barov
(85484, 36477), -- Chester, Broken Promises
(85749, 36791), -- Gimlet Ginfizz, Phantom Potion
(85749, 36741), -- Gimlet Ginfizz, Vintage Free Action Potion
(85840, 37290), -- Torgg Flexington, Upgrades in Ashran
(85849, 37288), -- Kinkade Jakobs, Resources in Ashran
(86366, 36935), -- Tyra Silverblood, Phantom Potion
(86366, 36742), -- Tyra Silverblood, Vintage Free Action Potion
(86696, 37571), -- Garbra Fizzwonk, Your First Engineering Work Order
(88173, 37276), -- Dark Ranger Velonara, Standing United
(88193, 37281), -- Hulda Shadowblade, Standing United
(88392, 37270), -- Alchemy Follower - Horde, Alchemy Experiment
(88570, 36055), -- Fate-Twister Tiklal, Sealing Fate: Apexis Crystals
(88570, 37458), -- Fate-Twister Tiklal, Sealing Fate: Extended Honor
(88570, 36056), -- Fate-Twister Tiklal, Sealing Fate: Garrison Resources
(88570, 36054), -- Fate-Twister Tiklal, Sealing Fate: Gold
(88570, 37452), -- Fate-Twister Tiklal, Sealing Fate: Heap of Apexis Crystals
(88570, 36057), -- Fate-Twister Tiklal, Sealing Fate: Honor
(88570, 37455), -- Fate-Twister Tiklal, Sealing Fate: Immense Fortune of Gold
(88570, 37459), -- Fate-Twister Tiklal, Sealing Fate: Monumental Honor
(88570, 37453), -- Fate-Twister Tiklal, Sealing Fate: Mountain of Apexis Crystals
(88570, 37454), -- Fate-Twister Tiklal, Sealing Fate: Piles of Gold
(88570, 37456), -- Fate-Twister Tiklal, Sealing Fate: Stockpiled Garrison Resources
(88570, 37457), -- Fate-Twister Tiklal, Sealing Fate: Tremendous Garrison Resources
(89400, 38223), -- Reshad, Dark Ascension
(90644, 38578), -- Lagar the Wise, A Message of Terrible Import
(91029, 38290), -- Rath'thul Moonvale, Some Dust
(91030, 38243), -- Trixxy Volt, A Bit of Ore
(91031, 38296), -- Nicholas Mitrik, Herbs Galore
(91033, 38287), -- Zeezu, Raw Beast Hides
(91034, 38293), -- Calvo Klyne, Sumptuous Fur
(92317, 38274), -- Ariok, The Eye of Kilrogg
(93658, 39394), -- Exarch Yrel, The Cipher of Damnation
(93659, 38463); -- Farseer Drek'Thar, The Cipher of Damnation
-- Bogpaddle Bruiser
SET @ENTRY := 46190;
UPDATE `creature_template` SET `AIName`='SmartAI' WHERE `entry`=@ENTRY;
DELETE FROM `smart_scripts` WHERE `source_type`=0 AND `entryorguid`=@ENTRY;
INSERT INTO `smart_scripts` (`entryorguid`,`source_type`,`id`,`link`,`event_type`,`event_phase_mask`,`event_chance`,`event_flags`,`event_param1`,`event_param2`,`event_param3`,`event_param4`,`action_type`,`action_param1`,`action_param2`,`action_param3`,`action_param4`,`action_param5`,`action_param6`,`target_type`,`target_param1`,`target_param2`,`target_param3`,`target_x`,`target_y`,`target_z`,`target_o`,`comment`) VALUES
(@ENTRY,0,0,0,0,0,100,0,0,0,2300,3900,11,6660,64,0,0,0,0,2,0,0,0,0,0,0,0,'Cast Shoot'),
(@ENTRY,0,1,0,9,0,100,0,0,8,13600,14500,11,12024,0,0,0,0,0,2,0,0,0,0,0,0,0,'Cast Net on Close');

-- Bogpaddle Privateer
SET @ENTRY := 45887;
UPDATE `creature_template` SET `AIName`='SmartAI' WHERE `entry`=@ENTRY;
DELETE FROM `smart_scripts` WHERE `source_type`=0 AND `entryorguid`=@ENTRY;
INSERT INTO `smart_scripts` (`entryorguid`,`source_type`,`id`,`link`,`event_type`,`event_phase_mask`,`event_chance`,`event_flags`,`event_param1`,`event_param2`,`event_param3`,`event_param4`,`action_type`,`action_param1`,`action_param2`,`action_param3`,`action_param4`,`action_param5`,`action_param6`,`target_type`,`target_param1`,`target_param2`,`target_param3`,`target_x`,`target_y`,`target_z`,`target_o`,`comment`) VALUES
(@ENTRY,0,0,0,0,0,100,0,0,0,2300,3900,11,6660,64,0,0,0,0,2,0,0,0,0,0,0,0,'Cast Shoot'),
(@ENTRY,0,1,0,9,0,100,0,0,8,13600,14500,11,12024,0,0,0,0,0,2,0,0,0,0,0,0,0,'Cast Net on Close');

-- Cersei Dusksinger
SET @ENTRY := 17109;
UPDATE `creature_template` SET `AIName`='SmartAI' WHERE `entry`=@ENTRY;
DELETE FROM `smart_scripts` WHERE `source_type`=0 AND `entryorguid`=@ENTRY;
INSERT INTO `smart_scripts` (`entryorguid`,`source_type`,`id`,`link`,`event_type`,`event_phase_mask`,`event_chance`,`event_flags`,`event_param1`,`event_param2`,`event_param3`,`event_param4`,`action_type`,`action_param1`,`action_param2`,`action_param3`,`action_param4`,`action_param5`,`action_param6`,`target_type`,`target_param1`,`target_param2`,`target_param3`,`target_x`,`target_y`,`target_z`,`target_o`,`comment`) VALUES
(@ENTRY,0,0,0,1,0,100,1,3000,5000,0,0,11,8722,0,0,0,0,0,1,0,0,0,0,0,0,0,'Cast Summon Succubus on Spawn');

-- Chupacabros
SET @ENTRY := 50882;
UPDATE `creature_template` SET `AIName`='SmartAI' WHERE `entry`=@ENTRY;
DELETE FROM `smart_scripts` WHERE `source_type`=0 AND `entryorguid`=@ENTRY;
INSERT INTO `smart_scripts` (`entryorguid`,`source_type`,`id`,`link`,`event_type`,`event_phase_mask`,`event_chance`,`event_flags`,`event_param1`,`event_param2`,`event_param3`,`event_param4`,`action_type`,`action_param1`,`action_param2`,`action_param3`,`action_param4`,`action_param5`,`action_param6`,`target_type`,`target_param1`,`target_param2`,`target_param3`,`target_x`,`target_y`,`target_z`,`target_o`,`comment`) VALUES
(@ENTRY,0,0,0,0,0,100,0,4000,4500,10000,12000,11,35321,0,0,0,0,0,2,0,0,0,0,0,0,0,'Cast Gushing Wound');

-- Coral Shark
SET @ENTRY := 5434;
UPDATE `creature_template` SET `AIName`='SmartAI' WHERE `entry`=@ENTRY;
DELETE FROM `smart_scripts` WHERE `source_type`=0 AND `entryorguid`=@ENTRY;
INSERT INTO `smart_scripts` (`entryorguid`,`source_type`,`id`,`link`,`event_type`,`event_phase_mask`,`event_chance`,`event_flags`,`event_param1`,`event_param2`,`event_param3`,`event_param4`,`action_type`,`action_param1`,`action_param2`,`action_param3`,`action_param4`,`action_param5`,`action_param6`,`target_type`,`target_param1`,`target_param2`,`target_param3`,`target_x`,`target_y`,`target_z`,`target_o`,`comment`) VALUES
(@ENTRY,0,0,0,25,0,100,1,0,0,0,0,11,12787,0,0,0,0,0,1,0,0,0,0,0,0,0,'Cast Thrash on Spawn');

-- Dreamwatcher Forktongue 
SET @ENTRY := 5348;
UPDATE `creature_template` SET `AIName`='SmartAI' WHERE `entry`=@ENTRY;
DELETE FROM `smart_scripts` WHERE `source_type`=0 AND `entryorguid`=@ENTRY;
INSERT INTO `smart_scripts` (`entryorguid`,`source_type`,`id`,`link`,`event_type`,`event_phase_mask`,`event_chance`,`event_flags`,`event_param1`,`event_param2`,`event_param3`,`event_param4`,`action_type`,`action_param1`,`action_param2`,`action_param3`,`action_param4`,`action_param5`,`action_param6`,`target_type`,`target_param1`,`target_param2`,`target_param3`,`target_x`,`target_y`,`target_z`,`target_o`,`comment`) VALUES
(@ENTRY,0,0,0,2,0,100,1,0,55,0,0,11,15114,0,0,0,0,0,1,0,0,0,0,0,0,0,'Cast Summon Illusionary Dreamwatchers at 55% HP');

-- Duskfang
SET @ENTRY := 47053;
UPDATE `creature_template` SET `AIName`='SmartAI' WHERE `entry`=@ENTRY;
DELETE FROM `smart_scripts` WHERE `source_type`=0 AND `entryorguid`=@ENTRY;
INSERT INTO `smart_scripts` (`entryorguid`,`source_type`,`id`,`link`,`event_type`,`event_phase_mask`,`event_chance`,`event_flags`,`event_param1`,`event_param2`,`event_param3`,`event_param4`,`action_type`,`action_param1`,`action_param2`,`action_param3`,`action_param4`,`action_param5`,`action_param6`,`target_type`,`target_param1`,`target_param2`,`target_param3`,`target_x`,`target_y`,`target_z`,`target_o`,`comment`) VALUES
(@ENTRY,0,0,0,4,0,100,1,0,0,0,0,11,75002,0,0,0,0,0,2,0,0,0,0,0,0,0,'Cast Leaping Rush on Aggro'),
(@ENTRY,0,1,0,0,0,100,0,5000,8000,18000,20000,11,31289,0,0,0,0,0,2,0,0,0,0,0,0,0,'Cast Claw'),
(@ENTRY,0,2,0,0,0,100,0,12000,14000,25000,28000,11,75930,0,0,0,0,0,2,0,0,0,0,0,0,0,'Cast Mangle');

-- Enthralled Crustacean
SET @ENTRY := 46369;
UPDATE `creature_template` SET `AIName`='SmartAI' WHERE `entry`=@ENTRY;
DELETE FROM `smart_scripts` WHERE `source_type`=0 AND `entryorguid`=@ENTRY;
INSERT INTO `smart_scripts` (`entryorguid`,`source_type`,`id`,`link`,`event_type`,`event_phase_mask`,`event_chance`,`event_flags`,`event_param1`,`event_param2`,`event_param3`,`event_param4`,`action_type`,`action_param1`,`action_param2`,`action_param3`,`action_param4`,`action_param5`,`action_param6`,`target_type`,`target_param1`,`target_param2`,`target_param3`,`target_x`,`target_y`,`target_z`,`target_o`,`comment`) VALUES
(@ENTRY,0,0,0,0,0,100,0,4000,9000,24000,27000,11,79175,0,0,0,0,0,2,0,0,0,0,0,0,0,'Cast Slap & Chop');

-- Enthralled Gilblin
SET @ENTRY := 46365;
UPDATE `creature_template` SET `AIName`='SmartAI' WHERE `entry`=@ENTRY;
DELETE FROM `smart_scripts` WHERE `source_type`=0 AND `entryorguid`=@ENTRY;
INSERT INTO `smart_scripts` (`entryorguid`,`source_type`,`id`,`link`,`event_type`,`event_phase_mask`,`event_chance`,`event_flags`,`event_param1`,`event_param2`,`event_param3`,`event_param4`,`action_type`,`action_param1`,`action_param2`,`action_param3`,`action_param4`,`action_param5`,`action_param6`,`target_type`,`target_param1`,`target_param2`,`target_param3`,`target_x`,`target_y`,`target_z`,`target_o`,`comment`) VALUES
(@ENTRY,0,0,0,0,0,100,0,5000,8500,15000,22000,11,86695,0,0,0,0,0,2,0,0,0,0,0,0,0,'Cast Whirlwind');

-- Enthralled Murloc
SET @ENTRY := 46367;
UPDATE `creature_template` SET `AIName`='SmartAI' WHERE `entry`=@ENTRY;
DELETE FROM `smart_scripts` WHERE `source_type`=0 AND `entryorguid`=@ENTRY;
INSERT INTO `smart_scripts` (`entryorguid`,`source_type`,`id`,`link`,`event_type`,`event_phase_mask`,`event_chance`,`event_flags`,`event_param1`,`event_param2`,`event_param3`,`event_param4`,`action_type`,`action_param1`,`action_param2`,`action_param3`,`action_param4`,`action_param5`,`action_param6`,`target_type`,`target_param1`,`target_param2`,`target_param3`,`target_x`,`target_y`,`target_z`,`target_o`,`comment`) VALUES
(@ENTRY,0,0,0,1,0,100,0,500,1000,600000,600000,11,12550,0,0,0,0,0,1,0,0,0,0,0,0,0,'Cast Lightning Shield on Spawn'),
(@ENTRY,0,1,0,16,0,100,0,12550,1,15000,30000,11,12550,0,0,0,0,0,1,0,0,0,0,0,0,0,'Cast Lightning Shield on Repeat'),
(@ENTRY,0,2,0,0,0,100,0,0,0,3400,4700,11,9739,64,0,0,0,0,2,0,0,0,0,0,0,0,'Cast Wrath'),
(@ENTRY,0,3,0,2,0,100,1,0,15,0,0,25,0,0,0,0,0,0,1,0,0,0,0,0,0,0,'Flee at 15% HP');

-- Enthralled Siren
SET @ENTRY := 46366;
UPDATE `creature_template` SET `AIName`='SmartAI' WHERE `entry`=@ENTRY;
DELETE FROM `smart_scripts` WHERE `source_type`=0 AND `entryorguid`=@ENTRY;
INSERT INTO `smart_scripts` (`entryorguid`,`source_type`,`id`,`link`,`event_type`,`event_phase_mask`,`event_chance`,`event_flags`,`event_param1`,`event_param2`,`event_param3`,`event_param4`,`action_type`,`action_param1`,`action_param2`,`action_param3`,`action_param4`,`action_param5`,`action_param6`,`target_type`,`target_param1`,`target_param2`,`target_param3`,`target_x`,`target_y`,`target_z`,`target_o`,`comment`) VALUES
(@ENTRY,0,0,0,2,0,100,1,0,15,0,0,25,0,0,0,0,0,0,1,0,0,0,0,0,0,0,'Flee at 15% HP');

-- Fingat
SET @ENTRY := 14446;
UPDATE `creature_template` SET `AIName`='SmartAI' WHERE `entry`=@ENTRY;
DELETE FROM `smart_scripts` WHERE `source_type`=0 AND `entryorguid`=@ENTRY;
INSERT INTO `smart_scripts` (`entryorguid`,`source_type`,`id`,`link`,`event_type`,`event_phase_mask`,`event_chance`,`event_flags`,`event_param1`,`event_param2`,`event_param3`,`event_param4`,`action_type`,`action_param1`,`action_param2`,`action_param3`,`action_param4`,`action_param5`,`action_param6`,`target_type`,`target_param1`,`target_param2`,`target_param3`,`target_x`,`target_y`,`target_z`,`target_o`,`comment`) VALUES
(@ENTRY,0,0,0,2,0,100,1,0,30,0,0,11,8599,0,0,0,0,0,1,0,0,0,0,0,0,0,'Cast Enrage at 30% HP'),
(@ENTRY,0,1,0,25,0,100,1,0,0,0,0,11,12787,0,0,0,0,0,1,0,0,0,0,0,0,0,'Cast Thrash on Spawn');

-- Gilblin Slasher
SET @ENTRY := 45934;
UPDATE `creature_template` SET `AIName`='SmartAI' WHERE `entry`=@ENTRY;
DELETE FROM `smart_scripts` WHERE `source_type`=0 AND `entryorguid`=@ENTRY;
INSERT INTO `smart_scripts` (`entryorguid`,`source_type`,`id`,`link`,`event_type`,`event_phase_mask`,`event_chance`,`event_flags`,`event_param1`,`event_param2`,`event_param3`,`event_param4`,`action_type`,`action_param1`,`action_param2`,`action_param3`,`action_param4`,`action_param5`,`action_param6`,`target_type`,`target_param1`,`target_param2`,`target_param3`,`target_x`,`target_y`,`target_z`,`target_o`,`comment`) VALUES
(@ENTRY,0,0,0,0,0,100,0,4000,6000,18000,21500,11,60842,0,0,0,0,0,2,0,0,0,0,0,0,0,'Cast Expose Armor');

-- Gilblin Stalker
SET @ENTRY := 45701;
UPDATE `creature_template` SET `AIName`='SmartAI' WHERE `entry`=@ENTRY;
DELETE FROM `smart_scripts` WHERE `source_type`=0 AND `entryorguid`=@ENTRY;
INSERT INTO `smart_scripts` (`entryorguid`,`source_type`,`id`,`link`,`event_type`,`event_phase_mask`,`event_chance`,`event_flags`,`event_param1`,`event_param2`,`event_param3`,`event_param4`,`action_type`,`action_param1`,`action_param2`,`action_param3`,`action_param4`,`action_param5`,`action_param6`,`target_type`,`target_param1`,`target_param2`,`target_param3`,`target_x`,`target_y`,`target_z`,`target_o`,`comment`) VALUES
(@ENTRY,0,0,0,0,0,100,0,2000,4500,12000,16500,11,14873,0,0,0,0,0,2,0,0,0,0,0,0,0,'Cast Sinister Strike'),
(@ENTRY,0,1,0,67,0,100,0,9000,12000,0,0,11,37685,0,0,0,0,0,2,0,0,0,0,0,0,0,'Cast Backstab');

-- Gilblin Trespasser
SET @ENTRY := 45914;
UPDATE `creature_template` SET `AIName`='SmartAI' WHERE `entry`=@ENTRY;
DELETE FROM `smart_scripts` WHERE `source_type`=0 AND `entryorguid`=@ENTRY;
INSERT INTO `smart_scripts` (`entryorguid`,`source_type`,`id`,`link`,`event_type`,`event_phase_mask`,`event_chance`,`event_flags`,`event_param1`,`event_param2`,`event_param3`,`event_param4`,`action_type`,`action_param1`,`action_param2`,`action_param3`,`action_param4`,`action_param5`,`action_param6`,`target_type`,`target_param1`,`target_param2`,`target_param3`,`target_x`,`target_y`,`target_z`,`target_o`,`comment`) VALUES
(@ENTRY,0,0,0,0,0,100,0,5500,7000,17500,22500,11,15976,0,0,0,0,0,2,0,0,0,0,0,0,0,'Cast Puncture');

-- Gilmorian
SET @ENTRY := 14447;
UPDATE `creature_template` SET `AIName`='SmartAI' WHERE `entry`=@ENTRY;
DELETE FROM `smart_scripts` WHERE `source_type`=0 AND `entryorguid`=@ENTRY;
INSERT INTO `smart_scripts` (`entryorguid`,`source_type`,`id`,`link`,`event_type`,`event_phase_mask`,`event_chance`,`event_flags`,`event_param1`,`event_param2`,`event_param3`,`event_param4`,`action_type`,`action_param1`,`action_param2`,`action_param3`,`action_param4`,`action_param5`,`action_param6`,`target_type`,`target_param1`,`target_param2`,`target_param3`,`target_x`,`target_y`,`target_z`,`target_o`,`comment`) VALUES
(@ENTRY,0,0,0,0,0,100,0,2000,4500,12000,16500,11,14873,0,0,0,0,0,2,0,0,0,0,0,0,0,'Cast Sinister Strike'),
(@ENTRY,0,1,0,0,0,100,0,7000,9000,22000,24000,11,13579,0,0,0,0,0,2,0,0,0,0,0,0,0,'Cast Gouge');

-- Grunt Tharlak
SET @ENTRY := 5547;
UPDATE `creature_template` SET `AIName`='SmartAI' WHERE `entry`=@ENTRY;
DELETE FROM `smart_scripts` WHERE `source_type`=0 AND `entryorguid`=@ENTRY;
INSERT INTO `smart_scripts` (`entryorguid`,`source_type`,`id`,`link`,`event_type`,`event_phase_mask`,`event_chance`,`event_flags`,`event_param1`,`event_param2`,`event_param3`,`event_param4`,`action_type`,`action_param1`,`action_param2`,`action_param3`,`action_param4`,`action_param5`,`action_param6`,`target_type`,`target_param1`,`target_param2`,`target_param3`,`target_x`,`target_y`,`target_z`,`target_o`,`comment`) VALUES
(@ENTRY,0,0,0,0,0,100,0,0,0,2300,3900,11,95826,64,0,0,0,0,2,0,0,0,0,0,0,0,'Cast Shoot');

-- Grunt Zuul
SET @ENTRY := 5546;
UPDATE `creature_template` SET `AIName`='SmartAI' WHERE `entry`=@ENTRY;
DELETE FROM `smart_scripts` WHERE `source_type`=0 AND `entryorguid`=@ENTRY;
INSERT INTO `smart_scripts` (`entryorguid`,`source_type`,`id`,`link`,`event_type`,`event_phase_mask`,`event_chance`,`event_flags`,`event_param1`,`event_param2`,`event_param3`,`event_param4`,`action_type`,`action_param1`,`action_param2`,`action_param3`,`action_param4`,`action_param5`,`action_param6`,`target_type`,`target_param1`,`target_param2`,`target_param3`,`target_x`,`target_y`,`target_z`,`target_o`,`comment`) VALUES
(@ENTRY,0,0,0,0,0,100,0,0,0,2300,3900,11,95826,64,0,0,0,0,2,0,0,0,0,0,0,0,'Cast Shoot');

-- Harborage Guardian
SET @ENTRY := 51809;
UPDATE `creature_template` SET `AIName`='SmartAI' WHERE `entry`=@ENTRY;
DELETE FROM `smart_scripts` WHERE `source_type`=0 AND `entryorguid`=@ENTRY;
INSERT INTO `smart_scripts` (`entryorguid`,`source_type`,`id`,`link`,`event_type`,`event_phase_mask`,`event_chance`,`event_flags`,`event_param1`,`event_param2`,`event_param3`,`event_param4`,`action_type`,`action_param1`,`action_param2`,`action_param3`,`action_param4`,`action_param5`,`action_param6`,`target_type`,`target_param1`,`target_param2`,`target_param3`,`target_x`,`target_y`,`target_z`,`target_o`,`comment`) VALUES
(@ENTRY,0,0,0,0,0,100,0,0,0,2300,3900,11,95826,64,0,0,0,0,2,0,0,0,0,0,0,0,'Cast Shoot');

-- Harborage Guardian
SET @ENTRY := 47370;
UPDATE `creature_template` SET `AIName`='SmartAI' WHERE `entry`=@ENTRY;
DELETE FROM `smart_scripts` WHERE `source_type`=0 AND `entryorguid`=@ENTRY;
INSERT INTO `smart_scripts` (`entryorguid`,`source_type`,`id`,`link`,`event_type`,`event_phase_mask`,`event_chance`,`event_flags`,`event_param1`,`event_param2`,`event_param3`,`event_param4`,`action_type`,`action_param1`,`action_param2`,`action_param3`,`action_param4`,`action_param5`,`action_param6`,`target_type`,`target_param1`,`target_param2`,`target_param3`,`target_x`,`target_y`,`target_z`,`target_o`,`comment`) VALUES
(@ENTRY,0,0,0,0,0,100,0,0,0,2300,3900,11,95826,64,0,0,0,0,2,0,0,0,0,0,0,0,'Cast Shoot');

-- Ionis
SET @ENTRY := 50790;
UPDATE `creature_template` SET `AIName`='SmartAI' WHERE `entry`=@ENTRY;
DELETE FROM `smart_scripts` WHERE `source_type`=0 AND `entryorguid`=@ENTRY;
INSERT INTO `smart_scripts` (`entryorguid`,`source_type`,`id`,`link`,`event_type`,`event_phase_mask`,`event_chance`,`event_flags`,`event_param1`,`event_param2`,`event_param3`,`event_param4`,`action_type`,`action_param1`,`action_param2`,`action_param3`,`action_param4`,`action_param5`,`action_param6`,`target_type`,`target_param1`,`target_param2`,`target_param3`,`target_x`,`target_y`,`target_z`,`target_o`,`comment`) VALUES
(@ENTRY,0,0,0,0,0,100,0,5000,8000,12000,15000,11,79085,0,0,0,0,0,2,0,0,0,0,0,0,0,'Cast Lightning Bolt');

-- Jade <Victim of the Nightmare>
SET @ENTRY := 1063;
UPDATE `creature_template` SET `AIName`='SmartAI' WHERE `entry`=@ENTRY;
DELETE FROM `smart_scripts` WHERE `source_type`=0 AND `entryorguid`=@ENTRY;
INSERT INTO `smart_scripts` (`entryorguid`,`source_type`,`id`,`link`,`event_type`,`event_phase_mask`,`event_chance`,`event_flags`,`event_param1`,`event_param2`,`event_param3`,`event_param4`,`action_type`,`action_param1`,`action_param2`,`action_param3`,`action_param4`,`action_param5`,`action_param6`,`target_type`,`target_param1`,`target_param2`,`target_param3`,`target_x`,`target_y`,`target_z`,`target_o`,`comment`) VALUES
(@ENTRY,0,0,0,0,0,100,0,5000,8000,12000,15000,11,12533,0,0,0,0,0,2,0,0,0,0,0,0,0,'Cast Acid Breath');

-- Jammal'an the Prophet
-- Earthgrab Totem
SET @ENTRY := 46656;
SET @TOTEMENTRY := 6066;
UPDATE `creature_template` SET `AIName`='SmartAI' WHERE `entry`=@ENTRY;
UPDATE `creature_template` SET `AIName`='SmartAI' WHERE `entry`=@TOTEMENTRY;
DELETE FROM `smart_scripts` WHERE `source_type`=0 AND `entryorguid`=@ENTRY;
DELETE FROM `smart_scripts` WHERE `source_type`=0 AND `entryorguid`=@TOTEMENTRY;
INSERT INTO `smart_scripts` (`entryorguid`,`source_type`,`id`,`link`,`event_type`,`event_phase_mask`,`event_chance`,`event_flags`,`event_param1`,`event_param2`,`event_param3`,`event_param4`,`action_type`,`action_param1`,`action_param2`,`action_param3`,`action_param4`,`action_param5`,`action_param6`,`target_type`,`target_param1`,`target_param2`,`target_param3`,`target_x`,`target_y`,`target_z`,`target_o`,`comment`) VALUES
(@ENTRY,0,0,0,0,0,100,0,5000,8000,25000,37000,11,8376,0,0,0,0,0,1,0,0,0,0,0,0,0,'Cast Earthgrab Totem'),
(@TOTEMENTRY,0,0,0,11,0,100,1,0,0,0,0,11,8377,0,0,0,0,0,1,0,0,0,0,0,0,0,'Cast Earthgrab on Spawn'),
(@ENTRY,0,1,0,2,0,100,0,0,40,22000,25000,11,12492,2,0,0,0,0,1,0,0,0,0,0,0,0,'Cast Healing Wave at 40% HP'),
(@ENTRY,0,2,0,0,0,100,0,9000,9000,16000,25000,11,12468,0,0,0,0,0,4,0,0,0,0,0,0,0,'Cast Flamestrike');

-- Jarquia
SET @ENTRY := 9916;
UPDATE `creature_template` SET `AIName`='SmartAI' WHERE `entry`=@ENTRY;
DELETE FROM `smart_scripts` WHERE `source_type`=0 AND `entryorguid`=@ENTRY;
INSERT INTO `smart_scripts` (`entryorguid`,`source_type`,`id`,`link`,`event_type`,`event_phase_mask`,`event_chance`,`event_flags`,`event_param1`,`event_param2`,`event_param3`,`event_param4`,`action_type`,`action_param1`,`action_param2`,`action_param3`,`action_param4`,`action_param5`,`action_param6`,`target_type`,`target_param1`,`target_param2`,`target_param3`,`target_x`,`target_y`,`target_z`,`target_o`,`comment`) VALUES
(@ENTRY,0,0,0,2,0,100,1,0,30,0,0,11,8599,0,0,0,0,0,1,0,0,0,0,0,0,0,'Cast Enrage at 30% HP');

-- Kash
SET @ENTRY := 50837;
UPDATE `creature_template` SET `AIName`='SmartAI' WHERE `entry`=@ENTRY;
DELETE FROM `smart_scripts` WHERE `source_type`=0 AND `entryorguid`=@ENTRY;
INSERT INTO `smart_scripts` (`entryorguid`,`source_type`,`id`,`link`,`event_type`,`event_phase_mask`,`event_chance`,`event_flags`,`event_param1`,`event_param2`,`event_param3`,`event_param4`,`action_type`,`action_param1`,`action_param2`,`action_param3`,`action_param4`,`action_param5`,`action_param6`,`target_type`,`target_param1`,`target_param2`,`target_param3`,`target_x`,`target_y`,`target_z`,`target_o`,`comment`) VALUES
(@ENTRY,0,0,0,4,0,100,1,0,0,0,0,11,75002,0,0,0,0,0,2,0,0,0,0,0,0,0,'Cast Leaping Rush on Aggro');

-- Lost One Chieftain
SET @ENTRY := 763;
UPDATE `creature_template` SET `AIName`='SmartAI' WHERE `entry`=@ENTRY;
DELETE FROM `smart_scripts` WHERE `source_type`=0 AND `entryorguid`=@ENTRY;
INSERT INTO `smart_scripts` (`entryorguid`,`source_type`,`id`,`link`,`event_type`,`event_phase_mask`,`event_chance`,`event_flags`,`event_param1`,`event_param2`,`event_param3`,`event_param4`,`action_type`,`action_param1`,`action_param2`,`action_param3`,`action_param4`,`action_param5`,`action_param6`,`target_type`,`target_param1`,`target_param2`,`target_param3`,`target_x`,`target_y`,`target_z`,`target_o`,`comment`) VALUES
(@ENTRY,0,0,0,2,0,100,1,0,30,0,0,11,8599,0,0,0,0,0,1,0,0,0,0,0,0,0,'Cast Enrage at 30% HP');

-- Lost One Cook
SET @ENTRY := 1106;
UPDATE `creature_template` SET `AIName`='SmartAI' WHERE `entry`=@ENTRY;
DELETE FROM `smart_scripts` WHERE `source_type`=0 AND `entryorguid`=@ENTRY;
INSERT INTO `smart_scripts` (`entryorguid`,`source_type`,`id`,`link`,`event_type`,`event_phase_mask`,`event_chance`,`event_flags`,`event_param1`,`event_param2`,`event_param3`,`event_param4`,`action_type`,`action_param1`,`action_param2`,`action_param3`,`action_param4`,`action_param5`,`action_param6`,`target_type`,`target_param1`,`target_param2`,`target_param3`,`target_x`,`target_y`,`target_z`,`target_o`,`comment`) VALUES
(@ENTRY,0,0,0,0,0,100,0,5000,6000,18000,22000,11,11962,0,0,0,0,0,2,0,0,0,0,0,0,0,'Cast Immolate'),
(@ENTRY,0,1,0,0,0,100,0,10000,12000,12000,25000,11,11428,0,0,0,0,0,2,0,0,0,0,0,0,0,'Cast Knockdown');

-- Lost One Hunter
SET @ENTRY := 759;
UPDATE `creature_template` SET `AIName`='SmartAI' WHERE `entry`=@ENTRY;
DELETE FROM `smart_scripts` WHERE `source_type`=0 AND `entryorguid`=@ENTRY;
INSERT INTO `smart_scripts` (`entryorguid`,`source_type`,`id`,`link`,`event_type`,`event_phase_mask`,`event_chance`,`event_flags`,`event_param1`,`event_param2`,`event_param3`,`event_param4`,`action_type`,`action_param1`,`action_param2`,`action_param3`,`action_param4`,`action_param5`,`action_param6`,`target_type`,`target_param1`,`target_param2`,`target_param3`,`target_x`,`target_y`,`target_z`,`target_o`,`comment`) VALUES
(@ENTRY,0,0,0,0,0,100,0,0,0,2300,3900,11,6660,64,0,0,0,0,2,0,0,0,0,0,0,0,'Cast Shoot'),
(@ENTRY,0,1,0,9,0,100,0,5,30,35000,45000,11,8806,0,0,0,0,0,2,0,0,0,0,0,0,0,'Cast Poisoned Shot on Close');

-- Lost One Muckdweller
SET @ENTRY := 760;
UPDATE `creature_template` SET `AIName`='SmartAI' WHERE `entry`=@ENTRY;
DELETE FROM `smart_scripts` WHERE `source_type`=0 AND `entryorguid`=@ENTRY;
INSERT INTO `smart_scripts` (`entryorguid`,`source_type`,`id`,`link`,`event_type`,`event_phase_mask`,`event_chance`,`event_flags`,`event_param1`,`event_param2`,`event_param3`,`event_param4`,`action_type`,`action_param1`,`action_param2`,`action_param3`,`action_param4`,`action_param5`,`action_param6`,`target_type`,`target_param1`,`target_param2`,`target_param3`,`target_x`,`target_y`,`target_z`,`target_o`,`comment`) VALUES
(@ENTRY,0,0,0,0,0,100,0,5000,6000,19000,27000,11,3256,0,0,0,0,0,2,0,0,0,0,0,0,0,'Cast Plague Cloud');

-- Lost One Seer
SET @ENTRY := 761;
SET @TOTEMENTRY := 6066;
UPDATE `creature_template` SET `AIName`='SmartAI' WHERE `entry`=@ENTRY;
UPDATE `creature_template` SET `AIName`='SmartAI' WHERE `entry`=@TOTEMENTRY;
DELETE FROM `smart_scripts` WHERE `source_type`=0 AND `entryorguid`=@ENTRY;
DELETE FROM `smart_scripts` WHERE `source_type`=0 AND `entryorguid`=@TOTEMENTRY;
INSERT INTO `smart_scripts` (`entryorguid`,`source_type`,`id`,`link`,`event_type`,`event_phase_mask`,`event_chance`,`event_flags`,`event_param1`,`event_param2`,`event_param3`,`event_param4`,`action_type`,`action_param1`,`action_param2`,`action_param3`,`action_param4`,`action_param5`,`action_param6`,`target_type`,`target_param1`,`target_param2`,`target_param3`,`target_x`,`target_y`,`target_z`,`target_o`,`comment`) VALUES
(@ENTRY,0,0,0,0,0,100,0,5000,8000,25000,37000,11,8376,0,0,0,0,0,1,0,0,0,0,0,0,0,'Cast Earthgrab Totem'),
(@TOTEMENTRY,0,0,0,25,0,100,1,0,0,0,0,11,8377,0,0,0,0,0,1,0,0,0,0,0,0,0,'Cast Earthgrab on Spawn'),
(@ENTRY,0,1,0,2,0,100,1,0,40,0,0,11,4971,2,0,0,0,0,1,0,0,0,0,0,0,0,'Cast Healing Ward at 40% HP');

-- Marshfin Murkdweller
SET @ENTRY := 45967;
UPDATE `creature_template` SET `AIName`='SmartAI' WHERE `entry`=@ENTRY;
DELETE FROM `smart_scripts` WHERE `source_type`=0 AND `entryorguid`=@ENTRY;
INSERT INTO `smart_scripts` (`entryorguid`,`source_type`,`id`,`link`,`event_type`,`event_phase_mask`,`event_chance`,`event_flags`,`event_param1`,`event_param2`,`event_param3`,`event_param4`,`action_type`,`action_param1`,`action_param2`,`action_param3`,`action_param4`,`action_param5`,`action_param6`,`target_type`,`target_param1`,`target_param2`,`target_param3`,`target_x`,`target_y`,`target_z`,`target_o`,`comment`) VALUES
(@ENTRY,0,0,0,2,0,100,1,0,15,0,0,25,0,0,0,0,0,0,1,0,0,0,0,0,0,0,'Flee at 15% HP'),
(@ENTRY,0,1,0,0,0,100,0,5000,8000,17000,24500,11,7357,0,0,0,0,0,2,0,0,0,0,0,0,0,'Cast Poisonous Stab');

-- Marshtide Cleric
SET @ENTRY := 46841;
UPDATE `creature_template` SET `AIName`='SmartAI' WHERE `entry`=@ENTRY;
DELETE FROM `smart_scripts` WHERE `source_type`=0 AND `entryorguid`=@ENTRY;
INSERT INTO `smart_scripts` (`entryorguid`,`source_type`,`id`,`link`,`event_type`,`event_phase_mask`,`event_chance`,`event_flags`,`event_param1`,`event_param2`,`event_param3`,`event_param4`,`action_type`,`action_param1`,`action_param2`,`action_param3`,`action_param4`,`action_param5`,`action_param6`,`target_type`,`target_param1`,`target_param2`,`target_param3`,`target_x`,`target_y`,`target_z`,`target_o`,`comment`) VALUES
(@ENTRY,0,0,0,0,0,100,0,0,0,3400,4700,11,9734,64,0,0,0,0,2,0,0,0,0,0,0,0,'Cast Holy Smite'),
(@ENTRY,0,1,0,2,0,100,1,0,55,0,0,11,11974,2,0,0,0,0,1,0,0,0,0,0,0,0,'Cast Power Word: Shield at 55% HP'),
(@ENTRY,0,2,0,14,0,100,0,850,40,15000,18000,11,11642,0,0,0,0,0,7,0,0,0,0,0,0,0,'Cast Heal on Friendlies');

-- Marshtide Footman
SET @ENTRY := 46164;
UPDATE `creature_template` SET `AIName`='SmartAI' WHERE `entry`=@ENTRY;
DELETE FROM `smart_scripts` WHERE `source_type`=0 AND `entryorguid`=@ENTRY;
INSERT INTO `smart_scripts` (`entryorguid`,`source_type`,`id`,`link`,`event_type`,`event_phase_mask`,`event_chance`,`event_flags`,`event_param1`,`event_param2`,`event_param3`,`event_param4`,`action_type`,`action_param1`,`action_param2`,`action_param3`,`action_param4`,`action_param5`,`action_param6`,`target_type`,`target_param1`,`target_param2`,`target_param3`,`target_x`,`target_y`,`target_z`,`target_o`,`comment`) VALUES
(@ENTRY,0,0,0,0,0,100,0,2000,4500,12000,20000,11,11976,0,0,0,0,0,2,0,0,0,0,0,0,0,'Cast Strike'),
(@ENTRY,0,1,0,13,0,100,0,5000,8000,20000,30000,11,11972,0,0,0,0,0,2,0,0,0,0,0,0,0,'Cast Shield Bash on Player Spell Cast');

-- Marshtide Invader
SET @ENTRY := 46869;
UPDATE `creature_template` SET `AIName`='SmartAI' WHERE `entry`=@ENTRY;
DELETE FROM `smart_scripts` WHERE `source_type`=0 AND `entryorguid`=@ENTRY;
INSERT INTO `smart_scripts` (`entryorguid`,`source_type`,`id`,`link`,`event_type`,`event_phase_mask`,`event_chance`,`event_flags`,`event_param1`,`event_param2`,`event_param3`,`event_param4`,`action_type`,`action_param1`,`action_param2`,`action_param3`,`action_param4`,`action_param5`,`action_param6`,`target_type`,`target_param1`,`target_param2`,`target_param3`,`target_x`,`target_y`,`target_z`,`target_o`,`comment`) VALUES
(@ENTRY,0,0,0,0,0,100,0,5000,8000,12000,15000,11,15496,0,0,0,0,0,2,0,0,0,0,0,0,0,'Cast Cleave'),
(@ENTRY,0,1,0,0,0,100,0,9000,12000,22000,27000,11,38770,0,0,0,0,0,2,0,0,0,0,0,0,0,'Cast Mortal Wound');

-- Marshtide Mage
SET @ENTRY := 46775;
UPDATE `creature_template` SET `AIName`='SmartAI' WHERE `entry`=@ENTRY;
DELETE FROM `smart_scripts` WHERE `source_type`=0 AND `entryorguid`=@ENTRY;
INSERT INTO `smart_scripts` (`entryorguid`,`source_type`,`id`,`link`,`event_type`,`event_phase_mask`,`event_chance`,`event_flags`,`event_param1`,`event_param2`,`event_param3`,`event_param4`,`action_type`,`action_param1`,`action_param2`,`action_param3`,`action_param4`,`action_param5`,`action_param6`,`target_type`,`target_param1`,`target_param2`,`target_param3`,`target_x`,`target_y`,`target_z`,`target_o`,`comment`) VALUES
(@ENTRY,0,0,0,0,0,100,0,0,0,3400,4700,11,79858,64,0,0,0,0,2,0,0,0,0,0,0,0,'Cast Frostbolt'),
(@ENTRY,0,1,0,0,0,100,0,8000,8000,19000,25000,11,79860,0,0,0,0,0,4,0,0,0,0,0,0,0,'Cast Blizzard'),
(@ENTRY,0,2,0,1,0,100,1,8000,15000,0,0,11,87116,0,0,0,0,0,1,0,0,0,0,0,0,0,'Cast Ritual of Summoning - Summon Portal');

-- Marshtide Marksman
SET @ENTRY := 46876;
UPDATE `creature_template` SET `AIName`='SmartAI' WHERE `entry`=@ENTRY;
DELETE FROM `smart_scripts` WHERE `source_type`=0 AND `entryorguid`=@ENTRY;
INSERT INTO `smart_scripts` (`entryorguid`,`source_type`,`id`,`link`,`event_type`,`event_phase_mask`,`event_chance`,`event_flags`,`event_param1`,`event_param2`,`event_param3`,`event_param4`,`action_type`,`action_param1`,`action_param2`,`action_param3`,`action_param4`,`action_param5`,`action_param6`,`target_type`,`target_param1`,`target_param2`,`target_param3`,`target_x`,`target_y`,`target_z`,`target_o`,`comment`) VALUES
(@ENTRY,0,0,0,0,0,100,0,0,0,2300,3900,11,6660,64,0,0,0,0,2,0,0,0,0,0,0,0,'Cast Shoot'),
(@ENTRY,0,1,0,9,0,100,0,0,5,15000,25000,11,32915,0,0,0,0,0,2,0,0,0,0,0,0,0,'Cast Raptor Strike on Close'),
(@ENTRY,0,2,0,9,0,100,0,5,30,18000,22000,11,18651,0,0,0,0,0,2,0,0,0,0,0,0,0,'Cast Multi-Shot on Close');

-- Marshtide Peasant
SET @ENTRY := 46487;
UPDATE `creature_template` SET `AIName`='SmartAI' WHERE `entry`=@ENTRY;
DELETE FROM `smart_scripts` WHERE `source_type`=0 AND `entryorguid`=@ENTRY;
INSERT INTO `smart_scripts` (`entryorguid`,`source_type`,`id`,`link`,`event_type`,`event_phase_mask`,`event_chance`,`event_flags`,`event_param1`,`event_param2`,`event_param3`,`event_param4`,`action_type`,`action_param1`,`action_param2`,`action_param3`,`action_param4`,`action_param5`,`action_param6`,`target_type`,`target_param1`,`target_param2`,`target_param3`,`target_x`,`target_y`,`target_z`,`target_o`,`comment`) VALUES
(@ENTRY,0,0,0,2,0,100,1,0,55,0,0,25,0,0,0,0,0,0,1,0,0,0,0,0,0,0,'Flee at 55% HP');

-- Marshtide Sentry
SET @ENTRY := 46714;
UPDATE `creature_template` SET `AIName`='SmartAI' WHERE `entry`=@ENTRY;
DELETE FROM `smart_scripts` WHERE `source_type`=0 AND `entryorguid`=@ENTRY;
INSERT INTO `smart_scripts` (`entryorguid`,`source_type`,`id`,`link`,`event_type`,`event_phase_mask`,`event_chance`,`event_flags`,`event_param1`,`event_param2`,`event_param3`,`event_param4`,`action_type`,`action_param1`,`action_param2`,`action_param3`,`action_param4`,`action_param5`,`action_param6`,`target_type`,`target_param1`,`target_param2`,`target_param3`,`target_x`,`target_y`,`target_z`,`target_o`,`comment`) VALUES
(@ENTRY,0,0,0,0,0,100,0,0,0,2300,3900,11,6660,64,0,0,0,0,2,0,0,0,0,0,0,0,'Cast Shoot'),
(@ENTRY,0,1,0,9,0,100,0,0,10,19000,25000,11,78578,0,0,0,0,0,2,0,0,0,0,0,0,0,'Cast Immolation Trap on Close'),
(@ENTRY,0,2,0,9,0,100,0,5,30,18000,22000,11,80009,0,0,0,0,0,2,0,0,0,0,0,0,0,'Cast Serpent Sting on Close'),
(@ENTRY,0,3,0,9,0,100,0,5,30,10000,20000,11,80012,0,0,0,0,0,2,0,0,0,0,0,0,0,'Cast Chimera Shot on Close');

-- Misty Grell
SET @ENTRY := 46950;
UPDATE `creature_template` SET `AIName`='SmartAI' WHERE `entry`=@ENTRY;
DELETE FROM `smart_scripts` WHERE `source_type`=0 AND `entryorguid`=@ENTRY;
INSERT INTO `smart_scripts` (`entryorguid`,`source_type`,`id`,`link`,`event_type`,`event_phase_mask`,`event_chance`,`event_flags`,`event_param1`,`event_param2`,`event_param3`,`event_param4`,`action_type`,`action_param1`,`action_param2`,`action_param3`,`action_param4`,`action_param5`,`action_param6`,`target_type`,`target_param1`,`target_param2`,`target_param3`,`target_x`,`target_y`,`target_z`,`target_o`,`comment`) VALUES
(@ENTRY,0,0,0,0,0,100,0,0,0,3400,4700,11,36227,64,0,0,0,0,2,0,0,0,0,0,0,0,'Cast Firebolt');

-- Molt Thorn
SET @ENTRY := 14448;
UPDATE `creature_template` SET `AIName`='SmartAI' WHERE `entry`=@ENTRY;
DELETE FROM `smart_scripts` WHERE `source_type`=0 AND `entryorguid`=@ENTRY;
INSERT INTO `smart_scripts` (`entryorguid`,`source_type`,`id`,`link`,`event_type`,`event_phase_mask`,`event_chance`,`event_flags`,`event_param1`,`event_param2`,`event_param3`,`event_param4`,`action_type`,`action_param1`,`action_param2`,`action_param3`,`action_param4`,`action_param5`,`action_param6`,`target_type`,`target_param1`,`target_param2`,`target_param3`,`target_x`,`target_y`,`target_z`,`target_o`,`comment`) VALUES
(@ENTRY,0,0,0,0,0,100,0,8000,8000,25000,28000,11,21748,0,0,0,0,0,1,0,0,0,0,0,0,0,'Cast Thorn Volley');

-- Monstrous Crawler
SET @ENTRY := 1088;
UPDATE `creature_template` SET `AIName`='SmartAI' WHERE `entry`=@ENTRY;
DELETE FROM `smart_scripts` WHERE `source_type`=0 AND `entryorguid`=@ENTRY;
INSERT INTO `smart_scripts` (`entryorguid`,`source_type`,`id`,`link`,`event_type`,`event_phase_mask`,`event_chance`,`event_flags`,`event_param1`,`event_param2`,`event_param3`,`event_param4`,`action_type`,`action_param1`,`action_param2`,`action_param3`,`action_param4`,`action_param5`,`action_param6`,`target_type`,`target_param1`,`target_param2`,`target_param3`,`target_x`,`target_y`,`target_z`,`target_o`,`comment`) VALUES
(@ENTRY,0,0,0,0,0,100,0,5000,6000,12000,18500,11,13443,0,0,0,0,0,2,0,0,0,0,0,0,0,'Cast Rend');

-- Orlix the Swamplord
SET @ENTRY := 50903;
UPDATE `creature_template` SET `AIName`='SmartAI' WHERE `entry`=@ENTRY;
DELETE FROM `smart_scripts` WHERE `source_type`=0 AND `entryorguid`=@ENTRY;
INSERT INTO `smart_scripts` (`entryorguid`,`source_type`,`id`,`link`,`event_type`,`event_phase_mask`,`event_chance`,`event_flags`,`event_param1`,`event_param2`,`event_param3`,`event_param4`,`action_type`,`action_param1`,`action_param2`,`action_param3`,`action_param4`,`action_param5`,`action_param6`,`target_type`,`target_param1`,`target_param2`,`target_param3`,`target_x`,`target_y`,`target_z`,`target_o`,`comment`) VALUES
(@ENTRY,0,0,0,2,0,100,0,0,30,22000,25000,11,34392,2,0,0,0,0,1,0,0,0,0,0,0,0,'Cast Stinger Rage at 30% HP');

-- Purespring Elemental
SET @ENTRY := 46953;
UPDATE `creature_template` SET `AIName`='SmartAI' WHERE `entry`=@ENTRY;
DELETE FROM `smart_scripts` WHERE `source_type`=0 AND `entryorguid`=@ENTRY;
INSERT INTO `smart_scripts` (`entryorguid`,`source_type`,`id`,`link`,`event_type`,`event_phase_mask`,`event_chance`,`event_flags`,`event_param1`,`event_param2`,`event_param3`,`event_param4`,`action_type`,`action_param1`,`action_param2`,`action_param3`,`action_param4`,`action_param5`,`action_param6`,`target_type`,`target_param1`,`target_param2`,`target_param3`,`target_x`,`target_y`,`target_z`,`target_o`,`comment`) VALUES
(@ENTRY,0,0,0,0,0,100,0,0,0,3400,4700,11,32011,64,0,0,0,0,2,0,0,0,0,0,0,0,'Cast Water Bolt'),
(@ENTRY,0,1,0,0,0,100,0,7000,8000,12000,15000,11,39207,0,0,0,0,0,2,0,0,0,0,0,0,0,'Cast Water Spout');

-- Sawtooth Crocolisk
SET @ENTRY := 45807;
UPDATE `creature_template` SET `AIName`='SmartAI' WHERE `entry`=@ENTRY;
DELETE FROM `smart_scripts` WHERE `source_type`=0 AND `entryorguid`=@ENTRY;
INSERT INTO `smart_scripts` (`entryorguid`,`source_type`,`id`,`link`,`event_type`,`event_phase_mask`,`event_chance`,`event_flags`,`event_param1`,`event_param2`,`event_param3`,`event_param4`,`action_type`,`action_param1`,`action_param2`,`action_param3`,`action_param4`,`action_param5`,`action_param6`,`target_type`,`target_param1`,`target_param2`,`target_param3`,`target_x`,`target_y`,`target_z`,`target_o`,`comment`) VALUES
(@ENTRY,0,0,0,0,0,100,0,5000,5000,15000,15000,11,48287,0,0,0,0,0,2,0,0,0,0,0,0,0,'Cast Powerful Bite'),
(@ENTRY,0,1,0,2,0,100,0,0,35,22000,25000,11,87228,2,0,0,0,0,1,0,0,0,0,0,0,0,'Cast Thick Hide at 35% HP');

-- Seawing
SET @ENTRY := 50886;
UPDATE `creature_template` SET `AIName`='SmartAI' WHERE `entry`=@ENTRY;
DELETE FROM `smart_scripts` WHERE `source_type`=0 AND `entryorguid`=@ENTRY;
INSERT INTO `smart_scripts` (`entryorguid`,`source_type`,`id`,`link`,`event_type`,`event_phase_mask`,`event_chance`,`event_flags`,`event_param1`,`event_param2`,`event_param3`,`event_param4`,`action_type`,`action_param1`,`action_param2`,`action_param3`,`action_param4`,`action_param5`,`action_param6`,`target_type`,`target_param1`,`target_param2`,`target_param3`,`target_x`,`target_y`,`target_z`,`target_o`,`comment`) VALUES
(@ENTRY,0,0,0,4,0,100,1,0,0,0,0,11,66060,0,0,0,0,0,1,0,0,0,0,0,0,0,'Cast Sprint on Aggro'),
(@ENTRY,0,1,0,0,0,100,0,5000,5000,12000,17500,11,81678,0,0,0,0,0,2,0,0,0,0,0,0,0,'Cast Eye Peck');

-- Shifting Mireglob
SET @ENTRY := 46997;
UPDATE `creature_template` SET `AIName`='SmartAI' WHERE `entry`=@ENTRY;
DELETE FROM `smart_scripts` WHERE `source_type`=0 AND `entryorguid`=@ENTRY;
INSERT INTO `smart_scripts` (`entryorguid`,`source_type`,`id`,`link`,`event_type`,`event_phase_mask`,`event_chance`,`event_flags`,`event_param1`,`event_param2`,`event_param3`,`event_param4`,`action_type`,`action_param1`,`action_param2`,`action_param3`,`action_param4`,`action_param5`,`action_param6`,`target_type`,`target_param1`,`target_param2`,`target_param3`,`target_x`,`target_y`,`target_z`,`target_o`,`comment`) VALUES
(@ENTRY,0,0,0,0,0,100,0,0,0,3400,4700,11,21067,64,0,0,0,0,2,0,0,0,0,0,0,0,'Cast Poison Bolt'),
(@ENTRY,0,1,0,9,0,100,0,0,5,15000,20000,11,22595,0,0,0,0,0,2,0,0,0,0,0,0,0,'Cast Poison Shock on Close');

-- Shimmerscale
SET @ENTRY := 50738;
UPDATE `creature_template` SET `AIName`='SmartAI' WHERE `entry`=@ENTRY;
DELETE FROM `smart_scripts` WHERE `source_type`=0 AND `entryorguid`=@ENTRY;
INSERT INTO `smart_scripts` (`entryorguid`,`source_type`,`id`,`link`,`event_type`,`event_phase_mask`,`event_chance`,`event_flags`,`event_param1`,`event_param2`,`event_param3`,`event_param4`,`action_type`,`action_param1`,`action_param2`,`action_param3`,`action_param4`,`action_param5`,`action_param6`,`target_type`,`target_param1`,`target_param2`,`target_param3`,`target_x`,`target_y`,`target_z`,`target_o`,`comment`) VALUES
(@ENTRY,0,0,0,0,0,100,0,5000,8000,12000,15000,11,89905,0,0,0,0,0,2,0,0,0,0,0,0,0,'Cast Venomous Bite');

-- Silt Crawler
SET @ENTRY := 922;
UPDATE `creature_template` SET `AIName`='SmartAI' WHERE `entry`=@ENTRY;
DELETE FROM `smart_scripts` WHERE `source_type`=0 AND `entryorguid`=@ENTRY;
INSERT INTO `smart_scripts` (`entryorguid`,`source_type`,`id`,`link`,`event_type`,`event_phase_mask`,`event_chance`,`event_flags`,`event_param1`,`event_param2`,`event_param3`,`event_param4`,`action_type`,`action_param1`,`action_param2`,`action_param3`,`action_param4`,`action_param5`,`action_param6`,`target_type`,`target_param1`,`target_param2`,`target_param3`,`target_x`,`target_y`,`target_z`,`target_o`,`comment`) VALUES
(@ENTRY,0,0,0,0,0,100,0,5000,8000,12000,15000,11,5424,0,0,0,0,0,2,0,0,0,0,0,0,0,'Cast Claw Grasp'),
(@ENTRY,0,1,0,25,0,50,0,0,0,0,0,11,88038,0,0,0,0,0,1,0,0,0,0,0,0,0,'Cast Stolen Silversnap Ice on Spawn');

-- Sorrow Screecher
SET @ENTRY := 48249;
UPDATE `creature_template` SET `AIName`='SmartAI' WHERE `entry`=@ENTRY;
DELETE FROM `smart_scripts` WHERE `source_type`=0 AND `entryorguid`=@ENTRY;
INSERT INTO `smart_scripts` (`entryorguid`,`source_type`,`id`,`link`,`event_type`,`event_phase_mask`,`event_chance`,`event_flags`,`event_param1`,`event_param2`,`event_param3`,`event_param4`,`action_type`,`action_param1`,`action_param2`,`action_param3`,`action_param4`,`action_param5`,`action_param6`,`target_type`,`target_param1`,`target_param2`,`target_param3`,`target_x`,`target_y`,`target_z`,`target_o`,`comment`) VALUES
(@ENTRY,0,0,0,0,0,100,0,7500,8000,12000,15000,11,3589,0,0,0,0,0,2,0,0,0,0,0,0,0,'Cast Deafening Screech');

-- Sorrow Spinner
SET @ENTRY := 858;
UPDATE `creature_template` SET `AIName`='SmartAI' WHERE `entry`=@ENTRY;
DELETE FROM `smart_scripts` WHERE `source_type`=0 AND `entryorguid`=@ENTRY;
INSERT INTO `smart_scripts` (`entryorguid`,`source_type`,`id`,`link`,`event_type`,`event_phase_mask`,`event_chance`,`event_flags`,`event_param1`,`event_param2`,`event_param3`,`event_param4`,`action_type`,`action_param1`,`action_param2`,`action_param3`,`action_param4`,`action_param5`,`action_param6`,`target_type`,`target_param1`,`target_param2`,`target_param3`,`target_x`,`target_y`,`target_z`,`target_o`,`comment`) VALUES
(@ENTRY,0,0,0,9,0,100,0,0,30,15000,25000,11,745,0,0,0,0,0,2,0,0,0,0,0,0,0,'Cast Web on Close'),
(@ENTRY,0,1,0,0,0,100,0,5000,5000,17000,19000,11,744,0,0,0,0,0,2,0,0,0,0,0,0,0,'Cast Poison');

-- Sorrow Venomspitter
SET @ENTRY := 48248;
UPDATE `creature_template` SET `AIName`='SmartAI' WHERE `entry`=@ENTRY;
DELETE FROM `smart_scripts` WHERE `source_type`=0 AND `entryorguid`=@ENTRY;
INSERT INTO `smart_scripts` (`entryorguid`,`source_type`,`id`,`link`,`event_type`,`event_phase_mask`,`event_chance`,`event_flags`,`event_param1`,`event_param2`,`event_param3`,`event_param4`,`action_type`,`action_param1`,`action_param2`,`action_param3`,`action_param4`,`action_param5`,`action_param6`,`target_type`,`target_param1`,`target_param2`,`target_param3`,`target_x`,`target_y`,`target_z`,`target_o`,`comment`) VALUES
(@ENTRY,0,0,0,0,0,100,0,6000,8000,12000,14500,11,6917,0,0,0,0,0,2,0,0,0,0,0,0,0,'Cast Venom Spit');

-- Sorrowmurk Snapjaw
SET @ENTRY := 45950;
UPDATE `creature_template` SET `AIName`='SmartAI' WHERE `entry`=@ENTRY;
DELETE FROM `smart_scripts` WHERE `source_type`=0 AND `entryorguid`=@ENTRY;
INSERT INTO `smart_scripts` (`entryorguid`,`source_type`,`id`,`link`,`event_type`,`event_phase_mask`,`event_chance`,`event_flags`,`event_param1`,`event_param2`,`event_param3`,`event_param4`,`action_type`,`action_param1`,`action_param2`,`action_param3`,`action_param4`,`action_param5`,`action_param6`,`target_type`,`target_param1`,`target_param2`,`target_param3`,`target_x`,`target_y`,`target_z`,`target_o`,`comment`) VALUES
(@ENTRY,0,0,0,0,0,100,0,5000,9000,18000,22000,11,80604,0,0,0,0,0,2,0,0,0,0,0,0,0,'Cast Crushing Bite');

-- Stagalbog Serpent
SET @ENTRY := 46146;
UPDATE `creature_template` SET `AIName`='SmartAI' WHERE `entry`=@ENTRY;
DELETE FROM `smart_scripts` WHERE `source_type`=0 AND `entryorguid`=@ENTRY;
INSERT INTO `smart_scripts` (`entryorguid`,`source_type`,`id`,`link`,`event_type`,`event_phase_mask`,`event_chance`,`event_flags`,`event_param1`,`event_param2`,`event_param3`,`event_param4`,`action_type`,`action_param1`,`action_param2`,`action_param3`,`action_param4`,`action_param5`,`action_param6`,`target_type`,`target_param1`,`target_param2`,`target_param3`,`target_x`,`target_y`,`target_z`,`target_o`,`comment`) VALUES
(@ENTRY,0,0,0,0,0,100,0,5000,8000,12000,15000,11,36594,0,0,0,0,0,2,0,0,0,0,0,0,0,'Cast Lightning Breath');

-- Stonard Defender
SET @ENTRY := 46870;
UPDATE `creature_template` SET `AIName`='SmartAI' WHERE `entry`=@ENTRY;
DELETE FROM `smart_scripts` WHERE `source_type`=0 AND `entryorguid`=@ENTRY;
INSERT INTO `smart_scripts` (`entryorguid`,`source_type`,`id`,`link`,`event_type`,`event_phase_mask`,`event_chance`,`event_flags`,`event_param1`,`event_param2`,`event_param3`,`event_param4`,`action_type`,`action_param1`,`action_param2`,`action_param3`,`action_param4`,`action_param5`,`action_param6`,`target_type`,`target_param1`,`target_param2`,`target_param3`,`target_x`,`target_y`,`target_z`,`target_o`,`comment`) VALUES
(@ENTRY,0,0,0,0,0,100,0,5000,8000,12000,13000,11,15496,0,0,0,0,0,2,0,0,0,0,0,0,0,'Cast Cleave'),
(@ENTRY,0,1,0,2,0,100,0,0,40,22000,25000,11,79878,2,0,0,0,0,1,0,0,0,0,0,0,0,'Cast Bloodthirst at 40% HP');

-- Stonard Grunt
SET @ENTRY := 866;
UPDATE `creature_template` SET `AIName`='SmartAI' WHERE `entry`=@ENTRY;
DELETE FROM `smart_scripts` WHERE `source_type`=0 AND `entryorguid`=@ENTRY;
INSERT INTO `smart_scripts` (`entryorguid`,`source_type`,`id`,`link`,`event_type`,`event_phase_mask`,`event_chance`,`event_flags`,`event_param1`,`event_param2`,`event_param3`,`event_param4`,`action_type`,`action_param1`,`action_param2`,`action_param3`,`action_param4`,`action_param5`,`action_param6`,`target_type`,`target_param1`,`target_param2`,`target_param3`,`target_x`,`target_y`,`target_z`,`target_o`,`comment`) VALUES
(@ENTRY,0,0,0,2,0,100,0,0,55,10000,15000,11,12169,2,0,0,0,0,1,0,0,0,0,0,0,0,'Cast Shield Block at 55% HP'),
(@ENTRY,0,1,0,0,0,100,0,5000,8000,12000,15000,11,12170,0,0,0,0,0,2,0,0,0,0,0,0,0,'Cast Revenge');

-- Stonard Hunter
SET @ENTRY := 863;
UPDATE `creature_template` SET `AIName`='SmartAI' WHERE `entry`=@ENTRY;
DELETE FROM `smart_scripts` WHERE `source_type`=0 AND `entryorguid`=@ENTRY;
INSERT INTO `smart_scripts` (`entryorguid`,`source_type`,`id`,`link`,`event_type`,`event_phase_mask`,`event_chance`,`event_flags`,`event_param1`,`event_param2`,`event_param3`,`event_param4`,`action_type`,`action_param1`,`action_param2`,`action_param3`,`action_param4`,`action_param5`,`action_param6`,`target_type`,`target_param1`,`target_param2`,`target_param3`,`target_x`,`target_y`,`target_z`,`target_o`,`comment`) VALUES
(@ENTRY,0,0,0,0,0,100,0,0,0,2300,3900,11,6660,64,0,0,0,0,2,0,0,0,0,0,0,0,'Cast Shoot');

-- Stonard Ogre
SET @ENTRY := 46765;
UPDATE `creature_template` SET `AIName`='SmartAI' WHERE `entry`=@ENTRY;
DELETE FROM `smart_scripts` WHERE `source_type`=0 AND `entryorguid`=@ENTRY;
INSERT INTO `smart_scripts` (`entryorguid`,`source_type`,`id`,`link`,`event_type`,`event_phase_mask`,`event_chance`,`event_flags`,`event_param1`,`event_param2`,`event_param3`,`event_param4`,`action_type`,`action_param1`,`action_param2`,`action_param3`,`action_param4`,`action_param5`,`action_param6`,`target_type`,`target_param1`,`target_param2`,`target_param3`,`target_x`,`target_y`,`target_z`,`target_o`,`comment`) VALUES
(@ENTRY,0,0,0,0,0,100,0,8000,8000,22000,28000,11,4955,0,0,0,0,0,1,0,0,0,0,0,0,0,'Cast Fist of Stone');

-- Stonard Peon
SET @ENTRY := 46486;
UPDATE `creature_template` SET `AIName`='SmartAI' WHERE `entry`=@ENTRY;
DELETE FROM `smart_scripts` WHERE `source_type`=0 AND `entryorguid`=@ENTRY;
INSERT INTO `smart_scripts` (`entryorguid`,`source_type`,`id`,`link`,`event_type`,`event_phase_mask`,`event_chance`,`event_flags`,`event_param1`,`event_param2`,`event_param3`,`event_param4`,`action_type`,`action_param1`,`action_param2`,`action_param3`,`action_param4`,`action_param5`,`action_param6`,`target_type`,`target_param1`,`target_param2`,`target_param3`,`target_x`,`target_y`,`target_z`,`target_o`,`comment`) VALUES
(@ENTRY,0,0,0,2,0,100,1,0,15,0,0,25,0,0,0,0,0,0,1,0,0,0,0,0,0,0,'Flee at 15% HP');

-- Stonard Wardrummer
SET @ENTRY := 46749;
UPDATE `creature_template` SET `AIName`='SmartAI' WHERE `entry`=@ENTRY;
DELETE FROM `smart_scripts` WHERE `source_type`=0 AND `entryorguid`=@ENTRY;
INSERT INTO `smart_scripts` (`entryorguid`,`source_type`,`id`,`link`,`event_type`,`event_phase_mask`,`event_chance`,`event_flags`,`event_param1`,`event_param2`,`event_param3`,`event_param4`,`action_type`,`action_param1`,`action_param2`,`action_param3`,`action_param4`,`action_param5`,`action_param6`,`target_type`,`target_param1`,`target_param2`,`target_param3`,`target_x`,`target_y`,`target_z`,`target_o`,`comment`) VALUES
(@ENTRY,0,0,0,0,0,100,0,5000,8000,27000,32000,11,81219,0,0,0,0,0,1,0,0,0,0,0,0,0,'Cast Battle Shout');

-- Stonard Warlock
SET @ENTRY := 46770;
UPDATE `creature_template` SET `AIName`='SmartAI' WHERE `entry`=@ENTRY;
DELETE FROM `smart_scripts` WHERE `source_type`=0 AND `entryorguid`=@ENTRY;
INSERT INTO `smart_scripts` (`entryorguid`,`source_type`,`id`,`link`,`event_type`,`event_phase_mask`,`event_chance`,`event_flags`,`event_param1`,`event_param2`,`event_param3`,`event_param4`,`action_type`,`action_param1`,`action_param2`,`action_param3`,`action_param4`,`action_param5`,`action_param6`,`target_type`,`target_param1`,`target_param2`,`target_param3`,`target_x`,`target_y`,`target_z`,`target_o`,`comment`) VALUES
(@ENTRY,0,0,0,1,0,100,1,3000,5000,0,0,11,11939,0,0,0,0,0,1,0,0,0,0,0,0,0,'Cast Summon Imp on Spawn'),
(@ENTRY,0,1,0,0,0,100,0,0,0,3400,4700,11,9613,64,0,0,0,0,2,0,0,0,0,0,0,0,'Cast Shadow Bolt'),
(@ENTRY,0,2,0,0,0,100,0,9000,9000,20000,27000,11,11980,0,0,0,0,0,2,0,0,0,0,0,0,0,'Cast Curse of Weakness'),
(@ENTRY,0,3,0,1,0,100,1,8000,15000,0,0,11,87116,0,0,0,0,0,1,0,0,0,0,0,0,0,'Cast Ritual of Summoning - Summon Portal');

-- Stonard Warrior
SET @ENTRY := 46166;
UPDATE `creature_template` SET `AIName`='SmartAI' WHERE `entry`=@ENTRY;
DELETE FROM `smart_scripts` WHERE `source_type`=0 AND `entryorguid`=@ENTRY;
INSERT INTO `smart_scripts` (`entryorguid`,`source_type`,`id`,`link`,`event_type`,`event_phase_mask`,`event_chance`,`event_flags`,`event_param1`,`event_param2`,`event_param3`,`event_param4`,`action_type`,`action_param1`,`action_param2`,`action_param3`,`action_param4`,`action_param5`,`action_param6`,`target_type`,`target_param1`,`target_param2`,`target_param3`,`target_x`,`target_y`,`target_z`,`target_o`,`comment`) VALUES
(@ENTRY,0,0,0,4,0,100,1,0,0,0,0,11,22120,0,0,0,0,0,2,0,0,0,0,0,0,0,'Cast Charge on Aggro'),
(@ENTRY,0,1,0,0,0,100,0,5000,5000,13000,17500,11,11977,0,0,0,0,0,2,0,0,0,0,0,0,0,'Cast Rend');

-- Swampshore Makrura
SET @ENTRY := 45809;
UPDATE `creature_template` SET `AIName`='SmartAI' WHERE `entry`=@ENTRY;
DELETE FROM `smart_scripts` WHERE `source_type`=0 AND `entryorguid`=@ENTRY;
INSERT INTO `smart_scripts` (`entryorguid`,`source_type`,`id`,`link`,`event_type`,`event_phase_mask`,`event_chance`,`event_flags`,`event_param1`,`event_param2`,`event_param3`,`event_param4`,`action_type`,`action_param1`,`action_param2`,`action_param3`,`action_param4`,`action_param5`,`action_param6`,`target_type`,`target_param1`,`target_param2`,`target_param3`,`target_x`,`target_y`,`target_z`,`target_o`,`comment`) VALUES
(@ENTRY,0,0,0,0,0,100,0,5000,8000,12000,15000,11,3148,0,0,0,0,0,2,0,0,0,0,0,0,0,'Cast Head Crack');

-- Swampstrider
SET @ENTRY := 45825;
UPDATE `creature_template` SET `AIName`='SmartAI' WHERE `entry`=@ENTRY;
DELETE FROM `smart_scripts` WHERE `source_type`=0 AND `entryorguid`=@ENTRY;
INSERT INTO `smart_scripts` (`entryorguid`,`source_type`,`id`,`link`,`event_type`,`event_phase_mask`,`event_chance`,`event_flags`,`event_param1`,`event_param2`,`event_param3`,`event_param4`,`action_type`,`action_param1`,`action_param2`,`action_param3`,`action_param4`,`action_param5`,`action_param6`,`target_type`,`target_param1`,`target_param2`,`target_param3`,`target_x`,`target_y`,`target_z`,`target_o`,`comment`) VALUES
(@ENTRY,0,0,0,4,0,100,1,0,0,0,0,11,66060,0,0,0,0,0,1,0,0,0,0,0,0,0,'Cast Sprint on Aggro'),
(@ENTRY,0,1,0,0,0,100,0,5000,5000,12000,17500,11,81678,0,0,0,0,0,2,0,0,0,0,0,0,0,'Cast Eye Peck');

-- Yukiko
SET @ENTRY := 50797;
UPDATE `creature_template` SET `AIName`='SmartAI' WHERE `entry`=@ENTRY;
DELETE FROM `smart_scripts` WHERE `source_type`=0 AND `entryorguid`=@ENTRY;
INSERT INTO `smart_scripts` (`entryorguid`,`source_type`,`id`,`link`,`event_type`,`event_phase_mask`,`event_chance`,`event_flags`,`event_param1`,`event_param2`,`event_param3`,`event_param4`,`action_type`,`action_param1`,`action_param2`,`action_param3`,`action_param4`,`action_param5`,`action_param6`,`target_type`,`target_param1`,`target_param2`,`target_param3`,`target_x`,`target_y`,`target_z`,`target_o`,`comment`) VALUES
(@ENTRY,0,0,0,0,0,100,0,5000,8000,12000,15000,11,79085,0,0,0,0,0,2,0,0,0,0,0,0,0,'Cast Lightning Bolt');
-- Actionlist SAI
SET @ACTIONLIST := 5458700;
DELETE FROM `smart_scripts` WHERE `entryorguid` IN(@ACTIONLIST,@ACTIONLIST+1,@ACTIONLIST+2,@ACTIONLIST+3) AND `source_type`=9;
INSERT INTO `smart_scripts` (`entryorguid`,`source_type`,`id`,`link`,`event_type`,`event_phase_mask`,`event_chance`,`event_flags`,`event_param1`,`event_param2`,`event_param3`,`event_param4`,`action_type`,`action_param1`,`action_param2`,`action_param3`,`action_param4`,`action_param5`,`action_param6`,`target_type`,`target_param1`,`target_param2`,`target_param3`,`target_x`,`target_y`,`target_z`,`target_o`,`comment`) VALUES
(@ACTIONLIST+0,9,0,0,0,0,100,1,1000,1000,0,0,5,509,0,0,0,0,0,1,0,0,0,0,0,0,0,"On Script - Play Emote 509 (No Repeat)"),
(@ACTIONLIST+1,9,0,0,0,0,100,1,1000,1000,0,0,5,543,0,0,0,0,0,1,0,0,0,0,0,0,0,"On Script - Play Emote 543 (No Repeat)"),
(@ACTIONLIST+2,9,0,0,0,0,100,1,1000,1000,0,0,5,511,0,0,0,0,0,1,0,0,0,0,0,0,0,"On Script - Play Emote 511 (No Repeat)"),
(@ACTIONLIST+3,9,0,0,0,0,100,1,1000,1000,0,0,5,507,0,0,0,0,0,1,0,0,0,0,0,0,0,"On Script - Play Emote 507 (No Repeat)");

-- Actionlist SAI
SET @ENTRY := 5458704;
DELETE FROM `smart_scripts` WHERE `entryorguid`=@ENTRY AND `source_type`=9;
INSERT INTO `smart_scripts` (`entryorguid`,`source_type`,`id`,`link`,`event_type`,`event_phase_mask`,`event_chance`,`event_flags`,`event_param1`,`event_param2`,`event_param3`,`event_param4`,`action_type`,`action_param1`,`action_param2`,`action_param3`,`action_param4`,`action_param5`,`action_param6`,`target_type`,`target_param1`,`target_param2`,`target_param3`,`target_x`,`target_y`,`target_z`,`target_o`,`comment`) VALUES
(@ENTRY,9,0,0,0,0,100,1,0,0,0,0,18,33554688,0,0,0,0,0,1,0,0,0,0,0,0,0,"Tushui Trainee - On Script - Set Flags Immune To Players & Not Selectable (No Repeat)"),
(@ENTRY,9,1,0,0,0,100,1,0,0,0,0,27,0,0,0,0,0,0,1,0,0,0,0,0,0,0,"Tushui Trainee - On Script - Combat Stop (No Repeat)"),
(@ENTRY,9,2,0,0,0,100,1,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,"Tushui Trainee - On Script - Say Line 0 (No Repeat)"),
(@ENTRY,9,3,0,0,0,100,1,0,0,0,0,41,4000,4000,0,0,0,0,1,0,0,0,0,0,0,0,"Tushui Trainee - On Script - Despawn Instant (No Repeat)");

UPDATE `creature_template` SET `flags_extra`=2 WHERE `entry` IN (65471,65470,54587,54586);
UPDATE `creature_template` SET `unit_flags`=32768, `unit_flags2`=2048 WHERE `entry`=53714;

-- Tushui Trainee SAI
SET @ENTRY := 54587;
UPDATE `creature_template` SET `AIName`="SmartAI" WHERE `entry`=@ENTRY;
DELETE FROM `smart_scripts` WHERE `entryorguid`=@ENTRY AND `source_type`=0;
INSERT INTO `smart_scripts` (`entryorguid`,`source_type`,`id`,`link`,`event_type`,`event_phase_mask`,`event_chance`,`event_flags`,`event_param1`,`event_param2`,`event_param3`,`event_param4`,`action_type`,`action_param1`,`action_param2`,`action_param3`,`action_param4`,`action_param5`,`action_param6`,`target_type`,`target_param1`,`target_param2`,`target_param3`,`target_x`,`target_y`,`target_z`,`target_o`,`comment`) VALUES
(@ENTRY,0,1,0,38,1,100,0,1,1,0,0,80,@ENTRY*100+00,0,0,0,0,0,1,0,0,0,0,0,0,0,"Tushui Trainee - On Data Set 1 1 - Run Script"),
(@ENTRY,0,2,0,38,1,100,0,1,2,0,0,80,@ENTRY*100+01,0,0,0,0,0,1,0,0,0,0,0,0,0,"Tushui Trainee - On Data Set 1 2 - Run Script"),
(@ENTRY,0,3,0,38,1,100,0,1,3,0,0,80,@ENTRY*100+02,0,0,0,0,0,1,0,0,0,0,0,0,0,"Tushui Trainee - On Data Set 1 3 - Run Script"),
(@ENTRY,0,4,0,38,1,100,0,1,4,0,0,80,@ENTRY*100+03,0,0,0,0,0,1,0,0,0,0,0,0,0,"Tushui Trainee - On Data Set 1 4 - Run Script"),
(@ENTRY,0,5,0,0,0,100,0,2000,2000,2000,2000,11,109080,0,0,0,0,0,2,0,0,0,0,0,0,0,"Tushui Trainee - In Combat - Cast 'Blackout Kick'"),
(@ENTRY,0,6,7,2,0,100,1,0,10,0,0,80,@ENTRY*100+04,2,0,0,0,0,1,0,0,0,0,0,0,0,"Tushui Trainee - Between 0-10% Health - Run Script (No Repeat)"),
(@ENTRY,0,7,8,61,0,100,0,0,10,0,0,33,54586,0,0,0,0,0,2,0,0,0,0,0,0,0,"Tushui Trainee - Between 0-10% Health - Quest Credit '54586' (No Repeat)"),
(@ENTRY,0,8,0,61,0,100,0,0,10,0,0,22,2,0,0,0,0,0,1,0,0,0,0,0,0,0,"Tushui Trainee - Between 0-10% Health - Set Event Phase 2 (No Repeat)"),
(@ENTRY,0,9,0,25,0,100,0,0,0,0,0,42,0,10,0,0,0,0,1,0,0,0,0,0,0,0,"Tushui Trainee - On Reset - Set Invincibility Hp 10%"),
(@ENTRY,0,10,0,25,0,100,0,0,0,0,0,22,1,0,0,0,0,0,1,0,0,0,0,0,0,0,"Tushui Trainee - On Reset - Set Event Phase 1"),
(@ENTRY,0,11,0,25,0,100,0,0,0,0,0,19,33554688,0,0,0,0,0,1,0,0,0,0,0,0,0,"Tushui Trainee - On Reset - Remove Flags Immune To Players & Not Selectable");

-- Tushui Trainee SAI
SET @ENTRY := 65471;
UPDATE `creature_template` SET `AIName`="SmartAI" WHERE `entry`=@ENTRY;
DELETE FROM `smart_scripts` WHERE `entryorguid`=@ENTRY AND `source_type`=0;
INSERT INTO `smart_scripts` (`entryorguid`,`source_type`,`id`,`link`,`event_type`,`event_phase_mask`,`event_chance`,`event_flags`,`event_param1`,`event_param2`,`event_param3`,`event_param4`,`action_type`,`action_param1`,`action_param2`,`action_param3`,`action_param4`,`action_param5`,`action_param6`,`target_type`,`target_param1`,`target_param2`,`target_param3`,`target_x`,`target_y`,`target_z`,`target_o`,`comment`) VALUES
(@ENTRY,0,1,0,38,1,100,0,1,1,0,0,80,54587*100+00,0,0,0,0,0,1,0,0,0,0,0,0,0,"Tushui Trainee - On Data Set 1 1 - Run Script"),
(@ENTRY,0,2,0,38,1,100,0,1,2,0,0,80,54587*100+01,0,0,0,0,0,1,0,0,0,0,0,0,0,"Tushui Trainee - On Data Set 1 2 - Run Script"),
(@ENTRY,0,3,0,38,1,100,0,1,3,0,0,80,54587*100+02,0,0,0,0,0,1,0,0,0,0,0,0,0,"Tushui Trainee - On Data Set 1 3 - Run Script"),
(@ENTRY,0,4,0,38,1,100,0,1,4,0,0,80,54587*100+03,0,0,0,0,0,1,0,0,0,0,0,0,0,"Tushui Trainee - On Data Set 1 4 - Run Script"),
(@ENTRY,0,5,0,0,0,100,0,2000,2000,2000,2000,11,109080,0,0,0,0,0,2,0,0,0,0,0,0,0,"Tushui Trainee - In Combat - Cast 'Blackout Kick'"),
(@ENTRY,0,6,7,2,0,100,1,0,10,0,0,80,54587*100+04,2,0,0,0,0,1,0,0,0,0,0,0,0,"Tushui Trainee - Between 0-10% Health - Run Script (No Repeat)"),
(@ENTRY,0,7,8,61,0,100,0,0,10,0,0,33,54586,0,0,0,0,0,2,0,0,0,0,0,0,0,"Tushui Trainee - Between 0-10% Health - Quest Credit '54586' (No Repeat)"),
(@ENTRY,0,8,0,61,0,100,0,0,10,0,0,22,2,0,0,0,0,0,1,0,0,0,0,0,0,0,"Tushui Trainee - Between 0-10% Health - Set Event Phase 2 (No Repeat)"),
(@ENTRY,0,9,0,25,0,100,0,0,0,0,0,42,0,10,0,0,0,0,1,0,0,0,0,0,0,0,"Tushui Trainee - On Reset - Set Invincibility Hp 10%"),
(@ENTRY,0,10,0,25,0,100,0,0,0,0,0,22,1,0,0,0,0,0,1,0,0,0,0,0,0,0,"Tushui Trainee - On Reset - Set Event Phase 1"),
(@ENTRY,0,11,0,25,0,100,0,0,0,0,0,19,33554688,0,0,0,0,0,1,0,0,0,0,0,0,0,"Tushui Trainee - On Reset - Remove Flags Immune To Players & Not Selectable");

-- Instructor Zhi SAI
SET @ENTRY := 61411;
UPDATE `creature_template` SET `AIName`="SmartAI" WHERE `entry`=@ENTRY;
DELETE FROM `smart_scripts` WHERE `entryorguid`=@ENTRY AND `source_type`=0;
INSERT INTO `smart_scripts` (`entryorguid`,`source_type`,`id`,`link`,`event_type`,`event_phase_mask`,`event_chance`,`event_flags`,`event_param1`,`event_param2`,`event_param3`,`event_param4`,`action_type`,`action_param1`,`action_param2`,`action_param3`,`action_param4`,`action_param5`,`action_param6`,`target_type`,`target_param1`,`target_param2`,`target_param3`,`target_x`,`target_y`,`target_z`,`target_o`,`comment`) VALUES
(@ENTRY,0,0,1,1,0,100,0,4000,4000,19000,19000,5,509,0,0,0,0,0,1,0,0,0,0,0,0,0,"Instructor Zhi - Out of Combat - Play Emote 509"),
(@ENTRY,0,1,2,61,0,100,0,4000,4000,19000,19000,45,1,1,0,0,0,0,9,54587,0,15,0,0,0,0,"Instructor Zhi - Out of Combat - Set Data 1 1"),
(@ENTRY,0,2,0,61,0,100,0,4000,4000,19000,19000,45,1,1,0,0,0,0,9,65471,0,15,0,0,0,0,"Instructor Zhi - Out of Combat - Set Data 1 1"),
(@ENTRY,0,3,4,1,0,100,0,9000,9000,19000,19000,5,543,0,0,0,0,0,1,0,0,0,0,0,0,0,"Instructor Zhi - Out of Combat - Play Emote 543"),
(@ENTRY,0,4,5,61,0,100,0,9000,9000,19000,19000,45,1,2,0,0,0,0,9,54587,0,15,0,0,0,0,"Instructor Zhi - Out of Combat - Set Data 1 2"),
(@ENTRY,0,5,0,61,0,100,0,9000,9000,19000,19000,45,1,2,0,0,0,0,9,65471,0,15,0,0,0,0,"Instructor Zhi - Out of Combat - Set Data 1 2"),
(@ENTRY,0,6,7,1,0,100,0,14000,14000,19000,19000,5,511,0,0,0,0,0,1,0,0,0,0,0,0,0,"Instructor Zhi - Out of Combat - Play Emote 511"),
(@ENTRY,0,7,8,61,0,100,0,14000,14000,19000,19000,45,1,3,0,0,0,0,9,54587,0,15,0,0,0,0,"Instructor Zhi - Out of Combat - Set Data 1 3"),
(@ENTRY,0,8,0,61,0,100,0,14000,14000,19000,19000,45,1,3,0,0,0,0,9,65471,0,15,0,0,0,0,"Instructor Zhi - Out of Combat - Set Data 1 3"),
(@ENTRY,0,9,10,1,0,100,0,19000,19000,19000,19000,5,507,0,0,0,0,0,1,0,0,0,0,0,0,0,"Instructor Zhi - Out of Combat - Play Emote 507"),
(@ENTRY,0,10,11,61,0,100,0,19000,19000,19000,19000,45,1,4,0,0,0,0,9,54587,0,15,0,0,0,0,"Instructor Zhi - Out of Combat - Set Data 1 4"),
(@ENTRY,0,11,0,61,0,100,0,19000,19000,19000,19000,45,1,4,0,0,0,0,9,65471,0,15,0,0,0,0,"Instructor Zhi - Out of Combat - Set Data 1 4");

DELETE FROM `gameobject_questitem` WHERE `GameObjectEntry` IN(210015,210016,210017,210018,210019,210020);
INSERT INTO `gameobject_questitem` (`GameObjectEntry`,`Idx`,`ItemId`) VALUES
(210015, 0, 76392),
(210015, 1, 76390),
(210016, 0, 73211),
(210017, 0, 73207),
(210017, 1, 76393),
(210018, 0, 73208),
(210018, 1, 73212),
(210019, 0, 76391),
(210019, 1, 73213),
(210020, 0, 73210);

DELETE FROM `gameobject_loot_template` WHERE `Entry` IN(40856,40859,40860,40861,40862,40863,40864);
INSERT INTO `gameobject_loot_template` (`Entry`,`Item`,`Chance`,`QuestRequired`) VALUES
(40856,73209,100,1),
(40859,76392,100,1),
(40859,76390,100,1),
(40860,73211,100,1),
(40861,73207,100,1),
(40861,76393,100,1),
(40862,73208,100,1),
(40862,73212,100,1),
(40863,76391,100,1),
(40863,73213,100,1),
(40864,73210,100,1);

DELETE FROM `creature_template_addon` WHERE `entry` IN(65470,54586,54587,65471,61411,54611,57753,53714,53565,57752,65469,57748);
INSERT INTO `creature_template_addon` (`entry`, `mount`, `bytes1`, `bytes2`, `emote`, `auras`) VALUES
(65470, 0, 0x0, 0x1, 510, ''),
(54586, 0, 0x0, 0x1, 510, ''),
(54587, 0, 0x0, 0x1, 510, ''),
(65471, 0, 0x0, 0x1, 510, ''),
(61411, 0, 0x0, 0x1, 510, ''),
(54611, 0, 0x1, 0x1, 0, ''),
(57753, 0, 0x0, 0x1, 510, ''),
(53714, 0, 0x0, 0x1, 0, ''),
(53565, 0, 0x0, 0x1, 510, ''),
(57752, 0, 0x0, 0x1, 510, ''),
(57748, 0, 0x0, 0x1, 510, ''),
(65469, 0, 0x0, 0x1, 510, '');

DELETE FROM `creature_addon` WHERE `guid` IN(138281,138284);
INSERT INTO `creature_addon` (`guid`, `mount`, `bytes1`, `bytes2`, `auras`) VALUES
(138281, 0, 0x0, 0x1, ''),
(138284, 0, 0x0, 0x1, '');
-- Darkmoon Island (5)
UPDATE `creature_template` SET `npcflag` = `npcflag`|2 WHERE `entry` = 90473;

DELETE FROM `creature_queststarter` WHERE `id` IN (67370, 74056, 85519, 85546, 90473);
DELETE FROM `creature_questender` WHERE `id` IN (67370, 74056, 85519, 85546, 90473);

INSERT INTO `creature_queststarter` VALUES
(67370, 32175), -- Jeremy Feasel, Darkmoon Pet Battle!
(74056, 33756), -- Malle Earnhard, Let's Keep Racing!
(74056, 37910), -- Malle Earnhard, The Real Race
(74056, 37819), -- Malle Earnhard, Welcome to the Darkmoon Races
(85519, 36471), -- Christoph VonFeasel, A New Darkmoon Challenger!
(85546, 36481), -- Ziggie Sparks, Firebird's Challenge
(90473, 37868), -- Patti Earnhard, More Big Racing!
(90473, 37911); -- Patti Earnhard, The Real Big Race

INSERT INTO `creature_questender` VALUES
(67370, 32175), -- Jeremy Feasel, Darkmoon Pet Battle!
(74056, 33756), -- Malle Earnhard, Let's Keep Racing!
(74056, 37910), -- Malle Earnhard, The Real Race
(74056, 37819), -- Malle Earnhard, Welcome to the Darkmoon Races
(85519, 36471), -- Christoph VonFeasel, A New Darkmoon Challenger!
(85546, 36481), -- Ziggie Sparks, Firebird's Challenge
(90473, 37868), -- Patti Earnhard, More Big Racing!
(90473, 37911); -- Patti Earnhard, The Real Big Race


-- Depholm (1)
DELETE FROM `creature_queststarter` WHERE `id` IN (66815);
DELETE FROM `creature_questender` WHERE `id` IN (66815);

INSERT INTO `creature_queststarter` VALUES
(66815, 31973); -- Bordin Steadyfist, Bordin Steadyfist

INSERT INTO `creature_questender` VALUES
(66815, 31973); -- Bordin Steadyfist, Bordin Steadyfist
